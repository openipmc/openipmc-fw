/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      xvc_common.h                                                      */
/* @brief     Xilinx Virtual Cable common headers for CM7 and CM4 cores.        */
/* @author    André Muller Cascadan                                             */
/********************************************************************************/


#ifndef XVC_COMMON_H
#define XVC_COMMON_H

/*
 * Maximum amount of bytes per vector to be transfered at once (in a single 
 * SHIFT command).
 * 
 * NOTE: One vector here refers to the bytes of ONE signal: TMS, TDI or TDO.
 *       Eg: For 256-bytes vector, there will be maximum of 256 for TMS, 256 for
 *       TDI and 256 for TDO
 *
 */
#define XVC_BYTEVEC_MAX_BYTES   256


#define XVC_DATA_BUFF_MAX_BYTES 2*XVC_BYTEVEC_MAX_BYTES


#ifndef HSEM_ID_1
#define HSEM_ID_1 (1U) /* HW semaphore 1 protecting XVC TDI/TMS control flags */
#endif


// XVC Inter Processor Communication data structure
typedef struct xvcjtag_tditms_t
{
	uint32_t start_shift;
	uint32_t shift_done;
	uint32_t clk_prescaler;
	uint32_t nshift;
	uint8_t* bytevec_tdi_tdo_data;
	uint8_t* bytevec_tms_data;
	uint8_t  rcvd_data[XVC_DATA_BUFF_MAX_BYTES];
}xvc_ipc_data_t;

#endif /* XVC_COMMON_H_ */
