/*
 * 
 * Tools for generating HPM upgrade file
 * 
 * Author: André Cascadan
 * 
 */


#ifndef HPM_GENERATOR_H
#define HPM_GENERATOR_H

#include <stdio.h>
#include <stdint.h>

#define COMPONENT_MASK_0    0x01
#define COMPONENT_MASK_1    0x02
#define COMPONENT_MASK_2    0x04


// Image Capabilities flags
#define IM_CAP_SELF_TEST_IS_SUPPORTED         0x01
#define IM_CAP_AUTO_ROLLBACK_IS_SUPPORTED     0x02
#define IM_CAP_MANUAL_ROLLBACK_IS_SUPPORTED   0x04
#define IM_CAP_PYLD_OR_FRU_COULD_BE_AFFECTED  0x08



typedef struct
{
	uint8_t major;  // 7 bits (0 ~ 127)
	uint8_t minor;  // BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	uint8_t aux[4];    // Any 32bit data
	
} revision_t;


typedef struct
{
	
	uint32_t   manufacturer_id;           // IANA number
	uint16_t   product_id;                // Manufacturer defined code 
	uint32_t   time;                      // Creation time of this image in seconds since 00:00:00 UTC on January 1, 1970
	uint8_t    image_capabilities;        // A combination of "Image Capabilities flags". See above.
	uint8_t    components;                // Mask informing the components present in this image
	
	uint8_t    self_test_timeout;
	uint8_t    rollback_timeout;
	uint8_t    inaccessibility_timeot;
	
	uint8_t    earliest_compatible_major_revision; // 7 bits (0 ~ 127)
	uint8_t    earliest_compatible_minor_revision; // BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	
	revision_t firmware_revision;  // Version of the firmware contained in this upgrade file
	
} image_header_t;





uint8_t* load_binary_from_file(char* file_name, uint32_t* size);

void add_image_header(FILE* file, image_header_t header);
void add_backup_componets_action(FILE* file, uint8_t component_mask);
void add_prepare_componets_action(FILE* file, uint8_t component_mask);
void add_upload_firmware_image_action(FILE* file, uint8_t component_number, char* description_string, uint8_t* image_data, uint32_t image_size);

void append_md5(char* file_name);


#endif /* HPM_GENERATOR_H */
