

/*
 * This program generates the HPM1 upgrade file for the CM7 firmware of OpenIPMC-FW project
 * 
 * For flexibility, the *.bin file must be passed as argument to this program
 * 
 * NOTE: For compiling, include flags: "-lcrypto -lssl -lz"
 *     e.g.: 
 *        gcc generate_upgrade_file.c -o generate_upgrade_file  -lcrypto -lssl -lz
 */



#include <stdio.h> 
#include <stdlib.h>
#include <stdint.h>


#include "hpm_generator.h"
#include "fw_metadata.h"


void print_usage()
{
  printf( "  Usage: generate_upgrade_file_bl7 <input_file.bin> <output_file.hpm>\r\n" );
}

int main( int argc, char **argv )
{
	
	image_header_t image_header;
	
	char*                 component1_bin_file_name;
	char*                 component1_hpm_file_name;
	uint8_t*              component1_binary_data;
	uint32_t              component1_binary_size;
	metadata_fields_v0_t* component1_metadata;
	
	
	// Check input args
	if( argc != 3 )
	{
		printf( "Generate Upgrade File: Invalid argument.\r\n" );
    print_usage();
		return 1;
	}
	
	component1_bin_file_name = argv[1];
  component1_hpm_file_name = argv[2];
	
	
	// Load binary for component 1
	component1_binary_data = load_binary_from_file( component1_bin_file_name, &component1_binary_size );
	if( component1_binary_data == NULL )
	{
		printf( "Generate Upgrade File: Image file does not exist.\r\n" );
		return 1;
	}
	component1_metadata = (metadata_fields_v0_t*)&component1_binary_data[FW_METADATA_ADDR];
	
	
	// Check image validity from metadata
	if ( component1_metadata->presence_word != FW_METADATA_PRESENCE_WORD )
	{
		printf( "Generate Upgrade File: Unexpected magic presence word in metadata.\r\n" );
		printf( "-- presence_word expected %X read %X.\r\n", component1_metadata->presence_word, FW_METADATA_PRESENCE_WORD     );
		return 1;
	}
	
	if ( component1_metadata->firmware_type != FW_METADATA_TYPE_OPENIPMC_CM7 )
	{
		printf( "Generate Upgrade File: Wrong image type.\r\n" );
		printf( "-- firmware_type expected %X read %X.\r\n", component1_metadata->firmware_type, FW_METADATA_TYPE_OPENIPMC_CM7 );
		return 1;
	}
	
	
	// Fill the header info
	image_header.manufacturer_id                    = component1_metadata->manufacturer_id;
	image_header.product_id                         = component1_metadata->product_id;
	image_header.time                               = 0x00;
	image_header.image_capabilities                 = IM_CAP_PYLD_OR_FRU_COULD_BE_AFFECTED |
	                                                  IM_CAP_MANUAL_ROLLBACK_IS_SUPPORTED;
	
	image_header.components                         = COMPONENT_MASK_1;
	
	image_header.self_test_timeout                  = 0x02; // 10 seconds
	image_header.rollback_timeout                   = 0x02; // 10 seconds
	image_header.inaccessibility_timeot             = 0x04; // 20 seconds
	
	image_header.earliest_compatible_major_revision = 0;    // Any version is accepted
	image_header.earliest_compatible_minor_revision = 0x00;
	
	image_header.firmware_revision.major  = component1_metadata->firmware_revision_major;
	image_header.firmware_revision.minor  = component1_metadata->firmware_revision_minor;
	image_header.firmware_revision.aux[0] = component1_metadata->firmware_revision_aux[0];
	image_header.firmware_revision.aux[1] = component1_metadata->firmware_revision_aux[1];
	image_header.firmware_revision.aux[2] = component1_metadata->firmware_revision_aux[2];
	image_header.firmware_revision.aux[3] = component1_metadata->firmware_revision_aux[3];
	
	
	// Compose the HPM1 upgrade file
	FILE* hpm_file = fopen( component1_hpm_file_name,"wb" );
	
	add_image_header(hpm_file, image_header);
	
	add_backup_componets_action(hpm_file, COMPONENT_MASK_1);
	
	// add_prepare_componets_action(hpm_file, COMPONENT_MASK_1);  Preparation is not used
	
	add_upload_firmware_image_action( hpm_file, 1, "CM7_fw", component1_binary_data, component1_binary_size );
	
	fclose(hpm_file);
	
	append_md5( component1_hpm_file_name ); // File is reopened inside
	
	
	
	free( component1_binary_data );
	
}




