
.PHONY := all hpm headers bin generate_upgrade_file clean

.DEFAULT_GOAL := all

all: hpm

generate_upgrade_file:
	@echo "Compiling HPM1 upgrade file generators"
	@gcc tools/hpm_generator.c tools/generate_upgrade_file_cm4.c -I Common/Inc -o generate_upgrade_file_cm4  -lcrypto -lssl -lz
	@gcc tools/hpm_generator.c tools/generate_upgrade_file_cm7.c -I Common/Inc -o generate_upgrade_file_cm7  -lcrypto -lssl -lz	

headers:
	@echo "Generating headers"
	@cd ./tools && sh ./header_gen.sh && cd - > /dev/null

bin: headers
	@echo "Building firmware for CM4 core"
	@cd CM4  &&  $(MAKE) clean_common
	@cd CM4  &&  $(MAKE) bin
	@echo "Building firmware for CM7 core"
	@cd CM7  &&  $(MAKE) clean_common
	@cd CM7  &&  $(MAKE) bin

hpm: generate_upgrade_file bin
	@echo "Generating HPM1 upgrade files"
	@cd CM4 && ../generate_upgrade_file_cm4 openipmc-fw_CM4.bin upgrade_CM4.hpm
	@cd CM7 && ../generate_upgrade_file_cm7 openipmc-fw_CM7.bin upgrade_CM7.hpm

clean:
	@rm -f ./Common/Inc/head_commit_sha1.h
	@rm -f ./Common/Inc/compiling_epoch.h
	@cd CM4 && $(MAKE) clean
	@cd CM7 && $(MAKE) clean
	@rm -f generate_upgrade_file_cm4
	@rm -f generate_upgrade_file_cm7
	@rm -f CM4/upgrade_CM4.hpm
	@rm -f CM7/upgrade_CM7.hpm

