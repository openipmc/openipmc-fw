## Syslog Client
OpenIPMC-FW has a syslog client task running on top of LwIP. A syslog client is responsible for reporting events of interest in a system through text messages over UDP connection. The messages are encoded in 7-bit ASCII (each character being 8-bit sized) and formatted in two different flavours: RFC3164 syslog and RFC5424 syslog. The user can exchange flavours during compile time by defining some configuration macros on ```syslog.c``` source file.

The syslog data frame is described by the diagram below.

```plain
RFC3164
+-----+--------+-----+
| PRI | HEADER | MSG | <= 1024 Bytes
+-----+--------+-----+

RFC5424
+-----+--------+-------------+-----+
| PRI | HEADER | STRUCT-DATA | MSG | <= 2048 Bytes
+-----+--------+-------------+-----+
```
Every syslog message in both flavours have a **priority value**  ```PRI``` field, that is responsible to describe the importance of the message, the values range from 0 to 191, being 0 the most critical type of message (usually kernel failures or dangerous sensor readings). The ```PRI``` field is computed by the equation below:

$PRI = 8 \cdot F + S$

Being $F$ and $S$ the **facility** and **severity** numerical codes respectively.

The ```HEADER``` field is common to both types of syslog format and contains the following sub-fields:

**1. Hostname**:  ASCII string that identifies the client.<br />
**2. Timestamp**: Date and time when the message was generated.<br />
**3. Version**: Only in RFC5424, identifies the client's version.

The *hostname* field can be configured at compile time by macro definitions in the source file, the user can opt in either using the IPMC's IPv4 address or the DHCP Client-ID field. Timestamp field is encoded at the Real-Time Clock (RTC) register present in the microprocessor which is then fetched and decoded by the syslog client source code. The version field is fixed with the value ```1.0```.

The ```STRUCT-DATA``` field is present only on RFC5424, and provides support for UTF-8 encoding. Currently OpenIPMC-FW offers no support for UTF-8 enconding, therefore this field is replaced by a ```NILVALUE``` (i.e. "-") character as defined by RFC5424 specification.