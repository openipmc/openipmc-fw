# OpenIPMC-FW

OpenIPMC-FW offers the infrastructure to create a custom firmware for the OpenIPMC-HW mini-DIMM mezzanine.

The function of this project is to give access to all the features supported by the OpenIPMC-HW mezzanine, such as the YAFFs flash file system, LwIP and IPMI management tools. All the code customization aiming a specific ATCA board must reside in a parent project, which imports OpenIPMC-FW as a *git submodule*.

A reference project for a customized IPMC is available at [OpenIPMC-FW-board-specific-demo](https://gitlab.com/openipmc/openipmc-fw-board-specific-demo).

This project has being developed using STM32CubeIDE and its associated open source GCC toolcain. It includes [OpenIPMC](https://gitlab.com/openipmc/openipmc) to provide the IPMI management functions required by the [PICMG standard](https://www.picmg.org/product/advancedtca-base-specification/) for the ATCA board.

We want to stress that the code present in this repo can not be compiled and run alone. It needs to be imported into a board-specific project which must provide implementation for missing configuration macros and functions to be minimally functional (since this generic firmware cannot know about the specifics of your ATCA board). The demo project above mentioned can guide the user on this task.



Repository: __[gitlab.com/openipmc/openipmc-fw](https://gitlab.com/openipmc/openipmc-fw)__

## Other Repositories

- __[OpenIPMC-HW](https://gitlab.com/openipmc/openipmc-hw)__: The hardware design which supports this firmware. 

- __[OpenIPMC](https://gitlab.com/openipmc/openipmc)__: Platform-independent  software to perform the IPMC operations according to PICMG.


## Licensing

OpenIPMC-FW is Copyright 2020-2021 of Andre Cascadan, Luigi Calligaris. OpenIPMC-FW is released under GNU General Public License version 3. Please refer to the LICENSE document included in this repository.


