## Introduction

The miniDIMM form factor OpenIPMC-HW offers a number of IOs across its edge connector. For a better comprehension, these IOs can be classified in two groups as follows:

- **Non-customizable IOs**: These IOs are already being used by some module implemented in OpenIPMC-FW infrastructure and no customization are needed (nor allowed) for them. As an example of such IO we can mention the IPMB buses, which are controlled by __[OpenIPMC](https://gitlab.com/openipmc/openipmc)__ submodule to perform IPMI communication with the ATCA Shelf Manager, AMC mezzanine or RTM. Signals which fits into this group are:

    - **IPMB buses**: IPMB-A_SCL, IPMB-A_SDA, IPMB-B_SCL, IPMB-B_SDA1, IPMB-L_SCL and IPMB-L_SDA.
    - **Hardware lines**: HW_0 to HW_7
    - **BLUE_LED**
    - **Handle**
    
- **Customizable IOs**: These IOs has no defined role (USR_IO_x and IPM_x), or its role depends on the specific board needs. In both cases, the user may need to control them. Signals which fits into this group are:

    - **LEDs**: LED_0, LED_1, LED_2.
    - **IPMs**
    - **USR_IOs**
    - **AMCs** (via API[^via_api])
    - **PAYLOAD_RESEST**
    - **Status Signals**: ALARM_A, ALARM_B, PWR_GOOD_A and PWR_GOOD_B
    - **12V_EN**
    - **UARTs**: UART0 and UART1 (also UART2, if implemented[^fn_UART2], via API[^via_api])
    - **I2Cs:** SENSE and MGM (via API[^via_api]) 

[^via_api]: User is not allowed to control the IOs directly, but OpenIPMC-FW offers dedicated API for that.

[^fn_UART2]: UART2 is specified by *CERN IPMC V4* and can be configured in MCU over **USR_IO_0** and **USR_IO_1** IOs (TX and RX, respectively).

This document page focus on the **Customizable IOs** group only.

**WARNING:** Any attempt to control or modify a **Non-customizable IO** will cause **conflicts** and **unpredictable behaviour**. Although just reading GPIOs is expected to be a safe operation.


## GPIO labels

Great part of the miniDIMM IOs are directly connected to MCU GPIOs, therefore they can be easily controlled by software.

STM32CubeIDE allows the developer to add a text label to each STM32 GPIO according to the functionality associated to them and the IDE automatically generates macros in `main.h` to facilitate the GPIO control. This features is used by OpenIPMC-FW to control all GPIOs being used in the IPMC module.

Example: The MCU pin PI4 receives the label IPM_0, and pin PI7 receives the label IPM1. After code generation, the following macros can be found in `main.h`:

```c
#define IPM_0_Pin       GPIO_PIN_4
#define IPM_0_GPIO_Port GPIOI

#define IPM_1_Pin       GPIO_PIN_7
#define IPM_1_GPIO_Port GPIOI
```

These macros allow the programmer to easily access the correspondent *Port* and *Pin#* of each IO, and control them using the *HAL_GPIO* driver offered by IDE:

```c
HAL_GPIO_WritePin( IPM_0_GPIO_Port, IPM_0_Pin, GPIO_PIN_SET   ); // Set IPM_0 to HIGH

HAL_GPIO_WritePin( IPM_1_GPIO_Port, IPM_1_Pin, GPIO_PIN_RESET ); // Set IPM_1 to LOW
```

For a complete list of the labels defined, please check `main.h`. The labels were chosen to be as similar as possible to the miniDIMM signal names.

**NOTE**: AMC signals are controlled via *IO expanders* and can not be controlled via labels. Please refers to the dedicated subsection below.

### Helpful macros

In order to simplify even more the control over GPIOs, this project includes the `dimm_gpios.h` file, with some helpful macros:

```c

GPIO_CONFIGURE_PIN(IPM_0, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL); // Set IPM_0 as push/pull output

GPIO_SET_STATE(IPM_0, SET); // Set IPM_0 pin to HIGH

ipm_1_state = GPIO_GET_STATE(IPM_1);  // Read IPM_1
```




## AMC signals

The IPMC module has a set of status and control signals dedicated for AMC mezzanines. They are 10 signals for each mezzanine, supporting up to 9 mezzanines: total of 90 IOs. 

In the miniDIMM card, these IOs are handled by 6 MCP23S17 *IO expanders* controlled by MCU via one SPI bus. OpenIPMC-FW offers a dedicated driver, AMC_GPIO, to allow a transparent control of each IO by just using their *Driver Number* or *Schematic Label*.

The table below shows the name of each signal, a simplified labeling notation used in the hardware schematics.

|      Signal Name      |  Schematic Label | Driver Number |
|:----------------------|:-----------------|:--------------|
| AMCn_PS1#             | AMCn_IO_0        | 10n + 0       |
| AMCn_EN#              | AMCn_IO_1        | 10n + 1       |
| AMCn_IPMB-L_EN        | AMCn_IO_2        | 10n + 2       |
| AMCn_MP_EN            | AMCn_IO_3        | 10n + 3       |
| AMCn_PWR_EN           | AMCn_IO_4        | 10n + 4       |
| AMCn_MP_GOOD          | AMCn_IO_5        | 10n + 5       |
| AMCn_PWR_GOOD         | AMCn_IO_6        | 10n + 6       |
| AMCn_MP_FAULT         | AMCn_IO_7        | 10n + 7       |
| AMCn_PWR_FAULT        | AMCn_IO_8        | 10n + 8       |
| AMCn_PWR_ORING        | AMCn_IO_9        | 10n + 9       |


where **n** is a number from 0 to 8 representing the mezzanines, *Schematic Label* is a simplified labeling notation used in the hardware schematics and *Driver Number* (0~89) is an integer used by the AMC_GPIO driver to identify each IOs.

### AMC_GPIO driver: API

This driver implements a typical GPIO API, making all the SPI messaging transparent to the user. This API includes control over pin functions offered by MCP23S17, like pin interrupt, pull-up and pin direction.

Some examples of use:

```c
#include "amc_gpios.h"

amc_gpios_set_pin_direction( AMC3_IO_4, AMC_DIR_OUT ); // Set AMC3_IO_4 as output

amc_gpios_write_pin( AMC3_IO_4, 1 ); // Set AMC3_IO_4 to HIGH

amc1_io_2_state = amc_gpios_read_pin( AMC1_IO_2 ); // Read AMC1_IO_2

amc_gpios_set_pin_interrupt( AMC1_IO_2, AMC_INT_BOTH_EDGES ); // Enable both-edges interrupt for AMC1_IO_2

```

The driver is implemented in `amc_gpios.c` and `amc_gpios.h` files. This driver is **thread safe**.

### AMC_GPIO driver: pin interrupts

A callback is used by this driver to catch the events. `amc_gpios_pin_interrupt_callback` is defined in `main.c` with some usage examples in the code.

**IMPORTANT**: `amc_gpios_pin_interrupt_callback` is called from a FreeRTOS task context (`amc_gpios_pin_interrupt_task`). The term "interrupt" is kept here to be coherent to the MCP23S17 nomenclature.


## I2Cs

The IPMC provides two I2C buses for board-specific purposes: MGM_I2C and SENSE_I2C. OpenIPMC-FW initializes them and offers an interface for each of them. This interface adds a **thread safety** layer on top of the standard HAL API provided by ST, what allows the user to use the same bus from different tasks (e.g.: using many dedicated tasks to read each sensor attached to the bus).

```c
# include "sense_i2c.h"

sense_i2c_transmit( device_address, data_buffer, data_size, timeout );

sense_i2c_receive( device_address, data_buffer, data_size, timeout );

```

Similar functions are available for MGM_I2C in `mgm_i2c.h`




## UARTs

UART0 is currently configured to give access to the Command Line Interpreter. UART1 and UART2 are unconfigured at this moment.

