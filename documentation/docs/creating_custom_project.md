
## Submodule Approach

OpenIPMC-FW code was organized in such way that all the hardware drivers, TCP/IP stack, FreeRTOS, and even the `main()` function are kept and maintained as a *git submodule* of a board-specific parent project, which contains all the customization code.

This organization allows a clear separation between the development and maintenance of the infrastructure code from the board-specific projects maintained by the various users.

## Example of Board-specific project

We present an example of board-specific project called [OpenIPMC-FW-board-specific-demo](https://gitlab.com/openipmc/openipmc-fw-board-specific-demo), which can be cloned and built an even flashed into a OpenIPMC-HW mezzanine for tests.

The example project offers:

- Example code in `board_specific`: It contains examples of configuration macros, callback definitions and sensor creation. For more information about this, please, refer to the Code Customization section.

- An auxiliary `board_specific.mk` file: It gives to the users the possibility to customize their own include and source paths.

- A top-level `Makefile`: It simplifies the build process by just typing `make` in the custom project root directory. The actual build recipes are in the `openipmc-fw/CM7/Makefile`.

- CI script `.gitlab-ci.yml`: Performs automatic build by GitLab CI/CD. This is not mandatory and is here as reference for environment setup. Other git platforms may require different scripts. 

## Creating a new Board-specific project

In order to create a new board-specific project,  [OpenIPMC-FW-board-specific-demo](https://gitlab.com/openipmc/openipmc-fw-board-specific-demo) can be cloned or downloaded, and then renamed conveniently.



Some details must be observed:

- If a new git repo is being created (`git init`) OpenIPMC-FW must be added manually as submodule

```console
$ git submodule add https://gitlab.com/openipmc/openipmc-fw.git
```

- `board_specific.mk` file **must** be present in the root of the board-specific project. Define the list of include paths `INC_BOARD` and source files `SRC_BOARD` according to the project needs. In the example project all headers and sources are in the  `board_specific` subdirectory.

- Both `custom_settings.h` and `fw_identification.h` files **must** be present in the custom include path (listed in `INC_BOARD`). Copy them from the example project and modify their content as convenient.

- Define the `void custom_startup_task( void )` task function. `CustomStartupTask` is created by OpenIPMC-FW to run user code.

- Define the void `get_product_and_board_info( board_and_product_info_t* info )` according to the example.

The example contains more suggestions which are optional and allow to:

- Create terminal commands
- Define the payload activation policy
- Define *Board Power Levels* according to PICMG
