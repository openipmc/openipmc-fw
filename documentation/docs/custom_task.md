 
## Custom Startup Task

After initializing the all the essential resources (GPIOs, UARTs, I2Cs, LwIP, etc.), OpenIPMC finally launches the `CustomStartupTask`, which gives the first execution context dedicated to the user needs. More tasks can be launched by the user at this point, if needed.

The task function is **not** defined by OpenIPMC-FW, so user must define it in the board-specific project:

```c
void custom_startup_task( void )
{
	.
	.
	.
}
```

An example is available [here](https://gitlab.com/openipmc/openipmc-fw-board-specific-demo/-/blob/master/board_specific/custom_startup_task.c) project

The task priority and stack size can be tuned in the `custom_settings.h` file by using `CUSTOM_STARTUP_TASK_PRIORITY` and `CUSTOM_STARTUP_TASK_STACK_SIZE`.
