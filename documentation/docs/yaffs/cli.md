## Command-Line Interface
In order to manage files and directories throughout OpenIPMC there is a UNIX-like set of command-line functions that can be used via terminal process.
The following commands can aid the user in managing the file system.

| Command-line | Definition | Arguments |
| ------------ | ---------- | --------- |
| format | Used for erasing the file system state table. <br /> This means **losing every link to any existing file, **<br />**therefore permanently deleting everything**.<br />Takes up to three optional arguments which<br />can be combined for the formatting process. |**\[-u\]**  Unmounts file system before formatting device. <br /> **\[-r\]**  Remounts device after finishing the process. <br />**\[-f\]**  Forces a format process, this is not advised. <br /> |
| mount | Mounts file system on installed device. Takes no arguments.| **no arguments used.** |
| umount | Unmounts file system from installed device.<br />Takes only one optional argument.|**\[-f\]** Forces an unmount operation, this is not advised as it may break any open file handle.|
| write | Creates a data file at given path with given user input<br />Takes 3 mandatory arguments in the following order:<br />1. A specifier.<br />2. Path to the file<br />3. User input. | **\[-mk\]** Specifier indicating to create the file if it does not exist<br />**\[-ow\]** Specifier indicating to overwrite the file.<br />**\[-ap\]** Specifier indicating append user input at the end of the file.<br />**\[/path/to/file.ext\]** Path to file, the directories contained must exist!<br />**\[input\]** User input.|
| print | Prints content inside a file into the terminal process output | **\[file name\]** Name of the file to be printed. |
| mkdir | Creates a directory, | **\[path\]** directory path to be created. | 
| rm | Removes an object from file system by<br />unlinking it and then closing the file handle | **\[object name\]** Name of the object (file, directory or symbolic link) to be removed. |
| ls | Lists content of a directory. if no arguments are provided<br />the command will list the root directory. | **\[directory name\]** Name of directory to be listed. |
| chmod | Changes file permissions. Takes 2 arguments:<br />1. Permission type<br />2. File system object| **\[-rd\]** Read permission flag.<br />**\[-wr\]** Write permission flag.<br />**\[object name\]**File or directory to change permissions.|
| memstat | Plots storage conditions and statistics on installed device.| **no arguments used.** |

It is important to note that the file system's working directory is **only** at root level, which means that to manipulate each object in the file system the user must provide the full path.

1.Creating a directory inside `/etc` directory:

```C
docs        inode 514         length 2032        drw-  directory
home        inode 257         length 2032        drw-  directory
etc         inode 769         length 2032        drw-  directory
>> mkdir /etc/folder
```

2.Printing the content of a file:

```C
docs        inode 514         length 2032        drw-  directory
home        inode 257         length 2032        drw-  directory
etc         inode 769         length 2032        drw-  directory
>> ls docs
diary       inode 515         length 88          -rw-  data file
>> print /docs/diary
print: Dear diary, today I learned how to use OpenIPMC-FW terminal to manage my file system!!
```

3.Creating a folder at root

```C
docs        inode 514         length 2032        drw-  directory
home        inode 257         length 2032        drw-  directory
etc         inode 769         length 2032        drw-  directory
>> mkdir /folder
```

4.Writing a new data file inside ```/folder```.

```C
folder      inode 513         length 2032        drw-  directory
home        inode 257         length 2032        drw-  directory
etc         inode 769         length 2032        drw-  directory
docs        inode 514         length 2032        drw-  directory
>> write -mk /folder/euler.txt 2.71828
yaffs: File successfully written! 7 bytes
```

Each CLI function listed above have callback functions which are defined in the source file ```yimpl_cli.c``` and every CLI callback is then informed to the terminal process on openipmc by providing each pointer to the terminal API. 

