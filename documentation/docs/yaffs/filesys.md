## YAFFS-2 File System

OpenIPMC-HW has a NAND/NOR flash memory file system integrated to its system utilities. YAFFS is an open-source file system designed for embedded systems. It is suitable for working with real-time operating systems (in our case FreeRTOS) and provides an extensive integration interface called YAFFS direct. Currently OpenIPMC works with YAFFS-2, which is an extension of YAFFS-1 that supports modern flash types with different memory geometries (i.e page, block and partition sizes). In particular YAFFS-2 is an interesting choice for MLC or SLC NAND flash memory types since it is able to track the file system state without the need of deletion markers.

YAFFS direct interface takes care of assigning the user's flash memory drivers to callbacks and configures the file system read/write cycles in accordance to the device specifications (e.g page size, block size, partition boundaries). Such flexibility however comes with a price of a bit more effort from the system integrator in designing the driver callbacks but nevertheless it is a clever way of encapsulating and delimiting the file system's core code and the user code, thus making a "clean" integration.

For more in-depth information about YAFFS-2, the documentation can be accessed [**here**](https://yaffs.net/documents/technical).

### Integration process

![image](pics/yaffs_building_blocks.png "title"){: style="height:414px;width:292px"}

The image above illustrates the layers of software necessary to integrate YAFFS file system. YAFFS Direct Interface is a framework that takes care of integrating the core file system for the user, provided that we give the requested layers. There are two important layers of integration depicted by the blocks "RTOS resources" and "Flash device" in the picture above, these layers are responsible for allowing the file system to communicate with the operating system and perform flash memory operations respectively.

In order to integrate YAFFS with a flash device we must first develop a set of driver callback functions and the flash device driver functions to enable YAFFS to perform read, write and erase operations. These functions are defined at ```yimpl_flash_drv.c```[^1] on OpenIPMC-FW. Some driver callbacks used by YAFFS perform bad block[^2] marking and checking as well, however these procedures are already taken care by the flash device used on OpenIPMC-HW, the driver only takes care of informing the file system about the current block marking situation. The driver functions are listed in the table below.

| API | Layer | Description |
| --- | ----- | ---------- |
| **yimpl_flash_write_chunk_fn** | **yimpl_flash_drv.c** | Writes a chunk[^3] worth of data into the flash memory. |
| **yimpl_flash_read_chunk_fn**  | **yimpl_flash_drv.c** | Reads a chunk worth of data from the flash memory. |
| **yimpl_flash_erase_block_fn** | **yimpl_flash_drv.c** | Erases a block[^4] worth of data from the flash memory. |
| **yimpl_flash_mark_bad_fn**    | **yimpl_flash_drv.c** | Loads bad block information from flash device and returns situation to the file system. |
| **yimpl_flash_check_bad_fn**   | **yimpl_flash_drv.c** | Checks if a block has gone "bad" and is no longer usable. | 
| **yimpl_flash_init_fn**        | **yimpl_flash_drv.c** | Initialises flash device and provides a hook for user parameter initialisation. |
| **yimpl_flash_deinit_fn**      | **yimpl_flash_drv.c** | Deinitialises flash device and closes communication. |
| **yimpl_flash_install**        | **yimpl_flash_drv.c** | Assigns partition parameters to ```yaffs_dev``` data structure and initialises partition. |

[^1]: The prefix "yimpl" stands for "yaffs implementation".
[^2]: Usually flash devices are shipped with a small number of unusable blocks of memory that are referred as bad blocks, the manufacturer always guarantee the memory size however some blocks might go bad during the manufacturing. In order to circumvent fragmentation a file system can check for bad blocks and reroute to skip the bad block, in this manner guaranteeing contiguous data organization. In our case, OpenIPMC-FW has a flash device that performs the checking and reroutes bad blocks internally.
[^3]: In yaffs terminology a chunk is equivalent to a page and a page is equivalent to a fixed, power of 2, number of bytes (e.g: 512, 1024, 2048), however YAFFS is capable in dealing with pages that have non-power 2 sizes.
[^4]: A block is a group of pages, a flash device is organized in blocks each block containing a fixed number of pages.

The flash memory driver functions are located at ```w25n01gv.c``` and are responsible for controlling the QuadSPI bus connected to the device. Each driver is appropriately associated with the driver callbacks. It is important to notice that YAFFS source code has an error correction code inbuilt in its core file system which consists of an 1-bit Hamming code, however since we are using YAFFS-2 the file system will not perform any error correction procedure whatsoever, the drivers must take care of these corrections.

The function **yimpl_flash_install** deserves some special attention since it takes care of assigning partition parameters to a data structure named ```yaffs_dev``` which holds all kinds of information about the partition: Chunk size, page size, spare are size, device/partition name, pointers to driver callbacks and file system tuning variables. Below we have an example on how to write a install driver function such as **yimpl_flash_install**.

```C

// Globals

#define KB 1024
#define TRUE 1

struct yaffs_dev dev;

int drv_callback_install( const char* name )
{
	struct yaffs_param *param;
	struct yaffs_driver *drv;

	dev.param.name = name; // Device name, will be the partition name as well.
	drv = &dev.drv;        // Driver struct pointer contained inside dev struct.
	param = &dev.param;    // Param struct pointer contained inside dev struct.

      // drv struct receives its respective callback pointer
	drv->drv_write_chunk_fn     = drv_callback_write_chunk_fn;
	drv->drv_read_chunk_fn      = drv_callback_read_chunk_fn;
	drv->drv_erase_fn           = drv_callback_erase_block_fn;
	drv->drv_mark_bad_fn        = drv_callback_mark_bad_fn;
	drv->drv_check_bad_fn       = drv_callback_check_bad_fn;
	drv->drv_initialise_fn      = drv_callback_init_fn;
	drv->drv_deinitialise_fn    = drv_callback_deinit_fn;

      // param struct receives flash device specifications and file system parameter configuration
	param->is_yaffs2                    = ( int )( TRUE );      // Set to '1' to enable YAFFS-2 mode.
	param->empty_lost_n_found           = ( int )( TRUE );      // If mounting for the first time empty lost and found directory.
	param->total_bytes_per_chunk        = ( u32 )( 2*KB );      // Number of bytes each chunk has.
	param->spare_bytes_per_chunk        = ( u32 )( 16 );        // Size of the spare area present on each memory chunk.
	param->chunks_per_block	            = ( u32 )( 32 );        // Number of chunks per block.
	param->start_block                  = ( u32 )( 0 );         // Partition's starting block.
	param->end_block                    = ( u32 )( 1023 );      // Partition's ending block.
	param->n_reserved_blocks            = ( u32 )( 5 );         // Number of reserved blocks for file system usage. '5' is the minimum necessary.

	yaffs_add_device( &dev );     // This function adds device to the list of devices managed by yaffs.
	return YAFFS_OK;
}
```
**yimpl_flash_install** is very similar to the example listed above. On OpenIPMC-FW YAFFS does not use the flash device's spare area but instead uses a mode of operation called in-band tags, which means that the metadata is stored inside the memory area instead of the spare area. The main reason is that OpenIPMC-HW's flash device uses the spare area for storing error correction results which are later fetched by **yimpl_flash_read_chunk_fn** during a read operation.


After developing the driver callbacks the next step is to integrate the file system to the real-time operating system, the system integrator must provide a layer which allows YAFFS to access system resources. This layer is defined at ```yaffsfs_rtos.c```, the table below lists all the APIs used to integrate YAFFS.

| API | Layer | Description |
| --- | ----- | ---------- |
| **yaffsfs_OSInitialisation** |    **yaffsfs_rtos.c** | Initialises MUTEX used to protect YAFFS from concurrent accesses. |
| **yaffsfs_CurrentTime** |         **yaffsfs_rtos.c** | Gets time in Epoch format from RTC peripheral |
| **yaffsfs_SetError** |            **yaffsfs_rtos.c** | Sets file system error fired by a task into a pool of errors. This is further explained at the next section. |
| **yaffsfs_Lock** |                **yaffsfs_rtos.c** | Reserves system resource access for current file system operation by taking a MUTEX. |
| **yaffsfs_Unlock** |              **yaffsfs_rtos.c** | Releases system resource access exclusivity by returning a previously taken MUTEX. | 
| **yaffsfs_malloc** |              **yaffsfs_rtos.c** | Allocates memory for file system operations. |
| **yaffsfs_free** |                **yaffsfs_rtos.c** | De-allocates memory previously allocated. |
| **yaffsfs_CheckMemRegion**|       **yaffsfs_rtos.c** | Checks if memory region is valid for access. |
| **yaffs_bug_fn**|                 **yaffsfs_rtos.c** | Reports bugs present on code. |

These functions are called by YAFFS on determinate conditions inside it's source code and are responsible to coordinate the file system operations on the running RTOS.
This layer is provides access to system resources such as error signaling, multi-thread protection and fetching time-stamps from the RTC peripheral. The functions **yaffsfs_malloc** and **yaffsfs_free** make use of a private allocator named [**umm_malloc**](https://github.com/rhempel/umm_malloc) which takes care of dynamical allocation at a special memory region assigned for YAFFS-2 operation.

Once the driver callbacks and RTOS layers are defined, the file system is ready to be launched. It can be done by placing a call to ``` yimpl_setup()``` which is an already implemented initialisation function that takes care of starting the file system and mounting the partition. This procedure is defined at ```yimpl_conf.c``` and is very straightforward.

### POSIX Interface and error handling

YAFFS has a set of POSIX-compliant APIs used on OpenIPMC-FW to develop the file system's utilities. These API's communicate with the core file system code to perform various management operations and abstracting the user from inner mechanisms. OpenIPMC-FW uses the following set of APIs.

| API                       | Description |
| ------------------------- | ----------- |
|**yaffs_format**         | Formats a device.                                      |
|**yaffs_mount**          | Mounts file system partition for read and write access.|
|**yaffs_unmount2**       | Unmounts file system partition, can perform a forced unmount<br />if the argument flag is set.|
|**yaffs_mkdir**          | Creates a directory in file system. |
|**yaffs_rmdir**          | Removes a directory from the file system. |
|**yaffs_unlink**         | Destroys link to a file, if it has only one link, once the handle is closed<br />the file will be unreachable due to the abscence of links and thus will<br />be treated as deleted. |
|**yaffs_opendir**        | Opens a directory for object manipulation.|
|**yaffs_readdir**        | Iterates through a directory, will return a NULL pointer once it reaches<br />the end of the directory. |
|**yaffs_closedir**       | Closes a directory. |
|**yaffs_open**           | Opens a file, returns a file handle that can be used in other APIs for operations. |
|**yaffs_read**           | Takes a file handle and reads the correspondent file's content. |
|**yaffs_write**          | Takes a file handle and writes content at the correspondent file. |
|**yaffs_close**          | Takes a file handle and closes file. |
|**yaffs_chmod**          | Changes file permissions. |
|**yaffs_freespace**      | Computes flash device's free amount of memory in bytes. |
|**yaffs_totalspace**     | Computes flash device's total amount of memory in bytes. |

The functions above return an integer greater or equal than zero on success and a negative integer on failure unless stated otherwise on the API description. Regarding the possible error codes specified by the POSIX interface, defined at ``` <errno.h>```, whenever a YAFFS-2 API function fails, it will call the FreeRTOS integration layer's error signaling function ```yaffsfs_SetError()``` which will take care of assigning the error code at the file system error pool by taking the error code and the task name that fired the error and storing them at a slot in the pool. It is advisable that after every unsuccessful file system operation the user should "pop" the error code from the pool right after by using ```yaffsfs_get_errno()```. If the user decides to not call ```yaffsfs_get_errno()``` the error stored at the correspondent slot in the pool will be overwritten by the next error fired by the same task. Have a look at the diagrams below:

```plain
1. Task 1 fires an error and calls yaffsfs_SetError()

                                                    File System error pool
+------+                                     
|task 1| ----- yaffsfs_SetError(ERR) ------> [1.1 Look for "task 1" inside the slots]                                              
+------+                                     [1.2 No matching name was found]
                                             [1.3 Storing at Nth position]
                                             +---------------+-------+-------+-----+
                                             | ERR, "task 1" | empty | empty | ... |
                                             +---------------+-------+-------+-----+
                                                   N            N+1     N+2    N+i


2. Right after finishing file system operation, user calls yaffsfs_get_errno() in task 1

                                                    File System error pool
+------+                                      
|task 1| ----- yaffsfs_get_errno(ERR) ------>[2.1 Look for "task 1" inside the slots] 
+------+                                     [2.2 Found matching task name]
                                             +-------+-------+-------+-----+
             [2.3 returns error code]        | empty | empty | empty | ... |
                    +-----+                  +-------+-------+-------+-----+
                    | ERR |                    { N }     N+1    N+2    N+i
                    +-----+ <-------------------/                
```
Notice how the Nth position gets empty and the error code is returned to the calling task.
Now, have a look at a case where the API is not called.

```plain
1. Task 1 fires an error and calls yaffsfs_SetError()

                                                    File System error pool
+------+                                     
|task 1| ---- yaffsfs_SetError(ERR_0) -----> [1.1 Look for "task 1" inside the slots]                                              
+------+                                     [1.2 No matching name was found]
                                             [1.3 Storing at Nth position]
                                             +-----------------+-------+-------+-----+
                                             | ERR_0, "task 1" | empty | empty | ... |
                                             +-----------------+-------+-------+-----+
                                                     N            N+1     N+2    N+i

2. API is not called, some time passes and then another error is fired by the same task.

                                                    File System error pool
+------+                                      
|task 1| --- yaffsfs_SetError(ERR_1) --->[2.1 Look for "task 1" inside the slots] 
+------+                                 [2.2 Found matching task name]
                                         [2.3 Overwriting Nth position with new error code]
                                         +-------------------+-------+-------+-----+
                                         | {ERR_1}, "task 1" | empty | empty | ... |
                                         +-------------------+-------+-------+-----+
                                                    N           N+1     N+2    N+i
```
Notice how the previous error code gets overwritten, this happens because the File System error pool always checks for present task names and updates the error code instead of filling up multiple slots for a single task. This way we can save some memory when dealing with a multi-thread environment. Depending of the file system operation and how sensitive it is inside the software context, the user might want to check for error codes inside the pool in order to keep track of the operations. We may do such check by evaluating the return value of the called YAFFS-2 POSIX API, if it is greater or equal than zero there's no necessity to call ```yaffsfs_get_errno()``` since no error was fired by the API. Otherwise we may call ```yaffsfs_get_errno()``` in order to retrieve the fired error.
