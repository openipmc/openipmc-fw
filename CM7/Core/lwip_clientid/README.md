# LWIP with client id option enabled

This project uses modified `dhcp.c` and `dhcp.h` files to accomodate CMS's client ID.

Since the LWIP source code is automatically manipulated by CubeMX code generator, changes made on `openipmc-fw.ioc` will replace the modified files to the CubeMX original ones. In order to circumvent such unwanted changes, **copy_dhcp_sources.sh** script should be launched to copy back the custom files from this directory into the LWIP directory.

In order to avoid issues, this script is already included in the IDE pre-build steps.
