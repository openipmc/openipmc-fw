/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      exti_gpio_driver.h                                                */
/* @brief     Library aimed to keep GPIO EXTI relate ISR                        */
/* @author    Carlos R. Dell'Aquila                                             */
/********************************************************************************/

#ifndef INC_EXTI_GPIO_DRIVER_H_
#define INC_EXTI_GPIO_DRIVER_H_

#include <stdint.h>
#include "main.h"

/* CONFIGURATION SECTION */

#define EXTI0_MAX_CALLBACKS     1
#define EXTI1_MAX_CALLBACKS     1
#define EXTI2_MAX_CALLBACKS     1
#define EXTI3_MAX_CALLBACKS     1
#define EXTI4_MAX_CALLBACKS     1
#define EXTI9_5_MAX_CALLBACKS   1
#define EXTI15_10_MAX_CALLBACKS 1

#ifndef EXTI0_ENABLE
#define EXTI0_ENABLE     0
#endif

#ifndef EXTI1_ENABLE
#define EXTI1_ENABLE     0
#endif

#ifndef EXTI2_ENABLE
#define EXTI2_ENABLE     0
#endif

#ifndef EXTI3_ENABLE
#define EXTI3_ENABLE     0
#endif

#ifndef EXTI4_ENABLE
#define EXTI4_ENABLE     0
#endif

#ifndef EXTI9_5_ENABLE
#define EXTI9_5_ENABLE   0
#endif

#ifndef EXTI15_10_ENABLE
#define EXTI15_10_ENABLE 0
#endif

/* END CONFIGURATION SECTION */


typedef enum {
  EXTI_ADD_CALLBACKS_RET_OK,
  EXTI_ADD_CALLBACKS_RET_ERROR_EXTI_DISABLE,
  EXTI_ADD_CALLBACKS_RET_ERROR_NO_SPACE
} exti_add_callback_ret_t;

typedef void (*exti_callback)(void);

typedef struct {
  uint32_t pin;
  void (*exti_callback)(void);
} exti_data_callback_t ;

exti_add_callback_ret_t exti_add_callback(uint32_t, exti_callback);


#endif // INC_EXTI_GPIO_DRIVER_H_
