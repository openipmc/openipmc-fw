/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: rtc_tools.c
 * @brief	: Real-Time Clock tools header.
 * @date	: YYYY / MM / DD - 2022 / 04 / 08
 * @author	: Antonio V. G. Bassi
 *
*/

#ifndef INC_RTC_TOOLS_H_
#define INC_RTC_TOOLS_H_

#define RTC_TOOLS_STAMP_FORMAT  "%.2d-%.2d-%.2d %.2d:%.2d:%.2d"
#define RTC_TOOLS_STAMP_SIZE    17UL

#include "stm32h7xx_hal.h"

/* functions */
extern uint32_t rtc_tool_fetch_epoch( void );
extern void rtc_tool_init( void );
extern void rtc_tool_get_timestamp(char* strbuf, size_t buf_size);
extern void rtc_tool_access_hal(RTC_TimeTypeDef* sTime, RTC_DateTypeDef* sDate);

#endif /* INC_RTC_TOOLS_H_ */
