/*
 * @file    emh_portenv.h
 * @author  Antonio Vitor Grossi Bassi (antoniovitor.gb@gmail.com)
 * @brief   emh_malloc configuration port.
 * @version 1.6
 * @date    2023-8-5
 */

#ifndef INC_EMH_PORTENV_H_
#define INC_EMH_PORTENV_H_

#include "FreeRTOS.h"
#include "semphr.h"

extern SemaphoreHandle_t emh_malloc_mtx;

#define EMH_MALLOC_N_HEAPS        (8UL)
#define EMH_MALLOC_BYTE_ALIGNMENT (4UL)

#define __emh_create_zone__()                 \
{                                             \
  emh_malloc_mtx = xSemaphoreCreateMutex();   \
}                                             \

#define __emh_lock_zone__()                      \
{                                                \
  xSemaphoreTake(emh_malloc_mtx, portMAX_DELAY); \
}                                                \

#define __emh_unlock_zone__()     \
{                                 \
  xSemaphoreGive(emh_malloc_mtx); \
}                                 \

#endif /* INC_EMH_PORTENV_H_ */


#ifndef INC_EMH_PORTENV_H_
#define INC_EMH_PORTENV_H_



#endif /* INC_EMH_PORTENV_H_ */
