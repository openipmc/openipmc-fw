#ifndef BOARD_AND_PRODUCT_INFO
#define BOARD_AND_PRODUCT_INFO

#include <stdint.h>

typedef struct
{
	uint32_t board_mfg_date_time;
	char*    board_manufacturer;
	char*    board_product_name;
	char*    board_serial_number;
	char*    board_part_number;
	char*    board_fru_file_id;
	char*    product_manufacturer;
	char*    product_name;
	char*    product_part_model_number;
	char*    product_version;
	char*    product_serial_number;
	char*    product_asset_tag;
	char*    product_fru_file_id;

}board_and_product_info_t;

#endif
