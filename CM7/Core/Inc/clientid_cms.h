/*
 * dhcp_clientid.h
 *
 *  Created on: 26 de jul de 2022
 *      Author: antoniobassi
 */

#ifndef SRC_CLIENTID_CMS_H_
#define SRC_CLIENTID_CMS_H_

#include <stdint.h>
#include <lwip/dhcp.h>

/** Definitions for error constants. */
enum {
/** No error, everything OK. */
  CLIENTID_ERR_OK           =  0,
/** Illegal argument passed to function */
  CLIENTID_ERR_ILLEGAL_ARG  = -1,
/** Error carrier HW address fails parity*/
  CLIENTID_ERR_HWADDR_PARITY_FAIL = -2,
/** Timeout while waiting IPMI data */
  CLIENTID_ERR_IPMI_TIMEOUT = -3,
/** Error in IPMI communication */
  CLIENTID_ERR_IPMI_ERROR   = -4,
/** Format not implemented */
  CLIENTID_ERR_FMT_NOT_IMPLEMENTED = -5
};

int client_id_from_atca_cms(struct dhcp_client_id* client_id);

#endif /* SRC_CLIENTID_CMS_H_ */
