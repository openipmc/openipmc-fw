/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/* *****************************************************************************
 * @file	: ipmc_scribe.h
 * @brief	: ipmc scribe task header, containing necessary includes and macros.
 * @date	: YYYY / MM / DD - 2022 / 04 / 08
 * @author	: Antonio V. G. Bassi
 * ******************************************************************************
*/

#ifndef INC_IPMC_SCRIBE_H_
#define INC_IPMC_SCRIBE_H_

/* Declarations and Macros */
extern void ipmc_scribe_log_entry( const char* message, const char* sender );
extern void ipmc_scribe_init( void );

#define DEFAULT_PATH "/ipmc_log"

#endif /* INC_IPMC_SCRIBE_H_ */
