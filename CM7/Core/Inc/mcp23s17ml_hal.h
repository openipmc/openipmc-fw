/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      mcp23s17ml_hal.h                                                  */
/* @brief     Library aimed to control MCP23s17ml SPI IO Expander               */
/* @author    Carlos R. Dell'Aquila                                             */
/********************************************************************************/

#ifndef INC_MCP23S17ML_HAL_H_
#define INC_MCP23S17ML_HAL_H_

#include "h7spi_bare.h"

// CONFIGURATION DRIVER PARAMETERS

#define MAX_MANAGED_DEV 16 // Define how many MCP23S17 this driver will control.

// END CONFIGURATION DRIVER PARAMETERS


// CONTROL REGISTER ADDRESS FOR IOCON.BANK = 0
#define IODIRA_BANK_0_MCP23S17      0x00
#define IODIRB_BANK_0_MCP23S17      0x01
#define IPOLA_BANK_0_MCP23S17       0x02
#define IPOLB_BANK_0_MCP23S17       0x03
#define GPINTENA_BANK_0_MCP23S17    0x04
#define GPINTENB_BANK_0_MCP23S17    0x05
#define DEFVALA_BANK_0_MCP23S17     0x06
#define DEFVALB_BANK_0_MCP23S17     0x07
#define INTCONA_BANK_0_MCP23S17     0x08
#define INTCONB_BANK_0_MCP23S17     0x09
#define IOCON_BANK_0_MCP23S17       0x0A
#define GPPUA_BANK_0_MCP23S17       0x0C
#define GPPUB_BANK_0_MCP23S17       0x0D
#define INTFA_BANK_0_MCP23S17       0x0E
#define INTFB_BANK_0_MCP23S17       0x0F
#define INTCAPA_BANK_0_MCP23S17     0x10
#define INTCAPB_BANK_0_MCP23S17     0x11
#define GPIOA_BANK_0_MCP23S17       0x12
#define GPIOB_BANK_0_MCP23S17       0x13
#define OLATA_BANK_0_MCP23S17       0x14
#define OLATB_BANK_0_MCP23S17       0x15

// CONTROL REGISTER ADDRESS FOR IOCON.BANK = 1
#define IODIRA_BANK_1_MCP23S17      0x00
#define IPOLA_BANK_1_MCP23S17       0x01
#define GPINTENA_BANK_1_MCP23S17    0x02
#define DEFVALA_BANK_1_MCP23S17     0x03
#define INTCONA_BANK_1_MCP23S17     0x04
#define IOCONA_BANK_1_MCP23S17      0x05
#define GPPUA_BANK_1_MCP23S17       0x06
#define INTFA_BANK_1_MCP23S17       0x07
#define INTCAPA_BANK_1_MCP23S17     0x08
#define GPIOA_BANK_1_MCP23S17       0x09
#define OLATA_BANK_1_MCP23S17       0x0A

#define IODIRB_BANK_1_MCP23S17      0x10
#define IPOLB_BANK_1_MCP23S17       0x11
#define GPINTENB_BANK_1_MCP23S17    0x12
#define DEFVALB_BANK_1_MCP23S17     0x13
#define INTCONB_BANK_1_MCP23S17     0x14
#define IOCONB_BANK_1_MCP23S17      0x15
#define GPPUB_BANK_1_MCP23S17       0x16
#define INTFB_BANK_1_MCP23S17       0x17
#define INTCAPB_BANK_1_MCP23S17     0x18
#define GPIOB_BANK_1_MCP23S17       0x19
#define OLATB_BANK_1_MCP23S17       0x1A

// PORTx PIN
#define GPX_0_MCP23S17  0x00
#define GPX_1_MCP23S17  0x01
#define GPX_2_MCP23S17  0x02
#define GPX_3_MCP23S17  0x03
#define GPX_4_MCP23S17  0x04
#define GPX_5_MCP23S17  0x05
#define GPX_6_MCP23S17  0x06
#define GPX_7_MCP23S17  0x07

// IOCON BITS FILEDS
#define UN_IMPLEMENTED  0x01 // NOT IMPLEMENTED BIT
#define INTPOL          0x02 // Sets the polarity of the INT output pin. DEFAULT 0: active-low.
#define ODR             0x04 // Configures the INT pin as an open-drain-output. DEFAULT 0: active driver output.
#define HAEN            0x08 // Hardware Address Enable bit. DEFAULT 0: Address pin disabled.
#define DISSLW          0x10 // Slew Rate control bit for SDA output. DEFAULT 0: Slew rate enabled.
#define SEQOP           0x20 // Sequential operation mode bit. DEFAULT 0: enabled, address pointer increments.
#define MIRROR          0x40 // INT pins mirror. DEFAULT 0: pins are not connected.
#define BANK            0x80 // Controls how the registers are addressed. DEFAULT 0:same bank, so addresses are sequential.

// DEFAULT OUTPUT VALUES
#define LOW_MCP23S17  0x00
#define HIGH_MCP23S17 0x01

typedef enum {
  MCP23S17_RET_OK,
  MCP23S17_RET_ERROR
} mcp23s17_ret_code_t;

typedef enum {
  MCP23S17_BANK_0 = 0,
  MCP23S17_BANK_1 = 1
} mcp23s17ml_bank_t;

typedef enum {
  MCP23S17_PORTA = 0,
  MCP23S17_PORTB = 1
} mcp23s17_port_t;

typedef enum {
  INPUT,
  OUTPUT
} mcp13s17_pin_dir_t;

typedef enum {
  POL_NO_INV,
  POL_INV
} mcp23s17_pin_ipol_t;

typedef enum {
  INT_DISABLE,
  INT_ENABLE_ON_CHANGE_MODE,
  INT_ENABLE_ON_INPUT_LOW,
  INT_ENABLE_ON_INPUT_HIGH
} mcp23s17_pin_interrupt_t;

typedef enum {
  PULL_UP_DISABLE,
  PULL_UP_ENABLE
} mcp23s17_pin_pullup_t;

typedef struct {
  uint8_t intf;    // Reflects the interrupt condition on the port. BIT=1, the pin caused interrupt. BIT=0 interrupt no pending.
  uint8_t intcap;  // Reflects the logic level on the port pins at the time of interrupt due to pin change.
} mcp23s17_int_status_t;

typedef struct {
  // Interface address
  h7spi_periph_t spi_periph;    // SPI interface.
  uint8_t mcp23s17_addr;        // MCP23S17 address device number.
  // Reset
  GPIO_TypeDef *port_reset;     // GPIO PORT associated to the MCP23S17 reset input.
  uint32_t pin_reset;           // GPIO PIN associated to the MCP23S17 resent input.
  // Interrupt
  GPIO_TypeDef *port_interrupt; // GPIO PORT associated to the MCP23S17 Interrupt output.
  uint32_t pin_interrupt;       // GPIO PIN associated to the MCP23S17 Interrupt output.

} mcp23s17_dev_t;

typedef struct {
  mcp23s17_dev_t* dev;           // Associated PIN IO Expander.
  mcp23s17_port_t port;          // Associated PIN PORT.
  uint8_t pin_number;            // PIN NUMBER.
  void (*pin_callback)(uint8_t); // pin_callback when it works as interrupt input pin.
} mcp23s17_pin_t;

typedef struct {
  mcp23s17_dev_t dev;
  void (*pin_callback_a[8])(uint8_t);
  void (*pin_callback_b[8])(uint8_t);
} mcp23s17_dev_record_t;

/**
 * @brief Function to initialize the MCP23S17_ML
 *
 * @param mcp23s17ml_dev_t pointer where SPI interface is specified and MCP23S17ML IO expander.
 *
 * @return Return the status of the command ({@link MCP23S17ML_RET_OK }, {@link MCP23S17ML_RET_ERROR })
 *
 * This function enables the Hardware Address mode for all MCP23S17ML connected at the same SPI interface bus. The mode is important for configure each
 * device in separated way according with the values on address physical pins. Also, it connects internally INT pin of both port according with OpenIPMC-HW
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_init(mcp23s17_dev_t* dev);

/**
 * @brief Function to set with default values MCP23S17_ML
 *
 * @param mcp23s17ml_dev_t pointer to a struct with two fields. The first one is the SPI where the device is connected and the second its physical
 * address.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This functions sets each register of MCP23S17_ML devices according to default values in datasheet.
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_full_reset(mcp23s17_dev_t* dev);

/**
 * @brief Function to configure a MCP23S17 PIN as OUTPUT
 *
 * @param mcp23s17_pin_t with SPI interface, MCP23S17ML and pin number information.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This function configures the IODIR register for the specific pin to work as output.
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_config_pin_output(mcp23s17_pin_t* pin);

/**
 * @brief Function to configure a MCP23S17 PIN as INPUT
 *
 * @param mcp23s17_pin_t with SPI interface, MCP23S17ML and pin number information.
 * @param mcp23s17_pin_ipol_t to configure the IPOL register.
 * @param mcp23s17_pin_interrupt_t to enable the interrupt and their behavior (on-change, falling or rising).
 * @param mcp23s17_pin_pullup_t to choose if the input pin has PULL-UP resistor.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This function configures all registers to setup a pin as input, enable interrupts and pull-up resistor.
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_config_pin_input(mcp23s17_pin_t* pin, mcp23s17_pin_ipol_t ipol, mcp23s17_pin_interrupt_t interrupt, mcp23s17_pin_pullup_t pullup);

/**
 * @brief Function to read the INPUT PIN VALUE a MCP23S17
 *
 * @param mcp23s17_pin_t with SPI interface, MCP23S17ML and pin number information.
 * @param uint8_t* pointer to return the value.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This function returns the value
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_read_pin_value(mcp23s17_pin_t* pin, uint8_t* in_val);

/**
 * @brief Function to read the INPUT PIN VALUE a MCP23S17
 *
 * @param mcp23s17_pin_t with SPI interface, MCP23S17 and pin number information.
 * @param uint8_t contents the required pin value.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This function set the pin value.
 *
 * @note This function uses the H7SPI driver.
 *
 */
mcp23s17_ret_code_t mcp23s17_write_pin_value(mcp23s17_pin_t* pin, uint8_t in_val);


/**
 * @brief Function to get the pin configuration
 *
 * @param mcp23s17_pin_t with SPI interface, MCP23S17 and pin number information.
 * @param uint8_t pointer to send-back the actual io_config. '1' is INPUTU and '0' OUTPUT.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK }, {@link MCP23S17_RET_ERROR })
 *
 * This function can be used for control signal that need to hold the pin state.
 *
 */
mcp23s17_ret_code_t mcp23s17_get_pin_config(mcp23s17_pin_t* pin, uint8_t* io_dir_config);



/**
 * @brief Function to read the pin status and capture interrupt registers
 *
 * @param mcp23s17_dev_t with SPI interface and address MCP23S17.
 * @param mcp23s17_port_t port to read.
 * @param uint8_t pointer to get MCP23S17 interrupt flag status registers.
 * @param uint8_t pointer to get MCP23S17 interrupt capture registers.
 *
 * @return Return the status of the command ({@link MCP23S17_RET_OK}, {@link MCP23S17_RET_ERROR})
 *
 * This function is specific for pins configured as interrupt.
 *
 * @note This function uses the H7SPI driver.
 */
mcp23s17_ret_code_t mcp23s17_read_pin_interrupt_status(mcp23s17_dev_t *dev, mcp23s17_port_t port_number,uint8_t *inter_flag, uint8_t *inter_cap);

/**
 * @brief Function used as callback to notify EXTIx interrupt event
 *
 * @param uint8_t the EXTI Flag status register value at moment when general interrupt ISR starts.
 * @param uint8_t* pointer to return a flag to clean the interrupt source.
 * @param BaseType_t* pointer to return a flag to force the context switch and gives a priority to enable a task which
 *                    is waiting for a binary semaphore.
 *
 * This function is executed EXITx hanlder when there is an interrupt event on GPIO pin from a MCP23s17 device.
 *
 * @note This function uses FreeRTOS binary Semaphore.
 */
void mcp23s17_isr(uint32_t pr1, uint32_t* pr1_clean_mask_pointer, BaseType_t* xHigherPriorityTaskWoken_pointer);

/**
 * @brief Function task to read out MCP23S17 device interrupt status.
 *
 * @param void pointer specific for FreeRTOS task parameter.
 *
 * This function task waits for interrupt event and then executes each user-specific callback associated to
 * MCP23S17 PIN PORT.
 *
 * @note This function creates and uses a FreeRTOS binary Semaphore.
 */
void mcp23s17_task(void *pvParameters);


#endif // INC_MCP23S17ML_HAL_H_
