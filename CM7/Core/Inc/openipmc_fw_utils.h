/*
 * openipmc_fw_utils.h
 *
 *  Created on: 8 de set de 2022
 *      Author: antoniobassi
 */

#ifndef INC_OPENIPMC_FW_UTILS_H_
#define INC_OPENIPMC_FW_UTILS_H_

#define FwALIGN_MASK(x, mask) (((x) + (mask) ) & ~(mask))
#define FwALIGN(x, n) FwALIGN_MASK(x, (typeof(x))(n) - 1)

extern size_t strnlen_s(const char* str, size_t strsz);


#endif /* INC_OPENIPMC_FW_UTILS_H_ */
