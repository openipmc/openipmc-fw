/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file  ipmiv2.h
 * @brief Macro definitions for IPMI version 2.0 network function, completion codes, commands and sub-functions.
 * @date YYYY / MM / DD - 2023 / 03 / 20
 * @author Antonio V. G. Bassi
 *
 */

#ifndef INC_IPMIV2_H_
#define INC_IPMIV2_H_

#define IPMI_MSGFMT_RSADDR_TYPE_Pos (0U)
#define IPMI_MSGFMT_RSADDR_TYPE_Msk (0x1UL << IPMI_MSGFMT_RSADDR_TYPE_Pos)
#define IPMI_MSGFMT_RSADDR          (IPMI_MSGFMT_RSADDR_TYPE_Msk)
#define IPMI_MSGFMT_HDR1_LUN_Pos    (0U)
#define IPMI_MSGFMT_HDR1_LUN_Msk    (0x3UL << IPMI_DATAGRAM_HDR1_LUN_Pos)
#define IPMI_MSGFMT_HDR1_LUN        IPMI_DATAGRAM_HDR1_LUN_Msk
#define IPMI_MSGFMT_HDR1_NetFN_Pos  (2U)
#define IPMI_MSGFMT_HDR1_NetFN_Msk  (0x3FUL << IPMI_DATAGRAM_HDR1_NetFn_Pos)
#define IPMI_MSGFMT_HDR1_NetFN      IPMI_DATAGRAM_HDR1_LUN_Msk

#define IPMIV2_NETFN_CHASSIS_RQ       0x00
#define IPMIV2_NETFN_CHASSIS_RS       0x01
#define IPMIV2_NETFN_BRIDGE_RQ        0x02
#define IPMIV2_NETFN_BRIDGE_RS        0x03
#define IPMIV2_NETFN_SENSOR_EVENT_RQ  0x04
#define IPMIV2_NETFN_SENSOR_EVENT_RS  0x05
#define IPMIV2_NETFN_APP_RQ           0x06
#define IPMIV2_NETFN_APP_RS           0x07
#define IPMIV2_NETFN_FIRMWARE_RQ      0x08
#define IPMIV2_NETFN_FIRMWARE_RS      0x09
#define IPMIV2_NETFN_STORAGE_RQ       0x0A
#define IPMIV2_NETFN_STORAGE_RS       0x0B
#define IPMIV2_NETFN_TXP_RQ           0x0C
#define IPMIV2_NETFN_TXP_RS           0x0D
#define IPMIV2_NETFN_GRPEXT_RQ        0x2C
#define IPMIV2_NETFN_GRPEXT_RS        0x2D
#define IPMIV2_NETFN_OEM_RQ           0x2E
#define IPMIV2_NETFN_OEM_RS           0x2F

#define IPMIV2_CPLTN_CODE_NORMAL      0x00    // Command executed normally.
#define IPMIV2_CPLTN_CODE_BUSY        0xC0    // Node is busy.
#define IPMIV2_CPLTN_CODE_INVAL       0xC1    // Invalid command.
#define IPMIV2_CPLTN_CODE_LUN         0xC2    // Invalid command for given LUN.
#define IPMIV2_CPLTN_CODE_TIMEOUT     0xC3    // Timeout while processing command.
#define IPMIV2_CPLTN_CODE_NOSPACE     0xC4    // Out of space.
#define IPMIV2_CPLTN_CODE_INVRESID    0xC5    // Reservation canceled or invalid reservation ID.
#define IPMIV2_CPLTN_CODE_RQTRUNC     0xC6    // Requested data truncated.
#define IPMIV2_CPLTN_CODE_INVRQLEN    0xC7    // Requested data length invalid.
#define IPMIV2_CPLTN_CODE_RQLENEXD    0xC8    // Requested data field length exceeded limit.
#define IPMIV2_CPLTN_CODE_OORPARAM    0xC9    // Parameter Out-Of-Range
#define IPMIV2_CPLTN_CODE_RET         0xCA    // Cannot return number of requested data bytes.
#define IPMIV2_CPLTN_CODE_NOSDR       0xCB    // Requested Sensor, Data or Record not present.
#define IPMIV2_CPLTN_CODE_INVRQ       0xCC    // Invalid data field in request
#define IPMIV2_CPLTN_CODE_SDRINVCMD   0xCD    // Command illegal for specified sensor or record type.
#define IPMIV2_CPLTN_CODE_NOCMDRS     0xCE    // Command response could not be provided.
#define IPMIV2_CPLTN_CODE_DUPLRQ      0xCF    // Cannot execute duplicated request.
#define IPMIV2_CPLTN_CODE_SDRUPDATE   0xD0    // Command response could not be provided. SDR repository in update mode.
#define IPMIV2_CPLTN_CODE_FWUPDATE    0xD1    // Command response could not be provided. Device in firmware update mode.
#define IPMIV2_CPLTN_CODE_BMCINIT     0xD2    // Command response could not be provided. Device is being initialised.
#define IPMIV2_CPLTN_CODE_DESTUA      0xD3    // Destination unavailable.
#define IPMIV2_CPLTN_CODE_LOWPRIV     0xD4    // Insufficient privilege level.
#define IPMIV2_CPLTN_CODE_NOSUPPORT   0xD5    // Command or requested parameter not supported.
#define IPMIV2_CPLTN_CODE_INVPARAM    0xD6    // Parameter is illegal.
#define IPMIV2_CPLTN_CODE_UNSPEC      0xFF    // Unspecified error.

#define IPMIV2_CPLTN_CODE_SET_SESSION_PRIV_UNAVAIL 0x80  // Privilege level not available for user.
#define IPMIV2_CPLTN_CODE_SET_SESSION_PRIV_EXCD    0x81  // Requested privilege exceed's channel's privilege.
#define IPMIV2_CPLTN_CODE_SET_SESSION_PRIV_USR_LVL 0x82  // Can't disable user level authentication.

#define IPMIV2_CPLTN_CODE_CLOSE_SESSION_INVHANDLE  0x87  // Invalid session handle
#define IPMIV2_CPLTN_CODE_CLOSE_SESSION_INV_ID     0x88  // Invalid session ID

#define IPMIV2_CPLTN_CODE_ACTIVATE_PLD_ALREADY_ACTIVE  0x80
#define IPMIV2_CPLTN_CODE_ACTIVATE_PLD_TYPE_DISABLED   0x81
#define IPMIV2_CPLTN_CODE_ACTIVATE_PLD_LIMIT_REACHED   0x82
#define IPMIV2_CPLTN_CODE_ACTIVATE_PLD_NO_ENCRYP       0x83
#define IPMIV2_CPLTN_CODE_ACTIVATE_PLD_ENCRYP_REQ      0x84

#define IPMIV2_CPLTN_CODE_DEACTIVATE_PLD_ALREADY_DEACTIVATED 0x80
#define IPMIV2_CPLTN_CODE_DEACTIVATE_PLD_TYPE_DISABLED       0x81

/* List of IPMI v2 commands, each macro here is listed
 * as in the following logic
 * {IPMIV2}-{Associated Network function}-{Command name}
 *
 */
#define IPMIV2_APP_GET_DEV_ID           0x01
#define IPMIV2_APP_COLD_RST             0x02
#define IPMIV2_APP_WARM_RST             0x03
#define IPMIV2_APP_GET_SELFTST_RESULTS  0x04
#define IPMIV2_APP_MFR_TST_ON           0x05
#define IPMIV2_APP_SET_ACPI_PWR_STATE   0x06
#define IPMIV2_APP_GET_ACPI_PWR_STATE   0x07
#define IPMIV2_APP_GET_DEV_GUID         0x08
#define IPMIV2_APP_GET_NETFN_SUPP       0x09
#define IPMIV2_APP_GET_CMD_SUPP         0x0A
#define IPMIV2_APP_GET_SUBFN_SUPP       0x0B
#define IPMIV2_APP_GET_CFGRBL_CMDS      0x0C
#define IPMIV2_APP_GET_CFGRBL_SUBFNS    0x0D
#define IPMIV2_APP_SET_CMD_ENABLES      0x60
#define IPMIV2_APP_GET_CMD_ENABLES      0x61
#define IPMIV2_APP_SET_SUBFN_ENABLES    0x62
#define IPMIV2_APP_GET_SUBFN_ENABLES    0x63
#define IPMIV2_APP_GET_OEM_SUPP         0x64
#define IPMIV2_APP_RST_WD_TIMER         0x22
#define IPMIV2_APP_SET_WD_TIMER         0x24
#define IPMIV2_APP_GET_WD_TIMER         0x25
#define IPMIV2_APP_SET_BMC_GLBL_ENABLES 0x2E
#define IPMIV2_APP_GET_BMC_GLBL_ENABLES 0x2F
#define IPMIV2_APP_CLEAR_MSG_FLAGS      0x30
#define IPMIV2_APP_GET_MSG_FLAGS        0x31
#define IPMIV2_APP_ENABLE_MSG_CH_RX     0x32
#define IPMIV2_APP_GET_MSG              0x33
#define IPMIV2_APP_SEND_MSG             0x34
#define IPMIV2_APP_RD_EVENT_MSG_BUF     0x35
#define IPMIV2_APP_GET_BT_IF_CPBLTS     0x36
#define IPMIV2_APP_GET_SYS_GUID         0x37
#define IPMIV2_APP_SET_SYS_INFO_PARAM   0x58
#define IPMIV2_APP_GET_SYS_INFO_PARAM   0x59
#define IPMIV2_APP_GET_CH_AUTH_CPBLTS   0x38
#define IPMIV2_APP_GET_SESSION_CHLG     0x39
#define IPMIV2_APP_ACTIVATE_SESSION     0x3A
#define IPMIV2_APP_SET_PRVLG_LVL        0x3B
#define IPMIV2_APP_CLOSE_SESSION        0x3C
#define IPMIV2_APP_GET_SESSION_INFO     0x3D
#define IPMIV2_APP_GET_AUTH_CODE        0x3F
#define IPMIV2_APP_SET_CH_ACCESS        0x40
#define IPMIV2_APP_GET_CH_ACCESS        0x41
#define IPMIV2_APP_GET_CH_INFO          0x42
#define IPMIV2_APP_SET_USR_ACCESS       0x43
#define IPMIV2_APP_GET_USR_ACCESS       0x44
#define IPMIV2_APP_SET_USR_NAME         0x45
#define IPMIV2_APP_GET_USR_NAME         0x46
#define IPMIV2_APP_SET_USR_PWRD         0x47
#define IPMIV2_APP_ACTIVATE_PLD         0x48
#define IPMIV2_APP_DEACTIVATE_PLD       0x49
#define IPMIV2_APP_GET_PLD_ACTVN_STATUS 0x4A
#define IPMIV2_APP_GET_PLD_INST_INFO    0x4B
#define IPMIV2_APP_SET_USR_PLD_ACCESS   0x4C
#define IPMIV2_APP_GET_USR_PLD_ACCESS   0x4D
#define IPMIV2_APP_GET_CH_PLD_SUPP      0x4E
#define IPMIV2_APP_GET_CH_PLD_VER       0x4F
#define IPMIV2_APP_GET_OEM_PLD_INFO     0x50
#define IPMIV2_APP_MASTER_WRRD          0x52
#define IPMIV2_APP_GET_CH_CIPHER        0x54
#define IPMIV2_APP_TOGGLE_PLD_ENC       0x55
#define IPMIV2_APP_SET_CH_SECKEYS       0x56
#define IPMIV2_APP_GET_SYS_IF_CPBLTS    0x57

#define IPMIV2_CHASSIS_GET_CPBLTS             0x00
#define IPMIV2_CHASSIS_GET_STATUS             0x01
#define IPMIV2_CHASSIS_GET_CTRL               0x02
#define IPMIV2_CHASSIS_RESET                  0x03
#define IPMIV2_CHASSIS_IDENTIFY               0x04
#define IPMIV2_CHASSIS_SET_FT_PANEL_BUTTON_EN 0x0A
#define IPMIV2_CHASSIS_SET_CPBLTS             0x05
#define IPMIV2_CHASSIS_PWR_RESTORE_POL        0x06
#define IPMIV2_CHASSIS_PWR_CYCLE_INTVL        0x0B
#define IPMIV2_CHASSIS_GET_SYS_RESTART_CAUSE  0x07
#define IPMIV2_CHASSIS_SET_SYS_BOOT_OPTS      0x08
#define IPMIV2_CHASSIS_GET_SYS_BOOT_OPTS      0x09
#define IPMIV2_CHASSIS_GET_POH_CNTR           0x0F

/* Commands for PICMG Group Extension */
#define IPMIV2_GRPEXT_PICMG_GET_PICMG_PPTS    0x00
#define IPMIV2_GRPEXT_PICMG_GET_ADDR_INFO     0x01
#define IPMIV2_GRPEXT_PICMG_GET_SHADDR_INFO   0x02
#define IPMIV2_GRPEXT_PICMG_SET_SHADDR_INFO   0x03
#define IPMIV2_GRPEXT_PICMG_FRU_CTL           0x04
#define IPMIV2_GRPEXT_PICMG_GET_FRU_LED_PPTS  0x05
#define IPMIV2_GRPEXT_PICMG_GET_LED_CPBLTS    0x06
#define IPMIV2_GRPEXT_PICMG_SET_LED_STATE     0x07
#define IPMIV2_GRPEXT_PICMG_GET_LED_STATE     0x08
#define IPMIV2_GRPEXT_PICMG_SET_FRU_ACTIV_POL 0x0A
#define IPMIV2_GRPEXT_PICMG_GET_FRU_ACTIV_POL 0x0B
#define IPMIV2_GRPEXT_PICMG_SET_FRU_ACTIV     0x0C
#define IPMIV2_GRPEXT_PICMG_GET_DEV_LOCTR_ID  0x0D
#define IPMIV2_GRPEXT_PICMG_SET_PORT_STATE    0x0E
#define IPMIV2_GRPEXT_PICMG_COMPUTER_PWR_PPTS 0x10
#define IPMIV2_GRPEXT_PICMG_SET_PWR_LVL       0x11
#define IPMIV2_GRPEXT_PICMG_GET_PWR_LVL       0x12
#define IPMIV2_GRPEXT_PICMG_FRU_CTL_CPBLTS    0x1E

#define IPMIV2_GRPEXT_PICMG_HPMx_GET_TGT_UPGRADE_CPBLTS 0x2E
#define IPMIV2_GRPEXT_PICMG_HPMx_GET_COMPONENT_PPTS     0x2F
#define IPMIV2_GRPEXT_PICMG_HPMx_ABORT_FW_UPGRADE       0x30
#define IPMIV2_GRPEXT_PICMG_HPMx_INIT_UPGRADE_ACT       0x31
#define IPMIV2_GRPEXT_PICMG_HPMx_UPLOAD_FW_BLOCK        0x32
#define IPMIV2_GRPEXT_PICMG_HPMx_FINISH_FW_UPLOAD       0x33
#define IPMIV2_GRPEXT_PICMG_HPMx_GET_UPGRADE_STATUS     0x34
#define IPMIV2_GRPEXT_PICMG_HPMx_ACTIVATE_FW            0x35
#define IPMIV2_GRPEXT_PICMG_HPMx_QUERY_SELF_TEST_RES    0x36
#define IPMIV2_GRPEXT_PICMG_HPMx_QUERY_ROLLBACK_STATUS  0x37
#define IPMIV2_GRPEXT_PICMG_HPMx_INIT_MANUAL_ROLLBACK   0x38
#define IPMIV2_GRPEXT_PICMG_HPMx_GET_CPBLTS             0x3E

#define IPMIV2_TXP_SET_SERIAL_ROUTING_MUX   0x1C
#define IPMIV2_TXP_SOL_ACTIVATING           0x20
#define IPMIV2_TXP_SET_SOL_CONF_PARAM       0x21
#define IPMIV2_TXP_GET_SOL_CONF_PARAM       0x22

#define IPMIV2_SET_EVENT_RCVR 0x00
#define IPMIV2_GET_EVENT_RCVR 0x01
#define IPMIV2_PLATFORM_EVENT 0x02

#endif /* INC_IPMIV2_H_ */
