#ifndef ETH_CTRLS_H
#define ETH_CTRLS_H

#include "main.h"

// User defined MAC Address.
// This definition overrides the address defined in the IOC file.
//#define USER_MAC_ADDRESS 0x0080E1000001UL

#define MAC_ADDR_STR_LEN_FROM_TERMINAL_CMD 17

// Private variable
extern _Bool openipmc_hw_uses_dhcp;


/*
 * This function is called by the "low_level_init()" functions in ethernetif.c file.
 *
 * It is responsible for defining the MAC Address before initialize the interface.
 *
 * It overrides the MAC Address defined in the IOC file via STM32CubeIDE graphical interface.
 */
void load_user_defined_mac_addr( uint8_t mac_addr[] );


/*
 * Function to change the IP address "on the fly" (after interface is already initialized
 * with some default IP address)
 */
void eth_ctrls_change_ip_addr( uint8_t ip0,   uint8_t ip1,   uint8_t ip2,   uint8_t ip3,
                               uint8_t mask0, uint8_t mask1, uint8_t mask2, uint8_t mask3,
                               uint8_t gw0,   uint8_t gw1,   uint8_t gw2,   uint8_t gw3    );


/* Function to send-back the actual network address */
extern void eth_ctrls_get_net_addr(uint8_t* ip_addr, uint8_t* mask, uint8_t *gtw_addr);

void eth_ctrls_dhcp_enable ( void );
void eth_ctrls_dhcp_disable( void );

int eth_ctrls_parse_ip_addr( const char* addr_str, uint8_t addr_num[] );
int eth_ctrls_parse_ip_addr_and_port(const char* str, uint8_t* ip, uint16_t* port);


/**
 * Set MAC address
 *
 */
extern int net_set_mac_addr(uint8_t value[], int size);


/**
 * Set MAC addr static from string
 *
 */
extern int net_set_mac_static_from_str(char mac_str[]);


/**
 * Set MAC addr chipid
 *
 */
extern int net_set_mac_chipid();


/**
 * Set MAC addr random
 *
 */
extern int net_set_mac_random();


/**
 * Get the IP mode source dhcp
 */
extern int net_get_is_ip_mode_dhcp(void);


/**
 * Set IP mode source static
 *
 */
extern int net_set_ip_mode_static(void);


/**
 * Set IP mode source DHCP
 *
 */
extern int net_set_ip_mode_dhcp(void);


/**
 * Load the IP address configuration according to the NET_KEY_IP_MODE configuration.
 *
 * Note: It uses a default configuration from custom_settings.h if that key is not present.
 *
 */
extern int load_ip_addr(void);


/**
 * Set static IP address
 *
 */
extern int net_set_static_ip(uint8_t value[], int size);


/**
 * Set static IP Address Mask
 *
 */
extern int net_set_static_mask(uint8_t value[], int size);


/**
 * Set static Gateway Address
 *
 */
extern int net_set_static_gateway_addr(uint8_t value[], int size);


#endif // ETH_CTRLS_H
