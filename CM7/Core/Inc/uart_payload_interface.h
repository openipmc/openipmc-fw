/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  @file  uart_payload_interface.h
 *
 *  @brief Describes a UART Payload Interface according IPMI spec. v1.5 to
 *         work in terminal mode.
 *
 *  @date   YYYY / MM / DD - 2023 / 11 / 08
 *  @author Carlos Ruben Dell'Aquila
 *
 */


#ifndef UART_PAYLOAD_INTERFACE_H
#define UART_PAYLOAD_INTERFACE_H

#define MAX_BYTES_PYLD_INTERFACE_TERMINAL_MODE            32
#define MAX_IPMI_ASCII_BYTES_PYLD_INTERFACE_TERMINAL_MODE 256

/**
 * @name IPMB BYTES FIELDS
 * @{
 */
/* Request/Response IPMB fields offsets */
#define IPMB0_MSG_OFFSET_RS_ADDR                   0UL
#define IPMB0_MSG_OFFSET_NET_FN_EVEN_RS_LUN        1UL
#define IPMB0_MSG_OFFSET_CHECKSUM_HEADER           2UL
#define IPMB0_MSG_OFFSET_RQ_ADDR                   3UL
#define IPMB0_MSG_OFFSET_RQ_SEQ_RQ_LUN             4UL
#define IPMB0_MSG_OFFSET_CMD                       5UL

// Specific for Request IPMB
#define IPMB0_RQ_MSG_OFFSET_DATA                   6UL

// Specifiv for Response IPMB
#define IPMB0_RS_MSG_OFFSET_COMPLETION             6UL
#define IPMB0_RS_MSG_OFFSET_DATA                   7UL

///@}

/* Request/Response UART TERMINAL MODE PAYLOAD INTERFACE fields offsets */

/**
 * @name TERMINAL MODE PAYLOAD INTERFACE BYTES FIELDS
 * @{
 */
#define TM_PYLDI_MSG_OFFSET_NET_FN_RS_LUN          0UL
#define TM_PYLDI_MSG_OFFSET_RQ_SEQ_BRIDGE          1UL
#define TM_PYLDI_MSG_OFFSET_CMD                    2UL

// Specific for Request UART TM PYLDI
#define TM_PYLDI_RQ_MSG_OFFSET_DATA                3UL

// Specific for Response UART TM PYLDI
#define TM_PYLDI_RS_MSG_OFFSET_COMPLETION_CODE     3UL
#define TM_PYLDI_RS_MSG_OFFSET_DATA                4UL
///@}


/**
 * UART PAYLOAD INTERFACE INCOMMING NEW MSG FLAG.
 *
 * Return if e new IPMI MSG in terminal mode can be sent to IPMI MSG Router.
 *
 */
typedef enum
{
  UART_PYLDI_NO_MSG,
  UART_PYLDI_NEW_MSG
} uart_pyldi_ret_code_t;

/**
 * UART PAYLOAD INTERFACE READING MAIN STATES
 *
 * States uses in reading process of UART paylaload interface.
 * Lets to map from ASCII to binary format.
 *
 */
typedef enum
{
  UART_PYLDI_STATE_NOT_INITIALIZATED,
  UART_PYLDI_STATE_IPMI_MSG_IDLE,
  UART_PYLDI_STATE_IPMI_MSG_RCV
} uart_pyldi_main_states_t;

/**
 * UART PAYLOAD INTERFACE READING SUB STATES
 *
 * States uses during the reading process of UART paylaload interface.
 * Lets to map from ASCII to binary format.
 *
 */
typedef enum
{
  READ_NIBBLE_MS,
  READ_NIBBLE_LS
} uart_pyldi_sub_states_t;

/**
 * UART PAYLOAD INTERFACE FULL STATES (sub and main states)
 *
 * Full sttes descriptions with sub and main states for reading
 * process from PAYLOAD INTERFACE.
 *
 */
typedef struct
{
  uart_pyldi_main_states_t main_state;
  uart_pyldi_sub_states_t  sub_state;
} uart_pyldi_states_t;

/**
 * BINARY PAYLOAD INTERFACE data stream
 *
 * Type to describe the incomming binary data stream
 * from PAYLOAD INTERFACE
 */
typedef struct
{
  int length;
  uint8_t data[MAX_BYTES_PYLD_INTERFACE_TERMINAL_MODE];
} data_pyldi;

/**
 * @brief Funtion to initialization UART TERMINAL MODE PAYLOAD INTERFACE
 *
 * @param void
 * @return void
 *
 * This function configures the UART interface and input stream buffer.
 *
 */
void pyldi_init( void );

/**
 * @brief Task to get a IPMI MESSAGE from UART PAYLOAD INTERFACE IN TERMINAL MODE
 *
 * @param void pointer to get FreeRTOS parameters.
 * @return void
 *
 * This task gets the ASCII stream from UART PAYLOAD INTERFACE, convert to binary stream and maps to information
 * to ipmb data. Finally, it sends to IPMI MSG Router to process the incomming data.
 */
void pyldi_receiver_task( void* );

/**
 * @brief Task to sent a IPMI MESSAGE to UART PAYLOAD INTERFACE IN TERMINAL MODE
 *
 * @param void pointer to get FreeRTOS parameters.
 * @return void
 *
 * This task gets from IPMI MSG ROUTER an message in ipmb data format, maps it into terminal mode data format and sents
 * thourght UART PAYLOAD INTERFACE
 *
 */
void pyldi_sender_task( void* );


#endif // UART_PAYLOAD_INTERFACE_H
