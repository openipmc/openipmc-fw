/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 *	@file	rmcp_impl.h
 *	@brief  RMCP+ Implementation header file
 *	@date	YYYY / MM / DD - 2024 / 03 / 05
 *	@author	Carlos Ruben Dell'Aquila
 *
 */
#ifndef RMCP_IMPL_H_
#define RMCP_IMPL_H_

/**
 * @name Enumeration of RMCP Implementation state.
 *
 * State code to get information about RMCP+ Implementation execution.
 */
typedef enum
{
  RMCP_IMPL_INIT_OK,
  RMCP_IMPL_INIT_NONE,
  RMCP_IMPL_NOT_INIT_ERROR_HEAP_MEMORY,
  RMCP_IMPL_NOT_INIT_ERROR_UDP_SERVER,
  OPENIPMC_RMCP_INIT_ERROR
} rmcp_impl_state_t;


/**
 * @brief Function to initialize the RMCP+ implementation.
 *
 * @return Returns the state of the execution.
 *
 * This function initializes the hardware and system resources which are used for RMCP+ software.
 */
extern rmcp_impl_state_t rmcp_impl_init(void);

#endif // RMCP_IMPL_H_
