/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/**
 * @file openipmc_interfaces_config.h
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Set IPMI interfaces available with name string and channel medium type number.
 *
 */

#ifndef OPENIPMC_CONFIG_H
#define OPENIPMC_CONFIG_H

#include "main.h"

/**
 * @name IPMI Channels enumeration
 * @{
 */
#define IPMI_CHANNELS_ENUMERATION  IPMI_ROUTER_CHANNEL_IPMB_0 = 0, /* IPMB-0 should to be #0, IPMI 2v0, 6.0 */  \
                                   IPMI_ROUTER_CHANNEL_RMCP   = 1,                                              \
                                   IPMI_ROUTER_CHANNEL_PYLDI  = 2,                                              \
                                   IPMI_ROUTER_CHANNEL_IPMB_L = 3                                               \
///@}

/**
 * @name IPMI Channels name
 * @{
 */
#define IPMI_STRING_NAMES [IPMI_ROUTER_CHANNEL_IPMB_0 ] = "IPMB0",  \
                          [IPMI_ROUTER_CHANNEL_RMCP   ] = "RMCP" ,  \
                          [IPMI_ROUTER_CHANNEL_PYLDI  ] = "PYLDI",  \
                          [IPMI_ROUTER_CHANNEL_IPMB_L ] = "IPMB-L"  \
///@}

/**
 * @name IPMI Channel Medium Type Numbers (Based on Table 6-3, Channel Medium Type Numbers, IPMI V1.5 )
 * @{
 */
#define IPMI_CHANNEL_MEDIUM_TYPE_NUMBERS [IPMI_ROUTER_CHANNEL_IPMB_0 ] = 0x01, /* IPMB (I2C)                    */ \
                                         [IPMI_ROUTER_CHANNEL_RMCP   ] = 0x04, /* 802.3 LAN                     */ \
                                         [IPMI_ROUTER_CHANNEL_PYLDI  ] = 0x05, /* Asynch. Serial/Modem (RS-232) */ \
                                         [IPMI_ROUTER_CHANNEL_IPMB_L ] = 0x01, /* IPMB (I2C)                    */ \
///@}

#endif // OPENIPMC_CONFIG_H
