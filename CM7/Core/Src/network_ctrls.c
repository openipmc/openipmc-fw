
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

#include <string.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "lwip.h"
#include "fru_payload_ipmc_control.h"
#include "custom_settings.h"
#include "network_ctrls.h"
#include "h7rng.h"
#include "clientid_cms.h"
#include "syscfg.h"
#include "mt_printf.h"
#include "syslog.h"

/* Network parameters for update */
#define NET_CTRL_DELAY_MS_TO_UPDATE 1000

/* MAC parameters */
#define NET_KEY_MAC_MODE               "net.macmode"

#define NET_VALUE_MAC_MODE_STATIC      "static"
#define NET_VALUE_MAC_MODE_RANDOM      "random_gen"
#define NET_VALUE_MAC_MODE_RANDOM_DONE "random_done"
#define NET_VALUE_MAC_MODE_CHIP_ID     "chipid"
#define MAX_STR_SIZE_KEY_MAC_MODE      12

#define MAX_STR_SIZE_MAC_FIELD       9
#define MAX_STR_SIZE_MAC_OCTECT      2
#define MAX_MAC_FIELD_NUM_OCTECTS    3

#define NET_KEY_MAC_PREFIX_ADDR      "net.macprefix"
#define MAX_STR_SIZE_NET_MAX_PREFIX  MAX_STR_SIZE_MAC_FIELD
#define MAC_PREFIX_NUM_OF_OCTECTS    MAX_MAC_FIELD_NUM_OCTECTS

#define NET_KEY_MAC_NIC_ADDR         "net.macnic"
#define MAX_STR_SIZE_NET_MAC_NIC     MAX_STR_SIZE_MAC_FIELD
#define MAC_NIC_NUM_OF_OCTECTS       MAX_MAC_FIELD_NUM_OCTECTS

#define MAC_NUM_OF_OCTECTS           (MAC_PREFIX_NUM_OF_OCTECTS+MAC_NIC_NUM_OF_OCTECTS)

#ifndef DEFAULT_MAC_PREFIX
#define DEFAULT_MAC_PREFIX {0x00,0x80,0xE1} // the default prefix used is from ST becauses should be easy
#endif                                      //  to detect from network packets if there is some issue.


/* IP parameters */
#define NET_KEY_IP_MODE              "net.ipmode"

#define NET_VALUE_IP_MODE_STATIC     "static"
#define NET_VALUE_IP_MODE_CLIENT_ID  "clientiddhcp"
#define NET_VALUE_IP_MODE_CLIENT_MAC "clientmacdhcp"
#define MAX_STR_SIZE_VALUE_IP_MODE   15

#define NET_KEY_IP_ADDR              "net.stat_ip"
#define NET_KEY_IP_MASK              "net.stat_mask"
#define NET_KEY_IP_GATW              "net.stat_gatw"
#define MAX_STR_SIZE_IP_ADDR         16
#define MAX_STR_SIZE_IP_OCTECT       3
#define MAX_OCTECTS_IP_ADDR          4

#ifndef DEFUALT_FW_IP_ADDR
#define DEFUALT_FW_IP_ADDR "192.168.200.238"
#endif

#ifndef DEFAULT_FW_MASK
#define DEFAULT_FW_MASK    "255.255.255.0"
#endif

#ifndef DEFAULT_FW_GATEWAY
#define DEFAULT_FW_GATEWAY "192.168.200.1"
#endif



typedef enum
{
  IP_MODE_STATIC,
  IP_MODE_DHCP_MAC,
  IP_MODE_DHCP_CLIENT_ID,
  IP_MODE_DONE
} ip_mode_t;

typedef enum
{
  STATIC_NET_PARAM_LOAD_STR_FROM_KEY,
  STATIC_NET_PARAM_LOAD_DEFAULT_FROM_FW,
  STATIC_NET_PARAM_LOAD_IP,
  STATIC_NET_PARAM_DONE
} state_load_param_static_t;

// Constant string - MAC
const char net_key_mac_mode[]               = NET_KEY_MAC_MODE;
const char net_value_mac_mode_static[]      = NET_VALUE_MAC_MODE_STATIC;
const char net_value_mac_mode_random[]      = NET_VALUE_MAC_MODE_RANDOM;
const char net_value_mac_mode_random_done[] = NET_VALUE_MAC_MODE_RANDOM_DONE;
const char net_value_mac_mode_chip_id[]     = NET_VALUE_MAC_MODE_CHIP_ID;

// Default firmware MAC prefix. The macro has values of ST-Microelectronics macro.
const uint8_t prefix_default[] = DEFAULT_MAC_PREFIX;

// These variable is globally defined in lwip.c file.
extern struct netif gnetif;

// Control if OpenIPMC-FW is using or not DHCP
_Bool openipmc_hw_uses_dhcp = DHCP_STARTUP_STATE;

// ClientID data struct
__attribute__((section(".client_id_section")))
static struct dhcp_client_id clientid;


/*
 * Function to change the IP address "on the fly" (after interface is already initialized
 * with some default IP address)
 */
void eth_ctrls_change_ip_addr( uint8_t ip0,   uint8_t ip1,   uint8_t ip2,   uint8_t ip3,
                               uint8_t mask0, uint8_t mask1, uint8_t mask2, uint8_t mask3,
                               uint8_t gw0,   uint8_t gw1,   uint8_t gw2,   uint8_t gw3    )
{
  ip4_addr_t ipaddr;
  ip4_addr_t netmask;
  ip4_addr_t gw;

  IP4_ADDR( &ipaddr,  ip0,  ip1,    ip2,   ip3   );
  IP4_ADDR( &netmask, mask0, mask1, mask2, mask3 );
  IP4_ADDR( &gw,      gw0,   gw1,   gw2,   gw3   );

  netif_set_addr( &gnetif, &ipaddr, &netmask, &gw);
}


/* Function to send-back the actual network address */
void eth_ctrls_get_net_addr(uint8_t* ip_addr, uint8_t* mask, uint8_t *gtw_addr)
{

  if( ip_addr != NULL)
  {
    ip_addr[0] = ( gnetif.ip_addr.addr & 0xFF000000 ) >> 24;
    ip_addr[1] = ( gnetif.ip_addr.addr & 0x00FF0000 ) >> 16;
    ip_addr[2] = ( gnetif.ip_addr.addr & 0x0000FF00 ) >> 8;
    ip_addr[3] = ( gnetif.ip_addr.addr & 0x000000FF );
  }

  if( mask != NULL)
  {
    mask[0] = ( gnetif.netmask.addr & 0xFF000000 ) >> 24;
    mask[1] = ( gnetif.netmask.addr & 0x00FF0000 ) >> 16;
    mask[2] = ( gnetif.netmask.addr & 0x0000FF00 ) >> 8;
    mask[3] = ( gnetif.netmask.addr & 0x000000FF );
  }

  if(gtw_addr != NULL)
  {
    gtw_addr[0] = ( gnetif.gw.addr & 0xFF000000 ) >> 24;
    gtw_addr[1] = ( gnetif.gw.addr & 0x00FF0000 ) >> 16;
    gtw_addr[2] = ( gnetif.gw.addr & 0x0000FF00 ) >> 8;
    gtw_addr[3] = ( gnetif.gw.addr & 0x000000FF );
  }
}


/*
 * Enable DHCP using clientID
 */
void eth_ctrls_dhcp_enable( void )
{
  client_id_from_atca_cms(&clientid);
  dhcp_init(&gnetif, NULL);
  dhcp_set_client_id(&gnetif, &clientid);
  dhcp_lazy_discover(&gnetif);
}

/*
 * Enable DHCP using mac address
 */
void eth_ctrls_dhcp_enable_by_mac( void );

/*
 * Disable DHCP service
 */

void eth_ctrls_dhcp_disable( void )
{
  dhcp_release_and_stop(&gnetif);
  dhcp_clear_client_id(&gnetif);
}

/*
 * Parse IP address
 *
 * Receives a string containing an IP address
 * addr_num is filled with the 4 bytes of the address, msb first.
 *
 * This function searches integer numbers separated by '.'
 *
 * eg: xxx.xxx.xxx.xxx, where xxx is an integer from 0 to 255
 *
 * Return 0 if successfully parsed.
 */
int eth_ctrls_parse_ip_addr( const char* addr_str, uint8_t addr_num[] )
{
  char const* current_pos = addr_str;

  for(int byte=0; byte<4; byte++)
  {
    addr_num[byte] = (uint8_t)( atoi( current_pos ) & 0xFF );

    if(byte < 3)
    {
      char* next_pos = strchr(current_pos, '.');
      if (next_pos == NULL)
        return 1;
      else
        current_pos = next_pos+1;
    }
  }

  return 0; // no error;
}

/**
 *
 * @name  eth_ctrls_parse_ip_addr_and_port
 * @brief Parses numeric value from a string containing the IPv4 address and port.
 * @param const char* str	String containing ip address and port in the following format:
 * 							e.g. "123.456.789.0:555"
 * @param uint8_t ip		Buffer to place parsed IPv4 address.
 * @param uint8_t port		Buffer to place parsed port number.
 * @return 0 on successful operations and -1 on failure.
 *
 */
int eth_ctrls_parse_ip_addr_and_port(const char* str, uint8_t* ip, uint16_t* port)
{
  char const* str_ptr = str;
  char* next = NULL;

  for(int oct=0; oct<4; oct++)
  {
    ip[oct] = (uint8_t)( atoi(str_ptr) & 0xFF );

    if(oct < 3)
    {
      next = strchr(str_ptr, '.');
      if( NULL == next )
      {
        return -1;
      }
      else
      {
        str_ptr = next + 1;
      }
    }
  }
  next = strchr(str_ptr, ':');
  if( NULL == next )
  {
    return - 1;
  }
  else
  {
    str_ptr = next + 1;
    *( port ) = (uint16_t)( atoi(str_ptr) & 0xFFFF );
  }
  return 0;
}


static int hex_to_byte(char* str_hex, uint8_t* value)
{
  char *rest_of_string;

  if( 2 != strlen(str_hex))
    return 0;

  *value = (uint8_t) strtol(str_hex,&rest_of_string,16);

  return 1;
}


/**
 * Save static network parameter in external memory from bytes values.
 *
 */
static int net_save_static_param_from_str(char* key, char* str_value)
{
  syscfg_err_t ret;

  ret = syscfg_insert(key,str_value);

  if((SYSCFG_SUCCESS != ret) && (SYSCFG_EREPLD != ret))
    return 0;

  ret = syscfg_save();

  if(SYSCFG_SUCCESS != ret)
    return 0;

  return 1;
}


static int net_bin_param_save(char *key,char *str_value, char *str_field,char *str_token,uint8_t *value, int size)
{
  uint8_t ms_nibble;
  uint8_t ls_nibble;

  int i;

  for(i=0;i<size;i++)
  {
    ms_nibble = (value[i]>> 4) & 0x0F;
    ls_nibble =  value[i] & 0x0F;

    itoa(ms_nibble,str_field,16); // converts integer to hexadecimal.
    strcat(str_value,str_field);

    itoa(ls_nibble,str_field,16); // converts integer to hexadecimal.
    strcat(str_value,str_field);

    if((size-1) > i )
    {
      strcat(str_value,str_token);
    }
  }

  if(!net_save_static_param_from_str(key,str_value))
    return 0;

  return 1;
}


static int net_bin_param_save_as_dec(char *key,char *str_value, char *str_field,char *str_token,uint8_t *value, int size)
{
  int i;

  for(i=0;i<size;i++)
  {
    itoa(value[i],str_field,10); // converts integer to decimal
    strcat(str_value,str_field);

    if((size-1)>i)
    {
      strcat(str_value,str_token);
    }
  }

  if(!net_save_static_param_from_str(key,str_value))
    return 0;

  return 1;
}


static int mac_field_save(char *key,uint8_t *value, int size)
{
  char str_value[MAX_STR_SIZE_MAC_FIELD]  = "";
  char str_field[MAX_STR_SIZE_MAC_OCTECT] = "";
  char str_token[] = ":";

  if(!net_bin_param_save(key,str_value,str_field,str_token,value,size))
    return 0;

  return 1;
}


static int load_mac_by_chipid(uint8_t *mac_addr)
{
  const uint32_t id = HAL_GetUIDw0() + HAL_GetUIDw1() + HAL_GetUIDw2();

  mac_addr[0] = prefix_default[0];
  mac_addr[1] = prefix_default[1];
  mac_addr[2] = prefix_default[2];
  mac_addr[3] = (id >> 16) & 0xFF;
  mac_addr[4] = (id >> 8 ) & 0xFF;
  mac_addr[5] = (id >> 0 ) & 0xFF;

  return 1;
}

static int save_mac(char *mac_mode, uint8_t *mac_addr, int mac_addr_size)
{
  if ( 6 != mac_addr_size)
    return 0;

  // Update net.macmode key
  const syscfg_err_t ret_syscfg = syscfg_insert(NET_KEY_MAC_MODE,mac_mode);

  if((SYSCFG_SUCCESS != ret_syscfg) && (SYSCFG_EREPLD != ret_syscfg))
    return 0;

  if(SYSCFG_SUCCESS != syscfg_save())
    return 0;

  if(!mac_field_save(NET_KEY_MAC_PREFIX_ADDR,mac_addr,MAC_PREFIX_NUM_OF_OCTECTS))
    return 0;

  if(!mac_field_save(NET_KEY_MAC_NIC_ADDR,&mac_addr[3],MAC_NIC_NUM_OF_OCTECTS))
    return 0;

  return 1;
}


static int load_mac_by_random(uint8_t *mac_addr)
{
  const uint32_t timeout = 5UL;
  const uint32_t bufSize = 16UL;
  uint8_t rng_buf[16] = {0};

  int attempts = 2;
  int ret = 0;

  while(attempts)
  {
    if(H7RNG_ERR_OK == h7rng_blockingFetchRandomNumber((void*)rng_buf,bufSize,timeout))
    {
      mac_addr[0] = rng_buf[0];
      mac_addr[1] = rng_buf[1];
      mac_addr[2] = rng_buf[2];
      mac_addr[3] = rng_buf[3];
      mac_addr[4] = rng_buf[4];
      mac_addr[5] = rng_buf[5];

      ret = 1;
      attempts = 0;

      syscfg_insert(NET_KEY_MAC_MODE,NET_VALUE_MAC_MODE_RANDOM_DONE);

      if(!mac_field_save(NET_KEY_MAC_PREFIX_ADDR,mac_addr,MAC_PREFIX_NUM_OF_OCTECTS))
        return 0;

      if(!mac_field_save(NET_KEY_MAC_NIC_ADDR,&mac_addr[3],MAC_NIC_NUM_OF_OCTECTS))
        return 0;
    }
    else
    {
      attempts--;
    }
  }
  return ret;
}

static int load_three_octets(char *three_octets_str, uint8_t *three_octets)
{
  uint8_t value   = 0;
  const char s[2] = ":";
  char *token;
  int cont_octets = 0;

  /* get the first token */
  token = strsep(&three_octets_str,s);

  while(NULL != token)
  {
    if((hex_to_byte(token,&value)) && (cont_octets < 3))
    {
      three_octets[cont_octets++] = value;
      token = strsep(&three_octets_str,s);
    }
    else
    { // the prefix argument has no an standard format.
      return 0;
    }
  }
  return 1;
}

static int load_mac_static(uint8_t *mac_addr)
{
  char nic_str[MAX_STR_SIZE_NET_MAC_NIC];
  char prefix_str[MAX_STR_SIZE_NET_MAX_PREFIX];

  uint8_t prefix_octets[3];
  uint8_t nic_octets[3];

  if ( SYSCFG_SUCCESS != syscfg_get_value_from_key(NET_KEY_MAC_PREFIX_ADDR,prefix_str,MAX_STR_SIZE_NET_MAX_PREFIX))
    return 0;

  if ( SYSCFG_SUCCESS != syscfg_get_value_from_key(NET_KEY_MAC_NIC_ADDR,nic_str,MAX_STR_SIZE_NET_MAC_NIC))
    return 0;


  if(!load_three_octets(prefix_str,prefix_octets))
    return 0;

  if(!load_three_octets(nic_str,nic_octets))
    return 0;

  mac_addr[0] = prefix_octets[0];
  mac_addr[1] = prefix_octets[1];
  mac_addr[2] = prefix_octets[2];
  mac_addr[3] = nic_octets[0];
  mac_addr[4] = nic_octets[1];
  mac_addr[5] = nic_octets[2];

  return 1;
}

/*
 * Compare if MAC address in external flash memory is the same that chipID.
 *
 * Returns: '0' if they are equal.
 *          '1' if they are different.
 */
static int compare_chipid_with_static(uint8_t *mac_addr)
{
  uint8_t mac_addr_static[6];
  int i=0;

  // There is not mac address fields in the external memory.
  if(!load_mac_static(mac_addr_static))
    return 1;

  for(i=0;i<6;i++)
  {
    if(mac_addr_static[i] - mac_addr[i])
      return 1;
  }

  return 0;
}



/**
 * Loads the MAC address on the devices.
 *
 * Reads the MAC address from the file system a send-back the corresponding value.
 *
 * If there is not any value on the file system, it loads the chipID as MAC.
 *
 */
int load_mac_addr(uint8_t *mac_addr)
{
  // Network parameters strings
  char mac_mode_str[MAX_STR_SIZE_KEY_MAC_MODE]="";

  if( SYSCFG_SUCCESS == syscfg_get_value_from_key(NET_KEY_MAC_MODE,mac_mode_str, MAX_STR_SIZE_KEY_MAC_MODE))
  {
    if(!strcmp(NET_VALUE_MAC_MODE_STATIC,mac_mode_str))
    {
      // Static mode
      if(load_mac_static(mac_addr))
      {
        // MAC address has been loaded from STATIC Mode.
        return 1;
      }
    }

    if(!strcmp(NET_VALUE_MAC_MODE_RANDOM,mac_mode_str))
    {
      if(load_mac_by_random(mac_addr))
      {
        // MAC address has been loaded from RANDOM number generator.
        return 1;
      }
    }

    if(!strcmp(NET_VALUE_MAC_MODE_RANDOM_DONE,mac_mode_str))
    {
      // Static mode
      if(load_mac_static(mac_addr))
      {
        // MAC address has been loaded from STATIC Mode.
        return 1;
      }
    }
  }

  // MAC address will be load from ChipID because it is the default option.
  load_mac_by_chipid(mac_addr);

  // Compare if MAC address in external flash memory is the same that chipID. a = '0' if they are equal.
  const int a = compare_chipid_with_static(mac_addr);

  // Gets if net.macmode is chipID. b = '0' if it is.
  const int b = strcmp(NET_VALUE_MAC_MODE_CHIP_ID,mac_mode_str);

  if( b || a)
  {
    // Update net.macmode
    save_mac(NET_VALUE_MAC_MODE_CHIP_ID,mac_addr,6);
  }

  return 1;
}


static int net_notify_mac_addr_change()
{
  // The MAC is leaded during LwIP initialization so the best way to load a new value is do "IPMC Cold Reset'

  mt_printf("\n\rMAC address has been changed from \'Set LAN Conf. IPMI cmd.\' IPMI Cold Reset required to complete");
  syslog_push("debug","MAC address has changed from \'Set LAN Conf. IPMI cmd.\' IPMI Cold Reset required to complete",SYSFAC_NETWORK_NEWS,SYSSEV_INFO);

  return 1;
}


static int  load_ip_from_str(char *ip_addr_str, uint8_t *address_value, int size_req)
{
  int cont_bytes = 0;
  const char s[2] = ".";
  char *token;

  /* get the first token */
  token = strsep(&ip_addr_str,s);

  while(NULL != token)
  {
    if( size_req > cont_bytes )
    {
      address_value[cont_bytes++] = atoi(token);
      token = strsep(&ip_addr_str,s);
    }
    else
    {
      return 0;
    }
  }

  if (size_req != cont_bytes)
    return 0;

  return 1;
}


/*
 * Load IP address from key
 */
static int load_ip_from_key(char *addr_key, uint8_t *address_value, int size_req)
{
  char ip_addr_str[MAX_STR_SIZE_IP_ADDR];
  syscfg_err_t ret;

  ret = syscfg_get_value_from_key(addr_key,ip_addr_str,MAX_STR_SIZE_IP_ADDR);

  if(SYSCFG_SUCCESS != ret)
    return 0;

  return load_ip_from_str(ip_addr_str,address_value,size_req);
}



/*
 * Load static IP from external memory
 *
 * Note: if it is not available, default custom setting value will be loaded.
 *       Likewise, if custom firmware does´t define one address this function provides one.
 *
 */
int load_ip_static()
{
  uint8_t ip_addr[MAX_OCTECTS_IP_ADDR];
  uint8_t ip_mask[MAX_OCTECTS_IP_ADDR];
  uint8_t ip_gatw[MAX_OCTECTS_IP_ADDR];

  state_load_param_static_t state = STATIC_NET_PARAM_LOAD_STR_FROM_KEY;

  while(STATIC_NET_PARAM_DONE != state)
  {
    switch(state)
    {
      case STATIC_NET_PARAM_LOAD_STR_FROM_KEY:

        state = STATIC_NET_PARAM_LOAD_IP;

        if(!load_ip_from_key(NET_KEY_IP_ADDR,ip_addr,MAX_OCTECTS_IP_ADDR))
          state = STATIC_NET_PARAM_LOAD_DEFAULT_FROM_FW;

        if(!load_ip_from_key(NET_KEY_IP_MASK,ip_mask,MAX_OCTECTS_IP_ADDR))
          state = STATIC_NET_PARAM_LOAD_DEFAULT_FROM_FW;

        if(!load_ip_from_key(NET_KEY_IP_GATW,ip_gatw,MAX_OCTECTS_IP_ADDR))
          state = STATIC_NET_PARAM_LOAD_DEFAULT_FROM_FW;

        break;

      case STATIC_NET_PARAM_LOAD_DEFAULT_FROM_FW:

        state = STATIC_NET_PARAM_LOAD_STR_FROM_KEY;

        if(!net_save_static_param_from_str(NET_KEY_IP_ADDR,DEFUALT_FW_IP_ADDR))
          return 0;

        if(!net_save_static_param_from_str(NET_KEY_IP_MASK,DEFAULT_FW_MASK))
          return 0;

        if(!net_save_static_param_from_str(NET_KEY_IP_GATW,DEFAULT_FW_GATEWAY))
          return 0;

        break;

      case STATIC_NET_PARAM_LOAD_IP:

        state = STATIC_NET_PARAM_DONE;
        eth_ctrls_change_ip_addr(ip_addr[0],ip_addr[1],ip_addr[2],ip_addr[3],
                                 ip_mask[0],ip_mask[1],ip_mask[2],ip_mask[3],
                                 ip_gatw[0],ip_gatw[1],ip_gatw[2],ip_gatw[3]);
        break;

      default:
        return 0;
        break;
    }
  }
  return 1;
}


/**
 * Load the IP address configuration according to the NET_KEY_IP_MODE configuration.
 *
 * Note: It uses a default configuration if that key is not present. The chosen default is DHCP Client ID.
 *
 */
int load_ip_addr()
{
  char ip_mode_str[MAX_STR_SIZE_VALUE_IP_MODE];
  ip_mode_t ip_mode = IP_MODE_DHCP_CLIENT_ID;

  if( SYSCFG_SUCCESS == syscfg_get_value_from_key(NET_KEY_IP_MODE, ip_mode_str,MAX_STR_SIZE_VALUE_IP_MODE))
  {
    if( !strcmp(NET_VALUE_IP_MODE_STATIC,ip_mode_str))
      ip_mode = IP_MODE_STATIC;

    if( !strcmp(NET_VALUE_IP_MODE_CLIENT_MAC,ip_mode_str))
      ip_mode = IP_MODE_DHCP_MAC;

    if( !strcmp(NET_VALUE_IP_MODE_CLIENT_ID,ip_mode_str))
      ip_mode = IP_MODE_DHCP_CLIENT_ID;
  }
  else
  {
    if(0 == DHCP_STARTUP_STATE)
    {
      ip_mode = IP_MODE_STATIC;
    }
  }

  switch(ip_mode)
  {
    case IP_MODE_STATIC:
      eth_ctrls_dhcp_disable();
      return load_ip_static();

    case IP_MODE_DHCP_CLIENT_ID:
      eth_ctrls_dhcp_enable();
      return 1;

    case IP_MODE_DHCP_MAC:
      eth_ctrls_dhcp_enable_by_mac();
      return 1;

    default:
      return 0;
  }
}


/**
 * Save static network parameters in external memory from bytes values.
 *
 */
static int net_save_static_param(char *key, uint8_t *value, int size)
{
  char str_value[MAX_STR_SIZE_IP_ADDR]     = "";
  char str_field[MAX_STR_SIZE_IP_OCTECT]  = "";
  char str_token[] = ".";

  if(!net_bin_param_save_as_dec(key,str_value,str_field,str_token,value,size))
    return 0;

  if(!net_get_is_ip_mode_dhcp())
  {
    // IP source is static so the net param should be changed.
    return load_ip_addr();
  }

  // IP source is dhcp so the network interface configuration should not be updated.
  return 1;
}


static int set_ip_mode(char *key, char *value)
{
  const syscfg_err_t ret = syscfg_insert(key,value);

  if((SYSCFG_SUCCESS != ret) && (SYSCFG_EREPLD != ret))
    return -1;

  if(SYSCFG_SUCCESS != syscfg_save())
    return -1;

  return load_ip_addr();
}


/******************************************/
/*   Functions to set MAC configurations  */
/******************************************/

/**
 * Set MAC addr from byte array
 *
 */
int net_set_mac_addr(uint8_t *value, int size)
{
  // char mode[MAX_STR_SIZE_KEY_MAC_MODE];

  // if there is a previous configuration the new value cannot be set.
  // if(SYSCFG_SUCCESS == syscfg_get_value_from_key(NET_KEY_MAC_MODE,mode,MAX_STR_SIZE_KEY_MAC_MODE))
  //   return 0;

  syscfg_insert(NET_KEY_MAC_MODE,NET_VALUE_MAC_MODE_STATIC);

  if(SYSCFG_SUCCESS != syscfg_save())
    return 0;

  if(MAC_NUM_OF_OCTECTS != size)
    return 0;

  if(!mac_field_save(NET_KEY_MAC_PREFIX_ADDR,value,MAC_PREFIX_NUM_OF_OCTECTS))
    return 0;

  if(!mac_field_save(NET_KEY_MAC_NIC_ADDR,&value[3],MAC_NIC_NUM_OF_OCTECTS))
    return 0;

  net_notify_mac_addr_change();

  return 1;
}


/**
 * Set MAC addr static from string
 *
 */
int net_set_mac_static_from_str(char *mac_str)
{
  char str_mac_field[MAX_STR_SIZE_MAC_FIELD];
  str_mac_field[MAX_STR_SIZE_MAC_FIELD-1] = '\0';
  syscfg_err_t ret_syscfg;

  // Set MAC mode as static.
  ret_syscfg = syscfg_insert(NET_KEY_MAC_MODE,NET_VALUE_MAC_MODE_STATIC);

  if((SYSCFG_SUCCESS != ret_syscfg) && (SYSCFG_EREPLD != ret_syscfg))
    return 0;

  // Set MAC prefix field
  strncpy(str_mac_field,mac_str,MAX_STR_SIZE_MAC_FIELD-1);

  ret_syscfg = syscfg_insert(NET_KEY_MAC_PREFIX_ADDR,str_mac_field);

  if((SYSCFG_SUCCESS != ret_syscfg) && (SYSCFG_EREPLD != ret_syscfg))
    return 0;

  // Set MAC nic field
  strncpy(str_mac_field,&mac_str[MAX_STR_SIZE_MAC_FIELD],MAX_STR_SIZE_MAC_FIELD-1);
  ret_syscfg = syscfg_insert(NET_KEY_MAC_NIC_ADDR,str_mac_field);

  if((SYSCFG_SUCCESS != ret_syscfg) && (SYSCFG_EREPLD != ret_syscfg))
    return 0;

  // Save all key values.
  ret_syscfg = syscfg_save();

  if(SYSCFG_SUCCESS != syscfg_save())
    return 0;

  net_notify_mac_addr_change();

  return 1;
}


/**
 * Set MAC addr chipid
 *
 */
int net_set_mac_chipid()
{
  uint8_t mac_addr[6];

  load_mac_by_chipid(mac_addr);

  // Update net.macmode
  if(!save_mac(NET_VALUE_MAC_MODE_CHIP_ID,mac_addr,6))
    return 0;

  net_notify_mac_addr_change();

  return 1;
}


/**
 * Set MAC addr random
 *
 */
int net_set_mac_random()
{
  syscfg_err_t ret_syscfg;

  // Set MAC mode as random.
  ret_syscfg = syscfg_insert(NET_KEY_MAC_MODE,NET_VALUE_MAC_MODE_RANDOM);

  if((SYSCFG_SUCCESS != ret_syscfg) && (SYSCFG_EREPLD != ret_syscfg))
    return 0;

  if(SYSCFG_SUCCESS != syscfg_save())
    return 0;

  net_notify_mac_addr_change();

  return 1;
}


/*
 * This function is called by the "low_level_init()" function in ethernetif.c file.
 *
 * It is responsible for defining the MAC Address before initialize the interface.
 *
 * it overrides the MAC Address defined in the IOC file via STM32CubeIDE graphical interface.
 */
void load_user_defined_mac_addr(uint8_t *mac_addr)
{
  load_mac_addr(mac_addr);
}


/*
 * Enable DHCP using MAC addr
 */
void eth_ctrls_dhcp_enable_by_mac( void )
{
  struct dhcp_client_id client_mac_id;

  load_mac_addr((uint8_t*)client_mac_id.id);
  client_mac_id.length = 6;

  dhcp_set_client_id(&gnetif, &client_mac_id);
  dhcp_lazy_discover(&gnetif);
}


/******************************************/
/*   Functions to set IP configurations   */
/******************************************/

/**
 * Get the IP mode source dhcp
 */
int net_get_is_ip_mode_dhcp()
{
  char ip_mode_str[MAX_STR_SIZE_VALUE_IP_MODE];

  // Checks if ipmode key is on file system
  if( SYSCFG_SUCCESS == syscfg_get_value_from_key(NET_KEY_IP_MODE, ip_mode_str,MAX_STR_SIZE_VALUE_IP_MODE))
  {
    if( !strcmp(NET_VALUE_IP_MODE_STATIC,ip_mode_str))
      return 0;

    if( !strcmp(NET_VALUE_IP_MODE_CLIENT_MAC,ip_mode_str))
      return 1;

    if( !strcmp(NET_VALUE_IP_MODE_CLIENT_ID,ip_mode_str))
      return 1;
  }

  // if ipmode key is not present, it ask for the firmware default setting.
  if(0 == DHCP_STARTUP_STATE)
  {
    return 0;
  }

  return 1;
}


/**
 * Set IP mode source static
 *
 */
int net_set_ip_mode_static()
{
  return set_ip_mode(NET_KEY_IP_MODE,NET_VALUE_IP_MODE_STATIC);
}


/**
 * Set IP mode source DHCP
 *
 */
int net_set_ip_mode_dhcp()
{
  return set_ip_mode(NET_KEY_IP_MODE,NET_VALUE_IP_MODE_CLIENT_ID);
}


/**
 * Set static IP address
 *
 */
int net_set_static_ip(uint8_t *value, int size)
{
  return net_save_static_param(NET_KEY_IP_ADDR,value,size);
}


/**
 * Set static IP Address Mask
 *
 */
int net_set_static_mask(uint8_t *value, int size)
{
  return net_save_static_param(NET_KEY_IP_MASK,value,size);
}


/**
 * Set static Gateway Address
 *
 */
int net_set_static_gateway_addr(uint8_t *value, int size)
{
  return net_save_static_param(NET_KEY_IP_GATW,value,size);
}

