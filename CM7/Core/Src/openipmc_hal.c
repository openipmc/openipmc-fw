
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 * This file contains all specific implementation of the OpenIPMC's Hardware
 * Abstraction Layer functions. These functions are all declared in ipmc_ios.h.
 */

// FreeRTOS includes
//#include <stdio.h>
//#include <stdlib.h>
#include <mt_printf.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// STM32H7xx HAL includes
#include "main.h"
#include "cmsis_os.h"

// OpenIPMC includes
#include "ipmc_ios.h"

/* LWIP resources */
#include "lwip.h"
#include "lwip/udp.h"
#include "network_ctrls.h"

#include "dimm_gpios.h"
#include "telnet_server.h"


#define I2C_MODE_MASTER 1
#define I2C_MODE_SLAVE  0
#define IPMB_BUFF_SIZE  32

static _Bool ipmc_ios_ready_flag = pdFALSE;

// I2C state control variables in OpenIPMC HAL
static uint8_t i2c_ipmba_current_state;
static uint8_t i2c_ipmbb_current_state;

// Length of the the received message
static uint32_t i2c_ipmba_recv_len = 0;
static uint32_t i2c_ipmbb_recv_len = 0;

// IPMB Hardware Address
static uint8_t  ipmb_addr;

//Buffers for IPMB receiving
uint8_t ipmba_input_buffer[IPMB_BUFF_SIZE] = {0};
uint8_t ipmbb_input_buffer[IPMB_BUFF_SIZE] = {0};

// Semaphores to synchronize the IPMB operations with the I2C transmission
static SemaphoreHandle_t ipmb_rec_semphr = NULL;
static SemaphoreHandle_t ipmba_send_semphr = NULL;
static SemaphoreHandle_t ipmbb_send_semphr = NULL;

// Mutex to protect GPIO readding.
static SemaphoreHandle_t haddress_pins_mutex = NULL;

// Re-map I2C peripherals handlers for IPMB channels
extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
#define hi2c_ipmba hi2c1
#define hi2c_ipmbb hi2c2

// Flag used to enable the IPMI messaging printout
int enable_ipmi_printouts = 0;

// IP Network data variable
extern struct netif gnetif;
extern _Bool openipmc_hw_uses_dhcp;


void hpm1_init(void);



/*
 * Initializations related to all peripherals involved in the OpenIPMC's
 * Hardware Abstraction Layer.
 *
 * Many initializations are already carried by automatically generated code,
 * under configuration made in .ioc file. This function address additional
 * initializations needed to connect OpenIPMC to peripherals.
 */
int openipmc_hal_init(void)
{

	// Create the semaphores and mutexes
	ipmb_rec_semphr = xSemaphoreCreateBinary();
	ipmba_send_semphr = xSemaphoreCreateBinary();
	ipmbb_send_semphr = xSemaphoreCreateBinary();
	haddress_pins_mutex = xSemaphoreCreateMutex();

	// Enable the IPMB channels
	GPIO_SET_STATE(SET, IPMB_A_EN);
	GPIO_SET_STATE(SET, IPMB_B_EN);
	vTaskDelay( 100/portTICK_PERIOD_MS );
	if(IPMB_A_RDY_GET_STATE() && IPMB_B_RDY_GET_STATE())
		ipmc_ios_printf("IPMB Decouplers Ready\n\r");
	else
		ipmc_ios_printf("IPMB Decouplers NOT Ready\n\r");


	// Start Receiving on IPMB
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmba, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmbb, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);
	i2c_ipmba_current_state = I2C_MODE_SLAVE;
	i2c_ipmbb_current_state = I2C_MODE_SLAVE;

	// Initializes the HPM1 Upgrade functionality
	hpm1_init();

	// Now peripherals are ready and can be used bu OpenIPMC
	ipmc_ios_ready_flag = pdTRUE;

	return 0;
}



/*
 * Check if the IOs are initialized.
 *
 * Returns TRUE if ipmc_ios_init() ran all the initializations successfully.
 */
_Bool ipmc_ios_ready(void)
{
	return ipmc_ios_ready_flag;
}



/*
 * Read the state of the ATCA handle
 *
 * Mechanically, when the handle is CLOSED, the pin is grounded, giving a LOW.
 * When the handle is OPEN, the micro-switch is also open, and a pull-up
 * resistor imposes a HIGH.
 *
 * return value:
 *   1: handle is OPEN
 *   0: handle is CLOSED
 */
int ipmc_ios_read_handle(void)
{
	if( HANDLE_GET_STATE() == GPIO_PIN_RESET ) // LOW = LOCKED/CLOSED
		return 0;
	else                                       // HIGH = UNLOCKED/OPEN
		return 1;
}

uint8_t get_haddress_pins(void)
{
	uint8_t  HA_bits = 0;
	
	while (haddress_pins_mutex == NULL)  { asm("nop"); }
	xSemaphoreTake( haddress_pins_mutex, portMAX_DELAY );
	
	// Get HA0 to HA7
	if( HW_0_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x01;
	if( HW_1_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x02;
	if( HW_2_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x04;
	if( HW_3_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x08;
	if( HW_4_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x10;
	if( HW_5_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x20;
	if( HW_6_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x40;
	if( HW_7_GET_STATE() == GPIO_PIN_SET )
		HA_bits |= 0x80;
	
	xSemaphoreGive( haddress_pins_mutex );
	
	return HA_bits;
}

/*
 * The Hardware Address of the IPMC (slave addr) is read from the ATCA backplane pins
 * A party check is considered for the address (parity must be ood)
 */
uint8_t ipmc_ios_read_haddress(void)
{
	int i;
	uint8_t  HA_pins;
	uint8_t  HA_bit[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	uint8_t  HA_num;
	int parity_odd;
	
	HA_pins = get_haddress_pins();

	// Get HA0 to HA7
	if( HA_pins & 0x01 )
		HA_bit[0] = 1;
	if( HA_pins & 0x02 )
		HA_bit[1] = 1;
	if( HA_pins & 0x04 )
		HA_bit[2] = 1;
	if( HA_pins & 0x08 )
		HA_bit[3] = 1;
	if( HA_pins & 0x10 )
		HA_bit[4] = 1;
	if( HA_pins & 0x20 )
		HA_bit[5] = 1;
	if( HA_pins & 0x40 )
		HA_bit[6] = 1;
	if( HA_pins & 0x80 )
		HA_bit[7] = 1;


	/* Calculate parity */
	parity_odd = 0; // initialize as EVEN
	for(i=0; i<8; i++)
		if(HA_bit[i] == 1)
			parity_odd = ~parity_odd; // flip parity

	/* Result */
	HA_num = 0;
	if( parity_odd )
	{
		for(i=0; i<=6; i++)
			HA_num |= (HA_bit[i]<<i);

		return HA_num; // 7bit addr
	}
	else
		return HA_PARITY_FAIL; //parity fail (must be ODD)
}



/*
 * Set the IPMB address.
 */
void ipmc_ios_ipmb_set_addr(uint8_t addr)
{
	// Convert into 7-bit format to be used by the I2C driver
	ipmb_addr = addr >> 1;

	// Set I2C address
	HAL_I2C_DeInit(&hi2c_ipmba);
	hi2c_ipmba.Init.OwnAddress1 = (0x300 | ipmb_addr << 1);
	HAL_I2C_Init(&hi2c_ipmba);

	HAL_I2C_DeInit(&hi2c_ipmbb);
	hi2c_ipmbb.Init.OwnAddress1 = (0x300 | ipmb_addr << 1);
	HAL_I2C_Init(&hi2c_ipmbb);

	// Start Receiving on IPMB
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmba, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmbb, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);
	i2c_ipmba_current_state = I2C_MODE_SLAVE;
	i2c_ipmbb_current_state = I2C_MODE_SLAVE;
}



/*
 * Master transmit function for IPMB-A
 * IPMB-A uses STM32H7xx I2C1 - set on pins Pxx as SCL and Pxx as SDA
 */
int ipmc_ios_ipmba_send(uint8_t *MsgPtr, int ByteCount)
{
	_Bool semphr_timeout;
	uint16_t dest_addr;
	HAL_StatusTypeDef tx_ret_val;

	dest_addr = (uint16_t)MsgPtr[0]; // Address already shifted

	// Wait until bus is free
	for(int i=0; i<60; i++)
	{
		if( !(hi2c_ipmba.Instance->ISR & I2C_ISR_BUSY_Msk) ) // If not busy
			break;

		if(i == 60-1)
			return IPMB_SEND_FAIL; // Timeout

		vTaskDelay(1);
	}


	// Must reconfigure the I2C peripheral before attempting to transmit in master mode
	HAL_I2C_DeInit(&hi2c_ipmba);
	HAL_I2C_Init(&hi2c_ipmba);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c_ipmba, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c_ipmba, 0);

	// set current mode as Master
	i2c_ipmba_current_state = I2C_MODE_MASTER;

	// Start the transmission
	tx_ret_val = HAL_I2C_Master_Transmit_IT(&hi2c_ipmba, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1);
	//tx_ret_val = HAL_I2C_Master_Transmit(&hi2c_ipmba, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1, 2000);

	// Wait for transmission to finish or timeout
	// Maximum typical transmission I2C time is around 3m. So 10ms is a reasonable timeout.
	semphr_timeout = xSemaphoreTake (ipmba_send_semphr, pdMS_TO_TICKS(10));

	// return I2C to Slave mode
	i2c_ipmba_recv_len = 0;
	HAL_I2C_DeInit(&hi2c_ipmba);
	HAL_I2C_Init(&hi2c_ipmba);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c_ipmba, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c_ipmba, 0);
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmba, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);
	i2c_ipmba_current_state = I2C_MODE_SLAVE;

	if ( (tx_ret_val == HAL_OK) && (semphr_timeout != pdFALSE) )
		return IPMB_SEND_DONE;
	else
		return IPMB_SEND_FAIL;
}



/*
* Master transmit function for IPMB-B
* IPMB-B uses STM32H7xx I2C4 - set on pins Pxx as SCL and Pxx as SDA
*/
int ipmc_ios_ipmbb_send(uint8_t *MsgPtr, int ByteCount)
{
	_Bool semphr_timeout;
	uint16_t dest_addr;
	HAL_StatusTypeDef tx_ret_val;

	dest_addr = (uint16_t)MsgPtr[0]; // Address already shifted

	// Wait until bus is free
	for(int i=0; i<60; i++)
	{
		if( !(hi2c_ipmbb.Instance->ISR & I2C_ISR_BUSY_Msk) ) // If not busy
			break;

		if(i == 60-1)
			return IPMB_SEND_FAIL; // Timeout

		vTaskDelay(1);
	}


	// Must reconfigure the I2C peripheral before attempting to transmit in master mode
	HAL_I2C_DeInit(&hi2c_ipmbb);
	HAL_I2C_Init(&hi2c_ipmbb);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c_ipmbb, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c_ipmbb, 0);

	// set current mode as Master
	i2c_ipmbb_current_state = I2C_MODE_MASTER;

	// Start the transmission
	tx_ret_val = HAL_I2C_Master_Transmit_IT(&hi2c_ipmbb, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1);
	//tx_ret_val = HAL_I2C_Master_Transmit(&hi2c_ipmbb, dest_addr, &MsgPtr[1], (uint16_t) ByteCount -1, 2000);

	// Wait for transmission to finish or timeout
	// Maximum typical transmission I2C time is around 3m. So 10ms is a reasonable timeout.
	semphr_timeout = xSemaphoreTake ( ipmbb_send_semphr, pdMS_TO_TICKS(10) );

	// return I2C to Slave mode
	i2c_ipmbb_recv_len = 0;
	HAL_I2C_DeInit(&hi2c_ipmbb);
	HAL_I2C_Init(&hi2c_ipmbb);
	HAL_I2CEx_ConfigAnalogFilter(&hi2c_ipmbb, I2C_ANALOGFILTER_ENABLE);
	HAL_I2CEx_ConfigDigitalFilter(&hi2c_ipmbb, 0);
	HAL_I2C_Slave_Receive_IT(&hi2c_ipmbb, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);
	i2c_ipmbb_current_state = I2C_MODE_SLAVE;

	if ( (tx_ret_val == HAL_OK) && (semphr_timeout != pdFALSE) )
		return IPMB_SEND_DONE;
	else
		return IPMB_SEND_FAIL;
}



int ipmc_ios_ipmba_read(uint8_t *MsgPtr)
{
    int i;
    // Length zero means no message received
    if(i2c_ipmba_recv_len > 0)
    {
        MsgPtr[0] = ipmb_addr << 1;
        for(i=0; (i < i2c_ipmba_recv_len); i++)
            MsgPtr[i+1] = ipmba_input_buffer[i];

        i2c_ipmba_recv_len = 0;
        HAL_I2C_Slave_Receive_IT(&hi2c_ipmba, &ipmba_input_buffer[0], IPMB_BUFF_SIZE);

        return i+1;
    }
    else
        return 0;
}



int ipmc_ios_ipmbb_read(uint8_t *MsgPtr )
{
    int i;
    // Length zero means no message received
    if(i2c_ipmbb_recv_len > 0)
    {
        MsgPtr[0] = ipmb_addr << 1;
        for(i=0; (i < i2c_ipmbb_recv_len); i++)
            MsgPtr[i+1] = ipmbb_input_buffer[i];

        i2c_ipmbb_recv_len = 0;
        HAL_I2C_Slave_Receive_IT(&hi2c_ipmbb, &ipmbb_input_buffer[0], IPMB_BUFF_SIZE);

        return i+1;
    }
    else
        return 0;
}



/*
 * Holds the ipmb_0_msg_receiver_task until the peripheral task releases the
 * semaphore (it will release when a message is received)
 */
void ipmc_ios_ipmb_wait_input_msg(void)
{
    xSemaphoreTake (ipmb_rec_semphr, portMAX_DELAY);
}





void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	static BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE;

    if( (hi2c->Instance==I2C1) && (i2c_ipmba_current_state==I2C_MODE_MASTER) )
	{
        xSemaphoreGiveFromISR(ipmba_send_semphr, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
 	else if ( (hi2c->Instance==I2C2) && (i2c_ipmbb_current_state==I2C_MODE_MASTER) )
    {
        xSemaphoreGiveFromISR(ipmbb_send_semphr, &xHigherPriorityTaskWoken);
 		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
 	}
}


void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
  /** Error_Handler() function is called when error occurs.
    * 1- When Slave don't acknowledge it's address, Master restarts communication.
    * 2- When Master don't acknowledge the last data transferred, Slave don't care in this example.
    */
	static BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE;

	if(hi2c->Instance==I2C1 && i2c_ipmba_current_state == I2C_MODE_SLAVE)
	{
		i2c_ipmba_recv_len = IPMB_BUFF_SIZE - hi2c->XferSize;
		if (i2c_ipmba_recv_len>0)
		{
			xSemaphoreGiveFromISR(ipmb_rec_semphr, &xHigherPriorityTaskWoken);
		}
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else if (hi2c->Instance==I2C2 && i2c_ipmbb_current_state == I2C_MODE_SLAVE)
    {
		i2c_ipmbb_recv_len = IPMB_BUFF_SIZE - hi2c->XferSize;
		if (i2c_ipmbb_recv_len>0)
		{
			xSemaphoreGiveFromISR(ipmb_rec_semphr, &xHigherPriorityTaskWoken);
		}
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else if ((i2c_ipmba_current_state == I2C_MODE_MASTER) || (i2c_ipmbb_current_state == I2C_MODE_MASTER))
    {
        //ipmc_ios_printf("I2C MASTER SEND FAIL - ISR ERROR CALLED");
    }

}


/*
 * Control the Blue Led
 */
void ipmc_ios_blue_led(int blue_led_state)
{
	if (blue_led_state == 0)
		BLUE_LED_SET_STATE(RESET); // LED OFF
	else
		BLUE_LED_SET_STATE(SET); // LED ON
}


/*
 * Implementation of printf used by OpenIPMC
 */
void ipmc_ios_printf(const char* format, ...)
{
	va_list args;

	if( enable_ipmi_printouts )
	{
		va_start( args, format );
		mt_vprintf_tstamp( format, args );
		va_end( args );
	}
}


/*
 * Implementing a function to get ticks clocks in millisecond
 */
uint32_t ipmc_ios_ms_tick(void)
{
  return HAL_GetTick();
}


/*
 * Returns IP address
 */
void ipmc_ios_get_ip_addr(uint8_t* res_ip, uint8_t* res_size, uint8_t req_bytes_size)
{
  if ( 4 == req_bytes_size )
  {
    const uint32_t ip = gnetif.ip_addr.addr;

    res_ip[3] = (uint8_t)((0xff000000 & ip) >> 24); // MS-byte first.
    res_ip[2] = (uint8_t)((0x00ff0000 & ip) >> 16);
    res_ip[1] = (uint8_t)((0x0000ff00 & ip) >> 8 );
    res_ip[0] = (uint8_t) (0x000000ff & ip);

    *res_size = 4;
  }
  else
  {
    *res_size = 0;
  }
}


/*
 * Returns Network Mask
 */
void ipmc_ios_get_netmask(uint8_t* res_netmask, uint8_t* res_size, uint8_t req_bytes_size)
{
  if ( 4 == req_bytes_size )
  {
    const uint32_t subnet = gnetif.netmask.addr;

    res_netmask[3] = (uint8_t)((0xff000000 & subnet) >> 24); // MS-byte first.
    res_netmask[2] = (uint8_t)((0x00ff0000 & subnet) >> 16);
    res_netmask[1] = (uint8_t)((0x0000ff00 & subnet) >> 8 );
    res_netmask[0] = (uint8_t) (0x000000ff & subnet);

    *res_size = 4;
  }
  else
  {
    *res_size = 0;
  }
}

/*
 * Returns default Gateway IP address
 */
void ipmc_ios_gatw_addr(uint8_t* res_gatw, uint8_t* res_size, uint8_t req_bytes_size)
{
  if( 4 == req_bytes_size )
  {
    const uint32_t gatw_addr = gnetif.gw.addr;

    res_gatw[3] = (uint8_t)((0xff000000 & gatw_addr) >> 24); // MS-byte first.
    res_gatw[2] = (uint8_t)((0x00ff0000 & gatw_addr) >> 16);
    res_gatw[1] = (uint8_t)((0x0000ff00 & gatw_addr) >> 8 );
    res_gatw[0] = (uint8_t) (0x000000ff & gatw_addr);

    *res_size = 4;
  }
  else
  {
    *res_size = 0;
  }
}


/*
 * Returns MAC address
 */
void ipmc_ios_get_mac_address(uint8_t* res_mac, uint8_t* res_size, uint8_t req_bytes_size)
{
  if ( 6 == req_bytes_size )
  {
    res_mac[0] = gnetif.hwaddr[0];
    res_mac[1] = gnetif.hwaddr[1];
    res_mac[2] = gnetif.hwaddr[2];
    res_mac[3] = gnetif.hwaddr[3];
    res_mac[4] = gnetif.hwaddr[4];
    res_mac[5] = gnetif.hwaddr[5];

    *res_size = 6;
  }
  else
  {
    *res_size = 0;
  }
}

/*
 * Returns DHCP state.
 * 	- Enable:  '1'
 * 	- Disable: '0'
 */
int ipmi_ios_dhcp_is_enable()
{
  return net_get_is_ip_mode_dhcp();
}


/*
 * Set MAC address on OpenIPMC-HW
 */
int ipmc_ios_set_mac_addr(uint8_t* mac_addr, uint8_t size)
{
  return net_set_mac_addr(mac_addr,size);
}


/*
 * Set the IP address source Static
 *
 */
int ipmc_ios_set_ip_source_static()
{
  return net_set_ip_mode_static();
}


/*
 * Set IP Source DHCP
 *
 */
int ipmc_ios_set_ip_source_dhcp()
{
  return net_set_ip_mode_dhcp();
}


/*
 * Set the IP address
 *
 */
int ipmc_ios_set_ip_addr(uint8_t ip_addr[],int size)
{
  return net_set_static_ip(ip_addr,size);
}


/*
 * Set the Net Mask
 *
 */
int ipmc_ios_set_netmask(uint8_t mask[],int size)
{
  return net_set_static_mask(mask,size);
}


/*
 * Set the Gateway address
 *
 */
int ipmc_ios_set_gatw_addr(uint8_t ip_addr[],int size)
{
  return net_set_static_gateway_addr(ip_addr,size);
}









