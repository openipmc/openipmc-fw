/*
 * h7rng.h
 *
 *  Created on: May 31, 2023
 *      Author: antoniobassi
 */

#ifndef SRC_H7RNG_H_
#define SRC_H7RNG_H_

#define H7RNG_RNG_BUF_SIZE 10UL

typedef enum
{
  H7RNG_ERR_OK       =  0,  // No errors.
  H7RNG_ERR_INIT     = -1,  // Driver not initialised.
  H7RNG_ERR_SEEDGEN  = -2,  // Seed generation error occurred.
  H7RNG_ERR_CLKERR   = -3,  // Clock error occurred.
  H7RNG_ERR_EMPTYBUF = -4,  // Circular buffer is empty.
  H7RNG_ERR_TIMEOUT  = -5,  // Timeout reached.
  H7RNG_ERR_INVSIZE  = -6,  // Invalid size passed as argument.
  H7RNG_ERR_NULLPTR  = -7,  // Null pointer passed as argument.
  H7RNG_ERR_MUTEX    = -8   // Failed to take mutex.
}h7rng_err_t;

extern h7rng_err_t  h7rng_init(void);
extern h7rng_err_t  h7rng_fetchRandomNumber(uint8_t *bufp, uint16_t bufSize);
extern h7rng_err_t  h7rng_blockingFetchRandomNumber(uint8_t *bufp, uint16_t bufSize, uint32_t msTimeout);
extern h7rng_err_t  h7rng_resetRNG(void);

#endif /* SRC_H7RNG_H_ */
