/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      mcp23s17ml_hal.c                                                  */
/* @brief     Library aimed to control MCP23s17ml SPI IO Expander               */
/* @author    Carlos R. Dell'Aquila                                             */
/********************************************************************************/


#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

#include "h7spi_config.h"
#include "h7spi_bare.h"
#include "exti_gpio_isr.h"
#include "mcp23s17ml_hal.h"

typedef enum {
  DEV_PRESENT,
  DEV_NO_PRESENT,
  DEV_ADDED,
  DEV_NO_SPACE,
  DEV_NO_ADDED
} mcp23s17_dev_present_t;

// SPI buffers and global status
static uint8_t spi_data_out[6] __attribute__((section(".sram4")));
static uint8_t spi_data_in[6]  __attribute__((section(".sram4")));

// This variable is used to track which EXTI line have been used.
static uint32_t active_interrupt_line = 0x00000000;

// This Semaphore is used to communicate interrupt events between ISR
// and MCP23S17 FreeRTOS task function.
static SemaphoreHandle_t mcp23s17_xSemaphore_bin;

// Array with devices information.
static mcp23s17_dev_record_t devices[MAX_MANAGED_DEV];
static int devices_counter = 0;

/**
 * Function to read a MCP23S17 register
 */
static mcp23s17_ret_code_t mcp23s17_read_reg(mcp23s17_dev_t* dev, uint8_t reg_addr, uint8_t* value)
{
  h7spi_spi_ret_code_t ret;
  uint16_t shift_size = 3;

  while ( h7spi_wait_until_ready(dev->spi_periph,10) != H7SPI_RET_CODE_OK )
  {
    if (h7spi_is_in_error(dev->spi_periph) == 1 )
      h7spi_clear_error_state(dev->spi_periph);
  }

  spi_data_out[0] = 0x41 | (dev->mcp23s17_addr<<1);
  spi_data_out[1] = reg_addr;
  spi_data_out[2] = 0;

  ret = h7spi_spi_master_shift_blocking(dev->spi_periph,shift_size,spi_data_in,spi_data_out,10);

  if (ret != H7SPI_RET_CODE_OK )
    return MCP23S17_RET_ERROR;

  *value = spi_data_in[2];

  return MCP23S17_RET_OK;
}

/**
 * Function to write a MCP23S17 register
 */
static mcp23s17_ret_code_t mcp23s17_write_reg(mcp23s17_dev_t* dev, uint8_t reg_addr, uint8_t value)
{
  h7spi_spi_ret_code_t ret;
  uint16_t shift_size = 3;

  while ( h7spi_wait_until_ready(dev->spi_periph,10) != H7SPI_RET_CODE_OK )
  {
    if (h7spi_is_in_error(dev->spi_periph) == 1 )
    h7spi_clear_error_state(dev->spi_periph);
  }

  spi_data_out[0] = 0x40 | (dev->mcp23s17_addr<<1);
  spi_data_out[1] = reg_addr;
  spi_data_out[2] = value;

  ret = h7spi_spi_master_shift_blocking(dev->spi_periph,shift_size,spi_data_in,spi_data_out,10);

  if (ret != H7SPI_RET_CODE_OK )
    return ret;

  return MCP23S17_RET_OK;
}

/**
 * Function to write one bit on one register.
 */
static mcp23s17_ret_code_t mcp23s17_write_bit(mcp23s17_dev_t* dev, uint8_t reg_addr, uint8_t bit_num, uint8_t value)
{
  mcp23s17_ret_code_t ret;
  uint8_t reg_data;

  // READ register actual value
  ret = mcp23s17_read_reg(dev,reg_addr,&reg_data);

  if (ret != MCP23S17_RET_OK)
    return ret;

  if (value == 0x00)
  {
    reg_data &= ~(0x01 << bit_num);
  }
  else
  {
    reg_data |= (0x01 << bit_num);
  }

  // WRITE new register value
  ret = mcp23s17_write_reg(dev,reg_addr,reg_data);

  if (ret != MCP23S17_RET_OK)
    return ret;

  return MCP23S17_RET_OK;
}

static mcp23s17_ret_code_t mcp23s17_interrupt_pin_config(GPIO_TypeDef *gpio, uint32_t pin)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  IRQn_Type exti_irq_specific;

  // The clock is enabled according to the GPIO function parameters.
  switch ((uint32_t)gpio)
  {
    case (uint32_t)GPIOA:
      __HAL_RCC_GPIOA_CLK_ENABLE();
      break;
    case (uint32_t)GPIOB:
      __HAL_RCC_GPIOB_CLK_ENABLE();
      break;
    case (uint32_t)GPIOC:
      __HAL_RCC_GPIOC_CLK_ENABLE();
      break;
    case (uint32_t)GPIOD:
      __HAL_RCC_GPIOD_CLK_ENABLE();
      break;
    case (uint32_t)GPIOE:
      __HAL_RCC_GPIOE_CLK_ENABLE();
      break;
    case (uint32_t)GPIOF:
      __HAL_RCC_GPIOF_CLK_ENABLE();
      break;
    case (uint32_t)GPIOG:
      __HAL_RCC_GPIOG_CLK_ENABLE();
      break;
    case (uint32_t)GPIOH:
      __HAL_RCC_GPIOH_CLK_ENABLE();
      break;
    case (uint32_t)GPIOI:
      __HAL_RCC_GPIOI_CLK_ENABLE();
      break;
    case (uint32_t)GPIOK:
      __HAL_RCC_GPIOK_CLK_ENABLE();
      break;
    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  // EXTI line is choose according to the pin function definition
  switch(pin)
  {
    case GPIO_PIN_0:
      exti_irq_specific =  EXTI0_IRQn;
      break;
    case GPIO_PIN_1:
      exti_irq_specific =  EXTI1_IRQn;
      break;
    case GPIO_PIN_2:
      exti_irq_specific =  EXTI2_IRQn;
      break;
    case GPIO_PIN_3:
      exti_irq_specific =  EXTI3_IRQn;
      break;
    case GPIO_PIN_4:
      exti_irq_specific =  EXTI4_IRQn;
      break;
    case GPIO_PIN_5:
    case GPIO_PIN_6:
    case GPIO_PIN_7:
    case GPIO_PIN_8:
    case GPIO_PIN_9:
      exti_irq_specific = EXTI9_5_IRQn;
      break;
    case GPIO_PIN_10:
    case GPIO_PIN_11:
    case GPIO_PIN_12:
    case GPIO_PIN_13:
    case GPIO_PIN_14:
    case GPIO_PIN_15:
      exti_irq_specific = EXTI15_10_IRQn;
      break;
    default:
      return MCP23S17_RET_ERROR;
  }

  active_interrupt_line |= pin; // Important to check the source of interrupt.

  // Interrupt pin configuration
  GPIO_InitStruct.Pin  = pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;

  HAL_GPIO_Init(gpio,&GPIO_InitStruct);

  /* EXTI interrupt init */
  HAL_NVIC_SetPriority(exti_irq_specific,8,0);
  HAL_NVIC_EnableIRQ(exti_irq_specific);

  return MCP23S17_RET_OK;
}

/*
 * Function to check if the device is present on the local devices record.
 *
 * The device localization is defined by the SPI interface and hardware address.
 */
static mcp23s17_dev_present_t mcp23s17_check_dev_present(mcp23s17_dev_t *dev, int *index)
{
  mcp23s17_dev_present_t ret = DEV_NO_PRESENT;
  mcp23s17_dev_t rec_dev;
  h7spi_periph_t rec_spi_periph;
  uint8_t rec_mcp23s17_addr;

  for(int i=0;i<devices_counter;i++)
  {
    rec_dev = devices[i].dev;
    rec_spi_periph = rec_dev.spi_periph;
    rec_mcp23s17_addr = rec_dev.mcp23s17_addr;

    if((rec_spi_periph == dev->spi_periph) && (rec_mcp23s17_addr == dev->mcp23s17_addr))
    {
      if (index != NULL)
        *index = i;
      ret = DEV_PRESENT;
      break;
     }
  }
  return ret;
}

/*
 * Function to add a new device on device data record.
 */
static mcp23s17_dev_present_t mcp23s17_add_device(mcp23s17_dev_t *dev)
{
  mcp23s17_dev_present_t ret;
  mcp23s17_dev_t local_dev;
  int i;

  ret = mcp23s17_check_dev_present(dev,NULL);

  if (ret == DEV_PRESENT)
    return DEV_NO_ADDED;


  if (devices_counter < MAX_MANAGED_DEV)
  {
    memcpy(&local_dev,dev,sizeof(mcp23s17_dev_t));
    devices[devices_counter].dev = local_dev;

    /* Initializes the callbacks */
    for(i=0;i<8;i++)
    {
      devices[devices_counter].pin_callback_a[i] = NULL;
      devices[devices_counter].pin_callback_b[i] = NULL;
    }

    devices_counter++;

  } else
    return DEV_NO_SPACE;

  return DEV_ADDED;
}

/**
 * Function to initialize the MCP23S17
 */
mcp23s17_ret_code_t mcp23s17_init(mcp23s17_dev_t* dev)
{
  mcp23s17_ret_code_t ret;
  mcp23s17_dev_present_t ret_add_device;
  uint8_t iocon_reg;

  // Add the device to the record.
  ret_add_device = mcp23s17_add_device(dev);

  if (ret_add_device != DEV_ADDED)
    return MCP23S17_RET_ERROR;

  // READ IOCON register actual value
  ret = mcp23s17_read_reg(dev,IOCON_BANK_0_MCP23S17,&iocon_reg);

  if (ret != MCP23S17_RET_OK)
    return ret;

  // Hardware address mode should be enable
  iocon_reg |= (uint8_t) HAEN;

  // INT PORT A and B connected internally.
  iocon_reg |= (uint8_t) MIRROR;

  // ODR: Configures the INT pin as an open-drain output
  iocon_reg |= (uint8_t) ODR;

  //WRITE the modified IOCON register value
  ret = mcp23s17_write_reg(dev,IOCON_BANK_0_MCP23S17,iocon_reg);

  if (ret != MCP23S17_RET_OK )
    return ret;

  // ADD the interrupt configuration.
  if (dev->port_interrupt != NULL)
  {
    ret = mcp23s17_interrupt_pin_config(dev->port_interrupt,dev->pin_interrupt);

    if (ret != MCP23S17_RET_OK)
      return ret;
  }

  return MCP23S17_RET_OK;
}

/**
 * Function to reset by /RESET pin the MCP23S17ML device and the initialize
 */
mcp23s17_ret_code_t mcp23s17_full_reset(mcp23s17_dev_t* dev)
{
  GPIO_TypeDef* port_reset = dev->port_reset;
  uint32_t pin_reset = dev->pin_reset;

  // The clock is enabled according to the GPIO function parameters.
  switch ((uint32_t)port_reset)
  {
    case (uint32_t)GPIOA:
      __HAL_RCC_GPIOA_CLK_ENABLE();
      break;
    case (uint32_t)GPIOB:
      __HAL_RCC_GPIOB_CLK_ENABLE();
      break;
    case (uint32_t)GPIOC:
      __HAL_RCC_GPIOC_CLK_ENABLE();
      break;
    case (uint32_t)GPIOD:
      __HAL_RCC_GPIOD_CLK_ENABLE();
      break;
    case (uint32_t)GPIOE:
      __HAL_RCC_GPIOE_CLK_ENABLE();
      break;
    case (uint32_t)GPIOF:
      __HAL_RCC_GPIOF_CLK_ENABLE();
      break;
    case (uint32_t)GPIOG:
      __HAL_RCC_GPIOG_CLK_ENABLE();
      break;
    case (uint32_t)GPIOH:
      __HAL_RCC_GPIOH_CLK_ENABLE();
      break;
    case (uint32_t)GPIOI:
      __HAL_RCC_GPIOI_CLK_ENABLE();
      break;
    case (uint32_t)GPIOK:
      __HAL_RCC_GPIOK_CLK_ENABLE();
      break;
    default:
      return MCP23S17_RET_ERROR;
      break;
    }

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  GPIO_InitStruct.Pin = pin_reset;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(port_reset,&GPIO_InitStruct);

  HAL_GPIO_WritePin(port_reset,pin_reset, GPIO_PIN_RESET);
  vTaskDelay(10); // 10 MS of Delay.
  HAL_GPIO_WritePin(port_reset,pin_reset, GPIO_PIN_SET);

  //return mcp23s17_init(dev);
}

/*
 * Function to configure the PIN direction as OUTPUT
 */
mcp23s17_ret_code_t mcp23s17_config_pin_output(mcp23s17_pin_t* pin)
{
  mcp23s17_ret_code_t ret;

  mcp23s17_dev_t *dev = pin->dev;
  mcp23s17_port_t port = pin->port;
  uint8_t pin_number = pin->pin_number;

  uint8_t iodir_addr;

  switch(port)
  {
    case MCP23S17_PORTA:
      iodir_addr    = IODIRA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      iodir_addr    = IODIRB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  ret = mcp23s17_write_bit(dev,iodir_addr,pin_number,0x00);

  if (ret != MCP23S17_RET_OK)
    return ret;

  return MCP23S17_RET_OK;
}

/**
 * Function to configure the PIN direction as input
 */
mcp23s17_ret_code_t mcp23s17_config_pin_input(mcp23s17_pin_t* pin, mcp23s17_pin_ipol_t ipol, mcp23s17_pin_interrupt_t interrupt, mcp23s17_pin_pullup_t pullup)
{
  int index;
  mcp23s17_dev_present_t ret_dev_present;
  mcp23s17_ret_code_t ret;

  mcp23s17_dev_t *dev = pin->dev;
  mcp23s17_port_t port = pin->port;
  uint8_t pin_number = pin->pin_number;

  uint8_t iodir_addr;
  uint8_t ipol_addr;
  uint8_t gpint_en_addr;
  uint8_t intcon_addr;
  uint8_t defval_addr;
  uint8_t gppu_addr;

  switch(port)
  {
    case MCP23S17_PORTA:
      iodir_addr    = IODIRA_BANK_0_MCP23S17;
      ipol_addr     = IPOLA_BANK_0_MCP23S17;
      gpint_en_addr = GPINTENA_BANK_0_MCP23S17;
      intcon_addr   = INTCONA_BANK_0_MCP23S17;
      defval_addr   = DEFVALA_BANK_0_MCP23S17;
      gppu_addr     = GPPUA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      iodir_addr    = IODIRB_BANK_0_MCP23S17;
      ipol_addr     = IPOLB_BANK_0_MCP23S17;
      gpint_en_addr = GPINTENB_BANK_0_MCP23S17;
      intcon_addr   = INTCONB_BANK_0_MCP23S17;
      defval_addr   = DEFVALB_BANK_0_MCP23S17;
      gppu_addr     = GPPUB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  // CONFIG DIR as INPUT
  ret = mcp23s17_write_bit(dev, iodir_addr,pin_number,0x01);

  if (ret != MCP23S17_RET_OK)
    return ret;

  // CONFIG POLARITY
  switch(ipol)
  {
    case POL_NO_INV:
      ret = mcp23s17_write_bit(dev,ipol_addr,pin_number,0x00);
      if (ret != MCP23S17_RET_OK)
        return ret;
      break;

    case POL_INV:
      ret = mcp23s17_write_bit(dev,ipol_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;
      break;

    default:
      break;
  }

  // CONFIG PULL-UP
  switch(pullup)
  {
    case PULL_UP_DISABLE:
      ret = mcp23s17_write_bit(dev,gppu_addr,pin_number,0x00);
      if (ret != MCP23S17_RET_OK)
        return ret;
    break;

    case PULL_UP_ENABLE:
      ret = mcp23s17_write_bit(dev,gppu_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;
      break;

    default:
      break;
  }

  // CONFIG INTERRUPT
  switch(interrupt)
  {
    case INT_DISABLE:
    ret = mcp23s17_write_bit(dev,gpint_en_addr,pin_number,0x00);
    if (ret != MCP23S17_RET_OK)
      return ret;
    break;

    case INT_ENABLE_ON_CHANGE_MODE:
      ret = mcp23s17_write_bit(dev,gpint_en_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

      ret = mcp23s17_write_bit(dev,intcon_addr,pin_number,0x00);
      if (ret != MCP23S17_RET_OK)
        return ret;
    break;

    case INT_ENABLE_ON_INPUT_LOW:
      ret = mcp23s17_write_bit(dev,gpint_en_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

      ret = mcp23s17_write_bit(dev,intcon_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

      ret = mcp23s17_write_bit(dev,defval_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

      break;

    case INT_ENABLE_ON_INPUT_HIGH:
      ret = mcp23s17_write_bit(dev,gpint_en_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

      ret = mcp23s17_write_bit(dev,intcon_addr,pin_number,0x01);
      if (ret != MCP23S17_RET_OK)
        return ret;

        ret = mcp23s17_write_bit(dev,defval_addr,pin_number,0x00);
      if (ret != MCP23S17_RET_OK)
       return ret;
      break;
    default:
      return MCP23S17_RET_ERROR;
    break;
  }

  // SET INTERRUPT CALLBACK
  if (interrupt != INT_DISABLE)
  {
    // Check if the dev is present.
  ret_dev_present = mcp23s17_check_dev_present(dev,&index);

  if (ret_dev_present != DEV_PRESENT)
    return MCP23S17_RET_ERROR;

  switch(port)
  {
    case MCP23S17_PORTA:
      devices[index].pin_callback_a[pin_number] = pin->pin_callback;
      break;

    case MCP23S17_PORTB:
      devices[index].pin_callback_a[pin_number] = pin->pin_callback;
        break;
    default:
      return MCP23S17_RET_ERROR;
      break;
     }
  }

  return MCP23S17_RET_OK;
}

/**
 * Function to read the GIPO pin value
 */
mcp23s17_ret_code_t mcp23s17_read_pin_value(mcp23s17_pin_t* pin, uint8_t* in_val)
{
  mcp23s17_ret_code_t ret;

  mcp23s17_dev_t *dev = pin->dev;
  mcp23s17_port_t port = pin->port;
  uint8_t pin_number = pin->pin_number;
  uint8_t value;

  uint8_t gpio_addr;

  switch(port)
  {
    case  MCP23S17_PORTA:
      gpio_addr  = GPIOA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      gpio_addr = GPIOB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
  }

  ret = mcp23s17_read_reg(dev,gpio_addr,&value);

  if (ret != MCP23S17_RET_OK)
    return ret;

  *in_val = (value >> pin_number ) & 0x01;

  return MCP23S17_RET_OK;
}

/**
 * Function to write the GPIO pin value
 */
mcp23s17_ret_code_t mcp23s17_write_pin_value(mcp23s17_pin_t* pin, uint8_t in_val)
{
  mcp23s17_ret_code_t ret;

  mcp23s17_dev_t *dev = pin->dev;
  mcp23s17_port_t port_number = pin->port;
  uint8_t pin_number = pin->pin_number;
  uint8_t value = 0x00;

  uint8_t gpio_addr;
  uint8_t olat_addr;

  switch(port_number)
  {
    case MCP23S17_PORTA:
      gpio_addr = GPIOA_BANK_0_MCP23S17;
      olat_addr = OLATA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      gpio_addr = GPIOB_BANK_0_MCP23S17;
      olat_addr = OLATB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  if (in_val != 0x00)
    value = 0x01;

  ret = mcp23s17_write_bit(dev,gpio_addr,pin_number,value);

  if (ret != MCP23S17_RET_OK)
    return ret;

  ret = mcp23s17_write_bit(dev,olat_addr,pin_number,value);

  if (ret != MCP23S17_RET_OK)
      return ret;

  return MCP23S17_RET_OK;
}


/**
 * Function to get the pin configuration
 */
mcp23s17_ret_code_t mcp23s17_get_pin_config(mcp23s17_pin_t* pin, uint8_t* io_dir_config)
{
  mcp23s17_ret_code_t ret;

  mcp23s17_dev_t *dev = pin->dev;
  mcp23s17_port_t port = pin->port;
  uint8_t pin_number = pin->pin_number;

  uint8_t reg_bit = 0x00;
  uint8_t reg_value;

  uint8_t iodir_addr;

  switch(port)
  {
    case MCP23S17_PORTA:
      iodir_addr    = IODIRA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      iodir_addr    = IODIRB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  ret = mcp23s17_read_reg(dev,iodir_addr,&reg_value);

  if (ret != MCP23S17_RET_OK)
    return ret;

  reg_bit = ( 0x01  << pin_number );


  if( reg_value & reg_bit )
  {
     //is an INPUT
     *io_dir_config = 1;
  }
  else
  {
    // is an Output
    *io_dir_config = 0;
  }
  return MCP23S17_RET_OK;
}








/*
 * Get Interrupt status PIN
 */
mcp23s17_ret_code_t mcp23s17_read_pin_interrupt_status(mcp23s17_dev_t *dev, mcp23s17_port_t port_number,uint8_t *inter_flag, uint8_t *inter_cap )
{
  mcp23s17_ret_code_t ret;

  uint8_t inter_flag_addr;
  uint8_t inter_cap_addr;

  switch(port_number)
  {
    case MCP23S17_PORTA:
      inter_flag_addr = INTFA_BANK_0_MCP23S17;
      inter_cap_addr  = INTCAPA_BANK_0_MCP23S17;
      break;

    case MCP23S17_PORTB:
      inter_flag_addr = INTFB_BANK_0_MCP23S17;
      inter_cap_addr  = INTCAPB_BANK_0_MCP23S17;
      break;

    default:
      return MCP23S17_RET_ERROR;
      break;
  }

  // Read the flag status.
  ret = mcp23s17_read_reg(dev,inter_flag_addr,inter_flag);

  if (ret != MCP23S17_RET_OK)
    return ret;

  // Read the Capture value.
  ret = mcp23s17_read_reg(dev,inter_cap_addr,inter_cap);

  if (ret != MCP23S17_RET_OK)
    return ret;

  return MCP23S17_RET_OK;
}

/*
 * Global ISR for MCP23S17.
 */
void mcp23s17_isr(uint32_t pr1, uint32_t* pr1_clean_mask_pointer, BaseType_t* xHigherPriorityTaskWoken_pointer)
{
  uint32_t interrupt_flag;
  BaseType_t xHigherPriorityTaskWoken;

  /* The xHigherPriorityTaskWoken parameter must be initialized to pdFALSE as
   * it will get set to pdTRUE inside the interrupt safe API function if a
   * context switch is required.
   */
  xHigherPriorityTaskWoken = pdFALSE;

  interrupt_flag = active_interrupt_line & pr1;

  if (interrupt_flag)
  {
  *pr1_clean_mask_pointer = interrupt_flag;

    /* 'Give' the semaphore to unblock the task, passing in the address of
     * xHigherPriorityTaskWoken as the interrupt safe API function's
     * pxHigherPriorityTaskWoken parameter.
     */
    xSemaphoreGiveFromISR(mcp23s17_xSemaphore_bin, &xHigherPriorityTaskWoken);

    /* The following pointer will help to do a context switch */
    *xHigherPriorityTaskWoken_pointer = xHigherPriorityTaskWoken;
  }
}

/*
 * Task to read the IO-Expander if an Interrupt occurs.
 */
void mcp23s17_task(void *pvParameters) {

  int i;
  int z;
  mcp23s17_ret_code_t ret_a, ret_b;
  mcp23s17_dev_t *dev;

  uint8_t inter_flag_a, inter_flag_b;
  uint8_t inter_cap_a, inter_cap_b;

  mcp23s17_xSemaphore_bin = xSemaphoreCreateBinary();

  // Create a binary Semaphore
  if(mcp23s17_xSemaphore_bin == NULL)
    return;

  for(;;)
  {
  /* Use the semaphore to wait for the event.
   * The semaphore was created before the scheduler was started, so before this task ran for the first time.
   * The task blocks indefinitely, meaning this function call will only return once the semaphore has been
   * successfully obtained - so there is no need to check the value  returned by xSemaphoreTake().
   */
    xSemaphoreTake(mcp23s17_xSemaphore_bin, portMAX_DELAY);

    for(i=0;i<devices_counter;i++)
    {
      dev = &devices[i].dev;
      ret_a = mcp23s17_read_pin_interrupt_status(dev,MCP23S17_PORTA, &inter_flag_a,&inter_cap_a);
      ret_b = mcp23s17_read_pin_interrupt_status(dev,MCP23S17_PORTB, &inter_flag_b,&inter_cap_b);


      if ((ret_a == MCP23S17_RET_OK) && (ret_b == MCP23S17_RET_OK))
      {
        for(z=0;z<8;z++)
        {
          if((inter_flag_a & (0x01 << z)) && (devices[i].pin_callback_a[z] != NULL))
          {
            devices[i].pin_callback_a[z](inter_cap_a);
          }

          if((inter_flag_b & (0x01 << z)) && (devices[i].pin_callback_b[z] != NULL))
          {
            devices[i].pin_callback_b[z](inter_cap_b);
          }
        }
      }
    }
  }
}



























