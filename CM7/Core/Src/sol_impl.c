/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *	@file  sol_impl.c
 *
 *	@brief Implements hardware specific functions for OpenIPMC-SW feature.
 *
 *
 *	@date	  YYYY / MM / DD - 2024 / 03 / 08
 *	@author	Carlos Ruben Dell'Aquila
 *
 */

/* LWIP resources */
#include "lwip.h"
#include "lwip/udp.h"

/* h7xx custom driver */
#include "h7uart_bare.h"

/* OpenIPMC-FW */
#include "mt_printf.h"
#include "openipmc_fw_utils.h"

/* RMCP+ */
#include "rmcp.h"

/* Serial Over LAN */
#include "sol.h"
#include "sol_impl.h"

extern h7uart_periph_t huart_sol;
extern h7uart_periph_init_config_t huart_sol_config;

static void uart_sol_rx_callback(uint8_t* data, uint32_t len);
static int  uart_tx_payload(uint8_t *payload, uint16_t length);
static int  uart_init_periph_fn(void);

static int uart_init_periph_fn(void)
{
  h7uart_uart_ret_code_t ret;

  huart_sol_config.rx_callback = uart_sol_rx_callback;
  ret = h7uart_uart_init_by_config(huart_sol,&huart_sol_config);

  return ret;
}

static int uart_tx_payload(uint8_t *payload, uint16_t length)
{
  const unsigned int tx_status = (H7UART_RET_CODE_OK == h7uart_uart_tx(huart_sol, payload, length, 10UL))?(1UL):(0UL);
  return tx_status;
}

static void uart_sol_rx_callback(uint8_t* data, uint32_t len)
{
  openipmc_sol_uart_rx(data,len);
}

static sol_callbacks_t sol_impl_callbacks = {.sol_uart_init = uart_init_periph_fn, .sol_uart_tx = uart_tx_payload};

void sol_impl_init( void )
{
  uart_init_periph_fn();
  openipmc_sol_impl_init(&sol_impl_callbacks);
}
