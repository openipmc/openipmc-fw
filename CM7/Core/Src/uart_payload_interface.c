/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  @file  uart_payload_interface.c
 *
 *  @brief Set of tasks and functions to get and send ipmi messages over UART PAYLOAD INTERFACE
 *         in TERMINAL MODE according IPMI specification v1.5
 *
 *  @date   YYYY / MM / DD - 2023 / 11 / 08
 *  @author Carlos Ruben Dell'Aquila
 *
 */

/* Standard C */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* MCU resources */
#include "h7uart_bare.h"

/* FreeRTOS resources */
#include "FreeRTOS.h"
#include "semphr.h"
#include "queue.h"
#include "stream_buffer.h"

/* Specific resources */
#include "main.h"
#include "ipmi_msg_router.h"
#include "uart_payload_interface.h"

/* Extern variables declaration */
extern h7uart_periph_t huart_pyldi;
extern h7uart_periph_init_config_t huart_pyldi_config;

/* Local variables declaration */
static StreamBufferHandle_t pyldi_input_stream = NULL;
static uart_pyldi_states_t  pyld_interface_state = {UART_PYLDI_STATE_NOT_INITIALIZATED,READ_NIBBLE_MS};

/* Public variables */
TaskHandle_t pyldi_receiver_task_handle;
TaskHandle_t pyldi_sender_task_handle;

/* Public function declaration */
void pyldi_init( void );
void pyldi_receiver_task( void* );
void pyldi_sender_task( void* );

/* Private function declaration */
static void pyldi_rx_callback(uint8_t* , uint32_t);

/*
 * Function for initialization
 *
 * It initializes UART interface and the receiving buffer.
 *
 */
void pyldi_init( void )
{
#if PYLDI_AVAILABLE_ON_UART == 1

  /* Receiver PAYLOAD UART queue initialization */
  pyldi_input_stream = xStreamBufferCreate(MAX_IPMI_ASCII_BYTES_PYLD_INTERFACE_TERMINAL_MODE,1);

  if (pyldi_input_stream == NULL)
    Error_Handler();

  /* UART INTERFACE CONFIGURATION */
  huart_pyldi_config.rx_callback = pyldi_rx_callback;

  if (h7uart_uart_init_by_config(huart_pyldi,&huart_pyldi_config) != H7UART_RET_CODE_OK )
    Error_Handler();

  /* State machine initialization */
  pyld_interface_state.main_state = UART_PYLDI_STATE_IPMI_MSG_IDLE;

#endif
}

/*
 * Callback Rx function for PYLD interface
 *
 * It adds the incoming data from UART PYLD Interface in a buffer stream.
 *
 */
static void pyldi_rx_callback(uint8_t* data, uint32_t len )
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  if(pyldi_input_stream != NULL)
  {
    xStreamBufferSendFromISR(pyldi_input_stream,data,len,&xHigherPriorityTaskWoken);
  }
}

/*
 * Function to Payload Interface State Machine
 *
 */
uart_pyldi_ret_code_t pyld_state_machine(uint8_t c, data_pyldi* p_in_msg_from_pyld, uart_pyldi_states_t* p_pyld_interface_state)
{
  uint8_t c_bin;

  switch(p_pyld_interface_state->main_state)
  {
    case UART_PYLDI_STATE_NOT_INITIALIZATED:
      break;

    case UART_PYLDI_STATE_IPMI_MSG_IDLE:
      if ( c == '[' )
      {
        p_in_msg_from_pyld->length = 0;
        p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_RCV;
        p_pyld_interface_state->sub_state  = READ_NIBBLE_MS;
      }
      break;

    case UART_PYLDI_STATE_IPMI_MSG_RCV:
      switch(c)
      {
        case ']':
          p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_IDLE;
          return UART_PYLDI_NEW_MSG;

        case ' ':
          break;

        case '[':
          p_in_msg_from_pyld->length = 0;
          p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_RCV;
          p_pyld_interface_state->sub_state  = READ_NIBBLE_MS;
          break;

        default:
          if( (c >= 0x30) && ( c <= 0x39 ) ) // Range of ASCII format values from 0 to 9.
          {
            c_bin = c - 0x30;
          }
          else
          {
            if( (c >= 0x41) && ( c <= 0x46 ) ) // Range of ASCII format values from A to F.
            {
              c_bin = c - 0x37;
            }
            else
            {
              if((c >= 0x61 ) && (c <= 0x66 )) // Range of ASCII format values from a to f.
              {
                c_bin = c - 0x57;
              }
              else
              {
                p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_IDLE;
                return UART_PYLDI_NO_MSG;
              }
            }
          }

          if(p_pyld_interface_state->sub_state == READ_NIBBLE_MS)
          {
            p_pyld_interface_state->sub_state = READ_NIBBLE_LS;
            p_in_msg_from_pyld->data[p_in_msg_from_pyld->length] = c_bin << 4;
          }
          else
          {
            if( p_in_msg_from_pyld->length < MAX_IPMI_ASCII_BYTES_PYLD_INTERFACE_TERMINAL_MODE  )
            {
              p_pyld_interface_state->sub_state = READ_NIBBLE_MS;
              p_in_msg_from_pyld->data[p_in_msg_from_pyld->length++] |= c_bin;
            }
            else
            {
              p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_IDLE;
              return UART_PYLDI_NO_MSG;
            }
          }
          break;
      }
      break;

      default:
        p_pyld_interface_state->main_state = UART_PYLDI_STATE_IPMI_MSG_IDLE;
        break;
  }
  return UART_PYLDI_NO_MSG;
}

/*
 *  Task for Receiver PYLD Interface
 *
 */
void pyldi_receiver_task(void* pvParameters)
{
  uart_pyldi_ret_code_t ret;
  ipmi_msg_t rcv_data;

  uint8_t c;

  uint8_t cont_cmds = 0;

  data_pyldi in_msg_from_pyld;

  uint8_t payload_data_length;
  uint8_t checksum1;
  uint8_t checksum2;

  while ( ipmi_router_channel_input_enable(IPMI_ROUTER_CHANNEL_PYLDI) == IPMI_MSG_ROUTER_RET_NOT_INITIALIZED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  while( pyld_interface_state.main_state == UART_PYLDI_STATE_NOT_INITIALIZATED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  for(;;)
  {
    xStreamBufferReceive(pyldi_input_stream,&c,1,portMAX_DELAY);

    ret = pyld_state_machine(c,&in_msg_from_pyld,&pyld_interface_state);

    if ( ret == UART_PYLDI_NEW_MSG )
    {

      rcv_data.channel = IPMI_ROUTER_CHANNEL_PYLDI;

      rcv_data.data[IPMB0_MSG_OFFSET_NET_FN_EVEN_RS_LUN] = in_msg_from_pyld.data[TM_PYLDI_MSG_OFFSET_NET_FN_RS_LUN];
      rcv_data.data[IPMB0_MSG_OFFSET_RQ_SEQ_RQ_LUN     ] = cont_cmds << 2;// in_msg_from_pyld.data[TM_PYLDI_MSG_OFFSET_RQ_SEQ_BRIDGE];
      rcv_data.data[IPMB0_MSG_OFFSET_CMD               ] = in_msg_from_pyld.data[TM_PYLDI_MSG_OFFSET_CMD          ];

      cont_cmds = (cont_cmds + 1) & 0x3F;

      if (( in_msg_from_pyld.data[TM_PYLDI_MSG_OFFSET_NET_FN_RS_LUN] & 0x01 ) == 0x00 )
      {
        // The incoming data is a request.
        payload_data_length = in_msg_from_pyld.length - 3; // 3  = ntFN(even)/rqLUN + rqSeq/rsLUN + cmd.
        memcpy(&rcv_data.data[IPMB0_RQ_MSG_OFFSET_DATA],&in_msg_from_pyld.data[TM_PYLDI_RQ_MSG_OFFSET_DATA],payload_data_length);

        ipmi_checksum_generator(&rcv_data,&checksum1,&checksum2);

        rcv_data.length = payload_data_length + 7; // 7 = rsAddr + netFn(even)/rsLUN + checksum1 + rqAddr + rqSeq/rqLUN + cmd + checksum2.

        rcv_data.data[IPMB0_MSG_OFFSET_CHECKSUM_HEADER] = checksum1;
        rcv_data.data[rcv_data.length-1] = checksum2;

        // Post request
        ipmi_router_queue_req_post(&rcv_data);
      }
      else
      {
        // The incomming data is a response.
        rcv_data.data[IPMB0_RS_MSG_OFFSET_COMPLETION ] = in_msg_from_pyld.data[TM_PYLDI_RS_MSG_OFFSET_COMPLETION_CODE];

        payload_data_length = rcv_data.length - 4; // 4  = ntFN(odd)/rqLUN + rqSeq/rsLUN + cmd + completion code.
        memcpy(&rcv_data.data[IPMB0_RS_MSG_OFFSET_DATA],&in_msg_from_pyld.data[TM_PYLDI_RS_MSG_OFFSET_DATA],payload_data_length);

        ipmi_checksum_generator(&rcv_data,&checksum1,&checksum2);

        rcv_data.length = payload_data_length + 8; // 8 = rsAddr + netFn(even)/rsLUN + checksum1 + rqAddr + rqSeq/rqLUN + cmd + completion + checksum2.

        rcv_data.data[IPMB0_MSG_OFFSET_CHECKSUM_HEADER] = checksum1;
        rcv_data.data[rcv_data.length-1] = checksum2;

        // Post response
        ipmi_router_queue_res_post(&rcv_data);
      }
    }
  }
}

/*
 * Funtion to map a nibble to ascii character
 *
 */
static char nibble_to_ascii(uint8_t nibble )
{
  char c;

  if( ( nibble >= 0x00 ) && ( nibble <= 0x09 ) )
  {
    c = nibble + 0x30;
  }
  else
  {
    c = nibble + 0x37;
  }
  return c;
}

/*
 * Funtion to sent by UART a binary as ascii characters
 *
 */
static h7uart_uart_ret_code_t uart_send_bin_as_ascii(uint8_t c_bin)
{
  h7uart_uart_ret_code_t ret;
  char c;

  // Splits c_bin in nibbles
  uint8_t const nibble_ms = (( c_bin & 0xF0 ) >> 4 );
  uint8_t const nibble_ls = ( c_bin & 0x0F );

  c = nibble_to_ascii(nibble_ms);

  ret = h7uart_uart_tx(huart_pyldi,(uint8_t*) &c,1, 10UL);

  if ( ret != H7UART_RET_CODE_OK )
    return ret;

  c = nibble_to_ascii(nibble_ls);
  ret = h7uart_uart_tx(huart_pyldi,(uint8_t*) &c,1, 10UL);

  if (ret != H7UART_RET_CODE_OK )
    return ret;

  return H7UART_RET_CODE_OK;
}


/*
 * Task for Sender PYLD Interface
 *
 */
void pyldi_sender_task(void* pvParameters)
{
  ipmi_router_ret_code_t ipmi_router_ret;
  h7uart_uart_ret_code_t ret;

  ipmi_msg_t rec_data;

  uint8_t c_bin;
  uint8_t c_ascii;

  int source_address;
  int payload_data_length;
  int i;

  while (ipmi_router_channel_output_enable(IPMI_ROUTER_CHANNEL_PYLDI) == IPMI_MSG_ROUTER_RET_NOT_INITIALIZED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  while( pyld_interface_state.main_state == UART_PYLDI_STATE_NOT_INITIALIZATED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  for(;;)
  {
    ipmi_router_ret = ipmi_router_queue_out_get(IPMI_ROUTER_CHANNEL_PYLDI,&rec_data);

    if ( ipmi_router_ret == IPMI_MSG_ROUTER_RET_OK )
    {
      c_ascii = '[';
      ret = h7uart_uart_tx(huart_pyldi,&c_ascii,1, 10UL);

      c_bin = rec_data.data[IPMB0_MSG_OFFSET_NET_FN_EVEN_RS_LUN];
      ret |= uart_send_bin_as_ascii(c_bin);

      c_ascii = ' ';
      ret = h7uart_uart_tx(huart_pyldi,&c_ascii,1, 10UL);

      c_bin = rec_data.data[IPMB0_MSG_OFFSET_RQ_SEQ_RQ_LUN];
      ret |= uart_send_bin_as_ascii(c_bin);

      c_ascii = ' ';
      ret = h7uart_uart_tx(huart_pyldi,&c_ascii,1, 10UL);

      c_bin = rec_data.data[IPMB0_MSG_OFFSET_CMD];
      ret |= uart_send_bin_as_ascii(c_bin);

      if (( rec_data.data[IPMB0_MSG_OFFSET_NET_FN_EVEN_RS_LUN] & (0x01 << 2) ) == 0x00 )
      {
        // Request message
        source_address = IPMB0_RQ_MSG_OFFSET_DATA;
        payload_data_length = rec_data.length - 7; // 7 = rsAddr + netFn(even)/rsLUN + checksum1 + rqAddr + rqSeq/rqLUN + cmd + checksum2.

      }
      else
      {
        c_ascii = ' ';
        ret = h7uart_uart_tx(huart_pyldi,&c_ascii,1, 10UL);

        // Response message
        c_bin = rec_data.data[IPMB0_RS_MSG_OFFSET_COMPLETION];
        ret |= uart_send_bin_as_ascii(c_bin);

        source_address = IPMB0_RS_MSG_OFFSET_DATA;
        payload_data_length = rec_data.length - 8; // 8 = rsAddr + netFn(even)/rsLUN + checksum1 + rqAddr + rqSeq/rqLUN + cmd + completion + checksum2.
      }

      for(i=0;i<payload_data_length;i++)
      {
        c_ascii = ' ';
        ret = h7uart_uart_tx(huart_pyldi,&c_ascii,1, 10UL);

        c_bin = rec_data.data[source_address+i];
        ret |= uart_send_bin_as_ascii(c_bin);
      }

      if ( ret == H7UART_RET_CODE_OK )
      {
        // if previous transactions don´t have any error complete the message with:
        h7uart_uart_tx(huart_pyldi,(uint8_t*)"]\n\r",2, 10UL);
      }
    }
  }
}
