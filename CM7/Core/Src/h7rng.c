/*
 * h7rng.c
 *
 *  Created on: 31 de mai de 2023
 *      Author: antoniobassi
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "openipmc_fw_utils.h"
#include <stm32h7xx_hal.h>
#include <stm32h745xx.h>
#include "h7rng.h"

#define BUF_SIZE (H7RNG_RNG_BUF_SIZE + 1UL)

/* System resources private functions and variables */
static SemaphoreHandle_t h7rng_cbuffer_mutex;
unsigned int msTimeout = 10UL;
static void __h7rng_FreeRTOS_create_mutex(void);
static void __h7rng_FreeRTOS_unlock_mutex(void);
static int  __h7rng_FreeRTOS_lock_mutex(void);

/* Private functions */
static void             __h7rng_solveIRQ(void);
static void             __h7rng_nvicEnableIRQ(void);
static void             __h7rng_nvicDisableIRQ(void);
static inline void      __h7rng_enableClkAccess(void);
static inline void      __h7rng_disableClkAccess(void);
static inline void      __h7rng_enableNoiseSource(void);
static inline void      __h7rng_disableNoiseSource(void);
static inline void      __h7rng_enableInterrupts(void);
static inline void      __h7rng_disableInterrupts(void);
static inline void      __h7rng_clearInterrupts(void);
static inline void      __h7rng_clearSeedErrFlag(void);
static inline void      __h7rng_clearClkErrFlag(void);
static inline uint32_t  __h7rng_isClkAccessEnabled(void);
static inline uint32_t  __h7rng_rdClkErrFlag(void);
static inline uint32_t  __h7rng_rdErrFlags(void);
static inline uint32_t  __h7rng_rdSeedGenFlag(void);
static inline uint32_t  __h7rng_rdDataRdyFlag(void);
static uint32_t         __h7rng_isDataRdyFlagSet(void);
static uint32_t         __h7rng_isClkErrFlagSet(void);
static uint32_t         __h7rng_isSeedGenErrFlagSet(void);

typedef struct
{
  uint32_t w0;
  uint32_t w1;
  uint32_t w2;
  uint32_t w3;
}ubyte16_t;

typedef struct cbuf
{
  ubyte16_t buffer[BUF_SIZE];     // RNG Pool Buffer
  int       head;                 // Write Position
  int       tail;                 // Read Position
  int       size;
  void      (*h7rng_cbuf_mutex_create)(void);
  void      (*h7rng_cbuf_mutex_unlock)(void);
  int       (*h7rng_cbuf_mutex_lock)(void);
}h7rng_cbuf_t;

static int cbuf_init = 0;

static h7rng_cbuf_t h7rng_cbuf =
{
    .head = 0,
    .tail = 0,
    .size = BUF_SIZE,
    .h7rng_cbuf_mutex_create = NULL,
    .h7rng_cbuf_mutex_lock  = NULL,
    .h7rng_cbuf_mutex_unlock = NULL
};

/* This is a weak function defined by ST-Microelectronics HAL. As soon as the
 * interrupt signal is fired from the Nested Vectored Interrupt Controller (NVIC),
 * This function will be called from the MCU's vector table.
 */
void HASH_RNG_IRQHandler(void)
{
  __h7rng_solveIRQ();
}

static void __h7rng_solveIRQ(void)
{
  uint32_t next = 0;
  uint32_t idx  = 0;
  if(0UL == __h7rng_rdErrFlags() )
  {
    if(__h7rng_isDataRdyFlagSet())
    {
      idx = h7rng_cbuf.head;
      next  = (h7rng_cbuf.head + 1)%h7rng_cbuf.size;
      if(h7rng_cbuf.h7rng_cbuf_mutex_lock())
      {
         h7rng_cbuf.buffer[idx].w0 = (uint32_t)RNG->DR;
         h7rng_cbuf.buffer[idx].w1 = (uint32_t)RNG->DR;
         h7rng_cbuf.buffer[idx].w2 = (uint32_t)RNG->DR;
         h7rng_cbuf.buffer[idx].w3 = (uint32_t)RNG->DR;
         if(next == h7rng_cbuf.tail)
         {
           /* Buffer is full, deactivate Noise Source until threshold is met */
           __h7rng_disableInterrupts();
           __h7rng_disableNoiseSource();
           __h7rng_disableClkAccess();
           __h7rng_nvicDisableIRQ();
         }
         else
         {
           h7rng_cbuf.head = next;
         }
         h7rng_cbuf.h7rng_cbuf_mutex_unlock();
      }
    }
  }
  else
  {
    if(__h7rng_isClkErrFlagSet())
    {
      // Do something about it and clear flag...
      __h7rng_clearClkErrFlag();
    }
    if(__h7rng_isSeedGenErrFlagSet())
    {
      // Do something about it and clear flag...
      __h7rng_clearSeedErrFlag();
    }
  }
  return;
}

static int __h7rng_FreeRTOS_lock_mutex(void)
{
  int err = 0;
  static BaseType_t mtxLockHigherPriorityTaskWoken = pdFALSE;
  if(NULL != h7rng_cbuffer_mutex)
  {
    err = (pdTRUE == xSemaphoreTakeFromISR(h7rng_cbuffer_mutex, &mtxLockHigherPriorityTaskWoken)) ? (1) : (0);
    if( pdTRUE == mtxLockHigherPriorityTaskWoken )
    {
      /* We force a context switch in case of a high priority task being "awakened" */
      taskYIELD();
    }
  }
  return err;
}

static void __h7rng_FreeRTOS_unlock_mutex(void)
{
  static BaseType_t mtxUnlockHigherPriorityTaskWoken = pdFALSE;
  xSemaphoreGiveFromISR(h7rng_cbuffer_mutex, &mtxUnlockHigherPriorityTaskWoken);
  if( pdTRUE == mtxUnlockHigherPriorityTaskWoken )
  {
    /* We force a context switch in case of a high priority task being "awakened" */
    taskYIELD();
  }
}

static void __h7rng_FreeRTOS_create_mutex(void)
{
  h7rng_cbuffer_mutex = xSemaphoreCreateMutex();
}

static uint32_t __h7rng_isClkErrFlagSet(void)
{
  return (__h7rng_rdClkErrFlag()) ? (1UL) : (0UL);
}

static uint32_t __h7rng_isDataRdyFlagSet(void)
{
  return (__h7rng_rdDataRdyFlag()) ? (1UL) : (0UL);
}

static uint32_t __h7rng_isSeedGenErrFlagSet(void)
{
  return (__h7rng_rdSeedGenFlag()) ? (1UL) : (0UL);
}

static uint32_t __h7rng_rdDataRdyFlag(void)
{
  return ((RNG->SR) & (RNG_SR_DRDY));
}

static inline uint32_t __h7rng_rdErrFlags(void)
{
  uint32_t regMsk = (RNG_SR_CEIS | RNG_SR_SEIS );
  return ((RNG->SR) & (regMsk));
}

static inline uint32_t __h7rng_rdSeedGenFlag(void)
{
  return ((RNG->SR) & (RNG_SR_SEIS));
}

static inline uint32_t __h7rng_rdClkErrFlag(void)
{
  return ((RNG->SR) & (RNG_SR_CEIS));
}

static inline uint32_t  __h7rng_isClkAccessEnabled(void)
{
  return (RCC->AHB2ENR & (RCC_AHB2ENR_RNGEN)) ? (1UL) : (0UL);
}

static inline uint32_t __h7rng_isClkAccessDisabled(void)
{
  return (RCC->AHB2ENR & (RCC_AHB2ENR_RNGEN)) ? (0UL) : (1UL);
}

static inline void __h7rng_enableClkAccess(void)
{
  RCC->AHB2ENR = ((RCC->AHB2ENR & (~(RCC_AHB2ENR_RNGEN))) | (RCC_AHB2ENR_RNGEN));
}

static inline void __h7rng_disableClkAccess(void)
{
  RCC->AHB2ENR &= ~(RCC_AHB2ENR_RNGEN);
}

static inline void __h7rng_enableInterrupts(void)
{
  uint32_t regMsk = (RNG_CR_IE | RNG_CR_CED);
  RNG->CR = ((RNG->CR & (~(regMsk))) | (regMsk));
}

static inline void __h7rng_disableInterrupts(void)
{
  uint32_t regMsk = (RNG_CR_IE | RNG_CR_CED);
  RNG->CR &= ~(regMsk);
}

static inline void __h7rng_clearInterrupts(void)
{
  uint32_t regMsk = (RNG_SR_SEIS | RNG_SR_CEIS);
  RNG->SR &= ~(regMsk);
}

static inline void __h7rng_clearSeedErrFlag(void)
{
  RNG->SR &= ~(RNG_SR_SEIS);
}

static inline void __h7rng_clearClkErrFlag(void)
{
  RNG->SR &= ~(RNG_SR_CEIS);
}

static inline void __h7rng_enableNoiseSource(void)
{
  RNG->CR = ((RNG->CR & (~(RNG_CR_RNGEN))) | (RNG_CR_RNGEN));
}

static inline void __h7rng_disableNoiseSource(void)
{
  RNG->CR &= ~(RNG_CR_RNGEN);
}

static void __h7rng_nvicDisableIRQ(void)
{
  HAL_NVIC_DisableIRQ(HASH_RNG_IRQn);
}

static void __h7rng_nvicEnableIRQ(void)
{
  HAL_NVIC_SetPriority(HASH_RNG_IRQn, 5UL, 0UL);
  HAL_NVIC_EnableIRQ(HASH_RNG_IRQn);
}

/**
 *  @brief  Reads randomly generated number from the peripheral data register.
 *  @param  *bufp pointer to user buffer to store the 16 byte number.
 *  @param  bufSize Size of the user buffer, must be at least 16 bytes.
 *  @retval err -H7RNG_ERR_OK if successful.
 *              -H7RNG_ERR_EMPTYBUF if buffer is empty.
 *              -H7RNG_ERR_NULLPTR  if user buffer pointer is NULL.
 *              -H7RNG_ERR_INVSIZE  if user buffer size is less than 16 bytes.
 */
h7rng_err_t h7rng_blockingFetchRandomNumber(uint8_t *bufp, uint16_t bufSize, uint32_t msTimeout)
{
  h7rng_err_t err = H7RNG_ERR_TIMEOUT;
  uint32_t    uTick = 0UL;

  uTick = HAL_GetTick();
  while( ( H7RNG_ERR_OK != err ) && ( msTimeout > (HAL_GetTick() - uTick) ) )
  {
    err = h7rng_fetchRandomNumber(bufp, bufSize);
  }
  return err;
}

/**
 *  @brief  Reads randomly generated number from the peripheral data register.
 *  @param  *bufp pointer to user buffer to store the 16 byte number.
 *  @param  bufSize Size of the user buffer, must be at least 16 bytes.
 *  @retval err -H7RNG_ERR_OK if successful.
 *              -H7RNG_ERR_EMPTYBUF if buffer is empty.
 *              -H7RNG_ERR_NULLPTR  if user buffer pointer is NULL.
 *              -H7RNG_ERR_INVSIZE  if user buffer size is less than 16 bytes.
 */
h7rng_err_t h7rng_fetchRandomNumber(uint8_t *bufp, uint16_t bufSize)
{
  uint8_t *byte;
  h7rng_err_t err = H7RNG_ERR_EMPTYBUF;
  int next = 0;
  int idx  = 0;

  if((cbuf_init) && (h7rng_cbuf.head != h7rng_cbuf.tail) && (16 <= bufSize) && (NULL != bufp))
  {
    err = H7RNG_ERR_MUTEX;
    next = (h7rng_cbuf.tail + 1) % h7rng_cbuf.size;
    idx  = h7rng_cbuf.tail;
    if(h7rng_cbuf.h7rng_cbuf_mutex_lock())
    {
      byte = (uint8_t *)((void *)&h7rng_cbuf.buffer[idx]);
      for(uint16_t b = 0; b < bufSize; b++)
      {
        bufp[b] = byte[b];
      }
      h7rng_cbuf.h7rng_cbuf_mutex_unlock();
      h7rng_cbuf.tail = next;
      err = H7RNG_ERR_OK;
    }
  }
  else
  {
    if(h7rng_cbuf.tail == h7rng_cbuf.head)
    {
      /* Buffer is empty, enable RNG.*/
      err = h7rng_init();
    }
    else if( NULL == bufp )
    {
      err = H7RNG_ERR_NULLPTR;
    }
    else if(16 > bufSize)
    {
      err = H7RNG_ERR_INVSIZE;
    }
    else if(0 == cbuf_init)
    {
      err = H7RNG_ERR_INIT;
    }
  }
  return err;
}

/**
 *  @brief  Erases every content in the circular buffer, resets head and tail indexes and
 *          restarts peripheral.
 *  @retval err - H7RNG_ERR_OK if successful
 *              - H7RNG_ERR_SEEDGEN if the first randomly generated number has an error.
 *
 */
h7rng_err_t  h7rng_resetRNG(void)
{
  h7rng_err_t err = H7RNG_ERR_OK;

  __h7rng_disableInterrupts();
  __h7rng_disableNoiseSource();
  __h7rng_disableClkAccess();
  __h7rng_nvicDisableIRQ();

  h7rng_cbuf.head  = 0;
  h7rng_cbuf.tail  = h7rng_cbuf.head;

  for(int i = 0; i < BUF_SIZE; i++)
  {
    h7rng_cbuf.buffer[i].w0 = 0UL;
    h7rng_cbuf.buffer[i].w1 = 0UL;
    h7rng_cbuf.buffer[i].w2 = 0UL;
    h7rng_cbuf.buffer[i].w3 = 0UL;
  }
  err = h7rng_init();
  return err;
}

/**
 * @brief   Initialises circular buffer resources and starts random number generator peripheral.
 * @retval  err - H7RNG_ERR_OK if successful.
 *              - H7RNG_ERR_INIT if invalid parameters are assigned for the h7rng_cbuf_t struct.
 *              - H7RNG_ERR_SEEDGEN if the first randomly generated number has an error.
 */
h7rng_err_t h7rng_init(void)
{
  h7rng_err_t err = H7RNG_ERR_INIT;
  uint32_t uTick = 0;

  if(0 == cbuf_init)
  {
    h7rng_cbuf.head      = 0;
    h7rng_cbuf.tail      = h7rng_cbuf.head;
    h7rng_cbuf.size      = BUF_SIZE;
    h7rng_cbuf.h7rng_cbuf_mutex_create = __h7rng_FreeRTOS_create_mutex;
    h7rng_cbuf.h7rng_cbuf_mutex_unlock = __h7rng_FreeRTOS_unlock_mutex;
    h7rng_cbuf.h7rng_cbuf_mutex_lock   = __h7rng_FreeRTOS_lock_mutex;

    if((NULL != h7rng_cbuf.buffer)                  &&
       (NULL != h7rng_cbuf.h7rng_cbuf_mutex_create) &&
       (NULL != h7rng_cbuf.h7rng_cbuf_mutex_unlock) &&
       (NULL != h7rng_cbuf.h7rng_cbuf_mutex_lock))
    {
      /* Create mutex */
      h7rng_cbuf.h7rng_cbuf_mutex_create();
      cbuf_init = 1;
    }
  }

  if(cbuf_init)
  {
    /* Circular buffer has been initialised
     * Start RNG peripheral
     */
    err = H7RNG_ERR_CLKERR;

    if(__h7rng_isClkAccessDisabled())
    {
      __h7rng_enableClkAccess();
    }
    __h7rng_nvicEnableIRQ();
    __h7rng_enableInterrupts();
    __h7rng_enableNoiseSource();

    if((0 == __h7rng_rdClkErrFlag()))
    {
      /* No clock error detected, Check
       * for valid seed generation.
       */
      uTick = HAL_GetTick();
      err = H7RNG_ERR_OK;
      while(__h7rng_rdSeedGenFlag())
      {
        if(2UL < (HAL_GetTick() - uTick))
        {
          __h7rng_clearInterrupts();
          __h7rng_disableInterrupts();
          __h7rng_disableNoiseSource();
          __h7rng_disableClkAccess();
          __h7rng_nvicDisableIRQ();
          err = H7RNG_ERR_SEEDGEN;
          break;
        }
      }
    }
  }
  return err;
}


