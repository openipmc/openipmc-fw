/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: 	yimpl_flash_drv.h
 * @brief	:	Winbond w25n01gv NAND Flash memory driver callbacks for yaffs.
 * @date	:	YYYY / MM / DD - 2022 / 02 / 21
 * @author	:	Antonio V. G. Bassi
 * @note	:	Some Macros defined here are for the exclusive use of w25n01gv drivers.
 */

#ifndef SRC_YIMPL_FLASH_DRV_H_
#define SRC_YIMPL_FLASH_DRV_H_

#include "yaffs_guts.h"

/* Useful Macros */
#define KB 1024
#define MB (KB*KB)
#define	TOTAL_MEM ( 128*MB )
#define BLOCK_SIZE ( 128*KB )
#define CHUNK_SIZE ( 2*KB )
#define SPARE_SIZE ( 64 )
#define TOTAL_BLOCKS ( TOTAL_MEM / BLOCK_SIZE )
#define TOTAL_CHUNKS ( TOTAL_MEM / CHUNK_SIZE )
#define N_CHUNKS ( BLOCK_SIZE / CHUNK_SIZE )

extern int yimpl_flash_write_chunk_fn(struct yaffs_dev *dev, int nand_chunk,
																			const uint8_t* data, int data_len,
																			const uint8_t* oob, int oob_len);
extern int yimpl_flash_read_chunk_fn(struct yaffs_dev *dev, int nand_chunk,
																			uint8_t* data, int data_len,
																			uint8_t* oob, int oob_len,
																			enum yaffs_ecc_result *ecc_result);
extern int yimpl_flash_erase_block_fn(struct yaffs_dev *dev, int block_no);
extern int yimpl_flash_mark_bad_fn(struct yaffs_dev *dev, int block_no);
extern int yimpl_flash_check_bad_fn(struct yaffs_dev *dev, int block_no);
extern int yimpl_flash_init_fn(struct yaffs_dev *dev);
extern int yimpl_flash_deinit_fn(struct yaffs_dev *dev);
extern int yimpl_flash_install(const char* name);

#endif /* SRC_YIMPL_FLASH_DRV_H_ */
