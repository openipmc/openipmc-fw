/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *	@file	  rmcp_impl.c
 *
 *	@brief	Implements hardware and UDP/IP stack specific functions for OpenIPMC-SW RMCP
 *
 *	@date	  YYYY / MM / DD - 2024 / 03 / 05
 *
 *	@author Carlos Ruben Dell'Aquila
 *
 */

/* Standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/* system resources */
#include "mt_printf.h"
#include "rtc_tools.h"
#include "emh_malloc.h"
#include "h7rng.h"

/* LWIP resources */
#include "lwip.h"
#include "lwip/udp.h"
#include "network_ctrls.h"

/* RMCP */
#include "rmcp.h"
#include "rmcp_impl.h"

/* RMCP implementation state variable */
static rmcp_impl_state_t rmcp_impl_initd = RMCP_IMPL_INIT_NONE;

/* Memory management variable resources */
static emh_heapId_t rmcp_heap_id = -1;
uint8_t __attribute__((section(".rmcp_sec"))) rmcp_heap[256] = {0};

/* UDP/IP management resources */
extern struct netif    gnetif;
static uint16_t        rmcp_impl_server_port = 0x026F;
static ip_addr_t       rmcp_impl_server_ip   = {0};
static struct udp_pcb *rmcp_impl_upcb = NULL;

/*
 * Type and variables to describe the General Unique ID (GUID) for OpenIPMC-FW
 */
typedef struct
{
  volatile uint32_t guid_0;
  volatile uint32_t guid_1;
  volatile uint32_t guid_2;
}__dev_guid_t__;

static const   __dev_guid_t__ *GUID = ((__dev_guid_t__ *) UID_BASE);
static uint8_t __guid__[16] = {0};

/*
 *
 * Function to generate the GUID for RMCP+ Session.
 *
 * It is used during a RMCP+ session initialization
 *
 */
static void __rmcp_load_dev_guid_from_reg(uint8_t** buf, uint16_t* len)
{
  __guid__[0]   = (uint8_t)(((GUID->guid_0) & 0xFF000000) >>  24);
  __guid__[1]   = (uint8_t)(((GUID->guid_0) & 0x00FF0000) >>  16);
  __guid__[2]   = (uint8_t)(((GUID->guid_0) & 0x0000FF00) >>  8 );
  __guid__[3]   = (uint8_t)(((GUID->guid_0) & 0x000000FF) >>  0 );
  __guid__[4]   = (uint8_t)(((GUID->guid_1) & 0xFF000000) >>  24);
  __guid__[5]   = (uint8_t)(((GUID->guid_1) & 0x00FF0000) >>  16);
  __guid__[6]   = (uint8_t)(((GUID->guid_1) & 0x0000FF00) >>  8 );
  __guid__[7]   = (uint8_t)(((GUID->guid_1) & 0x000000FF) >>  0 );
  __guid__[8]   = (uint8_t)(((GUID->guid_2) & 0xFF000000) >>  24);
  __guid__[9]   = (uint8_t)(((GUID->guid_2) & 0x00FF0000) >>  16);
  __guid__[10]  = (uint8_t)(((GUID->guid_2) & 0x0000FF00) >>  8 );
  __guid__[11]  = (uint8_t)(((GUID->guid_2) & 0x000000FF) >>  0 );
  __guid__[12]  = 'I';
  __guid__[13]  = 'P';
  __guid__[14]  = 'M';
  __guid__[15]  = 'C';

  *buf = __guid__;
  *len = 16;
  return;
}


/*
 * Blocking random number generation
 */
static int blocking_random_number_generation(uint8_t *bufp, uint16_t bufSize, uint32_t msTimeout)
{
  const int rng_status = ( H7RNG_ERR_OK == h7rng_blockingFetchRandomNumber(bufp,bufSize,msTimeout)) ? 1 : -1;

  return rng_status;
}

/*
 * Memory management functions
 */
static emh_heapId_t __rmcp_impl_heap_init(void)
{
  return (emh_create(rmcp_heap, 250));
}

static void *__rmcp_malloc(size_t size)
{
  void *mem = NULL;
  mem = emh_malloc(rmcp_heap_id, size);
  return mem;
}

static void __rmcp_free(void *mem)
{
  emh_free(mem);
  return;
}

/* UDP/IP management functions */
static err_t rmcp_impl_udp_send(const ip_addr_t *ip_addr, uint16_t port, void *data_buf, uint16_t length)
{
  err_t err = ERR_OK;
  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, length, PBUF_RAM);

  if ( NULL == p)
  {
    err = ERR_MEM;
  }
  else
  {
    memcpy(p->payload, data_buf, length);
    err = udp_sendto(rmcp_impl_upcb, p, ip_addr, port);
    pbuf_free(p);
  }
  return err;
}

static int rmcp_impl_udp_send_packet(rmcp_ip_net_params_t net_params, uint8_t *payload, size_t length)
{
  ip_addr_t ip_addr = {0};
  uint16_t port;

  if( net_params.ip_ver != IP_V4 )
    return -1;

  ip_addr.addr = net_params.ip_addr.ipv4;
  port = net_params.port;

  return (rmcp_impl_udp_send(&ip_addr, port, (void *)payload, length));
}

static void rmcp_impl_udp_recv(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port )
{
  uint16_t length = p->len;
  uint8_t  *payload = (uint8_t *) p->payload;
  rmcp_ip_net_params_t net_params = {.ip_ver = IP_V4, .ip_addr.ipv4 = addr->addr,.port = port};

  if( (RMCP_IMPL_INIT_OK == rmcp_impl_initd )  && (NULL != p))
  {
    rmcp_udp_recv(net_params,payload,length);
    pbuf_free(p);
  }
}

err_t rmcp_impl_udp_set(void)
{
  err_t err = ERR_OK;
  rmcp_ip_net_params_t local_net_params;
  struct udp_pcb *pcb = udp_new_ip_type(IPADDR_TYPE_ANY);

  if( NULL == pcb )
  {
    // Allocation for protocol control block failed
    err = ERR_MEM;
    return err;
  }

  err = udp_bind(pcb, &gnetif.ip_addr, rmcp_impl_server_port);
  if( ERR_OK != err )
  {
    // Binding UDP connection failed
	udp_remove(pcb);
	return err;
  }

  // Tells to RMCP+ in OpenIPMC-SW on which IP address and port RMCP is running.
  // It is important when a remote agent sends SOL Payload information to a specified port.
  local_net_params.ip_ver = IP_V4;
  local_net_params.ip_addr.ipv4 = gnetif.ip_addr.addr;
  local_net_params.port = rmcp_impl_server_port;
  rmcp_set_local_net_params(local_net_params);

  rmcp_impl_upcb = pcb;
  udp_recv(pcb,&rmcp_impl_udp_recv,NULL);
  return err;
}

err_t rmcp_impl_udp_reset(void)
{
  err_t err = ERR_OK;
  udp_remove(rmcp_impl_upcb);
  ip_addr_set_any(0,&rmcp_impl_server_ip);
  rmcp_impl_server_port = RMCP_DEFAULT_PORT; // TODO: Add to rmcp.h macros.
  return err;
}

/*
 * Notification callback
 *
 * It is used for RMCP software to send notification to the top level implementation.
 *
 */
void rmcp_imp_notification_callback(rmcp_event_report_t event)
{
  switch(event)
  {
    case WATCHDOG_CANNOT_START:
      mt_printf("rmcp: Failed to start rmcp server timeout whatchdog.\n");
      break;

    case RMCP_PLUS_SESSION_TERMINATED_CLIENT_UNRESPONSIVE:
      mt_printf("rmcp: IPMI session terminated due to unresponsive client (no Keep Alive packets received)\n");
      break;

    default:
    break;
  }
}

/*
 * System specific callback:
 *
 * Group of functions that are specific for IP/UDP stack, memory management and hardware.
 *
 */
static rmcp_callbacks_t rmcp_impl_callbacks = { .malloc               = __rmcp_malloc,
                                                .mem_free             = __rmcp_free,
                                                .blocking_rand_gen    = blocking_random_number_generation,
                                                .get_guid             = __rmcp_load_dev_guid_from_reg,
                                                .rmcp_udp_send_packet = rmcp_impl_udp_send_packet,
                                                .rmcp_event_send      = rmcp_imp_notification_callback };

/*
 * Function to initialize the implementation.
 *
 * It initializes the hardware and system resource for RMCP software.
 *
 */
rmcp_impl_state_t rmcp_impl_init(void)
{
  if (rmcp_impl_initd == RMCP_IMPL_INIT_OK )
    return rmcp_impl_initd;

  if( RMCP_CALLBACK_ERROR_OK != rmcp_init(&rmcp_impl_callbacks))
    return OPENIPMC_RMCP_INIT_ERROR;

  rmcp_heap_id = __rmcp_impl_heap_init();

  if( 0 > rmcp_heap_id )
  {
    rmcp_impl_initd = RMCP_IMPL_NOT_INIT_ERROR_HEAP_MEMORY;
    return rmcp_impl_initd;
  }

  if( ERR_OK != rmcp_impl_udp_set())
  {
    rmcp_impl_initd = RMCP_IMPL_NOT_INIT_ERROR_UDP_SERVER;
    return rmcp_impl_initd;
  }

  rmcp_impl_initd = RMCP_IMPL_INIT_OK;
  return rmcp_impl_initd;
}
