/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/* *****************************************************************************
 * @file	: rtc_tools.c
 * @brief	: Real-Time Clock tools function set.
 * @date	: YYYY / MM / DD - 2022 / 04 / 08
 * @author	: Antonio V. G. Bassi
 * ******************************************************************************
*/

/* Standard C */
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <time.h>

/* resources */
#include "stm32h7xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "rtc_tools.h"


static SemaphoreHandle_t rtc_mutex = NULL;
extern RTC_HandleTypeDef hrtc;

static void rtc_tool_lock_access( void )
{
	xSemaphoreTake(rtc_mutex, portMAX_DELAY);
	return;
}

static void rtc_tool_unlock_access( void )
{
	xSemaphoreGive(rtc_mutex);
	return;
}

uint32_t rtc_tool_fetch_epoch( void )
{
	time_t epoch = 0;
	struct tm time = {0};

	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};

	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

	time.tm_year = sDate.Year + 100;
	time.tm_mon  = sDate.Month - 1;
	time.tm_mday = sDate.Date;
	time.tm_wday = 1;
	time.tm_hour = sTime.Hours;
	time.tm_min  = sTime.Minutes;
	time.tm_sec  = sTime.Seconds;

	epoch = mktime( &time );

	return epoch;
}

void rtc_tool_init( void )
{
	if( rtc_mutex != NULL )
		return;

	rtc_mutex = xSemaphoreCreateMutex();
	return;
}

void rtc_tool_get_timestamp(char* strbuf, size_t buf_size )
{
	rtc_tool_lock_access();

	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};

	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

	// Perform necessary corrections to buffer length
	size_t stamp_length = ( RTC_TOOLS_STAMP_SIZE < buf_size ) ? RTC_TOOLS_STAMP_SIZE : buf_size;

	snprintf( strbuf, stamp_length, RTC_TOOLS_STAMP_FORMAT,
			 sDate.Year, sDate.Month, sDate.Date ,
			 sTime.Hours, sTime.Minutes, sTime.Seconds );

	rtc_tool_unlock_access();
	return;
}

void rtc_tool_access_hal(RTC_TimeTypeDef* sTime, RTC_DateTypeDef* sDate)
{
	rtc_tool_lock_access();

	HAL_RTC_GetTime(&hrtc, sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, sDate, RTC_FORMAT_BIN);

	rtc_tool_unlock_access();
	return;
}
