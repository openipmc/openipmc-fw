/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: yaffsfs_conf.c
 * @brief   : Yaffs2 driver configuration program body.
 * @date	: YYYY / MM / DD - 2021 / 11 / 05
 * @author  : Antonio V. G. Bassi
 *
 * yaffs tracing macros:
 * 		YAFFS_TRACE_SCAN
 * 		YAFFS_TRACE_GC
 * 		YAFFS_TRACE_ERASE
 * 		YAFFS_TRACE_ERROR
 * 		YAFFS_TRACE_TRACING
 * 		YAFFS_TRACE_ALLOCATE
 * 		YAFFS_TRACE_BAD_BLOCKS
 * 		YAFFS_TRACE_VERIFY
 */

#include "yimpl_conf.h"

/* yaffs resources */
#include "yaffs_osglue.h"
#include "yaffs_trace.h"
#include "yaffsfs.h"

/* private includes */
#include "yaffsfs_errno.h"
#include "mt_printf.h"
#include "yimpl_flash_drv.h"
#include "syslog.h"

unsigned int yaffs_trace_mask =
	YAFFS_TRACE_ERROR 		|
	YAFFS_TRACE_TRACING 	|
	YAFFS_TRACE_VERIFY		|
	0;

static int yimpl_ready = 0;


/*
 * @name 		: yimpl_setup
 * @brief		: Install drivers for yaffs2 file system.
 * @return int	: Returns an integer, if 1 the driver
 * 				  installation was successful, otherwise
 * 				  it has failed.
 */
int yimpl_setup(void)
{
	if( yimpl_ready )
	{
		mt_printf("/r/n Yaffs2 setup was already performed. \r\n");
		return yimpl_ready;
	}

	int retval;
	yaffsfs_OSInitialisation();
	yaffs_set_trace(yaffs_trace_mask);
	yimpl_flash_install("/");
	retval = yaffs_mount("/");

	if( 0 == retval )
	{
		yimpl_ready = 1;
		return yimpl_ready;
	}
	else
	{
		retval = yaffsfs_get_errno();
		mt_printf("\r\nyaffs: Something went wrong while mounting.");
		yimpl_ready = 0;
		return yimpl_ready;
	}
}

/*
 * @name	    : yimpl_setup_ready
 * @brief       : Function to know whether yimpl_setup method has been called.
 * @return int  : Returns call_flag variable value
 *
 *
 */
int yimpl_setup_ready(void)
{
  return yimpl_ready;
}



