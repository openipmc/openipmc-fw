/*
 * yaffs_errno.h
 *
 *  Created on: 16 de set de 2022
 *      Author: antoniobassi
 */

#ifndef SRC_YAFFSFS_ERRNO_H_
#define SRC_YAFFSFS_ERRNO_H_

extern int yaffsfs_get_errno(void);

#endif /* SRC_YAFFSFS_ERRNO_H_ */
