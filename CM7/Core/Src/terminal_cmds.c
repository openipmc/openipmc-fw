/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/


#include "main.h"
#include "terminal.h"

#include "string.h"
#include <time.h>
#include "printf.h"
#include "mt_printf.h"
#include "ipmb_0.h"
#include "ipmi_msg_manager.h"
#include "picmg_address_info.h"
#include "head_commit_sha1.h"
#include "fru_state_machine.h"
#include "device_id.h"
#include "tftp_client.h"
#include "bootloader_tools.h"
#include "network_ctrls.h"
#include "lwip.h"
#include "yaffsfs_errno.h"
#include "yaffs_guts.h"
#include "yaffs_osglue.h"
#include "syslog.h"
#include "yimpl_cli.h"
#include "syscfg.h"
#include "rmcp.h"
#include "fru_payload_ipmc_control.h"

extern bool mt_CLI_GetIntState();

// Defined in lwip.c file. Used here to access network resources.
extern struct netif gnetif;
extern struct yaffs_dev dev;

/*
 * Command "info"
 *
 * Prints general information about IPMC
 */
#define CMD_INFO_NAME "info"
#define CMD_INFO_DESCRIPTION "Print information about this IPMC."
#define CMD_INFO_CALLBACK info_cb
static uint8_t info_cb()
{
	// openIPMC-HW IDs
	uint32_t uIDw0 = HAL_GetUIDw0();
	uint32_t uIDw1 = HAL_GetUIDw1();
	uint32_t uIDw2 = HAL_GetUIDw2();

	// data variables to save FRU and Shelf address info.
	picmg_address_info_data_t fru_addr_data;
	picmg_shelf_address_info_data_t shelf_addr_data;
	int err_addr;
	int dhcp_on = net_get_is_ip_mode_dhcp();

	// Convert commit hash from hex to string.
	char commit_s[9];
	sprintf_(commit_s, "%08x", HEAD_COMMIT_SHA1);

	//Get MAC address
	uint8_t mac_addr[6];

	mac_addr[0] = gnetif.hwaddr[0];
  mac_addr[1] = gnetif.hwaddr[1];
  mac_addr[2] = gnetif.hwaddr[2];
  mac_addr[3] = gnetif.hwaddr[3];
  mac_addr[4] = gnetif.hwaddr[4];
  mac_addr[5] = gnetif.hwaddr[5];

	//Get IP address, mask and gateway
	ip4_addr_t const* ipaddr  = netif_ip4_addr   ( &gnetif );
	ip4_addr_t const* netmask = netif_ip4_netmask( &gnetif );
	ip4_addr_t const* gw      = netif_ip4_gw     ( &gnetif );

	mt_printf( "\r\n\n" );
	mt_printf( "OpenIPMC-HW\r\n" );
	mt_printf( " STM32 UniqueID: 0x%08X %08X %08X\r\n", uIDw0, uIDw1, uIDw2);
	mt_printf( " Firmware commit: %s\r\n", commit_s );
	mt_printf( "\r\n" );


	if (ipmc_device_id.device_id_string)
	{
		mt_printf( "Target Board: %s\r\n", ipmc_device_id.device_id_string );
	}
	else
	{
		mt_printf( "Target Board: [string not yet initialized]\r\n" );
	}


	// Get FRU address info.
	err_addr = picmg_get_address_info(&fru_addr_data);

	mt_printf("\r\n");
	if (err_addr == PICMG_ADDRESS_INFO_ERR_OK ) {

		mt_printf(" Hardware Address: %#04x \n\r", fru_addr_data.hardware_address);
		mt_printf(" IPMB-0 Address: %#04x \n\r", fru_addr_data.ipmb_0_address);
		mt_printf(" FRU Device ID # %#04x \n\r", fru_addr_data.fru_device_id);
		mt_printf(" Site Number: %d \n\r", fru_addr_data.site_number);
		mt_printf(" Site Type: %#04x \n\r", fru_addr_data.site_type);

	} else {
		mt_printf("\r\n");
		mt_printf(" FRU Address ERROR: Cannot get address information, try again.");
	}


	mt_printf( "\r\nNetwork:\r\n" );

	if(dhcp_on)
	{
	  mt_printf(" IP Source: DHCP\r\n");
	}
	else
	{
	  mt_printf(" IP Source: STATIC\r\n");
	}

	mt_printf( " MAC Addr: %02X:%02X:%02X:%02X:%02X:%02X\r\n", mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5] );
	mt_printf( " IP Addr: %d.%d.%d.%d\r\n",  ipaddr->addr&0xFF, ( ipaddr->addr>>8)&0xFF, ( ipaddr->addr>>16)&0xFF, ( ipaddr->addr>>24)&0xFF);
	mt_printf( " netmask: %d.%d.%d.%d\r\n", netmask->addr&0xFF, (netmask->addr>>8)&0xFF, (netmask->addr>>16)&0xFF, (netmask->addr>>24)&0xFF);
	mt_printf( " GW Addr: %d.%d.%d.%d\r\n",      gw->addr&0xFF, (     gw->addr>>8)&0xFF, (     gw->addr>>16)&0xFF, (     gw->addr>>24)&0xFF);

	mt_printf("\r\n");

	// Get Shelf Address.
	err_addr = picmg_get_shelf_address_info(&shelf_addr_data);

    if (err_addr == PICMG_ADDRESS_INFO_ERR_OK ) {
      mt_printf("Shelf Address: %s",shelf_addr_data.data);

    } else {
      mt_printf("Shelf Address ERROR: Cannot get address information, try again.");
    }


	return TE_OK;
}


/*
 * Command "handle"
 *
 * Force a state change for ATCA Face Plate Handle
 */
#define CMD_ATCA_HANDLE_NAME "handle"
#define CMD_ATCA_HANDLE_DESCRIPTION "\
Force a state change for ATCA Face Plate Handle.\r\n\
\t[ o | c ] for Open or Close."
#define CMD_ATCA_HANDLE_CALLBACK atca_handle_cb
static uint8_t atca_handle_cb()
{
	fru_transition_t fru_trigg_val;

	if ( CLI_IsArgFlag("o") )
	{
		fru_trigg_val = OPEN_HANDLE;
		xQueueSendToBack(queue_fru_transitions, &fru_trigg_val, 0UL);
	}
	else if ( CLI_IsArgFlag("c") ){
		fru_trigg_val = CLOSE_HANDLE;
		xQueueSendToBack(queue_fru_transitions, &fru_trigg_val, 0UL);
	}

	return TE_OK;
}


/*
 * Command "debug-ipmi"
 *
 * Enable to show the IPMI messaging from OpenIPMC
 */
#define CMD_DEBUG_IPMI_NAME "debug-ipmi"
#define CMD_DEBUG_IPMI_DESCRIPTION "\
Debug output from OpenIPMC.\r\n\
\t\t    <ipmb0>: add IPMB-0 bytes printout\r\n\
\t\t    Press ESC to stop"
#define CMD_DEBUG_IPMI_CALLBACK debug_ipmi_cb
extern int enable_ipmi_printouts;
static uint8_t debug_ipmi_cb()
{
	mt_printf( "\r\nDEBUG OUTPUT (press ESC to stop)\r\n" );

	enable_ipmi_printouts = 1; // Enable Debug
	ipmi_msg_debug_flag = 1;

	if( CLI_IsArgFlag("ipmb0") )
		ipmb_0_debug_flag = 1;




	while(1)
	{
		// Wait for ESC
		vTaskDelay( pdMS_TO_TICKS( 500 ) );
		if( mt_CLI_GetIntState() ){
			enable_ipmi_printouts = 0; // Disable debug
			ipmb_0_debug_flag = 0;
			ipmi_msg_debug_flag = 0;
			break;
		}
	}

	return TE_WorkInt;
}


/*
 * Command "bootloader"
 */
#define CMD_BOOT_NAME "bootloader"
#define CMD_BOOT_DESCRIPTION "\
Manage Bootloader.\r\n\
\t\tNo argument: Print Bootloader status\r\n\
\t\t     enable: Bootloader boots first after reset\r\n\
\t\t    disable: OpenIPMC-FW boots directly after reset"
#define CMD_BOOT_CALLBACK bootloader_cb
static uint8_t bootloader_cb()
{
	uint8_t major_ver; // (version info not used here)
	uint8_t minor_ver;
	uint8_t aux_ver[4];

	mt_printf( "\r\n\r\n" );

	if ( CLI_IsArgFlag("enable") )
	{
		if( bootloader_enable() == false )
			mt_printf( "Enabling failed!\r\n" );
	}
	else if( CLI_IsArgFlag("disable") )
	{
		if( bootloader_disable() == false )
			mt_printf( "Disabling failed!\r\n" );
	}

	mt_printf( "Bootloader is present in the Flash: " );
	if( bootloader_is_present( &major_ver, &minor_ver, aux_ver ) )
		mt_printf( "YES  ver:%d.%d.%d  %02x%02x%02x%02x\r\n", major_ver, (minor_ver>>4)&0x0F, (minor_ver)&0x0F, aux_ver[0], aux_ver[1], aux_ver[2], aux_ver[3] );
	else
		mt_printf( "NO\r\n" );

	mt_printf( "Run bootloader on boot is enabled: " );
	if( bootloader_is_active() )
		mt_printf( "YES\r\n" );
	else
		mt_printf( "NO\r\n" );

	return TE_OK;
}

#define CMD_SETMAC_NAME "setmac"
#define CMD_SETMAC_DESCRIPTION " \
Set MAC address.\r\n\
\t\t static: Set static mac: setmac static xx:xx:xx:xx:xx:xx\r\n \
\t\t chipid: built MAC address from chip GUID\r\n\
\t\t random: buil MAC address using random number generator\r\n\
Note: This commands executes IPMC Cold Reset to commit the new MAC address"
#define CMD_SETMAC_CALLBACK setmac_cb
static uint8_t setmac_cb()
{
  if( !strcmp("static", CLI_GetArgv()[1]))
  {
    if ( MAC_ADDR_STR_LEN_FROM_TERMINAL_CMD == strlen(CLI_GetArgv()[2]) )
    {
      if(!net_set_mac_static_from_str(CLI_GetArgv()[2]))
        return TE_ArgErr;
    }
  }
  else if(!strcmp("chipid", CLI_GetArgv()[1]))
  {
    if(!net_set_mac_chipid())
      return TE_ArgErr;
  }
  else if(!strcmp("random", CLI_GetArgv()[1]))
  {
    if(!net_set_mac_random())
      return TE_ArgErr;
  }
  else
  {
    mt_printf("Invalid argument\r\n");
    return TE_ArgErr;
  }
  return TE_OK;
}


#define CMD_DHCP_NAME "dhcp"
#define CMD_DHCP_DESCRIPTION "\
Manage DHCP.\r\n\
\t\t         on: Enable DHCP\r\n\
\t\t        off: Disable DHCP"
#define CMD_DHCP_CALLBACK dhcp_cb
static uint8_t dhcp_cb()
{
	if ( CLI_IsArgFlag("on") )
	{
		openipmc_hw_uses_dhcp = 1;
		if(!net_set_ip_mode_dhcp())
		  return TE_ExecErr;
	}
	else if ( CLI_IsArgFlag("off") )
	{
		openipmc_hw_uses_dhcp = 0;
		if(!net_set_ip_mode_static())
		  return TE_ExecErr;
	}
	else
	{
		mt_printf( "Invalid argument\r\n" );
	}

	return TE_OK;
}

#define CMD_SETIP_NAME "setip"
#define CMD_SETIP_DESCRIPTION "\
Set IP address STATIC.\r\n\
\t\t        Set the IP address, netmask and gateway\r\n\
\t\t         eg: setip 192.168.0.123 255.255.255.0 192.168.0.1"
#define CMD_SETIP_CALLBACK setip_cb
static uint8_t setip_cb()
{
	uint8_t ip[4], mask[4], gw[4];

	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[1], ip   ) != 0 )
		return TE_ArgErr;
	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[2], mask ) != 0 )
		return TE_ArgErr;
	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[3], gw   ) != 0 )
		return TE_ArgErr;

	if(!net_set_ip_mode_static())
	  return TE_ExecErr;

	if(!net_set_static_ip(ip,4))
	  return TE_ExecErr;

	if(!net_set_static_mask(mask,4))
	  return TE_ExecErr;

	if(!net_set_static_gateway_addr(gw,4))
	  return TE_ExecErr;

	return TE_OK;
}

#define CMD_TIME_NAME "time"
#define CMD_TIME_DESCRIPTION "\
Shows the ShMC and IPMC time. Set IPMC time.\r\n\
\t\t No argumet: Show time\r\n\
\t\t        set: Set IPMC time"
#define CMD_TIME_CALLBACK time_cb
extern RTC_HandleTypeDef hrtc;
static uint8_t time_cb()
{
	uint8_t  completion_code;
	uint8_t  response_bytes[24];
	int      res_len;
	time_t epoch;
	struct tm* time_tm;
	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};

	mt_printf("\r\n");

	// Get time from Shelf Manager (NetFn:0x0A, cmd:0x48)
	int ipmi_status = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0,
            0x0A, 0x48, NULL, 0, &completion_code, response_bytes, &res_len );

	// Print time from Shelf Manager
	if (ipmi_status == IPMI_MSG_SEND_OK)
	{
		epoch = (time_t)(((uint32_t)response_bytes[3]<<24) | ((uint32_t)response_bytes[2]<<16) | ((uint32_t)response_bytes[1]<<8) | ((uint32_t)response_bytes[0]<<0));
		time_tm = gmtime (&epoch);
		mt_printf("ShMC time: %.2d-%.2d-%.2d %.2d:%.2d:%.2d\r\n", time_tm->tm_year-100,  time_tm->tm_mon+1, time_tm->tm_mday, time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec);
	}
	else
		mt_printf("ShMC time: UNAVAILABLE\r\n");

	// Set the RTC time from ShMC
	if(CLI_IsArgFlag("set"))
	{
		if (ipmi_status == IPMI_MSG_SEND_OK)
		{
			sDate.Year    = time_tm->tm_year-100;
			sDate.Month   = time_tm->tm_mon+1;
			sDate.Date    = time_tm->tm_mday;
			sDate.WeekDay = 1; // Not used
			sTime.Hours   = time_tm->tm_hour;
			sTime.Minutes = time_tm->tm_min;
			sTime.Seconds = time_tm->tm_sec;
			HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
			HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
		}
	}
	else if (CLI_IsArgFlag("set-man"))
	{
		sDate.Year    = atoi( CLI_GetArgv()[2] );
		sDate.Month   = atoi( CLI_GetArgv()[3] );
		sDate.Date    = atoi( CLI_GetArgv()[4] );
		sDate.WeekDay = 1; // Not used
		sTime.Hours   = atoi( CLI_GetArgv()[5] );
		sTime.Minutes = atoi( CLI_GetArgv()[6] );
		sTime.Seconds = atoi( CLI_GetArgv()[7] );
		HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	}

	// Print RTC time
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	mt_printf("IPMC time: %.2d-%.2d-%.2d %.2d:%.2d:%.2d\r\n",  sDate.Year, sDate.Month, sDate.Date , sTime.Hours, sTime.Minutes, sTime.Seconds);

	return TE_OK;
}


/*
 * Command "reset"
 *
 * It replaces the native command "~"
 */
#define CMD_RESET_NAME "reset"
#define CMD_RESET_DESCRIPTION "\
Resets the IPMC or the Payload as specified by IPMI and PICMG. Usage:\r\n\
\t 1) reset ipmc cold        : IPMI-compliant IPMC cold reset\r\n\
\t 2) reset ipmc warm        : IPMI-compliant IPMC warm reset\r\n\
\t 3) reset payload cold     : PICMG-compliant Payload cold reset\r\n\
\t 4) reset payload warm     : PICMG-compliant Payload warm reset\r\n\
\t 5) reset payload graceful : PICMG-compliant Payload graceful reboot\r\n"
#define CMD_RESET_CALLBACK reset_cb
static uint8_t reset_cb()
{
  int const argc = CLI_GetArgc();

  if (argc < 3)
    return TE_ArgErr;

  if ( !strcmp("ipmc", CLI_GetArgv()[1]) )
  {
    if ( !strcmp("cold", CLI_GetArgv()[2]) )
    {
      syslog_push("debug", "IPMC Cold Reset: request via CLI.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
      mt_printf("\r\nIPMC Cold Reset: request via CLI.");
      uint8_t compl_code;
      do_begin_ipmc_cold_reset(&compl_code);
      if (compl_code != 0x00)
      {
        syslog_push("debug", "IPMC Cold Reset: cannot initiate.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
        mt_printf("\r\nIPMC Cold Reset: cannot initiate.");
      }
    }
    else if ( !strcmp("warm", CLI_GetArgv()[2]) )
    {
      syslog_push("debug", "IPMC Warm Reset: request via CLI.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
      mt_printf("\r\nIPMC Warm Reset: request via CLI.");
      uint8_t compl_code;
      do_begin_ipmc_warm_reset(&compl_code);
      if (compl_code != 0x00)
      {
        syslog_push("debug", "IPMC Warm Reset: cannot initiate.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
        mt_printf("\r\nIPMC Warm Reset: cannot initiate.");
      }
    }
  }
  else if ( !strcmp("payload", CLI_GetArgv()[1]) )
  {
    if ( !strcmp("cold", CLI_GetArgv()[2]) )
    {
      syslog_push("debug", "Payload Cold Reset: request via CLI.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
      mt_printf("\r\nPayload Cold Reset: request via CLI.");
      uint8_t compl_code;
      do_begin_payload_cold_reset(&compl_code);
      if (compl_code != 0x00)
      {
        syslog_push("debug", "Payload Cold Reset: cannot initiate.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
        mt_printf("\r\nPayload Cold Reset: cannot initiate.");
      }
    }
    else if ( !strcmp("warm", CLI_GetArgv()[2]) )
    {
      syslog_push("debug", "Payload Warm Reset: request via CLI.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
      mt_printf("\r\nPayload Warm Reset: request via CLI.");
      uint8_t compl_code;
      do_begin_payload_warm_reset(&compl_code);
      if (compl_code != 0x00)
      {
        syslog_push("debug", "Payload Warm Reset: cannot initiate.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
        mt_printf("\r\nPayload Warm Reset: cannot initiate.");
      }
    }
    else if ( !strcmp("graceful", CLI_GetArgv()[2]) )
    {
      syslog_push("debug", "Payload Graceful Reboot: request via CLI.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
      mt_printf("\r\nPayload Graceful Reboot: request via CLI.");
      uint8_t compl_code;
      do_begin_payload_graceful_reboot(&compl_code);
      if (compl_code != 0x00)
      {
        syslog_push("debug", "Payload Graceful Reboot: cannot initiate.", SYSFAC_USER_LVL, SYSSEV_DEBUG);
        mt_printf("\r\nPayload Graceful Reboot: cannot initiate.");
      }
    }
    else
      return TE_ArgErr;
  }
  else
    return TE_ArgErr;
  return TE_OK;
}

/*
 * Command syslog
 */
#define CMD_SYSLOG_NAME "syslog"
#define CMD_SYSLOG_DESCRIPTION "\
Configures syslog client's server information on file system.\r\n\
The path /etc/syslog MUST exist! \r\n\
Available commands: \r\n\
\t1. sendtest : sends a test message to syslog server\r\n\
\t2. status   : prints syslog client status information \r\n\
\t3. reload   : refreshes syslog udp settings from configuration files \r\n\
\t4. verbose on  : sets verbosity to 1 \r\n\
\t5. verbose off : sets verbosity to 0 \r\n\
\t5. udpmsg disable : disables sending of messages via UDP\r\n\
\t5. udpmsg enable  : enables sending of messages via UDP (default)\r\n"
#define CMD_SYSLOG_CALLBACK syslog_cb
static uint8_t syslog_cb(void)
{
  int const argc = CLI_GetArgc();

  if (argc < 2)
  {
    goto syslog_cb_error_missing_arguments;
  }

  /* Sends a test message through syslog client */
  if (!strcmp("sendtest", CLI_GetArgv()[1]))
  {
    syslog_push("debug", "Syslog CLI test message", SYSFAC_USER_LVL, SYSSEV_DEBUG);
  }

  /* Checks syslog client status */
  else if (!strcmp("status", CLI_GetArgv()[1]))
  {
    char const* str_server_status;
    char const* str_server_addr;
    char const* str_server_port;

    syslog_status_as_string(&str_server_status);
    syslog_ip_addr_as_string(&str_server_addr);
    syslog_ip_port_as_string(&str_server_port);

    mt_printf("\r\nsyslog status: %s\r\nsyslog server address: %s\r\nsyslog server port: %s",
      str_server_status, str_server_addr, str_server_port);
  }
  else if (!strcmp("reload", CLI_GetArgv()[1]))
  {
    syslog_reload_config();
  }
  else if (!strcmp("verbose",  CLI_GetArgv()[1]))
  {
    if (argc < 3)
    {
      goto syslog_cb_error_missing_arguments;
    }

    if (!strcmp("on", CLI_GetArgv()[2]))
    {
      syslog_set_verbosity(1);
      mt_printf("\r\nVerbose mode enabled");
    }
    else if (!strcmp("off", CLI_GetArgv()[2]))
    {
      syslog_set_verbosity(0);
      mt_printf("\r\nVerbose mode disabled");
    }
    else
    {
      goto syslog_cb_error_unknown_command;
    }
  }
  else if (!strcmp("udpmsg",  CLI_GetArgv()[1]))
  {
    if (argc < 3)
    {
      goto syslog_cb_error_missing_arguments;
    }

    if (!strcmp("disable", CLI_GetArgv()[2]))
    {
      syslog_set_disable_udp_messages(1);
      mt_printf("\r\nSyslog message sending disabled");
    }
    else if (!strcmp("enable", CLI_GetArgv()[2]))
    {
      syslog_set_disable_udp_messages(0);
      mt_printf("\r\nSyslog message sending enabled");
    }
    else
    {
      goto syslog_cb_error_unknown_command;
    }
  }
  else
  {
    goto syslog_cb_error_unknown_command;
  }

  // Default return value is a success
  return TE_OK;

  // NOTE: the two below are among the few legit uses of goto
  // and they were chosen as they save program space

  // Manage case of less than expected argc
syslog_cb_error_missing_arguments:
  mt_printf("\r\nSyslog CLI: not enough arguments");
  return TE_ArgErr;

  // Manage case of unknown commands
syslog_cb_error_unknown_command:
  mt_printf("\r\nSyslog CLI: unknown command");
  return TE_ArgErr;
}

#define CMD_SYSCFG_NAME "syscfg"
#define CMD_SYSCFG_DESCRIPTION "\
Sets the configuration options and limits for openIPMC services.\r\n\
syscfg uses \"/etc\" directory for managing the system service's files.\r\n\
Takes following arguments:\r\n\
\t set [\"key\"] [\"value\"] : Inserts a configuration *key* with given *value* \r\n\
\t\t If *key* exists, it will replace its value at the list.\r\n\
\t\t Keys are written as \"service.parameter\" format and values are a \r\n\
\t\t string storing the desired parameter value. This is important as \r\n\
\t\t syscfg service will parse the information based on this format.\r\n\
\t get [\"key\"] : Gets *value* assigned to a specific configuration *key*, or \r\n\
\t\t throws an error in case there is no key in the database. \r\n\
\t ls : Lists all keys present on system.\r\n\
\t save : Saves current configuration state on file system.\r\n\
\t rm [\"key\"] : Removes configuration node from file system based on given key.\r\n\
\t e.g: syscfg set tftp.ip 192.168.0.1 "

#define CMD_SYSCFG_CALLBACK syscfg_cb

static uint8_t syscfg_cb(void)
{
  char* key = NULL;
  char* value = NULL;
  int err = 0;
  int i = 1;
  uint8_t retval = TE_ArgErr;

  if( 0 == strcmp(CLI_GetArgv()[i], "set") )
  {
    if (CLI_GetArgc() == 4)
    {
      key = CLI_GetArgv()[++i];
      value = CLI_GetArgv()[++i];
      syscfg_insert(key, value);
      retval = TE_OK;
    }
    else
    {
      mt_printf("syscfg set: wrong number of arguments\r\n");
      retval = TE_ArgErr;
    }
  }
  else if (0 == strcmp(CLI_GetArgv()[i], "get") )
  {
    if (CLI_GetArgc() == 3)
    {
      size_t valuesize;
      if (SYSCFG_SUCCESS == syscfg_get_valuesize_from_key(CLI_GetArgv()[2], &valuesize))
      {
        char value[SYSCFG_VALUE_MAXLEN + 1];
        syscfg_get_value_from_key(CLI_GetArgv()[2], value, SYSCFG_VALUE_MAXLEN + 1);
        mt_printf("value of \"%s\" = \"%s\"\r\n", CLI_GetArgv()[2], value);
      }
      else
      {
        mt_printf("syscfg get: key not in database\r\n");
        retval = TE_ExecErr;
      }
    }
    else
    {
      mt_printf("syscfg get: wrong number of arguments\r\n");
      retval = TE_ArgErr;
    }
  }
  else if (0 == strcmp(CLI_GetArgv()[i], "ls") )
  {
    syscfg_print_node_list();
    retval = TE_OK;
  }
  else if (0 == strcmp(CLI_GetArgv()[i], "save") )
  {
    if (0 > ( err = syscfg_save() ) )
    {
      mt_printf("syscfg: could not save configuration state, returned with error code %d\r\n", err);
      retval = TE_ExecErr;
    }
    else
    {
      retval = TE_OK;
    }
  }
  else if (0 == strcmp(CLI_GetArgv()[i], "rm") )
  {
    if (CLI_GetArgc() == 3)
    {
      key = CLI_GetArgv()[++i];
      if (0 > ( err = syscfg_remove(key) ) )
      {
        mt_printf("syscfg: could not remove node, returned with error code %d\r\n", err);
        retval = TE_ExecErr;
      }
      else
      {
        retval = TE_OK;
      }
    }
    else
    {
      mt_printf("syscfg del: wrong number of arguments\r\n");
      retval = TE_ArgErr;
    }
 }
 return retval;
}

#define CMD_RMCP_NAME "rmcp"
#define CMD_RMCP_DESCRIPTION "Gets information about current active session.\n\
  Takes following arguments\n\
  \t -status: Prints session status from current active session.\n"
#define CMD_RMCP_CALLBACK rmcp_info_cb
#define CMD_RMCP_SESSION_PRINTOUT_FMT "\nactive ipmi session:\n\
  ~ user: %s\n\
  ~ user privilege: %.2x\n\
  ~ session privilege: %.2x\n\
  ~ remote client id: %.8x\n\
  ~ active session id: %.8x\n\
  ~ remote client address: %u.%u.%u.%u\n\
  ~ remote client port: %u\n"
static uint8_t rmcp_info_cb(void)
{
 rmcp_ipmi_session_handle_t h = {0UL};
  uint8_t retval = TE_ArgErr;

  if(CLI_IsArgFlag("-status"))
  {
    retval = TE_OK;
    if(rmcp_get_a_copy_session_status(&h))
    {
      uint32_t ip_addr = h.client_net_params.ip_addr.ipv4;
      uint16_t port    = h.client_net_params.port;

      mt_printf(CMD_RMCP_SESSION_PRINTOUT_FMT,
          h.user->username,
          h.user->user_privilege_code,
          h.privilege,
          h.remote_console_id,
          h.id,
          ((ip_addr >> 0) & 0xFF),
          ((ip_addr >> 8) & 0xFF),
          ((ip_addr >> 16) & 0xFF),
          ((ip_addr >> 24) & 0xFF),
		  port);
    }
    else
    {
      mt_printf("\nNo RMCP/IPMI active session.");
    }
  }
  return retval;
}



/*
 * Add commands to the CLI
 */
void add_terminal_commands( void )
{
	CLI_AddCmd( CMD_INFO_NAME,        CMD_INFO_CALLBACK,        0, 0, CMD_INFO_DESCRIPTION          );
	CLI_AddCmd( CMD_ATCA_HANDLE_NAME, CMD_ATCA_HANDLE_CALLBACK, 1, 0, CMD_ATCA_HANDLE_DESCRIPTION   );
	CLI_AddCmd( CMD_DEBUG_IPMI_NAME,  CMD_DEBUG_IPMI_CALLBACK,  0, 0, CMD_DEBUG_IPMI_DESCRIPTION    );
	CLI_AddCmd( CMD_BOOT_NAME,        CMD_BOOT_CALLBACK,        0, TMC_None, CMD_BOOT_DESCRIPTION   );
	CLI_AddCmd( CMD_SETMAC_NAME,      CMD_SETMAC_CALLBACK,      1, TMC_None, CMD_SETMAC_DESCRIPTION );
	CLI_AddCmd( CMD_DHCP_NAME,        CMD_DHCP_CALLBACK,        1, TMC_None, CMD_DHCP_DESCRIPTION   );
	CLI_AddCmd( CMD_SETIP_NAME,       CMD_SETIP_CALLBACK,       3, TMC_None, CMD_SETIP_DESCRIPTION  );
	CLI_AddCmd( CMD_TIME_NAME,        CMD_TIME_CALLBACK,        0, TMC_None, CMD_TIME_DESCRIPTION   );
	CLI_AddCmd( CMD_RESET_NAME,       CMD_RESET_CALLBACK,       0, TMC_None, CMD_RESET_DESCRIPTION  );

	/* --------------------------------- YAFFS file system callbacks --------------------------------- */
	CLI_AddCmd(YIMPL_CLI_FORMAT_NAME  ,YIMPL_CLI_FORMAT_CALLBACK , 0, TMC_None, YIMPL_CLI_FORMAT_DESCRIPTION );
	CLI_AddCmd(YIMPL_CLI_MOUNT_NAME   ,YIMPL_CLI_MOUNT_CALLBACK  , 0, TMC_None, YIMPL_CLI_MOUNT_DESCRIPTION  );
	CLI_AddCmd(YIMPL_CLI_UMOUNT_NAME  ,YIMPL_CLI_UMOUNT_CALLBACK , 0, TMC_None, YIMPL_CLI_UMOUNT_DESCRIPTION );
	CLI_AddCmd(YIMPL_CLI_MKDIR_NAME   ,YIMPL_CLI_MKDIR_CALLBACK  , 1, TMC_None, YIMPL_CLI_MKDIR_DESCRIPTION  );
	CLI_AddCmd(YIMPL_CLI_REMOVE_NAME  ,YIMPL_CLI_REMOVE_CALLBACK , 1, TMC_None, YIMPL_CLI_REMOVE_DESCRIPTION );
	CLI_AddCmd(YIMPL_CLI_LIST_NAME    ,YIMPL_CLI_LIST_CALLBACK   , 0, TMC_None, YIMPL_CLI_LIST_DESCRIPTION   );
	CLI_AddCmd(YIMPL_CLI_WRITE_NAME   ,YIMPL_CLI_WRITE_CALLBACK  , 3, TMC_None, YIMPL_CLI_WRITE_DESCRIPTION  );
	CLI_AddCmd(YIMPL_CLI_PRINT_NAME   ,YIMPL_CLI_PRINT_CALLBACK  , 1, TMC_None, YIMPL_CLI_PRINT_DESCRIPTION  );
	CLI_AddCmd(YIMPL_CLI_CHMOD_NAME   ,YIMPL_CLI_CHMOD_CALLBACK  , 2, TMC_None, YIMPL_CLI_CHMOD_DESCRIPTION  );
	CLI_AddCmd(YIMPL_CLI_MEMSTAT_NAME ,YIMPL_CLI_MEMSTAT_CALLBACK, 0, TMC_None, YIMPL_CLI_MEMSTAT_DESCRIPTION);
	CLI_AddCmd(YIMPL_CLI_READBBM_NAME ,YIMPL_CLI_READBBM_CALLBACK, 0, TMC_None, YIMPL_CLI_READBBM_DESCRIPTION);
	/* ----------------------------------------------------------------------------------------------- */
	CLI_AddCmd( CMD_SYSCFG_NAME,	CMD_SYSCFG_CALLBACK,	1,	TMC_None,	CMD_SYSCFG_DESCRIPTION);
	CLI_AddCmd( CMD_SYSLOG_NAME,    CMD_SYSLOG_CALLBACK,	1,	TMC_None,	CMD_SYSLOG_DESCRIPTION);
	CLI_AddCmd( CMD_RMCP_NAME, CMD_RMCP_CALLBACK, 1, TMC_None, CMD_RMCP_DESCRIPTION);
}
