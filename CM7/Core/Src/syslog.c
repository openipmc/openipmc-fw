/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *	@file	syslog.c
 *	@brief	Syslog protocol source code.
 *	@date	YYYY / MM / DD - 2022 / 08 / 30
 *	@author	Antonio V. G. Bassi, Luigi Calligaris
 */

/* standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/* private resources */
#include "main.h"
#include "openipmc_fw_utils.h"
#include "mt_printf.h"
#include "clientid_cms.h"
#include "network_ctrls.h"
#include "stm32h7xx_hal.h"
#include "rtc_tools.h"
#include "lwip.h"
#include "lwip/udp.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "yaffsfs.h"
#include "yaffs_guts.h"
#include "yaffsfs_errno.h"
#include "yimpl_conf.h"
#include "syscfg.h"
#include "syslog.h"

#define RFC5424_TIMESTAMP_FORMAT "%.4d-%.2d-%.2dT%.2d:%.2d:%.2d%s"
#define RFC3164_TIMESTAMP_FORMAT "%s %d %.2d:%.2d:%.2d"
#define MILLENIUM	2000

static char const SYSLOG_MSG_CONFIG_MISSING[] =
  "\r\nsyslog: Failed to retrieve syslog client configuration. check configuration file at /etc or "
  "use \"syscfg set \" and \"syslog udp\" command-lines respectively for adding configuration "
  "files for syslog and update the udp settings on the syslog client service";
static char const SYSLOG_MSG_CONFIG_WRONG[] =
  "\r\nsyslog: Configuration parameters are invalid";
static char const SYSLOG_MSG_UDP_INIT_FAIL[] = "\nsyslog: Failed to set up UDP protocol infrastructure";

static char const SYSLOG_STATUSMSG_CLIENT_UP[]     = "Client running";
static char const SYSLOG_STATUSMSG_CLIENT_DOWN[]   = "Client stopped";
static char const SYSLOG_STATUSMSG_NO_IP[]         = "NOCFG";
static char const SYSLOG_STATUSMSG_NO_PORT[]       = "NOCFG";
static char const SYSLOG_EMPTY_STR[]         = "";

/* ****************************************************************************************************************
 * Syslog format configuration
 * Currently, syslog has 2 different formats issued by IETF: the BSD syslog (referred to as RFC3164 syslog) and
 * the most recent format RFC5424.
 * Syslog format can be configured by the following macros:
 *
 * SYSLOG_USES_RFC5424_FORMAT
 * SYSLOG_USES_RFC5424_FORMAT
 *
 * The user should use only one format. If more than one macro is defined the compiler will throw an error.
 * If no format is specified RFC3164 is default.
 * ****************************************************************************************************************
 */

#if defined(SYSLOG_USES_RFC5424_FORMAT) && defined(SYSLOG_USES_RFC3164_FORMAT)
#error Error: both SYSLOG_USES_RFC5424_FORMAT and SYSLOG_USES_RFC3164_FORMAT are defined, Choose only one or leave undefined.
#endif /* defined(SYSLOG_IS_RFC5424) && defined(SYSLOG_IS_RFC3164) */

// Allow for a default
#if !defined(SYSLOG_USES_RFC5424_FORMAT) && !defined(SYSLOG_USES_RFC3164_FORMAT)
#define SYSLOG_FORMAT_IS_DEFAULT
#endif /* SYSLOG_DEFAULT_FORMAT */

#ifdef SYSLOG_FORMAT_IS_DEFAULT
#undef SYSLOG_FORMAT_IS_RFC5424
#undef SYSLOG_FORMAT_IS_RFC3164
#define SYSLOG_FORMAT_IS_RFC3164
#endif /* SYSLOG_DEFAULT_FORMAT */

#if defined(SYSLOG_USES_RFC5424_FORMAT) && !defined(SYSLOG_USES_RFC3164_FORMAT)
#define SYSLOG_FORMAT_IS_RFC5424
#endif /* defined(SYSLOG_USES_RFC5424_FORMAT) && !defined(SYSLOG_USES_RFC3164_FORMAT) */

#if !defined(SYSLOG_USES_RFC5424_FORMAT) && defined(SYSLOG_USES_RFC3164_FORMAT)
#define SYSLOG_FORMAT_IS_RFC3164
#endif /* defined(SYSLOG_USES_RFC5424_FORMAT) && !defined(SYSLOG_USES_RFC3164_FORMAT) */

/* ****************************************************************************************************************
 * Hostname configuration
 * syslog will either use the client id string field or the IPv4 address by defining one of the following macros
 *
 * SYSLOG_USES_IP_HOSTNAME
 * SYSLOG_USES_CLIENTID_HOSTNAME
 *
 * The user should choose only one hostname, if more than one is defined the compiler will throw an error.
 * If no hostname is specified IPv4 is the default choice.
 * ****************************************************************************************************************
 */
// Check if conflicting choices are set
#if defined(SYSLOG_USES_IP_HOSTNAME) && defined(SYSLOG_USES_CLIENTID_HOSTNAME)
#error Error: both SYSLOG_USES_IP_HOSTNAME and SYSLOG_USES_CLIENTID_HOSTNAME are defined. Choose only one or leave undefined for default.
#endif /* defined(SYSLOG_USES_IP_HOSTNAME) && defined(SYSLOG_USES_CLIENTID_HOSTNAME) */

// Allow for a default
#if !defined(SYSLOG_USES_IP_HOSTNAME) && !defined(SYSLOG_USES_CLIENTID_HOSTNAME)
#define SYSLOG_HOSTNAME_IS_DEFAULT
#endif /* !defined(SYSLOG_USES_IP_HOSTNAME) && !defined(SYSLOG_USES_CLIENTID_HOSTNAME) */

#ifdef SYSLOG_HOSTNAME_IS_DEFAULT
#undef SYSLOG_HOSTNAME_IS_CLIENTID
#undef SYSLOG_HOSTNAME_IS_IPV4
#define SYSLOG_HOSTNAME_IS_IPV4
#endif /* SYSLOG_HOSTNAME_IS_DEFAULT */

#if defined(SYSLOG_USES_IP_HOSTNAME) &&  !defined(SYSLOG_USES_CLIENTID_HOSTNAME)
#define SYSLOG_HOSTNAME_IS_IPV4
#endif /* defined(SYSLOG_USES_IP_HOSTNAME) &&  !defined(SYSLOG_USES_CLIENTID_HOSTNAME) */

#if !defined(SYSLOG_USES_IP_HOSTNAME) &&  defined(SYSLOG_USES_CLIENTID_HOSTNAME)
#define SYSLOG_HOSTNAME_IS_CLIENTID
#endif /* !defined(SYSLOG_USES_IP_HOSTNAME) &&  defined(SYSLOG_USES_CLIENTID_HOSTNAME) */

#ifdef SYSLOG_USES_CLIENTID_HOSTNAME
#define SYSLOG_CLIENTID_STR_OFFSET 19
#define SYSLOG_CLIENTID_STR_LEN    20
#endif /*SYSLOG_USES_CLIENTID_HOSTNAME*/

/* ****************************************************************************************************************a
 * FreeRTOS resources
 * ****************************************************************************************************************
 */
char    __attribute__((section(".syslog_sec"))) syslog_buffer[SYSLOG_BUFFER_SIZE]; // Syslog message buffer
uint8_t __attribute__((section(".syslog_sec"))) syslog_queue[SYSLOG_FREERTOSQUEUE_SIZE]; // FreeRTOS static syslog queue buffer

static StaticQueue_t syslog_static_queue_handle;
static QueueHandle_t syslog_queue_handle = NULL;

/* ****************************************************************************************************************
 * Real-Time Clock Resources
 * ****************************************************************************************************************
 */
extern RTC_HandleTypeDef hrtc;

/* ****************************************************************************************************************
 * LwIP resources
 * ****************************************************************************************************************
 */
extern struct netif gnetif; // Global network interface struct
static struct udp_pcb* syslog_upcb = NULL; // UDP protocol control block

/* ****************************************************************************************************************
 * YAFFS resources
 * ****************************************************************************************************************
 */
extern struct yaffs_dev dev;

/* ****************************************************************************************************************
 * Client state is held by this struct
 * ****************************************************************************************************************
 */

typedef struct syslog_client_state_s
{
  int8_t    udp_status; // client UDP status flag
  int8_t    init_status; // client initialisation status flag
  int8_t    disable_udp_messages; // this user-switchable flag disables the sending of syslog messages
  int8_t    syslog_verbosity; // Verbosity of the client output
  uint16_t  syslog_server_port_num; // Server port, IANA specifies 514
  ip_addr_t syslog_server_addr_num; // Server IPv4 address
  char*     status_msg;
  char      syslog_server_addr_str[16];
  char      syslog_server_port_str[6];
} syslog_state_t;

static syslog_state_t client_state = {0};

/* ****************************************************************************************************************
 * syslog client function prototypes
 * ****************************************************************************************************************
 */

// Public functions declared in header
void syslog_ip_addr_as_string(char const** p_to_string_addr);
void syslog_ip_port_as_string(char const** p_to_string_addr);
void syslog_status_as_string(char const** p_to_string_addr);

syslog_err_t syslog_init(void);
syslog_err_t syslog_reload_config(void);

void syslog_set_verbosity(int8_t verbosity);

syslog_err_t syslog_push(char* msg_id, char* message,
                         syslog_facility_t facility,
                         syslog_severity_t severity);
syslog_err_t syslog_push_from_isr(char* msg_id, char* message,
                                  syslog_facility_t facility,
                                  syslog_severity_t severity);

void syslog_client_task(void* argument);

// Private functions restricted to this compilation unit
static syslog_err_t  syslog_load_config(void);
static syslog_err_t  syslog_clear_config(void);
static syslog_err_t  syslog_udp_init(void);
static syslog_err_t  syslog_udp_deinit(void);

static syslog_err_t  syslog_send(const ip_addr_t *addr, u16_t port, char* syslog_buffer);
static int           syslog_compute_prival(syslog_facility_t facility, syslog_severity_t severity);
static void          syslog_fetch_hostname(char* hoststr);
static void          syslog_fetch_timestamp(char* tstamp);
static uint16_t      syslog_build_string(syslog_msg_info_t* syslog_info, char* syslog_buffer);
static void          syslog_build_info(char* msg_id, char* message,
                                       syslog_facility_t facility,
                                       syslog_severity_t severity,
                                       syslog_msg_info_t* syslog_info);




/* ****************************************************************************************************************
 * syslog public client function implementations
 * ****************************************************************************************************************
 */

/**
* @name	syslog_ip_addr_as_string
* @brief	This function returns through the "p_to_string_addr" output argument the currently configured syslog server IP address
* @param	p_to_string_addr Pointer to char array holding the address string
* @return void
*/
void syslog_ip_addr_as_string(char const** p_to_string_addr)
{
  *p_to_string_addr = client_state.syslog_server_addr_str;
}

/**
* @name	syslog_ip_port_as_string
* @brief	This function returns through the "p_to_string_addr" output argument the currently configured syslog server UDP port
* @param	p_to_string_addr Pointer to char array holding the port string
* @return void
*/
void syslog_ip_port_as_string(char const** p_to_string_addr)
{
  *p_to_string_addr = client_state.syslog_server_port_str;
}


/**
* @name	syslog_status_as_string
* @brief	This function returns through the "p_to_string_addr" output argument the current status string
* @param	p_to_string_addr Pointer to char array holding the status string
* @return void
*/
void syslog_status_as_string(char const** p_to_string_addr)
{
  *p_to_string_addr = client_state.status_msg;
}


/**
 * @name	syslog_init
 * @brief	Initialises syslog client resources. This function should be called before using syslog_push to
 *  send syslog messages.
 * @param	void
 * @return syslog_err_t Error code
 */
syslog_err_t syslog_init(void)
{
  // Lazy initialization
  if (0 == client_state.init_status)
  {
    // Checks if the file system is installed.
	if(!yimpl_setup_ready())
	  return SYSLOG_ERR_FS_NOT_AVAILABLE;

    /* Initialise FreeRTOS resources */
    memset((void*)(syslog_queue),  0, (size_t)(SYSLOG_FREERTOSQUEUE_SIZE));
    memset((void*)(syslog_buffer), 0, (size_t)(SYSLOG_BUFFER_SIZE));
    syslog_queue_handle = xQueueCreateStatic(SYSLOG_QUEUE_ITEM_LENGTH, sizeof(syslog_msg_info_t),
                                             syslog_queue, &syslog_static_queue_handle);

    syslog_err_t ret;
    ret = syslog_load_config();
    if (SYSLOG_ERR_OK != ret) return ret;

    ret = syslog_udp_init();
    if (SYSLOG_ERR_OK != ret) return ret;

    client_state.init_status = 1;
  }

  return SYSLOG_ERR_OK;
}


/**
* @name	syslog_reload_config
* @brief Updates the server ip address and port number used by the client.
* @return syslog_err_t
*/
syslog_err_t syslog_reload_config(void)
{
  syslog_err_t ret;
  ret = syslog_init();
  if (SYSLOG_ERR_OK != ret) return ret;

  ret = syslog_clear_config();
  if (SYSLOG_ERR_OK != ret) return ret;

  ret = syslog_udp_deinit();
  if (SYSLOG_ERR_OK != ret) return ret;

  ret = syslog_load_config();
  if (SYSLOG_ERR_OK != ret) return ret;

  ret = syslog_udp_init();
  if (SYSLOG_ERR_OK != ret) return ret;

  return SYSLOG_ERR_OK;
}


/**
 * @name syslog_set_verbosity
 * @brief Sets the verbosity for the syslog client
 * @param	int8_t verbosity Chosen verbosity level. Zero means non verbose.
 */
void syslog_set_verbosity(int8_t verbosity)
{
  client_state.syslog_verbosity = verbosity;
}

/**
 * @name syslog_get_verbosity
 * @brief Returns the verbosity for the syslog client
 * @param	int8_t verbosity Chosen verbosity level. Zero means non verbose.
 */
void syslog_get_verbosity(int8_t* verbosity)
{
  *verbosity = client_state.syslog_verbosity;
}


/**
 * @name syslog_set_disable_udp_messages
 * @brief Allows to explicitly disable the sending of UDP messages by the syslog client
 * @param	int8_t disable_flag. Zero means enable, nonzero means disable.
 */
void syslog_set_disable_udp_messages(int8_t disable_flag)
{
  client_state.disable_udp_messages = disable_flag;
}

/**
 * @name syslog_get_disable_udp_messages
 * @brief Allows to get the flag disabling the UDP messages
 * @param	int8_t disable_flag. Zero means enable, nonzero means disable.
 */
void syslog_get_disable_udp_messages(int8_t* disable_flag)
{
  *disable_flag = client_state.disable_udp_messages;
}



/**
 * @name	syslog_push
 * @brief	Pushes syslog message into client's queue to be sent.
 * 			This function should be called at the condition of failure or notification of interest. e.g:
 * 					void your_function(int arg)
 * 					{
 * 						[...]
 * 						if( condition )
 * 						{
 * 							syslog_push_into_queue("your_function", "debug", "example of usage.",
 * 													SYSFAC_LOCAL_0, SYSSEV_DEBUG);
 * 						}
 * 						[...]
 * 					}
 * 			After calling syslog_push_into_queue your message will be placed in the queue and dealt with by the syslog client task.
 *
 * @note	Before using this API function. Some considerations must be taken first.
 * 			  Depending on how we configure our syslog client we may use two different formats:
 * 			  the BSD syslog format (referred as RFC3164 format) and the RFC5424 format.
 * 			  You may configure which format you desire in compile time by specific macros (check the beginning of this file)
 *			  We don't work with process id's on our client so this field will be omitted by a NILVALUE on RFC5424
 *			  and by simply not transmitting it on RFC3164.
 *			  The default choice is RFC3164 format.
 *
 *			  The hostname used by our syslog client will be either the CLIENT ID option sent on dhcp or the IPv4 address
 *			  of the device. This can be configured by compile time using the specified macros (check the beginning of this file)
 *			  The default choice is IPv4.
 *
 *			  The application name which has issued the syslog message is automatically fetched by this API.
 *
 * @note	Beware that this function DOES NOT OWN the strings to be sent,
 * 			  you need to take care of memory allocation and placement (DTCM, AXI, Flash...)
 * 			  The standard requires the following lengths for the strings.
 *
 * 				APP-NAME max  48 bytes (If a task named on FreeRTOS has a string name exceeding this, it will be replaced.)
 * 				MSGID    max  32 bytes
 *
 * 			  The message length is implementation defined, openIPMC uses 256 bytes for the syslog message itself.
 * 			  Any violation of these lengths will make the client ignore the fields by either replacing
 * 			  the string with a horizontal dash "-" or a default prompt informing the string was invalid.
 *
 * @param char*             msg_id    A message identifier. May be used to further specify the content of the message.
 *                                    If not used, provide the argument with NULL.
 *
 * @param char*             message   Syslog message string.
 *
 * @param syslog_facility_t facility  Integer code indicating which software layer the message is coming from.
 *
 * @param syslog_severity_t severity  Integer code indicating how severe is the message.
 *
 * @note	check syslog_facility_t and syslog_severity_t structs on syslog.h for more information about the codes.
 *
 * @return void Error code
 */
syslog_err_t syslog_push(char* msg_id, char* message,
                         syslog_facility_t facility,
                         syslog_severity_t severity)
{
  if (syslog_init() != SYSLOG_ERR_OK) // Lazy initialization
    return SYSLOG_ERR_UNINITIALIZED;

  // If the user explicitly disabled the client, do nothing and do it silently
  if (1 == client_state.disable_udp_messages)
    return SYSLOG_ERR_OK;

  syslog_msg_info_t syslog_info;
  syslog_build_info(msg_id, message, facility, severity, &syslog_info);
  xQueueSend(syslog_queue_handle, (void*)(&syslog_info), portMAX_DELAY);

  return SYSLOG_ERR_OK;
}


/**
 * @name  syslog_push_from_isr
 * @brief Sends a syslog message from an interrupt status register exception.
 * @return syslog_err_t Error code
 */
syslog_err_t syslog_push_from_isr(char* msg_id, char* message,
                                  syslog_facility_t facility,
                                  syslog_severity_t severity)
{
  // As we are in called from an ISR, avoid any printouts or anything else using the MCU peripherals
  // If Syslog is still unconfigured, do nothing.
  if ( (0 == client_state.udp_status) || (0 == client_state.init_status) )
    return SYSLOG_ERR_UNINITIALIZED;

  // If the user explicitly disabled the client, do nothing and do it silently
  if (1 == client_state.disable_udp_messages)
    return SYSLOG_ERR_OK;

  syslog_msg_info_t syslog_info;
  syslog_build_info( msg_id, message, facility, severity, &syslog_info );
  xQueueSendFromISR(syslog_queue_handle, (void*)(&syslog_info), NULL);

  return SYSLOG_ERR_OK;
}




/* ****************************************************************************************************************
 * syslog private client function implementations
 * ****************************************************************************************************************
 */

/**
* @name syslog_load_config
* @brief This function reloads the configuration parameters from the configuration storage,
* @return syslog_err_t Error code
*/
static syslog_err_t syslog_load_config(void)
{
  syscfg_err_t ret_key_res;

  uint8_t  addr_num[4] = {0};
  uint16_t port_num    = SYSLOG_DEFAULT_PORT;

  char addr_str[16] = {0}; // 15 chars for '255.255.255.255' + one char for '\0'
  char port_str[ 6] = {0}; // max port 65535 + one char for '\0'

  ret_key_res = syscfg_get_value_from_key("syslog.ip",addr_str, 16);

  if(( SYSCFG_SUCCESS != ret_key_res ) && (0 != eth_ctrls_parse_ip_addr(addr_str, addr_num)))
  {
    mt_printf("\r\nSyslog INFO: Syslog server ip addr cannot be read from FS. The Gateway addr is used as default\r\n");
    eth_ctrls_get_net_addr(addr_num,NULL,NULL);
  }

  ret_key_res = syscfg_get_value_from_key("syslog.port", port_str,  6);

  if ( SYSCFG_SUCCESS == ret_key_res )
  {
    char *next_char_ptr;
    long int read_port = strtol(port_str,&next_char_ptr,10);

    if(( read_port < 65536 ) && ( next_char_ptr != port_str ) )
    {
      port_num = (uint16_t) read_port;
      return SYSLOG_ERR_OK;
    }
  }
  // Port cannot be read so the deafult one is used.
  mt_printf("\r\nSyslog INFO: Syslog server port cannot be read from FS. DEFAULT port %d.",port_num);

  return SYSLOG_ERR_OK;
}


/**
* @name	syslog_clear_config
* @brief	This function clears the configuration parameters from the runtime
* @return syslog_err_t Error code
*/
static syslog_err_t syslog_clear_config(void)
{
  // Clears the server IPv4 address
  ip_addr_set_any(0, &(client_state.syslog_server_addr_num));
  //memset(client_state.syslog_server_addr_str, '\0', 16);
  strncpy(client_state.syslog_server_addr_str, SYSLOG_STATUSMSG_NO_IP, 16);

  // Clears the server port
  client_state.syslog_server_port_num = SYSLOG_DEFAULT_PORT;
  //memset(client_state.syslog_server_port_str, '\0',  6);
  strncpy(client_state.syslog_server_port_str, SYSLOG_STATUSMSG_NO_PORT,  6);

  return SYSLOG_ERR_OK;
}


/**
* @name   syslog_udp_init
* @brief  Allocates UDP protocol control structures in LwIP, binds UDP port, sets up the UDP datagram destination
* @param  void
* @return syslog_err_t Error code
*/
static syslog_err_t syslog_udp_init(void)
{
  struct udp_pcb* pcb = udp_new_ip_type(IPADDR_TYPE_ANY);

  if (NULL == pcb)
  {
    mt_printf("%s", SYSLOG_MSG_UDP_INIT_FAIL);
    return SYSLOG_ERR_MEM;
  }

  if (ERR_OK != udp_bind(pcb, IP_ADDR_ANY, client_state.syslog_server_port_num))
  {
    udp_remove(pcb);
    return SYSLOG_ERR_UDP_BIND;
  }

  if (ERR_OK != udp_connect(pcb, &(client_state.syslog_server_addr_num), client_state.syslog_server_port_num))
  {
    udp_remove(pcb);
    return SYSLOG_ERR_UDP_CONN;
  }

  syslog_upcb = pcb;
  client_state.udp_status = 1;
  client_state.status_msg = SYSLOG_STATUSMSG_CLIENT_UP;
  return SYSLOG_ERR_OK;
}


/**
 * @name   syslog_udp_deinit
 * @brief  Closes client, deallocates UDP protocol control configuration on LwIP and resets IPv4 and port values
 * @param	 void
 * @return syslog_err_t
 */
static syslog_err_t syslog_udp_deinit(void)
{
  // Unbinds port, deallocates UDP control structures
  udp_remove(syslog_upcb);
  syslog_upcb = NULL;

  client_state.udp_status = 0;
  client_state.status_msg = SYSLOG_STATUSMSG_CLIENT_DOWN;
  return SYSLOG_ERR_OK;
}


/**
 * @name   syslog_send
 * @brief  Sends syslog message via UDP
 * @param  addr Server IP address
 * @param  port Port number, usually 514
 * @param  syslog_buffer syslog message string buffer
 * @return syslog_err_t error code
 */
static syslog_err_t syslog_send(const ip_addr_t* addr, u16_t port, char* syslog_buffer)
{
  size_t payload_length = strlen(syslog_buffer);
  struct pbuf* p = pbuf_alloc(PBUF_TRANSPORT, (uint16_t)(payload_length), PBUF_RAM);

  if (p == NULL)
  {
    return SYSLOG_ERR_OK;
  }

  char* payload = (char*)( p->payload );
  memcpy(payload, syslog_buffer, payload_length);

  err_t const result = udp_sendto(syslog_upcb, p, addr, port);
  pbuf_free(p);

  return result;
}


/**
 * @name    syslog_compute_prival
 * @brief   Computes priority value assigned to syslog message.
 * @param   syslog_facility_t facility	Integer code indicating which software layer the message is coming from.
 * @param   syslog_severity_t severity	Integer code indicating how severe is the message.
 *
 * @note	check syslog_facility_t and syslog_severity_t on syslog.h for more information about the codes.
 *
 * @return  int prival priority value
 */
static int syslog_compute_prival(syslog_facility_t facility, syslog_severity_t severity)
{
  return (8*facility + severity);
}


/**
 * @name   syslog_fetch_hostname
 * @brief  Builds hostname string for syslog message.
 *         A hostname is used to identify the device over the network.
 *         The hostname format is configured on compile time, check the macro
 *         definitions at the file's top for more information.
 * @param  char* hostname Pointer to hostname string buffer
 * @return void
 */
static void syslog_fetch_hostname(char* hostname)
{
#if defined(SYSLOG_HOSTNAME_IS_IPV4)
  ip4_addr_t const* ipaddr = netif_ip4_addr( &gnetif );

  sprintf(hostname, "%u.%u.%u.%u", (uint8_t)(ipaddr->addr&0xFF), (uint8_t)((ipaddr->addr>>8)&0xFF),
      (uint8_t)(( ipaddr->addr>>16)&0xFF), (uint8_t)(( ipaddr->addr>>24)&0xFF));

#elif defined(SYSLOG_HOSTNAME_IS_CLIENTID)
  struct dhcp_client_id clientid;
  err_t result = dhcp_get_client_id(&gnetif, &clientid);

  if (ERR_OK != result)
    return;

  memcpy(hostname, &clientid.id[SYSLOG_CLIENTID_STR_OFFSET], SYSLOG_CLIENTID_STR_LEN);
#endif /* SYSLOG_USES_IP_HOSTNAME  && SYSLOG_USES_IP_HOSTNAME */
  return;
}


/**
 * @name	syslog_fetch_timestamp
 * @brief	Builds time stamp string for syslog message.
 *  			The time stamp format is configured on compile time.
 *  			Check the macro definitions at the file's top for more information.
 * @param	char* tstamp Pointer to timestamp buffer.
 * @return	void
 */
static void syslog_fetch_timestamp(char* tstamp)
{
#if defined(SYSLOG_FORMAT_IS_RFC5424)
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  rtc_tool_access_hal(&sTime, &sDate);

  float const secfrac =
    ((float)(sTime.SecondFraction - sTime.SubSeconds)) / ((float)(sTime.SecondFraction + 1));

  char secfrac_str[11] = {0};
  snprintf(secfrac_str, 11, "%.6f", secfrac);

  sprintf(tstamp, RFC5424_TIMESTAMP_FORMAT,
      (sDate.Year + MILLENIUM), sDate.Month, sDate.Date ,
      sTime.Hours, sTime.Minutes, sTime.Seconds, &secfrac_str[1]);

#elif defined(SYSLOG_FORMAT_IS_RFC3164)
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  rtc_tool_access_hal(&sTime, &sDate);

#define MONTH_LEN 4
  char month[MONTH_LEN];

  switch(sDate.Month)
  {
    case  1: strncpy(month, "Jan", MONTH_LEN); break;
    case  2: strncpy(month, "Feb", MONTH_LEN); break;
    case  3: strncpy(month, "Mar", MONTH_LEN); break;
    case  4: strncpy(month, "Apr", MONTH_LEN); break;
    case  5: strncpy(month, "May", MONTH_LEN); break;
    case  6: strncpy(month, "Jun", MONTH_LEN); break;
    case  7: strncpy(month, "Jul", MONTH_LEN); break;
    case  8: strncpy(month, "Aug", MONTH_LEN); break;
    case  9: strncpy(month, "Sep", MONTH_LEN); break;
    case 10: strncpy(month, "Oct", MONTH_LEN); break;
    case 11: strncpy(month, "Nov", MONTH_LEN); break;
    case 12: strncpy(month, "Dec", MONTH_LEN); break;
    default: strncpy(month, "---", MONTH_LEN); break;
  }

  sprintf(tstamp, RFC3164_TIMESTAMP_FORMAT, month, sDate.Date, sTime.Hours, sTime.Minutes, sTime.Seconds);
#endif /* SYSLOG_IS_RFC5424 && SYSLOG_IS_RFC3164 */
  return;
}


/**
 * @name    syslog_build_string
 * @brief   Formats syslog message into buffer.
 * @param   syslog_msg_info_t* syslog_info	syslog message info struct sent by
 *          API function syslog_push_into_queue.
 * @param   char* syslog_payload
 * @return  uint16_t payload_length
 */
static uint16_t syslog_build_string(syslog_msg_info_t* syslog_info, char* syslog_buffer)
{
  int  bytes = 0;
  char hostname[SYSLOG_MAX_HOSTNAME_STR + 1] = NILVALUE;
  char timestamp[SYSLOG_MAX_TIMESTAMP_STR + 1] = {0};

  uint8_t prival = syslog_compute_prival(syslog_info->syslog_facility, syslog_info->syslog_severity);

  syslog_fetch_hostname(hostname);
  syslog_fetch_timestamp(timestamp);

#if defined(SYSLOG_FORMAT_IS_RFC5424)
  bytes = snprintf(syslog_buffer, (size_t)(SYSLOG_BUFFER_SIZE), SYSLOG_RFC5424_FORMAT_SPEC_STR, prival,
    SYSLOG_VERSION, timestamp, hostname, syslog_info->syslog_field_app_name,
    syslog_info->syslog_field_msg_id, syslog_info->syslog_field_message);

#elif defined(SYSLOG_FORMAT_IS_RFC3164)
  if (0 == strcmp(syslog_info->syslog_field_msg_id, NILVALUE))
    syslog_info->syslog_field_msg_id = SYSLOG_EMPTY_STR;

  bytes = snprintf(syslog_buffer, (size_t)(SYSLOG_BUFFER_SIZE), SYSLOG_RFC3164_FORMAT_SPEC_STR, prival,
    timestamp, hostname, syslog_info->syslog_field_app_name,
    syslog_info->syslog_field_msg_id, syslog_info->syslog_field_message);
#endif
  return bytes;
}


static void syslog_build_info(char* msg_id, char* message,
                              syslog_facility_t facility,
                              syslog_severity_t severity,
                              syslog_msg_info_t* syslog_info)
{
  // We initialize the value at  taking into account:
  // 5 bytes for PRI= '<' 3-digits '>' with no byte at the end
  // 1 byte for VERSION
  // 5 bytes for the whitespace between all the fields
  size_t syslog_str_totlen 	= 15u;
  size_t syslog_str_field_len = 0u;

  syslog_info->syslog_field_app_name = pcTaskGetName(NULL);

  // Check for NULL string or oversized "app name" field.
  syslog_str_field_len = strnlen_s(syslog_info->syslog_field_app_name, SYSLOG_MAX_APPNAME_STR + 1);
  if( ( 0 == syslog_str_field_len ) || ( SYSLOG_MAX_APPNAME_STR + 1 == syslog_str_field_len ) )
  {
    // Field ignored, replaced by a dash '-' of length 1
    syslog_info->syslog_field_app_name = NILVALUE;
    syslog_str_totlen += 1u;
  }
  else
  {
    syslog_str_totlen += syslog_str_field_len;
  }

  // Check for NULL string or oversized "msg id" field.
  syslog_str_field_len = strnlen_s(msg_id, SYSLOG_MAX_MSGID_STR + 1);
  if( 0 == syslog_str_field_len || SYSLOG_MAX_MSGID_STR + 1 == syslog_str_field_len )
  {
    // Field ignored, replaced by a dash '-' of length 1
    syslog_info->syslog_field_msg_id = NILVALUE;
    syslog_str_totlen += 1u;
  }
  else
  {
    syslog_info->syslog_field_msg_id = msg_id;
    syslog_str_totlen += syslog_str_field_len;
  }

  // Check for NULL string or oversized "message" field.
  syslog_str_field_len = strnlen_s(message, SYSLOG_MSG_MAX_STR + 1);
  if( 0 == syslog_str_field_len || ( SYSLOG_MSG_MAX_STR + 1 == syslog_str_field_len ) )
    syslog_info->syslog_field_message = SYSLOG_INVALID_MSG_STR;
  else
    syslog_info->syslog_field_message = message;

  syslog_info->syslog_facility = facility;
  syslog_info->syslog_severity = severity;
}


/**
 * @name	syslog_client_task
 * @brief	Performs syslog client functionality.
 * @param	void* argument
 * @note	This function is a FreeRTOS task therefore it should not return under normal conditions.
 */
void syslog_client_task(void* argument)
{
  while(1)
  {
    if (SYSLOG_ERR_OK != syslog_init())
    {
      taskYIELD();
      continue;
    }

    syslog_msg_info_t syslog_info;

    if (pdTRUE == xQueueReceive(syslog_queue_handle, &(syslog_info), portMAX_DELAY))
    {
      size_t const payload_length = syslog_build_string(&syslog_info, syslog_buffer);
      int const udp_stat =
        syslog_send(&(client_state.syslog_server_addr_num), client_state.syslog_server_port_num, syslog_buffer);

      if (client_state.syslog_verbosity)
      {
        if ( (ERR_OK != udp_stat) || (SYSLOG_BUFFER_SIZE < payload_length) )
        {
          if (SYSLOG_BUFFER_SIZE < payload_length)
          {
            mt_printf("\nsyslog error: syslog message string from %s was truncated due to exceeding length.\n",
                      syslog_info.syslog_field_app_name);
          }
          if (ERR_OK != udp_stat)
          {
            mt_printf("\nsyslog error: failed to send message through UDP, error code %d\n",udp_stat);
          }
        }
        else
        {
          mt_printf("\nsyslog sent: %s\rpayload size: %zu B", syslog_buffer, payload_length);
        }
      }
      // Clear buffer when finished.
      memset(syslog_buffer, 0, sizeof(syslog_buffer));
    }
  }
  return; /* flow should never reach here... */
}
