/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 * Tolls for used by CM7 to control the CM4 firmware
 */

#include "main.h"
#include "fw_metadata.h"
#include "image_int_flash.h"
#include "cm4_fw_tools.h"


#define CM4_FW_SECTOR    13   // Sector where the CM4 firmware is placed

#define FLASH_ORIGIN  0x8000000  // First address of Flash in absolute address

// MCU Flash Memory Organization.
extern const uint32_t openipmc_bl7_run_addr;
extern const uint32_t openipmc_cm7_run_addr;
extern const uint32_t openipmc_cm4_run_addr;


/*
 * Check if CM4 is present and return its version via pointers
 */
bool cm4_fw_is_present( uint8_t* major_version, uint8_t* minor_version, uint8_t aux_version[4] )
{
  const uint32_t sector_addr = (uint32_t) &openipmc_cm4_run_addr;

	metadata_fields_v0_t* fw_metadata = (metadata_fields_v0_t*)(sector_addr + FW_METADATA_ADDR);

	// Check presence word
	if( fw_metadata->presence_word != FW_METADATA_PRESENCE_WORD )
		return false;

	// Check metadata checksum
	uint32_t sum = 0;
	for( int i=0; i<(sizeof(metadata_fields_v0_t)/sizeof(uint32_t)); i++ )
		sum += ((uint32_t*)fw_metadata)[i];
	if( sum != 0 )
		return false;

	// Check type of firmware
	if( fw_metadata->firmware_type != FW_METADATA_TYPE_OPENIPMC_CM4 )
		return false;

	*major_version = fw_metadata->firmware_revision_major;
	*minor_version = fw_metadata->firmware_revision_minor;
	aux_version[0] = fw_metadata->firmware_revision_aux[0];
	aux_version[1] = fw_metadata->firmware_revision_aux[1];
	aux_version[2] = fw_metadata->firmware_revision_aux[2];
	aux_version[3] = fw_metadata->firmware_revision_aux[3];

	return true;  // Bootloader is present!
}

/*
 * Sets the STM32 CM4 boot address to the correct address
 */
cm4_fw_boot_addr_status_t cm4_fw_update_boot_address( void )
{
	FLASH_OBProgramInitTypeDef pOBInit;

	const uint32_t cm4_sector_addr = (uint32_t) &openipmc_cm4_run_addr;


	HAL_FLASHEx_OBGetConfig( &pOBInit );

	if( pOBInit.CM4BootAddr0 == cm4_sector_addr )
		return cm4_fw_boot_addr_ok; // Already set. Nothing to do.

	pOBInit.OptionType = OPTIONBYTE_CM4_BOOTADD;
	pOBInit.CM4BootConfig = OB_BOOT_ADD0;
	pOBInit.CM4BootAddr0 = cm4_sector_addr;

	HAL_FLASH_OB_Unlock();
	HAL_FLASHEx_OBProgram( &pOBInit );
	HAL_FLASH_OB_Launch();
	HAL_FLASH_OB_Lock();

	// Cross check!
	HAL_FLASHEx_OBGetConfig( &pOBInit );

	if( pOBInit.CM4BootAddr0 == cm4_sector_addr )
		return cm4_fw_boot_addr_updated; // Update successful
	else
		return cm4_fw_boot_addr_error; // Update error
}
