 /*
 * OpenIPMC-FW
 * Copyright (C) 2020-2024 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * @file   syscfg.c
 * @brief  Configuration storage source code.
 * @date   YYYY / MM / DD - 2022 / 08 / 17
 * @author Antonio V. G. Bassi, Luigi Calligaris
 *
 */

#include "syscfg.h"

/* standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

/* FreeRTOS resources */
#include "FreeRTOS.h"
#include "semphr.h"
#include "emh_malloc.h"

/* yaffs resources */
#include "yaffsfs.h"

/* private resources */
#include "mt_printf.h"
#include "yaffsfs_errno.h"
#include "openipmc_fw_utils.h"

#define KB 1024
#define SYSCFG_SERIAL_BUF_LEN	( SYSCFG_KEY_MAXLEN + SYSCFG_VALUE_MAXLEN )
#define SYSCFG_FORMAT_SPEC		"%s=%s\n"
#define SYSCFG_FILE_EXTENSION	".conf"
#define SYSCFG_HEAP_SIZE      4*KB

typedef struct syscfg_node_t
{
	char key  [SYSCFG_KEY_MAXLEN+1];
	char value[SYSCFG_VALUE_MAXLEN+1];
	struct syscfg_node_t* next;
}syscfg_node_t;

extern struct yaffs_dev dev;

static uint8_t syscfg_initd_flag = 0;

static syscfg_node_t* head = NULL;
static uint8_t __attribute__((section(".syscfg_sec"))) syscfg_heap[KB/2] = {0};
static emh_heapId_t syscfg_heap_id = 0;
static char const* configdirpath = "/etc";


static int  syscfg_node_is_less(syscfg_node_t *lhs, syscfg_node_t *rhs);
static int  syscfg_node_is_equal(syscfg_node_t *lhs, syscfg_node_t *rhs);
static void syscfg_node_list(void);
static void syscfg_parse_parent_from_key(char* key, char* buf);
static void syscfg_node_serialize(char *key, char *buf);
static void syscfg_node_deserialize(char *buf, syscfg_node_t *node);
static syscfg_err_t   syscfg_heap_init(void);
static syscfg_err_t   syscfg_init(void);
static syscfg_err_t   syscfg_node_insert(char *key, char *value);
static syscfg_err_t   syscfg_remove_node(syscfg_node_t *node);
static syscfg_node_t  *syscfg_search_node_key(char *key);


static int syscfg_node_is_less(syscfg_node_t *lhs, syscfg_node_t *rhs)
{
  return ( 0 > strcmp(lhs->key, rhs->key) );
}

static int syscfg_node_is_equal(syscfg_node_t *lhs, syscfg_node_t *rhs)
{
  return ( 0 == strcmp(lhs->key, rhs->key) );
}


static void syscfg_node_list(void)
{
  int node_count = 0;
  syscfg_node_t *current_node = head;
  while ( ( NULL != current_node ) )
  {
    if ( 0 == ( node_count % 4 ) )
    {
      mt_printf("\n");
    }
    mt_printf("%-20s", current_node->key);
    current_node = current_node->next;
    node_count++;
  }
  return;
}


static void syscfg_parse_parent_from_key(char* key, char* buf)
{
  for (int ch = 0; '.' != key[ch]; ch++)
  {
    buf[ch] = key[ch];
  }
  return;
}


static void syscfg_node_serialize(char* key, char* buf)
{
  syscfg_node_t* current_conf = syscfg_search_node_key(key);
  snprintf(buf, (size_t)( SYSCFG_SERIAL_BUF_LEN + 1 ),
    SYSCFG_FORMAT_SPEC, current_conf->key, current_conf->value);
  return;
}


static void syscfg_node_deserialize(char* buf, syscfg_node_t* node)
{
  size_t diff = 0;
  char* next_pos = NULL;
  char* pos = NULL;

  pos = strchr(buf, '=');
  diff = (size_t)( pos - buf );
  memcpy(node->key, buf, diff);

  pos += 1;
  next_pos = strchr(pos, '\n');
  diff = (size_t)( next_pos - pos );
  memcpy(node->value, pos, diff);

  node->next = NULL;
  return;
}

static syscfg_err_t syscfg_heap_init(void)
{
  syscfg_heap_id = emh_create(syscfg_heap, SYSCFG_HEAP_SIZE);
  // No space available for the heap
  if (0 > syscfg_heap_id)
  {
    return SYSCFG_EHEAPINIT;
  }

  return SYSCFG_SUCCESS;
}


static syscfg_err_t syscfg_node_insert(char *key, char *value)
{
  syscfg_node_t* new_node = (syscfg_node_t*)(emh_malloc(syscfg_heap_id, sizeof(syscfg_node_t)));

  /* Was malloc successful? */
  if ( NULL == new_node )
  {
    return SYSCFG_EMALLOCFAIL;
  }

  /* Assign variables */
  strncpy(new_node->key, key, (size_t) SYSCFG_KEY_MAXLEN+1);
  strncpy(new_node->value, value, (size_t) SYSCFG_VALUE_MAXLEN+1);

  /* Is the head of the list empty? */
  if ( NULL == head )
  {
    new_node->next = NULL;
    head = new_node;
    return SYSCFG_SUCCESS;
  }

  /* Does the configuration node go at the beginning? */
  if ( syscfg_node_is_less(new_node, head) )
  {
    new_node->next = head;
    head = new_node;
    return SYSCFG_SUCCESS;
  }

  if ( syscfg_node_is_equal(new_node, head) )
  {
    strncpy(head->key, new_node->key, (size_t) SYSCFG_KEY_MAXLEN+1 );
    strncpy(head->value, new_node->value, (size_t) SYSCFG_VALUE_MAXLEN+1);
    emh_free(new_node);
    return SYSCFG_EREPLD;
  }

  /* Not at the beginning, find out correct position */
  syscfg_node_t* current_node = head;
  while( NULL != current_node->next )
  {
    if ( syscfg_node_is_equal(new_node, current_node->next) )
    {
      strncpy(current_node->next->key, new_node->key, (size_t) SYSCFG_KEY_MAXLEN+1 );
      strncpy(current_node->next->value, new_node->value, (size_t) SYSCFG_VALUE_MAXLEN+1);
      emh_free(new_node);
      return SYSCFG_EREPLD;
    }

    if ( syscfg_node_is_less(new_node, current_node->next) )
    {
      new_node->next 		= current_node->next;
      current_node->next 	= new_node;
      return SYSCFG_SUCCESS;
    }
    current_node = current_node->next;
  }

  /* Traversed the entire list and reached the bottom, insert here */
  new_node->next = NULL;
  current_node->next = new_node;
  return SYSCFG_SUCCESS;
}


static syscfg_err_t syscfg_remove_node(syscfg_node_t *node)
{
  syscfg_err_t  result = SYSCFG_EINVAL;
  syscfg_node_t *iterator, *prev;

  // Is the given pointer valid?
  if ( NULL != node )
  {
    // Are we at the head?
    if ( head == node )
    {
      head = node->next;
      memset(node, 0x00, sizeof(syscfg_node_t));
      emh_free(node);
      result = SYSCFG_SUCCESS;
    }
    else
    {
      // Not at the head. Look for given node.
      for (iterator = head; NULL != iterator; iterator = iterator->next)
      {
        if (node == iterator)
        {
          // Configuration node was found, unlink it from the list.
          prev->next = iterator->next;
          memset(iterator, 0x00, sizeof(syscfg_node_t));
          emh_free(iterator);
          result = SYSCFG_SUCCESS;
          break;
        }
        // Store previous node for unlinking process.
        prev = iterator;
      }
      if ( NULL == iterator )
      {
        // The given configuration node is not present in the list.
        result = SYSCFG_ENODE;
      }
    }
  }
  return result;

}


static syscfg_node_t* syscfg_search_node_key(char *key)
{
  syscfg_node_t *node = head;
  while ( ( NULL != node ) )
  {
    if ( 0 == strcmp(key, node->key) )
    {
      break;
    }
    node = node->next;
  }
  return node;
}


void syscfg_print_node_list(void)
{
  if ( 0 == syscfg_initd_flag )
  {
    if ( 0 <= syscfg_init() )
    {
      syscfg_initd_flag = 1;
    }
  }
	syscfg_node_list();
}

syscfg_err_t syscfg_save(void)
{
  if (SYSCFG_SUCCESS != syscfg_init())
    return SYSCFG_EINIT;

  syscfg_err_t  result = SYSCFG_ENOLST;
  syscfg_node_t *curr_node = head;
  yaffs_DIR* dir;
  struct yaffs_dirent* dirent;
  struct yaffs_stat fstat;
  char line_buf[SYSCFG_KEY_MAXLEN + 1] = {0};
  char path[SYSCFG_DIRSTR_MAXLEN + 1] = {0};
  char prev_path[SYSCFG_DIRSTR_MAXLEN + 1] = {0};
  char serial[SYSCFG_SERIAL_BUF_LEN + 2] = {0};
  int handle = 0;
  int errno = 0;
  int dir_exists = 1;

  if ( 0 > yaffs_access(configdirpath, (R_OK | W_OK)))
  {
    if ( 0 > yaffs_mkdir(configdirpath, ( S_IREAD | S_IWRITE ) ) )
    {
      dir_exists = 0;
      result = SYSCFG_EDIR;
    }
  }

  if ( dir_exists )
  {
    /* Remove every configuration file */
    dir = yaffs_opendir(configdirpath);
    while ( ( NULL != ( dirent = yaffs_readdir(dir) ) ) )
    {
      snprintf(path, (size_t) SYSCFG_DIRSTR_MAXLEN, "%s/%s", configdirpath, dirent->d_name);
      yaffs_lstat(path, &fstat);

      /* is it a data file? */
      if ( S_IFREG == (fstat.st_mode & S_IFMT) )
      {
        /* is the extension valid? */
        if ( NULL != strstr(dirent->d_name, SYSCFG_FILE_EXTENSION) )
        {
          /* remove the file */
          yaffs_unlink(path);
        }
      }
    }
    memset(path, 0x00, SYSCFG_DIRSTR_MAXLEN+1);

	  /* Lets write the new configuration files with changed key/value pairs */
    while ( ( NULL != curr_node ) )
    {
      syscfg_parse_parent_from_key(curr_node->key, line_buf);
      snprintf(path, (size_t)(SYSCFG_DIRSTR_MAXLEN), "%s/%s%s", configdirpath, line_buf, SYSCFG_FILE_EXTENSION);
      if ( 0 != strncmp(prev_path, path, (size_t)SYSCFG_DIRSTR_MAXLEN + 1 ) )
      {
        handle = yaffs_open(path, (O_CREAT | O_TRUNC | O_RDWR), (S_IREAD | S_IWRITE));
      }
      else
      {
        handle = yaffs_open(path, (O_CREAT | O_APPEND | O_RDWR), (S_IREAD | S_IWRITE));
      }

      syscfg_node_serialize(curr_node->key, serial);

      if ( 0 > yaffs_write(handle, serial, strlen(serial) ) )
      {
        errno = yaffsfs_get_errno();
      }
      else
      {
        yaffs_close(handle);
      }
      strncpy(prev_path, path, (size_t) SYSCFG_DIRSTR_MAXLEN + 1);
      memset(serial, 0x00, SYSCFG_SERIAL_BUF_LEN + 2);
      memset(line_buf, 0x00, (size_t) SYSCFG_KEY_MAXLEN + 1);
      curr_node = curr_node->next;
    }
	}
	return errno;
}

syscfg_err_t syscfg_insert(char *key, char *value)
{
  if (SYSCFG_SUCCESS != syscfg_init())
    return SYSCFG_EINIT;

  syscfg_err_t result = SYSCFG_SUCCESS;

  if ( (0 == strnlen_s(key, (size_t)SYSCFG_KEY_MAXLEN) )             ||
       (0 == strnlen_s(value, (size_t)SYSCFG_VALUE_MAXLEN) )         ||
       (SYSCFG_KEY_MAXLEN < strnlen_s(key, (size_t)SYSCFG_KEY_MAXLEN) ) ||
       (SYSCFG_VALUE_MAXLEN < strnlen_s(value, (size_t)SYSCFG_VALUE_MAXLEN ) ) )
  {
    result = SYSCFG_EINVAL;
  }
  else
  {
    result = syscfg_node_insert(key, value);
  }
  return result;
}

syscfg_err_t syscfg_remove(char *key)
{
  if (SYSCFG_SUCCESS != syscfg_init())
    return SYSCFG_EINIT;

  syscfg_err_t result = SYSCFG_SUCCESS;
  syscfg_node_t *node = NULL;
  node = syscfg_search_node_key(key);
  result = syscfg_remove_node(node);
  return result;
}


syscfg_err_t syscfg_init(void)
{
  if (0 == syscfg_initd_flag)
  {
    syscfg_err_t result = syscfg_heap_init();

    if (SYSCFG_SUCCESS != result)
    {
      mt_printf("syscfg: Could not initialise heap for syscfg service.\n");
      return result;
    }

    if (0 > yaffs_access(configdirpath, (R_OK | W_OK) ))
    {
      if (0 > yaffs_mkdir(configdirpath, ( S_IREAD | S_IWRITE )))
      {
        mt_printf("syscfg: Could not create %S directory.", configdirpath);
        return SYSCFG_EDIR;
      }
    }

    yaffs_DIR* dir = yaffs_opendir(configdirpath);
    struct yaffs_dirent* dirent;
    while (NULL != ( dirent = yaffs_readdir(dir) ))
    {
      char path[SYSCFG_DIRSTR_MAXLEN + 1] = {0};
      snprintf(path, (size_t) SYSCFG_DIRSTR_MAXLEN, "%s/%s", configdirpath, dirent->d_name);

      struct yaffs_stat fstat;
      yaffs_lstat(path, &fstat);

      /* is it a data file? */
      if (S_IFREG == (fstat.st_mode & S_IFMT))
      {
        /* is the extension valid? */
        if (NULL != strstr(dirent->d_name, SYSCFG_FILE_EXTENSION))
        {
          int handle = yaffs_open(path, 0, (S_IREAD) );
          char line_buf[SYSCFG_DIRSTR_MAXLEN + 2] = {0};
          int idx = 0;
          char ch = 0;
          while (0 < yaffs_read(handle, &ch, 1))
          {
            /* did we reach a new line? */
            line_buf[idx++] = ch;
            if ( ( '\n' == ch ) || ( '\0' == ch ) )
            {
              syscfg_node_t node = {0};
              syscfg_node_deserialize(line_buf, &node);
              syscfg_node_insert(node.key, node.value);
              memset(line_buf, 0x00, (size_t)SYSCFG_DIRSTR_MAXLEN + 1 );
              memset(&node, 0x00, sizeof(syscfg_node_t));
              if( '\0' == ch )
              {
                break;
              }
              else
              {
                idx = 0;
              }
            }
          }
        }
      }
    }
    syscfg_initd_flag = 1;
    return SYSCFG_SUCCESS;
  }
  return SYSCFG_SUCCESS;
}


syscfg_err_t syscfg_get_valuesize_from_key(char* key, size_t* valuesize)
{
  *valuesize = 0;

  if (SYSCFG_SUCCESS != syscfg_init())
    return SYSCFG_EINIT;

  syscfg_node_t* node = NULL;
  node = syscfg_search_node_key(key);

  if (NULL != node)
  {
    *valuesize = strlen(node->value);
    return SYSCFG_SUCCESS;
  }
  return SYSCFG_ENODE;
}


syscfg_err_t syscfg_get_value_from_key(char* key, char* value, size_t valuebufsize)
{
  if (valuebufsize < 1)
    return SYSCFG_ETRUNC;

  if (SYSCFG_SUCCESS != syscfg_init())
    return SYSCFG_EINIT;

  syscfg_node_t* node = NULL;
  node = syscfg_search_node_key(key);

  if (NULL != node)
  {
    strncpy(value, node->value, valuebufsize);
    return SYSCFG_SUCCESS;
  }
  return SYSCFG_ENODE;
}
