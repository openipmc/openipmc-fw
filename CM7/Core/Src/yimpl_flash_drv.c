/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	:	yimpl_flash_drv.c
 * @brief	:	Winbond w25n01gv NAND Flash memory driver callback functions for yaffs.
 * @date	:	YYYY / MM / DD - 2022 / 02 / 21
 * @author	:	Antonio V. G. Bassi
 * @note	:	Flash memory model "W25N01GV 3V 1-Gbit Winbond"
 */

/* standard C */
#include <stdint.h>

/* yaffs */
#include "yaffs_guts.h"

/* FreeRTOS */
#include "FreeRTOS.h"
#include "task.h"

/* Drivers for w25n01gv flash */
#include "w25n01gv.h"

#include "yimpl_flash_drv.h"
#include "ext_flash_organization.h"

struct yaffs_dev dev;

/*
 * @name    yimpl_flash_write_chunk_fn
 * @brief   Writes a chunk (nand page) worth of data into
 * 			    flash memory data buffer via QuadSPI bus.
 *
 * @param  yaffs_dev *dev  data structure holding information about current yaffs partition.
 * @param  int nand_chunk  Chunk index to be written.
 * @param  const uint8_t *data  Pointer to data buffer.
 * @param  int data_len    Length of data buffer.
 * @param  const uint8_t *oob   Pointer to oob buffer.
 * @param  int oob_len     Length of oob buffer.
 * @retval int             YAFFS_OK or YAFFS_FAIL
 *
 * @note yaffs always writes within the chunk (page) size.
 *
 * @note yaffs will iterate through chunks of different
 * blocks, therefore for w25n01gv we'll have chunks varying
 * from 0 to 65535.
 */
int yimpl_flash_write_chunk_fn( struct yaffs_dev *dev, int nand_chunk,
							 const uint8_t* data, int data_len,
							 const uint8_t* oob, int oob_len )
{
	if( !data )
		return YAFFS_FAIL;

	w25n01gv_mutex_take();

	/* Write into designated data area */
	w25n01gv_write_enable();
	w25n01gv_quad_data_load(0, data_len, data);
	w25n01gv_program_execute( nand_chunk );

	while( w25n01gv_is_busy() )
	{
		__asm__("NOP");
	}

	w25n01gv_write_disable();

	w25n01gv_mutex_give();

	return YAFFS_OK;
}

/*
 * @name    yimpl_flash_read_chunk_fn
 * @brief   Reads a chunk (page) worth of data from flash
 * 			    memory's data buffer via QuadSPI bus.
 *
 * @param   yaffs_dev *dev  data structure holding information about current yaffs partition.
 * @param   int nand_chunk  Chunk index to be read.
 * @param   uint8_t *data        Pointer to data buffer.
 * @param   int data_len    Length of data buffer.
 * @param   uint8_t *oob         Pointer to oob buffer
 * @param   int oob_len     Length of oob buffer.
 * @retval  int             YAFFS_OK or YAFFS_FAIL
 *
*/
int yimpl_flash_read_chunk_fn( struct yaffs_dev *dev, int nand_chunk,
							uint8_t* data, int data_len,
							uint8_t* oob, int oob_len,
							enum yaffs_ecc_result *ecc_result)
{
	uint8_t ecc_stat;
	enum yaffs_ecc_result ecc_out;
	int result = YAFFS_OK;

	w25n01gv_mutex_take();

	w25n01gv_page_data_read(nand_chunk);

	while( w25n01gv_is_busy() )
	{
		__asm__("NOP");
	}

	w25n01gv_fast_read_quad(0, data_len, data);

	while( w25n01gv_is_busy() )
	{
		__asm__("NOP");
	}

	ecc_stat = w25n01gv_ecc_check();

	w25n01gv_mutex_give();

	/* ECC results are in...  */
	switch(ecc_stat)
	{
		case 0x00:
			ecc_out = YAFFS_ECC_RESULT_NO_ERROR;
			break;
		case 0x10:
			ecc_out = YAFFS_ECC_RESULT_FIXED;
			break;
		case 0x20:
			ecc_out = YAFFS_ECC_RESULT_UNFIXED;
			break;
		default:
			ecc_out = YAFFS_ECC_RESULT_UNKNOWN;
	}
	*ecc_result = ecc_out;

	if( *ecc_result == YAFFS_ECC_RESULT_UNKNOWN )
	{
		result = YAFFS_FAIL;
	}
	return result;
}

/*
 * @name    yimpl_flash_erase_block_fn
 * @brief   Erases block on flash memory.
 *
 * @param  yaffs_dev *dev        Data structure holding information about current yaffs partition.
 * @param  int       block_no    Block number to be erased.
 * @retval int                   YAFFS_OK or YAFFS_FAIL
 *
*/
int yimpl_flash_erase_block_fn(struct yaffs_dev *dev, int block_no)
{
	int result = YAFFS_FAIL;

	// If the block is within the existing range, perform operation.
	w25n01gv_mutex_take();
	if( TOTAL_BLOCKS > block_no )
	{
		w25n01gv_write_enable();
		w25n01gv_block_erase(block_no);

		while( w25n01gv_is_busy() )
		{
			__asm__("NOP");
		}

		w25n01gv_write_disable();
		result = YAFFS_OK;
	}
	w25n01gv_mutex_give();
	return result;
}

/*
 * @name    yimpl_flash_mark_bad_fn
 * @brief   Writes bad block flag on flash memory spare
 * area.
 *
 * @param  yaffs_dev *dev      Data structure holding information about current yaffs partition.
 * @param  int       block_no  Block number to be marked.
 * @retval int                 YAFFS_OK or YAFFS_FAIL
 *
 */
int yimpl_flash_mark_bad_fn(struct yaffs_dev *dev, int block_no)
{
	const uint8_t bb_marker = 0x00;
	const int pages_per_block = 64;
	int nand_chunk = 0;
	int result = YAFFS_FAIL;

	w25n01gv_mutex_take();
	if( TOTAL_BLOCKS > block_no )
	{
		nand_chunk = pages_per_block * block_no;
		w25n01gv_write_enable();
		w25n01gv_quad_data_load(0, 1, &bb_marker);
		w25n01gv_program_execute(nand_chunk);

		while( w25n01gv_is_busy() )
		{
			__asm__("NOP");
		}
		w25n01gv_write_disable();
		result = YAFFS_OK;
	}
	w25n01gv_mutex_give();
	return result;
}


/*
 * @name    yimpl_flash_check_bad_fn
 * @brief   Checks bad block flag on flash memory spare
 * area.
 *
 * @param  yaffs_dev  *dev      Data structure holding information about current yaffs partition.
 * @param  int        block_no  Block number to be checked.
 * @retval int                  YAFFS_OK or YAFFS_FAIL
 *
 */
int yimpl_flash_check_bad_fn(struct yaffs_dev *dev, int block_no)
{
	const int pages_per_block = 64;
	int nand_chunk = 0;
	int result = YAFFS_FAIL;
	uint8_t bb_marker = 0xFF;

	w25n01gv_mutex_take();
	if( TOTAL_BLOCKS > block_no )
	{
		nand_chunk = block_no * pages_per_block;
		w25n01gv_page_data_read(nand_chunk);

		while( w25n01gv_is_busy() )
		{
			__asm__("NOP");
		}

		w25n01gv_fast_read_quad(0, 1, &bb_marker);

		while( w25n01gv_is_busy() )
		{
			__asm__("NOP");
		}

		if( 0x00 != bb_marker )
		{
			result = YAFFS_OK;
		}
	}
	w25n01gv_mutex_give();

	return result;
}

/*
 * @name   yimpl_flash_init_fn
 * @brief  Provides hook for parameter and driver initialisation.
 * @param  yaffs_dev *dev  Data structure holding information about current yaffs partition.
 * @retval int             YAFFS_OK or YAFFS_FAIL
 *
 */
int yimpl_flash_init_fn(struct yaffs_dev *dev)
{
	const uint8_t status_reg_1 = 0x00;
	const uint8_t status_reg_2 = 0x18;

	w25n01gv_mutex_take();
	/* Check if flash is busy to not break any operations */
	while( w25n01gv_is_busy() )
	{
		__asm__("NOP");
	}
	w25n01gv_device_reset();
	w25n01gv_set_status_reg(W25N01GV_STATUS_REG_1, status_reg_1);
	w25n01gv_set_status_reg(W25N01GV_STATUS_REG_2, status_reg_2);
	w25n01gv_mutex_give();

	return YAFFS_OK;
}

/*
 * @name   yimpl_flash_deinit_fn
 * @brief  Provides hook for parameter and driver deinitialisation.
 * @param  yaffs_dev *dev  Data structure holding information about current yaffs partition.
 * @retval int             YAFFS_OK or YAFFS_FAIL
 *
 */
int yimpl_flash_deinit_fn(struct yaffs_dev *dev)
{
	/* Check if flash is busy to not break any operations */
	w25n01gv_mutex_take();
	while( w25n01gv_is_busy() )
	{
		__asm__("NOP");
	}

	w25n01gv_device_reset();
	w25n01gv_write_disable();
	w25n01gv_mutex_give();
	return YAFFS_OK;
}

/*
 * @name   yimpl_flash_install
 * @brief  Performs yaffs partition installation and driver callback functions configuration.
 * @param  const char*    name     Name of the partition to be mounted on device
 * @retval int                     YAFFS_OK or YAFFS_FAIL
 *
 */
int yimpl_flash_install( const char* name )
{
	struct yaffs_param *param;
	struct yaffs_driver *drv;

	dev.param.name = name;
	drv = &dev.drv;
	param = &dev.param;

	drv->drv_write_chunk_fn     = yimpl_flash_write_chunk_fn;
	drv->drv_read_chunk_fn      = yimpl_flash_read_chunk_fn;
	drv->drv_erase_fn           = yimpl_flash_erase_block_fn;
	drv->drv_mark_bad_fn        = yimpl_flash_mark_bad_fn;
	drv->drv_check_bad_fn       = yimpl_flash_check_bad_fn;
	drv->drv_initialise_fn      = yimpl_flash_init_fn;
	drv->drv_deinitialise_fn    = yimpl_flash_deinit_fn;

	param->is_yaffs2                    = ( int )( CONFIG_YAFFS_YAFFS2 );
	param->empty_lost_n_found           = ( int )( 1 );
	param->total_bytes_per_chunk        = ( uint32_t )( CHUNK_SIZE );
	param->spare_bytes_per_chunk        = ( uint32_t )( 0 );
	param->chunks_per_block	            = ( uint32_t )( N_CHUNKS );
	param->start_block                  = ( uint32_t )( EXT_FLASHORG_YAFFS_START_BLOCK );
	param->end_block                    = ( uint32_t )( EXT_FLASHORG_YAFFS_START_BLOCK + EXT_FLASHORG_YAFFS_NUM_OF_BLOCKS - 1 );
	param->n_reserved_blocks            = ( uint32_t )( 5  );
	param->n_caches                     = ( uint32_t )( 10 );
	param->inband_tags                  = ( int )( 1 );
	param->refresh_period               = ( uint32_t )( 1000 );

	yaffs_add_device( &dev );
	return YAFFS_OK;
}
