/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      exti_gpio_driver.c                                                */
/* @brief     Library aimed to configure EXTI for GPIO                          */
/* @author    Carlos R. Dell'Aquila                                             */
/********************************************************************************/


/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

#include "stm32h7xx.h"
#include "mcp23s17ml_hal.h"
#include "exti_gpio_isr.h"


void EXTI_IRQHandler()
{
  EXTI_TypeDef* exti_base = EXTI;
  uint32_t  pr1 = READ_REG(exti_base->PR1); // PR1 because the ISR is for Cortex-M7 core.
  uint32_t  pr1_clean_mask = 0x00;
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  /* EACH APPLICATION-CALLBACK SHOULD BE ADDED MANUALLY IN THIS SECTION */

  mcp23s17_isr(pr1,&pr1_clean_mask,&xHigherPriorityTaskWoken); // Callback related to the MCP23S17 devices.

  /* END APLICATION-CALLBACK SECTION */

  // Commit the Line mask to clean the pending interrupt.
  MODIFY_REG(exti_base->PR1,pr1_clean_mask,0xFFFFFFFF);
  READ_REG(exti_base->PR1);

  /* Pass the xHigherPriorityTaskWoken value into portYIELD_FROM_ISR():
   * - If xHigherPriorityTaskWoken was set to pdTRUE inside xSemaphoreGiveFromISR()
   *   then calling portYIELD_FROM_ISR() will request a context switch.
   *
   * - If xHigherPriorityTaskWoken is still pdFALSE the calling portYIELD_FROM_ISR()
   *   will have no effect.
   */
   portYIELD_FROM_ISR(xHigherPriorityTaskWoken);

  return;
}



#if EXTI0_ENABLE == 1

void EXTI0_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI1_ENABLE == 1

void EXTI1_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI2_ENABLE == 1

void EXTI2_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI3_ENABLE == 1

void EXTI3_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI4_ENABLE == 1

void EXTI4_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI9_5_ENABLE == 1

void EXTI9_5_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif

#if EXTI15_10_ENABLE == 1

void EXTI15_10_IRQHandler(void)
{
  EXTI_IRQHandler();
  return;
}

#endif




