/*
 * xvc_jtag.h
 *
 *  Created on: Jun 20, 2022
 *      Author: antoniobassi
 */

#ifndef SRC_XVC_SERVER_H
#define SRC_XVC_SERVER_H

void xvc_init_server( void );

#endif /* SRC_XVC_SERVER_H */
