/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      xvc_server.c                                                      */
/* @brief     Xilinx Virtual Cable server source code.                          */
/* @author    André Muller Cascadan                                             */
/********************************************************************************/

#include <stdint.h>
#include <memory.h>

#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "lwip/api.h"
#include "lwip/sockets.h"

#include "mt_printf.h"
#include "xvc_common.h"


// Minimum period/max frequency is dictated by the DMA latency (higher frequency leads to lost clk triggers)
// 200 MHz --> 5 ns * 20 (TIM8 ARR) * 2 (patterns per TCK rising edge) = 200 ns
// Maximum period/min frequency has been found through trial and error, and is around 20 us/50 kHz
#define XVC_MIN_NS_PER_TIM8          200UL
#define XVC_MAX_NS_PER_TIM8        20000UL
#define XVC_MAX_PSC_VALUE_TIM8     32768UL

// IPC data goes to absolute memory position (see linker script)
volatile xvc_ipc_data_t xvc_ipc_data __attribute__((section(".xvcjtag")));

// TCP connection variables
static const  uint16_t xvc_port = 2542; // Default XVC port
static int listen_sock, connection_sock;

static osThreadId_t xvc_task_handle;
static const osThreadAttr_t xvc_task_attributes = {
  .name = "XVCTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};

// XVC commands
typedef enum
{
	XVC_CMD_GETINFO,
	XVC_CMD_SETTCK,
	XVC_CMD_SHIFT,
	XVC_CMD_NONE
} xvc_cmd_t;

/*
 * Prescaler value
 *
 * This value is passed to CM4 core to control the shift clock period.
 * It is set by xvc_set_clk_period() function (see below)
 */
static uint32_t xvc_jtag_clk_prescaler = 0;

uint32_t xvc_bit_counter;

static void      xvc_task (void *arg);
static xvc_cmd_t get_command( void* rx_data);
static uint32_t  get_command_arg( void* rx_data , int cmd_length );
static int       take_hsem_when_shift_is_done(void);
static int       take_hsem(void);
static void      xvc_set_clk_period(uint32_t const requested_period_ns, uint32_t* actual_period_ns);


void xvc_init_server( void )
{
	// Opens a listening socket
	listen_sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;
    addr.sin_port        = htons(xvc_port);
    addr.sin_family      = PF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    bind (listen_sock, (struct sockaddr *)&addr, sizeof(addr));
    listen (listen_sock, 1);

	// Create the accept sentd task
	xvc_task_handle = osThreadNew(xvc_task, NULL, &xvc_task_attributes);

}


/*
 * XVC task
 *
 * This task manages the TCP connection status and controls the data flow
 * through the JTAG shifter at CM4 core.
 */
static void xvc_task (void *arg)
{
	//void *rx_data;
	int rx_data_len;
	xvc_cmd_t cmd;
	char response[24];
	uint8_t cmd_buffer[11];
	uint32_t requested_period, actual_period;
	uint32_t num_bits, vector_length;
	uint32_t expected_bytes, total_rcvd;

	volatile uint8_t* rcvd_data = xvc_ipc_data.rcvd_data;

	if( !take_hsem() )
	{
		mt_printf("XVC error: IPC timeout\r\n");
	}

	// Waits for the first connection
	connection_sock = accept (listen_sock, NULL, NULL);

	for(;;)
	{
		// Waits for command from client
		rx_data_len = recv (connection_sock, cmd_buffer, 6, 0); // Wait for 6 bytes to detect the command
		if ( rx_data_len > 0 )
		{
			cmd = get_command( cmd_buffer );
			switch( cmd )
			{
			case XVC_CMD_GETINFO:
				rx_data_len = recv (connection_sock, &cmd_buffer[6], 2, 0); // 2 remaining bytes to complete the command. Just to clean the shocket.
				strcpy(response, "xvcServer_v1.0:");
				itoa(XVC_DATA_BUFF_MAX_BYTES, response + strlen(response), 10); // Tells the total amount of bytes (TDI+TMS) to be received at a time
				strcat(response, "\n");
				send(connection_sock, response, strlen(response), 0);
				break;
			case XVC_CMD_SETTCK:
				rx_data_len = recv (connection_sock, &cmd_buffer[6], 5, 0); // 5 remaining bytes to complete the command. It contains an argument.
				requested_period = get_command_arg( cmd_buffer, strlen("settck:") );
				xvc_set_clk_period(requested_period, &actual_period);
				// Respond actual_period by directly casting the little-endian variable into the socket
				send(connection_sock, (void*)(&actual_period), sizeof(uint32_t), 0);
				break;
			case XVC_CMD_SHIFT:
				rx_data_len = recv (connection_sock, &cmd_buffer[6], 4, 0); // 4 remaining bytes to complete the command. It contains the num_bits.
				num_bits  = get_command_arg( cmd_buffer, strlen("shift:") );
				vector_length = ( num_bits+7 )>>3; // Sufficient amount of bytes to contain the bits to be shifted
				total_rcvd = 0;
				expected_bytes = vector_length<<1 ;// 2 vectors
				// Iterates to receive more data
				while(total_rcvd < expected_bytes)
				{
					rx_data_len = recv (connection_sock, (uint8_t*)&rcvd_data[total_rcvd], expected_bytes-total_rcvd, 0);
					if( rx_data_len <= 0 )
						break; // Break the loop in case of connection loss
					total_rcvd += rx_data_len;
				}

				//Configure the CM4 shifter
				xvc_ipc_data.nshift        = num_bits;
				xvc_ipc_data.clk_prescaler = xvc_jtag_clk_prescaler;
				xvc_ipc_data.bytevec_tms_data     = (uint8_t*)xvc_ipc_data.rcvd_data; // TMS vector starts at tenth byte of the received data
				xvc_ipc_data.bytevec_tdi_tdo_data = (uint8_t*)&(xvc_ipc_data.rcvd_data[vector_length]); // TDI vector is immediately after TMS
				xvc_ipc_data.start_shift = 1;
				xvc_ipc_data.shift_done  = 0;
				SCB_CleanDCache_by_Addr((uint32_t*)( &xvc_ipc_data ), sizeof(xvc_ipc_data_t));
				HAL_HSEM_Release(HSEM_ID_1, 0); // By releasing this HSEM, CM4 is allowed to start the shift process

				xvc_bit_counter += num_bits;

				if( take_hsem_when_shift_is_done() )
				{
					// Send TDO data back to the client (TDO data replaces TDI content to reuse memory)
					send( connection_sock, xvc_ipc_data.bytevec_tdi_tdo_data, vector_length, 0 );
				}
				else
					mt_printf("XVC error: IPC timeout\r\n");
				break;

			default:
				mt_printf("XVC error: bad command\r\n");
				break;
			}
		}
		else // if rx_data_len <= 0 (connection terminated or error)
		{
			closesocket( connection_sock );
			connection_sock = accept( listen_sock, NULL, NULL ); // Waits for new connection
		}
	}
}

/*
 * Parse the command sent by the client
 */
static xvc_cmd_t get_command( void* rx_data)
{
	if(strncmp( (char*)rx_data, "shift:", 6) == 0) // The most frequent command is tested first for efficiency
		return XVC_CMD_SHIFT;

	if(strncmp( (char*)rx_data, "settck", 6) == 0)
		return XVC_CMD_SETTCK;

	if(strncmp( (char*)rx_data, "getinf", 6) == 0)
		return XVC_CMD_GETINFO;

	return XVC_CMD_NONE;
}

/*
 * Parse the uint32 command argument argument
 *
 * This function extracts the 4-byte little-endian integer present after the
 * command label string by informing its length in cmd_length
 */
static uint32_t get_command_arg( void* rx_data , int cmd_length )
{
	int const arg_pos = cmd_length;

	uint8_t* arg_array = (uint8_t*)(rx_data + arg_pos);

	// Recompose as little-endian
	uint32_t arg = (((uint32_t)arg_array[0])    ) +
	               (((uint32_t)arg_array[1])<<8 ) +
	               (((uint32_t)arg_array[2])<<16) +
	               (((uint32_t)arg_array[3])<<24);

	return arg;
}


/*
 * Wait for the semaphore
 */
static int take_hsem(void)
{
	int	struct_is_ready = 0;
	TickType_t const loop_init_time = xTaskGetTickCount();
	while( !struct_is_ready )
	{
		if( HAL_OK == HAL_HSEM_FastTake(HSEM_ID_1) )
		{
			struct_is_ready = 1; // Done. Leave the loop and keep the semaphore
		}

		//Timeout logic (1 second)
		if( ( xTaskGetTickCount() - loop_init_time ) > pdMS_TO_TICKS(1000) )
		{
			HAL_HSEM_Release(HSEM_ID_1, 0);
			return 0; // Timeout!
		}
	}

	return 1; // HSEM successfully taken
}


/*
 * Wait for the shift process to be done by the CM4 core and take the HSEM
 */
static int take_hsem_when_shift_is_done(void)
{
	int	struct_is_ready = 0;
	TickType_t const loop_init_time = xTaskGetTickCount();
	while( !struct_is_ready )
	{
		if( HAL_OK == HAL_HSEM_FastTake(HSEM_ID_1) )
		{
			SCB_InvalidateDCache_by_Addr((uint32_t*)( &xvc_ipc_data ), sizeof(xvc_ipc_data_t));
			if( xvc_ipc_data.shift_done )
				struct_is_ready = 1; // Done. Leave the loop and keep the semaphore
			else
				HAL_HSEM_Release(HSEM_ID_1, 0); // NOT done. Keep in the loop and release the semaphore
		}

		//Timeout logic (1 second)
		if( ( xTaskGetTickCount() - loop_init_time ) > pdMS_TO_TICKS(1000) )
		{
			HAL_HSEM_Release(HSEM_ID_1, 0);
			return 0; // Timeout!
		}
	}

	return 1; // HSEM successfully taken
}


static void xvc_set_clk_period(uint32_t const requested_period_ns, uint32_t* actual_period_ns)
{
	// If above max freq, set & return max freq
	if (requested_period_ns <= XVC_MIN_NS_PER_TIM8)
	{
		xvc_jtag_clk_prescaler = 0;
		*actual_period_ns = XVC_MIN_NS_PER_TIM8;
	}
	else if (requested_period_ns >= XVC_MAX_NS_PER_TIM8)
	{
		xvc_jtag_clk_prescaler = 99;
		*actual_period_ns = XVC_MIN_NS_PER_TIM8;
	}
	else
	{
		*actual_period_ns = XVC_MIN_NS_PER_TIM8;
		// Find a compatible value
		uint32_t i = 0;
		while( ( i < XVC_MAX_PSC_VALUE_TIM8 ) && ( *actual_period_ns < requested_period_ns ) )
		{
			*actual_period_ns += XVC_MIN_NS_PER_TIM8;
			 ++i;
		}
		xvc_jtag_clk_prescaler = i;
	}
}
