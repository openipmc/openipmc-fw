/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: 	yimpl_cli.c
 * @brief	:	Terminal callback functions source code.
 * @date	:	YYYY / MM / DD - 2022 / 03 / 30
 * @author	:	Antonio V. G. Bassi
 */

/* standard C*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* yaffs resources */
#include "yaffs_guts.h"
#include "yaffsfs.h"

/* private includes */
#include "terminal.h"
#include "mt_printf.h"
#include "yimpl_cli.h"
#include "w25n01gv.h"

#define MEMSTAT_CB_OUTPUT_FMT "\r\n\
Device : Winbond 25N01GVZEIG\r\n\
YAFFS-2 Partition Name : %s\r\n\
Total Space: %d Bytes\r\n\
Free Space:  %d Bytes\r\n\
In use:      %d Bytes\r\n\
Usage:       %.2f%%\r\n\
Available:   %.2f%%\r\n"

extern struct yaffs_dev dev;

static int 	yimpl_cli_format(int u_flag, int f_flag, int r_flag);
static int 	yimpl_cli_mount(void);
static int 	yimpl_cli_umount(int f_flag);
static int 	yimpl_cli_mkdir(const char* path);
static int 	yimpl_cli_remove(const char* path);
static int 	yimpl_cli_list(const char* path);
static int 	yimpl_cli_write(const char* path, const char* mode, const char* input);
static int 	yimpl_cli_print(const char* path);
static int	yimpl_cli_chmod(const char* path, int rd_flag, int wr_flag);
static void yimpl_cli_print_errno(int err);

static int yimpl_cli_format(int u_flag, int f_flag, int r_flag)
{
	int result = -1;

	// Can't operate unless partition is mounted
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}
	const char* name = dev.param.name;
	result = yaffs_format(name, u_flag, f_flag ,r_flag);
	return result;
}

static int yimpl_cli_mount(void)
{
	int result = -1;
	const char* name = dev.param.name;
	result = yaffs_mount(name);
	return result;
}

static int yimpl_cli_umount(int f_flag)
{
	int result = -1;
	const char* name = dev.param.name;
	result = yaffs_unmount2(name, f_flag);
	return result;
}

static int yimpl_cli_mkdir(const char* path)
{
	int result = -1;

	// Can't operate unless partition is mounted
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	int depth = 0;
	int dir;
	int s_flag = ( S_IREAD | S_IWRITE );

	/* Let's check directory depth */
	for( int index = 0; index < strlen(path); ++index )
		( ( path[index] == '/') && ( ++depth ) );

	if( depth > 4 )
	{
		mt_printf("\r\nyaffs: Maximum directory depth reached! Aborted.");
		return result;
	}

	dir = yaffs_mkdir(path, s_flag);
	if( dir < 0 )
	{
		return result;
	}
	else
	{
		result = 0;
		return result;
	}
}

static int yimpl_cli_list(const char* path)
{
	int result = -1;
	// Can't operate unless partition is mounted
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	yaffs_DIR* dir;               // Directory
	struct yaffs_dirent* dirent;  // Directory entity, i.e files, other directories, symbolic links...
	struct yaffs_stat stat;       // Directory entity status
	int  ent_len = 0;
	int  path_len = strlen(path);
	char buf[DIRSTR_LEN]= {0};
	char perms[5] = "----";       // file system permissions

	dir = yaffs_opendir(path);
	if( !dir )
		return result;

	else
	{
		while( ( dirent = yaffs_readdir(dir) ) != NULL )
		{
			ent_len = strlen(dirent->d_name);
			// Format "dirent" into given path.
			snprintf(buf, (size_t)(path_len + ent_len + 2),"%s/%s", path, dirent->d_name);
			// Get "dirent" information
			yaffs_lstat(buf, &stat);

			// File system permissions
			( ( ( stat.st_mode & S_IFMT ) == S_IFDIR ) && (perms[0] = 'd') );
			( ( stat.st_mode &   S_IREAD  ) && ( perms[1] = 'r' ) );
			( ( stat.st_mode &   S_IWRITE ) && ( perms[2] = 'w' ) );
			( ( stat.st_mode &   S_IEXEC  ) && ( perms[3] = 'x' ) );

			mt_printf("\r\n%-10s  inode %-10d  length %-10d  %-4s  ",
					dirent->d_name, stat.st_ino, (int)stat.st_size, perms);

			switch(stat.st_mode & S_IFMT)
			{
				case S_IFREG:
					mt_printf("data file");
					break;

				case S_IFDIR:
					mt_printf("directory");
					break;

				case S_IFLNK:
					mt_printf("symlink -->");
					if( yaffs_readlink( buf, buf, 100 ) < 0 )
						mt_printf("no alias");
					else
						mt_printf("\"%s\"", buf);
					break;

				default:
					mt_printf("unknown");
					break;
			}
			memset(perms, '-', ( sizeof(perms) - 1 ) );
		}
		yaffs_closedir(dir);
		result = 0;
		return result;
	}
}

static int yimpl_cli_remove(const char* path)
{
	int result = -1;

	// Deleting mount point is prohibited.
	if( (!strcmp(path, "/") ) || (!strcmp(path, "") ) )
	{
		yaffs_set_error(-EFAULT);
		return result;
	}
	// Can't operate if device is not mounted.
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	yaffs_DIR* dir;                   // Directory
	struct yaffs_dirent* dirent;      // Directory entity, i.e files, other directories, symbolic links...
	struct yaffs_stat stat, entstat;  // Directory entity status
	char buf[DIRSTR_LEN] = {0x00};
	int  ent_len = 0;
	int  path_len = strlen(path);

	if( 0 > yaffs_lstat(path, &stat) )
	{
		return result;
	}

	else if( ( stat.st_mode & S_IFMT ) == S_IFREG )	// Is given path a file?
	{
		result = yaffs_unlink(path);
		return result;
	}
	else if( ( stat.st_mode & S_IFMT ) == S_IFDIR ) // Is given path a directory?
	{
		dir = yaffs_opendir(path);

		if( !dir )
			return result;

		while( ( dirent = yaffs_readdir(dir) ) != NULL )
		{
			ent_len = strlen(dirent->d_name);
			snprintf(buf, (size_t)(path_len + ent_len + 2) ,"%s/%s", path, dirent->d_name);
			yaffs_lstat(buf, &entstat);
			switch( entstat.st_mode & S_IFMT )
			{
				case S_IFREG:
					yaffs_unlink(buf);
					break;
				case S_IFLNK:
					yaffs_unlink(buf);
					break;
				case S_IFDIR:
					break;
			}
			if( ( entstat.st_mode & S_IFMT ) == S_IFDIR )
			{
				mt_printf("\r\nyaffs: There is a directory in the specified path!");
				yaffs_closedir(dir);
				return result;
			}
		}
		yaffs_closedir(dir);
		yaffs_rmdir(path);
		result = 0;
		return result;
	}
	else
		return result;
}


static int yimpl_cli_write( const char* mode,
					  const char* path,
					  const char* input )
{
	int result = -1;

	// Can't operate if device is not mounted.
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	const int s_flag = ( S_IREAD | S_IWRITE );
	int o_flag;
	int h;
	size_t length;

	if( !strcmp(mode, "-mk") )
		o_flag = ( O_CREAT | O_EXCL  | O_RDWR );
	else if( !strcmp(mode, "-ow") )
		o_flag = ( O_CREAT | O_TRUNC | O_RDWR );
	else if( !strcmp(mode, "-ap") )
		o_flag = ( O_CREAT | O_APPEND| O_RDWR );
	else
	{
		yaffs_set_error( (int)(-EINVAL) );
		return result;
	}

	h = yaffs_open(path, o_flag, s_flag);

	if( h < 0 )
		return result;
	else
	{
		length = strlen( input );
		yaffs_lseek(h, 0, SEEK_SET);
		result = yaffs_write(h, input, length);
		yaffs_close(h);
		return result;
	}
}

static int yimpl_cli_print(const char* path)
{
	int result = -1;

	// Can't operate if device is not mounted.
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	struct yaffs_stat stat;
	const int o_flag = O_RDONLY;
	const int s_flag = S_IREAD;
	char ch = 0x00;
	int nBytes = 0;

	if( 0 > yaffs_lstat(path, &stat) )
	{
		return result;
	}

	else if( (stat.st_mode & S_IFMT ) == S_IFDIR )
	{
		mt_printf("\nprint: %s is a directory.", path);
		result = 0;
		return result;
	}

	int h = yaffs_open(path, o_flag, s_flag);

	if( h < 0 )
		return result;
	else
	{
		yaffs_lseek(h, 0, SEEK_SET);
		mt_printf("\r\n");
		while( yaffs_read(h, &ch, 1) > 0 )
		{
			mt_printf("%c", ch);
			++nBytes;
			if( '\n' == ch )
			{
			  mt_printf("\r");
			}
		}
		mt_printf("\r\nprint: %d bytes read", nBytes);
		result = 0;
		yaffs_close(h);
		return result;
	}
}

static int yimpl_cli_chmod(const char* path, int rd_flag, int wr_flag)
{
	int result = -1;

	// Can't operate if device is not mounted.
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return result;
	}

	int s_flag = 0;

	s_flag |= rd_flag ? S_IREAD  : 0;
	s_flag |= wr_flag ? S_IWRITE : 0;

	result = yaffs_chmod(path, s_flag);

	return result;
}

static void yimpl_cli_print_errno(int err)
{
	switch( err )
	{
		case 0:
			break;
		case -ENOENT:
			mt_printf("\r\nyaffs: No such file or directory.");
			break;
		case -EBADF:
			mt_printf("\r\nyaffs: Bad file number.");
			break;
		case -ENOMEM:
			mt_printf("\r\nyaffs: Out of memory!");
			break;
		case -EACCES:
			mt_printf("\r\nyaffs: Permission denied.");
			break;
		case -EFAULT:
			mt_printf("\r\nyaffs: Bad address.");
			break;
		case -EBUSY:
			mt_printf("\r\nyaffs: Device is busy.");
			break;
		case -ENODEV:
			mt_printf("\r\nyaffs: No such device.");
			break;
		case -EINVAL:
			mt_printf("\r\nyaffs: Invalid argument.");
			break;
		case -EXDEV:
			mt_printf("\r\nyaffs: Cross-device link.");
			break;
		case -ENOSPC:
			mt_printf("\r\nyaffs: No space left on device!");
			break;
		case -ERANGE:
			mt_printf("\r\nyaffs: Math result not representable.");
			break;
		case -ENODATA:
			mt_printf("\r\nyaffs: No data available.");
			break;
		case -ENOTEMPTY:
			mt_printf("\r\nyaffs: Directory is not empty.");
			break;
		case -ENAMETOOLONG:
			mt_printf("\r\nyaffs: File name too long.");
			break;
		case -EEXIST:
			mt_printf("\r\nyaffs: File exists.");
			break;
		case -ENOTDIR:
			mt_printf("\r\nyaffs: Not a directory.");
			break;
		case -EISDIR:
			mt_printf("\r\nyaffs: Is a directory.");
			break;
		case -ENFILE:
			mt_printf("\r\nyaffs: File table overflow!");
			break;
		case -EROFS:
			mt_printf("\r\nyaffs: Read-only file system.");
			break;
		default:
			mt_printf("\r\nyaffs: An unknown error has occurred -> error code : %d", err);
			break;
	}
	return;
}

uint8_t format_cb(void)
{
	int result = -1;
	int u_flag = 0;
	int f_flag = 0;
	int r_flag = 0;

	// if IsArgFlag > 0 then flag = 1 otherwise flag = 0
	u_flag = CLI_IsArgFlag("-u") ? 1 : 0;
	f_flag = CLI_IsArgFlag("-f") ? 1 : 0;
	r_flag = CLI_IsArgFlag("-r") ? 1 : 0;

	result = yimpl_cli_format(u_flag, f_flag, r_flag);
	if( 0 == result )
	{
		mt_printf("\r\nyaffs: Device successfully formatted.");
	}
	else
	{
    	result = yaffsfs_get_errno();
    	yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t mount_cb( void )
{
	int result = yimpl_cli_mount();
    if(0 > result)
    {
    	mt_printf("\r\nyaffs: Device could not be mounted");
    	result = yaffsfs_get_errno();
    	yimpl_cli_print_errno(result);
        return TE_OK;
    }
    else
    {
    	mt_printf("\r\nyaffs: Device successfully mounted");
        return TE_OK;
    }
}

uint8_t umount_cb( void )
{
	// if IsArgFlag > 0 then flag = 1 otherwise flag = 0
	int f_flag = CLI_IsArgFlag("-f") ? 1 : 0;
	int result = yimpl_cli_umount(f_flag);

	if(0 == result)
		mt_printf("\r\nyaffs: Device successfully unmounted");
	else
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t write_cb( void )
{
	const char* mode  = CLI_GetArgv()[1];
	const char* path  = CLI_GetArgv()[2];
	const char* input = CLI_GetArgv()[3];

	int result = yimpl_cli_write(mode, path, input);

	if(0 <= result)
		mt_printf("\r\nyaffs: File successfully written! %d bytes", result);
	else
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}

	return TE_OK;
}

uint8_t print_cb( void )
{
	const char* path = CLI_GetArgv()[1];
	int result = yimpl_cli_print( path );
	if( 0 > result )
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t mkdir_cb( void )
{
	int result = -1;
	const char* path = CLI_GetArgv()[1];
	result = yimpl_cli_mkdir(path);
	if( 0 > result )
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t remove_cb( void )
{
	int result = -1;
	char *path;
	path = CLI_GetArgv()[1];
	result = yimpl_cli_remove(path);
	if( 0 > result )
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t list_cb( void )
{
	char const* path = NULL;
	int result = -1;

	if(CLI_GetArgc() >= 2)
	{
		path = CLI_GetArgv()[1];
	}
	else
	{
		path = dev.param.name;
	}
	result = yimpl_cli_list(path);
	if( 0 > result )
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
	return TE_OK;
}

uint8_t chmod_cb( void )
{
	int i = 1;
	int rd_flag = 0;
	int wr_flag = 0;
	char* path = NULL;

	while( i < CLI_GetArgc() )
	{
		if( !strcmp(CLI_GetArgv()[i], "-rd") )
		{
			rd_flag = 1;
		}
		else if( !strcmp(CLI_GetArgv()[i], "-wr") )
		{
			wr_flag = 1;
		}
		else
		{
			path = CLI_GetArgv()[i];
		}
		i++;
	}

	int result = yimpl_cli_chmod(path, rd_flag, wr_flag);
	if( 0 > result )
	{
		result = yaffsfs_get_errno();
		yimpl_cli_print_errno(result);
	}
  return TE_OK;
}

uint8_t memstat_cb( void )
{
	// Can't operate unless partition is mounted
	if( !dev.is_mounted )
	{
		yaffs_set_error(-ENODEV);
		return TE_OK;
	}
	const char* name = dev.param.name;
	int result_free = ( int )( yaffs_freespace(name) );
	yaffsfs_get_errno();
	int result_total = ( int )( yaffs_totalspace(name) );
	yaffsfs_get_errno();

	if( result_free < 0 || result_total < 0 )
	{
		mt_printf("\r\nyaffs: Could not get requested information.");
		return TE_OK;
	}
	else
	{
		int diff = result_total - result_free;
		float usg = 100.000*((float)diff/(float)result_total);
		float avl = 100.000*((float)result_free/(float)result_total);
		mt_printf(MEMSTAT_CB_OUTPUT_FMT, name, result_total, result_free, diff, usg, avl);
		return TE_OK;
	}
}

#define MSB_LBA_MASK 0xC000
uint8_t read_bbm_lut_cb(void)
{
	uint16_t lut[W25N01GV_LUT_NSHORTS] = {0};

	// read bad block look up table
	w25n01gv_mutex_take();
	while (w25n01gv_is_busy())
	{
		__asm__("nop");
	}
	int const result = w25n01gv_read_bbm_lut(lut);
	w25n01gv_mutex_give();

	if (result < 0)
	{
		mt_printf("Could not read flash device's bad block look-up table.\r\n");
		return TE_OK;
	}

	for (size_t i_short = 0U; i_short < W25N01GV_LUT_NSHORTS; i_short += 2U)
	{
	  uint16_t const meta_lba = lut[i_short    ];           // metadata(2 bit) + logical block address (14-bit)
	  uint16_t const pba      = lut[i_short + 1];           // physical block address
	  uint16_t const msb      = meta_lba & MSB_LBA_MASK;    // most significant bits
    uint16_t const lba      = meta_lba & (~MSB_LBA_MASK); // logical block address

    switch(msb)
    {
      case 0x0000:
        mt_printf("LBA[%04d]->PBA[%04d] is free for use.\r\n", lba, pba);
        break;
      case 0x8000:
        mt_printf("LBA[%04d]->PBA[%04d] is a valid remapping.\r\n", lba, pba);
        break;
      case 0xC000:
        mt_printf("LBA[%04d]->PBA[%04d] is an invalidated remapping.\r\n", lba, pba);
        break;
      default:
        mt_printf("LBA[%04d]->PBA[%04d] is in a corrupted state.\r\n", lba, pba);
        break;
    }
	}

	return TE_OK;
}
