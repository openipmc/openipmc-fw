/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * 	@file	  : yimpl_tftp.h
 * 	@brief  :	TFTP protocol integration with YAFFS-2 File System
 * 	@date	  :	YYYY / MM / DD - 2022 / 08 / 17
 * 	@author	: Antonio V. G. Bassi
 */

#ifndef SRC_YIMPL_TFTP_H_
#define SRC_YIMPL_TFTP_H_

#include <string.h>
#include "tftp_server.h"
#include "yaffs_guts.h"
#include "yaffsfs.h"
#include "mt_printf.h"

extern struct yaffs_dev dev;
err_t yimpl_tftp_close_server(void);
err_t yimpl_tftp_launch_server(void);

#endif /* SRC_YIMPL_TFTP_H_ */
