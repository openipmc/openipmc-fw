/*
 * openipmc_fw_utils.c
 *
 *  Created on: 8 de set de 2022
 *      Author: antoniobassi
 */

#include <stddef.h>
#include "openipmc_fw_utils.h"

/**
 *
 *
 */
size_t strnlen_s(const char* str, size_t strsz)
{
	size_t len = 0;

	if( str == NULL )
		return len;

	for(len = 0; len < strsz; len++)
	{
		if(str[len] == '\0')
		{
			break;
		}
	}
	return len;
}
