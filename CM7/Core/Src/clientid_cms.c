/*
 * atca_clientid.c
 *
 *  Created on: 26 de jul de 2022
 *      Author: antoniobassi
 */

#include "clientid_cms.h"

#include "ipmc_ios.h"
#include "ipmi_msg_manager.h"

#include <stdio.h>
#include <memory.h>

// This definition follows CERN EDMS 2735323 (rev. 1.0 on 09/06/2022)
// "Custom hardware network interface specification for Phase-2 CMS"
// "Applicable to ATCA on-board controllers and any other custom network devices"
#define CLIENTID_SIZE                  (46U)
#define CLIENTID_HEADER_Pos             (0U)
#define CLIENTID_HEADER_Len             (1U)
#define CLIENTID_DATALENGTH_Pos         (1U)
#define CLIENTID_DATALENGTH_Len         (1U)
#define CLIENTID_TYPE_Pos               (2U)
#define CLIENTID_TYPE_Len               (1U)
#define CLIENTID_IAID_Pos               (3U)
#define CLIENTID_IAID_Len               (4U)
#define CLIENTID_DUIDFMT_Pos            (7U)
#define CLIENTID_DUIDFMT_Len            (2U)
#define CLIENTID_DUIDHDR1_Pos           (9U)
#define CLIENTID_DUIDHDR1_Len           (4U)
#define CLIENTID_DUIDHDR2_Pos          (13U)
#define CLIENTID_DUIDHDR2_Len           (5U)
#define CLIENTID_DUIDHDR3_Pos          (18U)
#define CLIENTID_DUIDHDR3_Len           (2U)
#define CLIENTID_DUIDSHADTYPE_Pos      (20U)
#define CLIENTID_DUIDSHADTYPE_Len       (1U)
#define CLIENTID_DUIDSHAD_Pos          (21U)
#define CLIENTID_DUIDSHAD_Len          (20U)
#define CLIENTID_DUIDSHTYPE_Pos        (41U)
#define CLIENTID_DUIDSHTYPE_Len         (1U)
#define CLIENTID_DUIDPRISITETYPE_Pos   (42U)
#define CLIENTID_DUIDPRISITETYPE_Len    (1U)
#define CLIENTID_DUIDPRISITENUM_Pos    (43U)
#define CLIENTID_DUIDPRISITENUM_Len     (1U)
#define CLIENTID_DUIDSECSITETYPE_Pos   (44U)
#define CLIENTID_DUIDSECSITETYPE_Len    (1U)
#define CLIENTID_DUIDSECSITENUM_Pos    (45U)
#define CLIENTID_DUIDSECSITENUM_Len     (1U)


//#define HPM3_HEADER                 0x3D
#define HPM3_DATA_LENGTH            0x2C
#define HPM3_TYPE                   0xFF
#define HPM3_IAID                   0x00000002
#define HPM3_IAID_0                 0x00
#define HPM3_IAID_1                 0x00
#define HPM3_IAID_2                 0x00
#define HPM3_IAID_3                 0x02
#define HPM3_DUID_FORMAT            0x0002
#define HPM3_DUID_FORMAT_0          0x00
#define HPM3_DUID_FORMAT_1          0x02
#define HPM3_DUID_HEADER1           0x0000315A
#define HPM3_DUID_HEADER1_0         0x00
#define HPM3_DUID_HEADER1_1         0x00
#define HPM3_DUID_HEADER1_2         0x31
#define HPM3_DUID_HEADER1_3         0x5A
#define HPM3_DUID_HEADER2          "HPM.3"
#define HPM3_DUID_HEADER2_0        'H'
#define HPM3_DUID_HEADER2_1        'P'
#define HPM3_DUID_HEADER2_2        'M'
#define HPM3_DUID_HEADER2_3        '.'
#define HPM3_DUID_HEADER2_4        '3'
#define HPM3_DUID_HEADER3          0x2D31
#define HPM3_DUID_HEADER3_0        0x2D
#define HPM3_DUID_HEADER3_1        0x31

#define CMSHPM3_DUIDSHADTYPE     	  0xD4
#define CMSHPM3_DUIDSHTYPE          0x00 // 0x00 = This component lies inside an ATCA shelf
#define CMSHPM3_DUIDPRISITETYPE     0x00 // 0x00 = This is the IPMC of an ATCA carrier
#define CMSHPM3_DUIDSECSITETYPE     0x00 // 0x00 = The IPMC is a primary site, so secondary is zero
#define CMSHPM3_DUIDSECSITENUM      0x00 // 0x00 = The IPMC is a primary site, so secondary is zero

int client_id_from_atca_cms(struct dhcp_client_id* client_id)
{
	client_id->length = HPM3_DATA_LENGTH;
	client_id->id[ 0] = HPM3_TYPE;
	client_id->id[ 1] = HPM3_IAID_0;
	client_id->id[ 2] = HPM3_IAID_1;
	client_id->id[ 3] = HPM3_IAID_2;
	client_id->id[ 4] = HPM3_IAID_3;
	client_id->id[ 5] = HPM3_DUID_FORMAT_0;
	client_id->id[ 6] = HPM3_DUID_FORMAT_1;
	client_id->id[ 7] = HPM3_DUID_HEADER1_0;
	client_id->id[ 8] = HPM3_DUID_HEADER1_1;
	client_id->id[ 9] = HPM3_DUID_HEADER1_2;
	client_id->id[10] = HPM3_DUID_HEADER1_3;
	client_id->id[11] = HPM3_DUID_HEADER2_0;
	client_id->id[12] = HPM3_DUID_HEADER2_1;
	client_id->id[13] = HPM3_DUID_HEADER2_2;
	client_id->id[14] = HPM3_DUID_HEADER2_3;
	client_id->id[15] = HPM3_DUID_HEADER2_4;
	client_id->id[16] = HPM3_DUID_HEADER3_0;
	client_id->id[17] = HPM3_DUID_HEADER3_1;
	client_id->id[18] = CMSHPM3_DUIDSHADTYPE;

	client_id->id[39] = CMSHPM3_DUIDSHTYPE;
	client_id->id[40] = CMSHPM3_DUIDPRISITETYPE;

	client_id->id[42] = CMSHPM3_DUIDSECSITETYPE;
	client_id->id[43] = CMSHPM3_DUIDSECSITENUM;


	{

		/* Shelf Address format as specified by CMS:
		 * ATCA-{building}-{rack id}-{rack unit-number}
		 * e.g. ATCA-40-SB/01/R01-16
		 */

		uint8_t request_data[1] = {
			0x00  // PICMG Identifier 0x00
			};

		uint8_t completion_code;
		uint8_t response_data[24];
		int     response_len;

        int const transaction_outcome = ipmi_msg_send_request(
            IPMI_ROUTER_CHANNEL_IPMB_0, // IPMB-0 channel
            0x2C, // NetFn: PICMG
            0x02, // CMD  : Get Shelf Address Info
            request_data,
            1,
            &completion_code,
            response_data,
            &response_len
			);

		switch (transaction_outcome)
		{
			case IPMI_MSG_SEND_TIMEOUT:
				return CLIENTID_ERR_IPMI_TIMEOUT;
				break;
			case IPMI_MSG_SEND_ERROR:
			default:
				return CLIENTID_ERR_IPMI_ERROR;
				break;
			case IPMI_MSG_SEND_OK:
				break;
		}

		uint8_t const shelf_addr_type_len = response_data[1];
		uint8_t const shelf_addr_type = (shelf_addr_type_len & 0b11000000) >> 6;
		uint8_t const shelf_addr_len  = (shelf_addr_type_len & 0b00111111)     ;

		if (completion_code != 0x00)
			return CLIENTID_ERR_IPMI_ERROR;

		// See PICMG 3.0 section 3.1.5.2 paragraph 45
		switch (shelf_addr_type)
		{
			case 0b00: // binary or unspecified
			case 0b11: // Conventional ASCII
				for (unsigned i = 0; i < 20; ++i)
					client_id->id[19+i] = 0; // Clean Field
				for (unsigned i = 0; i < shelf_addr_len; ++i)
					client_id->id[19+i] = response_data[2+i];
				break;
			case 0b01: // BCD plus
			case 0b10: // 6-bit ASCII, packed
			default:
				client_id->id[19] = 'F';
				client_id->id[20] = 'O';
				client_id->id[21] = 'R';
				client_id->id[22] = 'M';
				client_id->id[23] = 'A';
				client_id->id[24] = 'T';
				client_id->id[25] = ' ';
				client_id->id[26] = 'N';
				client_id->id[27] = 'O';
				client_id->id[28] = 'T';
				client_id->id[29] = ' ';
				client_id->id[30] = 'S';
				client_id->id[31] = 'U';
				client_id->id[32] = 'P';
				client_id->id[33] = 'P';
				client_id->id[34] = 'O';
				client_id->id[35] = 'R';
				client_id->id[36] = 'T';
				client_id->id[37] = 'E';
				client_id->id[38] = 'D';
				return CLIENTID_ERR_FMT_NOT_IMPLEMENTED;
				break;
		}
	}

	// Block resolving the physical slot address
	// TODO: verify with the HW Infra group if this is correct
	{
		uint8_t const atca_hw_addr = ipmc_ios_read_haddress();

		if (atca_hw_addr == HA_PARITY_FAIL)
		{
			client_id->id[41] = 0xFF;
			return CLIENTID_ERR_HWADDR_PARITY_FAIL;
		}

		uint8_t request_data[5] = {
			0x00, // PICMG Identifier 0x00
			0x00, // FRU Device ID in this carrier (IPMC = 0x00)
			0x00, // 0x00 = Hardware Address, 0x01 = IPMB-0 Address, 0x02 = Reserved for PICMG® 2.9, 0x03 = Physical Address
			0x00, // We will set it below
			0x00  // Site Type 0x00 = Front board/IPMC
			};

		request_data[3] = atca_hw_addr;

		uint8_t completion_code;
		uint8_t response_data[7];
		int     response_len;

        int const transaction_outcome = ipmi_msg_send_request(
            IPMI_ROUTER_CHANNEL_IPMB_0, // IPMB-0 Channel
            0x2C, // NetFn: PICMG
            0x01, // CMD  : Get Address Info
            request_data,
            5,
            &completion_code,
            response_data,
            &response_len
			);

		switch(transaction_outcome)
		{
			case IPMI_MSG_SEND_OK:
				client_id->id[41] = response_data[5]; // Site number is the sixth byte (or seventh, if including the completion code)
				break;
			case IPMI_MSG_SEND_TIMEOUT:
				client_id->id[41] = 0xFF;
				return CLIENTID_ERR_IPMI_TIMEOUT;
			case IPMI_MSG_SEND_ERROR:
			default:
				client_id->id[41] = 0xFF;
				return CLIENTID_ERR_IPMI_ERROR;
				break;
		}
	}

	return CLIENTID_ERR_OK;
}


