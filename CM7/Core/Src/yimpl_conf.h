/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file    : yaffsfs_conf.h
 * @brief   : Yaffs2 configuration header body.
 * @date	: YYYY / MM / DD - 2021 / 11 / 05
 * @author  : Antonio V. G. Bassi
 *
 */

#ifndef SRC_YIMPL_CONF_H_
#define SRC_YIMPL_CONF_H_

/* functions */
extern int yimpl_setup( void );
extern int yimpl_setup_ready(void);

#endif /* SRC_YIMPL_CONF_H_ */
