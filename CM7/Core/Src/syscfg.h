/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2024 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file   syscfg.h
 * @brief  Configuration storage management resources.
 * @date   YYYY / MM / DD - 2022 / 10 / 07
 * @author Antonio V. G. Bassi, Luigi Calligaris
 */

#ifndef SRC_SYSCFG_H_
#define SRC_SYSCFG_H_

#include <stddef.h>

#define SYSCFG_KEY_MAXLEN     16
#define SYSCFG_VALUE_MAXLEN   16
#define SYSCFG_DIRSTR_MAXLEN  32

/* Error codes */
typedef enum syscfg_err
{
  SYSCFG_ETRUNC      =  2, // Value or key strings assigned to node were truncated due to size
  SYSCFG_EREPLD      =  1, // Key had its value replaced
	SYSCFG_SUCCESS     =  0, // Operation successful
	SYSCFG_EMALLOCFAIL = -1, // Dynamic allocation of space for new node failed
	SYSCFG_EHEAPINIT   = -2, // Failure to allocate syscfg heap
	SYSCFG_ENODE       = -3, // Configuration node does not exist
	SYSCFG_ENOFILE     = -4, // File does not exist
	SYSCFG_EDIR        = -5, // Directory could not be created
	SYSCFG_EINVAL      = -6, // Invalid function argument
	SYSCFG_ENOLST      = -7, // List is empty
  SYSCFG_EINIT       = -8  // Generic error in syscfg init
}syscfg_err_t;

extern void syscfg_print_node_list(void);
extern syscfg_err_t syscfg_remove(char *key);
extern syscfg_err_t syscfg_save(void);
extern syscfg_err_t syscfg_get_valuesize_from_key(char* key, size_t* valuesize);
extern syscfg_err_t syscfg_get_value_from_key(char* key, char* value, size_t maxlen);
extern syscfg_err_t syscfg_insert(char *key, char *value);

#endif /* SRC_SYSCFG_H_ */
