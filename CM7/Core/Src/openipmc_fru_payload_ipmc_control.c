
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2023 Luigi Calligaris                                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

#include "FreeRTOS.h"
#include "task.h"

#include "fru_state_machine.h"
#include "fru_payload_ipmc_control.h"
#include "dimm_gpios.h"

void impl_ipmc_get_device_guid(uint8_t* compl_code, uint8_t* guid_16bytes);


extern void reply_begin_ipmc_cold_reset(uint8_t* compl_code);
extern void reply_begin_ipmc_warm_reset(uint8_t* compl_code);
extern void reply_issue_ipmc_manufacturing_test(uint8_t* compl_code);

extern void reply_begin_payload_cold_reset(uint8_t* compl_code);
extern void reply_begin_payload_warm_reset(uint8_t* compl_code);
extern void reply_begin_payload_graceful_reboot(uint8_t* compl_code);
extern void reply_issue_payload_diagnostic_int(uint8_t* compl_code);


extern void impl_begin_ipmc_cold_reset();
extern void impl_begin_ipmc_warm_reset();
extern void impl_start_ipmc_manufacturing_test();

extern void impl_begin_payload_cold_reset();
extern void impl_begin_payload_warm_reset();
extern void impl_begin_payload_graceful_reboot();
extern void impl_issue_payload_diagnostic_int();


/**
 * @brief Function implementing the calculation and formatting of the GUID/UUID of the IPMC
 * 
 * @param compl_code      Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * @param guid_16bytes    GUID returned for transmission back to the requesting agent.
 */
void impl_ipmc_get_device_guid(uint8_t* compl_code, uint8_t* guid_16bytes)
{
  // We use the random number-based algorithm described in RFC4122, as it is easier to
  // use with an MCU which already gives us its own UID. As this MCU UID is 96-bits long, 
  // we do some padding here and there. The UUID version used is the one for 
  // random/pseudorandom numbers, with rules:
  //
  // * Set the four most significant bits (bits 12 through 15) of the time_hi_and_version 
  //   field to 0 (LSbit), 0, 1, 0 (MSbit)
  // * Set the two most significant bits (bits 6 and 7) of the clock_seq_hi_and_reserved 
  //   to zero and one, respectively
  //
  // Why people do not use the (15 downto 12) paradigm? *sigh*
  //
  // Which we understood to be:
  // time_hi_and_version = (time_hi_and_version & 0b00001111) | (0b0100 << 4)
  // clock_seq_hi_and_reserved = (clock_seq_hi_and_reserved & 0b00111111) | (0b10 << 6)
  
  uint32_t const uIDw0 = HAL_GetUIDw0();
  uint32_t const uIDw1 = HAL_GetUIDw1();
  uint32_t const uIDw2 = HAL_GetUIDw2();
  
  guid_16bytes[ 0] = (uIDw0 && 0x000000FF)      ; //  0 time_low LSByte
  guid_16bytes[ 1] = (uIDw0 && 0x0000FF00) >>  8; //  1 time_low
  guid_16bytes[ 2] = (uIDw0 && 0x00FF0000) >> 16; //  2 time_low
  guid_16bytes[ 3] = (uIDw0 && 0xFF000000) >> 24; //  3 time_low MSByte
  guid_16bytes[ 4] = (uIDw1 && 0x000000FF)      ; //  4 time_mid LSByte
  guid_16bytes[ 5] = (uIDw1 && 0x0000FF00) >>  8; //  5 time_mid MSByte
  guid_16bytes[ 6] = (uIDw1 && 0x00FF0000) >> 16; //  6 time_hi_and_version LSByte
  guid_16bytes[ 7] = 0b01000000                 ; //  7 time_hi_and_version MSByte
  guid_16bytes[ 8] = 0b10000000                 ; //  8 clock_seq_hi_and_reserved
  guid_16bytes[ 9] = (uIDw1 && 0xFF000000) >> 24; //  9 clock_seq_low
  guid_16bytes[10] = (uIDw2 && 0x000000FF)      ; // 10 node LSByte
  guid_16bytes[11] = (uIDw2 && 0x0000FF00) >>  8; // 11 node
  guid_16bytes[12] = (uIDw2 && 0x00FF0000) >> 16; // 12 node
  guid_16bytes[13] = (uIDw2 && 0xFF000000) >> 24; // 13 node
  guid_16bytes[14] = 'I'                        ; // 14 node
  guid_16bytes[15] = 'O'                        ; // 15 node MSByte
  
  compl_code = 0x00; // Command Completed Normally
}



/**
 * @brief Function implementing the completion code to an IPMC Cold Reset request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_begin_ipmc_cold_reset(uint8_t* compl_code)
{
  // We reply OK as this function is implemented
  *compl_code = 0x00;
}

__attribute__((weak)) void impl_begin_ipmc_cold_reset()
{
#if 0
  fru_enqueue_state_transition(DO_IPMC_COLD_RESET);
#else
  NVIC_SystemReset();
#endif
}

__attribute__((weak)) void do_begin_ipmc_cold_reset(uint8_t* compl_code)
{
  reply_begin_ipmc_cold_reset(compl_code);

  if (*compl_code == 0x00)
  {
    // Notice that this command fires the cold reset immediately, without passing
    // through the FRU state machine queue.
    impl_begin_ipmc_cold_reset();
  }
}


/**
 * @brief Function implementing the completion code to an IPMC Warm Reset request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_begin_ipmc_warm_reset(uint8_t* compl_code)
{
  // So far in this dummy implementation, we reply that the function is not available
  // (i.e. 0xCC, Invalid data field in Request, see PICMG 3.0 REQ 3.183). Once this 
  // function is properly implemented, we can reply 0x00 (Command Completed Normally).
  *compl_code = 0xCC; // Invalid data field in Request (See PICMG 3.0 REQ 3.183)
}

__attribute__((weak)) void impl_begin_ipmc_warm_reset()
{
  
}

__attribute__((weak)) void do_begin_ipmc_warm_reset(uint8_t* compl_code)
{
  reply_begin_ipmc_warm_reset(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_IPMC_WARM_RESET);
  }
}


/**
 * @brief Function implementing the completion code to an IPMC Enable Manufacturing Test request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_start_ipmc_manufacturing_test(uint8_t* compl_code)
{
  // So far in this dummy implementation, we reply that the function is not available
  // (i.e. 0xCC, Invalid data field in Request, see PICMG 3.0 REQ 3.183). Once this 
  // function is properly implemented, we can reply 0x00 (Command Completed Normally).
  *compl_code = 0xCC; // Invalid data field in Request (See PICMG 3.0 REQ 3.183)
}

__attribute__((weak)) void impl_start_ipmc_manufacturing_test()
{
  
}

__attribute__((weak)) void do_start_ipmc_manufacturing_test(uint8_t* compl_code)
{
  reply_start_ipmc_manufacturing_test(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_IPMC_MANUFACTURING_TEST);
  }
}


/**
 * @brief Function implementing the completion code to a Payload Cold Reset request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_begin_payload_cold_reset(uint8_t* compl_code)
{
  // We reply OK as this functon is implemented
  *compl_code = 0x00;
}

__attribute__((weak)) void impl_begin_payload_cold_reset()
{
  EN_12V_SET_STATE(RESET);
  vTaskDelay( pdMS_TO_TICKS(2000) );
  EN_12V_SET_STATE(SET);
}

__attribute__((weak)) void do_begin_payload_cold_reset(uint8_t* compl_code)
{
  reply_begin_payload_cold_reset(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_PAYLOAD_COLD_RESET);
  }
}


/**
 * @brief Function implementing the completion code to a Payload Warm Reset request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_begin_payload_warm_reset(uint8_t* compl_code)
{
  // So far in this dummy implementation, we reply that the function is not available
  // (i.e. 0xCC, Invalid data field in Request, see PICMG 3.0 REQ 3.183). Once this 
  // function is properly implemented, we can reply 0x00 (Command Completed Normally).
  *compl_code = 0xCC; // Invalid data field in Request (See PICMG 3.0 REQ 3.183)
}

__attribute__((weak)) void impl_begin_payload_warm_reset()
{
  
}

__attribute__((weak)) void do_begin_payload_warm_reset(uint8_t* compl_code)
{
  reply_begin_payload_warm_reset(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_PAYLOAD_WARM_RESET);
  }
}


/**
 * @brief Function implementing the completion code to a Payload Graceful Reboot request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_begin_payload_graceful_reboot(uint8_t* compl_code)
{
  // So far in this dummy implementation, we reply that the function is not available
  // (i.e. 0xCC, Invalid data field in Request, see PICMG 3.0 REQ 3.183). Once this 
  // function is properly implemented, we can reply 0x00 (Command Completed Normally).
  *compl_code = 0xCC; // Invalid data field in Request (See PICMG 3.0 REQ 3.183)
}

__attribute__((weak)) void impl_begin_payload_graceful_reboot()
{
  
}

__attribute__((weak)) void do_begin_payload_graceful_reboot(uint8_t* compl_code)
{
  reply_begin_payload_graceful_reboot(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_PAYLOAD_GRACEFUL_REBOOT);
  }
}


/**
 * @brief Function implementing the completion code to a Payload Diagnostic Interrupt request.
 *        This function is meant to check wether the system is ready to execute the 
 *        request and reply this state to the agent via the completion code.
 * @param compl_code Completion Code (definition in IPMI v1.5).
 */
__attribute__((weak)) void reply_issue_payload_diagnostic_int(uint8_t* compl_code)
{
  // So far in this dummy implementation, we reply that the function is not available
  // (i.e. 0xCC, Invalid data field in Request, see PICMG 3.0 REQ 3.183). Once this 
  // function is properly implemented, we can reply 0x00 (Command Completed Normally).
  *compl_code = 0xCC; // Invalid data field in Request (See PICMG 3.0 REQ 3.183)
}

__attribute__((weak)) void impl_issue_payload_diagnostic_int()
{
  
}

__attribute__((weak)) void do_issue_payload_diagnostic_int(uint8_t* compl_code)
{
  reply_issue_payload_diagnostic_int(compl_code);

  if (*compl_code == 0x00)
  {
    fru_enqueue_state_transition(DO_PAYLOAD_DIAGNOSTIC_INT);
  }
}
