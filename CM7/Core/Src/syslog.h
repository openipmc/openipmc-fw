/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *	@file	syslog.h
 *	@brief	Syslog protocol resources.
 *	@date	YYYY / MM / DD - 2022 / 08 / 30
 *	@author	Antonio V. G. Bassi, Luigi Calligaris
 */

#ifndef SRC_SYSLOG_H_
#define SRC_SYSLOG_H_

// Here the IP address bytes use an alphabetic specifier, since
// endianess can alter the meaning of "0","1","2" and "3"

#ifndef SYSLOG_DEFAULT_PORT
#define SYSLOG_DEFAULT_PORT 514
#endif


#ifdef SYSLOG_USES_CLIENTID_HOSTNAME
#define SYSLOG_CLIENTID_STR_OFFSET 19
#define SYSLOG_CLIENTID_STR_LEN    20
#endif /*SYSLOG_USES_CLIENTID_HOSTNAME*/

#define SYSLOG_RFC5424_FORMAT_SPEC_STR "<%u>%d %s %s %s - %s - %s\n"
#define SYSLOG_RFC3164_FORMAT_SPEC_STR "<%u>%s %s %s: %s %s\n"

#define SYSLOG_INVALID_MSG_STR "Message string is invalid or out of bounds!"
#define NILVALUE	"-"
#define WHITESPACE  0x20

#define SYSLOG_VERSION      1
#define SYSLOG_PRIVAL_CEIL  191

#define SYSLOG_PAYLOAD_SIZE       522
#define SYSLOG_BUFFER_SIZE        (SYSLOG_PAYLOAD_SIZE + 1)
#define SYSLOG_FREERTOSQUEUE_SIZE 1024

#define SYSLOG_MAX_APPNAME_STR     48U
#define SYSLOG_MAX_PROCID_STR     128U
#define SYSLOG_MAX_MSGID_STR       32U
#define SYSLOG_MAX_PRIVAL_STR       5U
#define SYSLOG_MAX_HOSTNAME_STR    20U
#define SYSLOG_MAX_TIMESTAMP_STR   32U
#define SYSLOG_MSG_MAX_STR        256U

#define SYSLOG_QUEUE_ITEM_LENGTH  32

typedef enum facility
{
  SYSFAC_KERNEL         = 0,
  SYSFAC_USER_LVL       = 1,
  SYSFAC_MAIL           = 2,
  SYSFAC_SYS_DAEMONS    = 3,
  SYSFAC_SEC_N_AUTH_0   = 4,
  SYSFAC_SYSLOGD        = 5,
  SYSFAC_LINE_PRINTER   = 6,
  SYSFAC_NETWORK_NEWS   = 7,
  SYSFAC_UUCP           = 8,
  SYSFAC_CLOCK_DAEMON_0 = 9,
  SYSFAC_SEC_N_AUTH_1   = 10,
  SYSFAC_FTP_DAEMON     = 11,
  SYSFAC_NTP_SUBSYS     = 12,
  SYSFAC_LOG_AUDIT      = 13,
  SYSFAC_LOG_ALERT      = 14,
  SYSFAC_CLOCK_DAEMON_1 = 15,
  SYSFAC_LOCAL_0        = 16, // Used for YAFFS!
  SYSFAC_LOCAL_1        = 17,
  SYSFAC_LOCAL_2        = 18,
  SYSFAC_LOCAL_3        = 19,
  SYSFAC_LOCAL_4        = 20,
  SYSFAC_LOCAL_5        = 21,
  SYSFAC_LOCAL_6        = 22,
  SYSFAC_LOCAL_7        = 23,
  SYSFAC_RANGE          = 24
} syslog_facility_t;

typedef enum severity
{
  SYSSEV_EMERGENCY = 0,
  SYSSEV_ALERT     = 1,
  SYSSEV_CRITICAL  = 2,
  SYSSEV_ERROR     = 3,
  SYSSEV_WARNING   = 4,
  SYSSEV_NOTICE    = 5,
  SYSSEV_INFO      = 6,
  SYSSEV_DEBUG     = 7,
  SYSSEV_RANGE     = 8
} syslog_severity_t;

typedef struct syslog_msg_info
{
  syslog_facility_t syslog_facility;
  syslog_severity_t syslog_severity;
  char* syslog_field_app_name;
  char* syslog_field_msg_id;
  char* syslog_field_message;
} syslog_msg_info_t;

typedef enum syslog_err
{
  SYSLOG_ERR_OK                 = 0,
  SYSLOG_ERR_MEM                = 1,
  SYSLOG_ERR_UDP_BIND           = 2,
  SYSLOG_ERR_UDP_CONN           = 3,
  SYSLOG_ERR_CONF_MISSING       = 4,
  SYSLOG_ERR_CONF_WRONG         = 5,
  SYSLOG_ERR_UNINITIALIZED      = 6,
  SYSLOG_ERR_FS_NOT_AVAILABLE   = 7
} syslog_err_t;


extern void syslog_ip_addr_as_string(char const** p_to_string_addr);
extern void syslog_ip_port_as_string(char const** p_to_string_addr);
extern void syslog_status_as_string(char const** p_to_string_addr);

extern syslog_err_t syslog_init(void);
extern syslog_err_t syslog_reload_config(void);

extern void syslog_set_verbosity(int8_t verbosity);
extern void syslog_get_verbosity(int8_t* verbosity);
extern void syslog_set_disable_udp_messages(int8_t disable_flag);
extern void syslog_get_disable_udp_messages(int8_t* disable_flag);

extern syslog_err_t syslog_push(char* msg_id, char* message,
                                syslog_facility_t facility,
                                syslog_severity_t severity);

extern syslog_err_t syslog_push_from_isr(char* msg_id, char* message,
                                         syslog_facility_t facility,
                                         syslog_severity_t severity);

extern void syslog_client_task(void* argument);

/*
 * Define here your SYSFAC_LOCAL"X"
 */
#define SYSFAC_YAFFS SYSFAC_LOCAL_0

#endif /* SRC_SYSLOG_H_ */
