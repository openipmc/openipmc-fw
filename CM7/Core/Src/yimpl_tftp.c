/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * @file   yimpl_tftp.c
 * @brief  TFTP protocol integration with YAFFS-2 File System
 * @date   YYYY / MM / DD - 2022 / 08 / 17
 * @author Antonio V. G. Bassi
 */

#include "yimpl_tftp.h"

static void* yimpl_tftp_open(const char* fname, const char* mode, u8_t write);
static void  yimpl_tftp_close(void* handle);
static int   yimpl_tftp_read(void* handle, void* buf, int bytes);
static int   yimpl_tftp_write(void* handle, struct pbuf* p);
static void  yimpl_tftp_error(void* handle, int err, const char* msg, int size);

static struct tftp_context yimpl_tftp_ctxt =
{
	.open  = NULL,
	.close = NULL,
	.read  = NULL,
	.write = NULL,
	.error = NULL
};

static int yimpl_tftp_file_handle = -1;

err_t yimpl_tftp_close_server(void)
{
	err_t err = ERR_OK;
	tftp_cleanup();
	memset(&yimpl_tftp_ctxt, 0x00, sizeof(yimpl_tftp_ctxt));

#ifdef DEBUG_YAFFS_TFTPD
	mt_printf("tftpd: server closed.");
#endif /* DEBUG_YAFFS_TFTPD */

	return err;
}

err_t yimpl_tftp_launch_server(void)
{
	err_t err = ERR_OK;

	// Assign callback functions
	yimpl_tftp_ctxt.open  = yimpl_tftp_open;
	yimpl_tftp_ctxt.close = yimpl_tftp_close;
	yimpl_tftp_ctxt.read  = yimpl_tftp_read;
	yimpl_tftp_ctxt.write = yimpl_tftp_write;
	yimpl_tftp_ctxt.error = yimpl_tftp_error;

	// Initialize tftp in server mode
	err = tftp_init_server(&yimpl_tftp_ctxt);

#ifdef DEBUG_YAFFS_TFTPD
	if(!err)
		mt_printf("tftpd: server launched successfully.\r\n");
	else
		mt_printf("tftpd: error on launching server.\r\n");
#endif /* DEBUG_YAFFS_TFTPD */

	return err;
}

static void* yimpl_tftp_open(const char* fname, const char* mode, u8_t write)
{
	const int s_flag = S_IREAD | S_IWRITE;
	int o_flag = 0;
	const char* mtd = dev.param.name;
	char dir[256] = {0};

	snprintf(dir, 256, "%s/%s", mtd, fname);

	// Is write greater than 0, if yes then open for Read+Write, otherwise just Read.
	write > 0 ? o_flag = ( O_WRONLY | O_CREAT | O_EXCL ) : 0;
	yimpl_tftp_file_handle = yaffs_open(dir, o_flag, s_flag);

#ifdef DEBUG_YAFFS_TFTPD
	if( ( write > 0 ) && ( yimpl_tftp_file_handle >= 0 ) )
		mt_printf("tftpd: opened file for writing, file \"%s\", handle %d\r\n", fname, yimpl_tftp_file_handle);
	else if( ( !write ) && ( yimpl_tftp_file_handle >= 0 ) )
		mt_printf("tftpd: opened file for reading, file \"%s\", handle %d\r\n", fname, yimpl_tftp_file_handle);
	else
		mt_printf("tftpd: could not open file due to bad file handle.\r\n");
#endif /* DEBUG_YAFFS_TFTPD */

	return (&yimpl_tftp_file_handle);
}

static void yimpl_tftp_close(void* handle)
{
	yaffs_close( *( (int*)(handle) ) );

#ifdef DEBUG_YAFFS_TFTPD
	mt_printf("tftpd: file handle %d closed.\r\n", *( (int*)(handle) ) );
#endif /* DEBUG_YAFFS_TFTPD */

	yimpl_tftp_file_handle = -1;
	return;
}

static int yimpl_tftp_read(void* handle, void* buf, int bytes)
{
	int result = -1;
	int h = *( (int*)(handle) );

	if( h < 0 )
		return result;

	result = yaffs_read(h, buf, bytes);

#ifdef DEBUG_YAFFS_TFTPD
	if( result >= 0 )
		mt_printf("tftpd: file read successfully. %d bytes read.\r\n", bytes);
	else
		mt_printf("tftpd: file could not be read.\r\n");
#endif

	return result;
}

static int yimpl_tftp_write(void* handle, struct pbuf* p)
{
	int result = -1;
	int h = *( (int*)(handle) );

	if( h < 0 )
		return result;
	result = yaffs_write(h, p->payload, p->len);

#ifdef DEBUG_YAFFS_TFTPD
	if( result > 0 )
	{
		mt_printf("tftpd: payload received has p->totlen = %u and p->len = %u.\r\n", p->tot_len, p->len);
		mt_printf("tftpd: file written successfully, %d bytes written.\r\n", result);
	}
	else
		mt_printf("tftpd: file could not be written.\r\n");
#endif

	return result;
}

static void yimpl_tftp_error(void* handle, int err, const char* msg, int size)
{
	mt_printf("\r\nyaffs: error %d, on TFTP transmission -> %s", err, msg);
	return;
}


