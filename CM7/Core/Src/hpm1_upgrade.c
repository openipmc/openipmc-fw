
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 * This file implements the hardware specific operations required by HPM1 Upgrade
 * in OpenIPMC submodule
 */

#include <stdbool.h>
#include <string.h>
#include "main.h"

#include "FreeRTOS.h"
#include "task.h"

#include "fw_identification.h"
#include "hpm1_ctrls.h"
#include "fw_metadata.h"
#include "image_ext_flash.h"
#include "image_int_flash.h"
#include "head_commit_sha1.h"
#include "bootloader_ctrl.h"
#include "bootloader_tools.h"
#include "cm4_fw_tools.h"
#include "dimm_gpios.h"
#include "ext_flash_organization.h"


//#define OPENIPMC_BL7_BLOCK 1039 // Block in internal flash for Bootloader firmware
//#define OPENIPMC_CM4_BLOCK 13   // Block in internal flash for CM4 firmware

#define UPGRADE_BUFFER_SIZE 16384
__attribute__((section(".upgrade_buffer")))
static uint8_t upgrade_buffer[UPGRADE_BUFFER_SIZE];

// MCU Flash Memory Organization.
extern const uint32_t openipmc_bl7_run_addr;
extern const uint32_t openipmc_cm7_run_addr;
extern const uint32_t openipmc_cm4_run_addr;

static void update_component_rollback_version(int);
static void update_CM4_version(void);

/*
 * Function called during the OpenIPMC HAL initializations.
 * It prepares the HPM1 Upgrade functionality by setting attributes
 */
void hpm1_init(void)
{
	uint8_t bl_major_ver; // (version info not used here)
	uint8_t bl_minor_ver;
	uint8_t bl_aux_ver[4];

	hpm1_global_capabilities = MANUAL_ROLLBACK;
	hpm1_timeouts.inaccessibility_timeout = 4;
	hpm1_timeouts.rollback_timeout = 4;
	hpm1_timeouts.selftest_timeout = 4;
	hpm1_timeouts.upgrade_timeout = 2;


	// Load info for the Bootloader (if installed)
	if( bootloader_is_present( &bl_major_ver, &bl_minor_ver, bl_aux_ver ) )
	{
		hpm1_add_component( 0 ); // Bootloader is Component 0

		hpm1_component_properties[0]->payload_cold_reset_is_required   = false;
		hpm1_component_properties[0]->deferred_activation_is_supported = false;
		hpm1_component_properties[0]->firmware_comparison_is_supported = false;
		hpm1_component_properties[0]->rollback_is_supported = true;
		hpm1_component_properties[0]->backup_cmd_is_required = true;

		hpm1_component_properties[0]->current_firmware_revision_major  = bl_major_ver;
		hpm1_component_properties[0]->current_firmware_revision_minor  = bl_minor_ver;
		hpm1_component_properties[0]->current_firmware_revision_aux[0] = bl_aux_ver[0];
		hpm1_component_properties[0]->current_firmware_revision_aux[1] = bl_aux_ver[1];
		hpm1_component_properties[0]->current_firmware_revision_aux[2] = bl_aux_ver[2];
		hpm1_component_properties[0]->current_firmware_revision_aux[3] = bl_aux_ver[3];

		hpm1_component_properties[0]->storage_info.run_start_block    = ((uint32_t) &openipmc_bl7_run_addr ) / INT_FLASH_SECTOR_SIZE;
		hpm1_component_properties[0]->storage_info.backup_start_block = EXT_FLASHORG_BACKUP_BL7_AREA_START_BLOCK;
		hpm1_component_properties[0]->storage_info.temp_start_block   = EXT_FLASHORG_BL7_TEMP_AREA_START_BLOCK;

		hpm1_component_properties[0]->boot_ctrl = BOOT_CTRL_LOAD_NONE;

		strncpy( hpm1_component_properties[0]->description_string, "Bootloader", 11 ); // Max of 11 chars + null termination
	}

	update_component_rollback_version(0);

	// Load info for the OpenIPMC-FW_CM7 (Component 1)
	hpm1_add_component( 1 );

	hpm1_component_properties[1]->payload_cold_reset_is_required   = true;
	hpm1_component_properties[1]->deferred_activation_is_supported = false;
	hpm1_component_properties[1]->firmware_comparison_is_supported = false;
	hpm1_component_properties[1]->rollback_is_supported = true;
	hpm1_component_properties[1]->backup_cmd_is_required = true;

	hpm1_component_properties[1]->current_firmware_revision_major  = CM7_FW_VERSION_MAJOR;
	hpm1_component_properties[1]->current_firmware_revision_minor  = CM7_FW_VERSION_MINOR;
	hpm1_component_properties[1]->current_firmware_revision_aux[0] = (HEAD_COMMIT_SHA1>>24)&0xFF;
	hpm1_component_properties[1]->current_firmware_revision_aux[1] = (HEAD_COMMIT_SHA1>>16)&0xFF;
	hpm1_component_properties[1]->current_firmware_revision_aux[2] = (HEAD_COMMIT_SHA1>>8)&0xFF;
	hpm1_component_properties[1]->current_firmware_revision_aux[3] = (HEAD_COMMIT_SHA1)&0xFF;

	hpm1_component_properties[1]->storage_info.run_start_block    = ((uint32_t) &openipmc_cm7_run_addr) / INT_FLASH_SECTOR_SIZE;
	hpm1_component_properties[1]->storage_info.backup_start_block = EXT_FLASHORG_BACKUP_CM7_AREA_START_BLOCK;
	hpm1_component_properties[1]->storage_info.temp_start_block   = EXT_FLASHORG_CM7_TEMP_AREA_START_BLOCK;

	hpm1_component_properties[1]->boot_ctrl = BOOT_CTRL_LOAD_NONE;

	strncpy( hpm1_component_properties[1]->description_string, "CM7_fw", 7 ); // Max of 11 chars + null termination

	// Check for CM7 firmware in backup area and update the properties if available;
	update_component_rollback_version(1);


	// Load info for the CM4 FW
	hpm1_add_component( 2 ); // CM4 FW is Component 2

	hpm1_component_properties[2]->payload_cold_reset_is_required   = true;
	hpm1_component_properties[2]->deferred_activation_is_supported = false;
	hpm1_component_properties[2]->firmware_comparison_is_supported = false;
	hpm1_component_properties[2]->rollback_is_supported = true;
	hpm1_component_properties[2]->backup_cmd_is_required = true;

	hpm1_component_properties[2]->storage_info.run_start_block    = ((uint32_t) &openipmc_cm4_run_addr) / INT_FLASH_SECTOR_SIZE;
	hpm1_component_properties[2]->storage_info.backup_start_block = EXT_FLASHORG_BACKUP_CM4_AREA_START_BLOCK;
	hpm1_component_properties[2]->storage_info.temp_start_block   = EXT_FLASHORG_CM4_TEMP_AREA_START_BLOCK;

	hpm1_component_properties[2]->boot_ctrl = BOOT_CTRL_LOAD_NONE;

	strncpy( hpm1_component_properties[2]->description_string, "CM4_fw", 7 ); // Max of 11 chars + null termination

	// Check for CM4 firmware the internal flash and update the version number
	update_CM4_version();
	update_component_rollback_version(2);

}


/*
 * This function checks if there is some backup available in backup are for a firmware component.
 * If so, update the component properties
 */
static void update_component_rollback_version(int component)
{
  int block_number;

  if(hpm1_component_properties[component] == NULL)
    return;

  block_number = hpm1_component_properties[component]->storage_info.backup_start_block;

 if( !image_ext_flash_CRC_is_valid(block_number))
 {
   hpm1_component_properties[component]->backup_is_available = false;
   return;
 }
  hpm1_component_properties[component]->backup_is_available = true;

  metadata_fields_v0_t metadata_fields;

  //Read metadata
  image_ext_flash_read(block_number, FW_METADATA_ADDR, sizeof(metadata_fields), (uint8_t*)&metadata_fields);

  hpm1_component_properties[component]->rollback_firmware_revision_major  = metadata_fields.firmware_revision_major;
  hpm1_component_properties[component]->rollback_firmware_revision_minor  = metadata_fields.firmware_revision_minor;
  hpm1_component_properties[component]->rollback_firmware_revision_aux[0] = metadata_fields.firmware_revision_aux[0];
  hpm1_component_properties[component]->rollback_firmware_revision_aux[1] = metadata_fields.firmware_revision_aux[1];
  hpm1_component_properties[component]->rollback_firmware_revision_aux[2] = metadata_fields.firmware_revision_aux[2];
  hpm1_component_properties[component]->rollback_firmware_revision_aux[3] = metadata_fields.firmware_revision_aux[3];
}


/*
 * This functions checks if the CM4 FW is available.
 * If so, update the component properties.
 */
static void update_CM4_version(void)
{
	uint8_t bl_major_ver; // (version info not used here)
	uint8_t bl_minor_ver;
	uint8_t bl_aux_ver[4];

	if( hpm1_component_properties[2] == NULL )
		return;

	if( cm4_fw_is_present( &bl_major_ver, &bl_minor_ver, bl_aux_ver ) )
	{
		hpm1_component_properties[2]->current_firmware_revision_major  = bl_major_ver;
		hpm1_component_properties[2]->current_firmware_revision_minor  = bl_minor_ver;
		hpm1_component_properties[2]->current_firmware_revision_aux[0] = bl_aux_ver[0];
		hpm1_component_properties[2]->current_firmware_revision_aux[1] = bl_aux_ver[1];
		hpm1_component_properties[2]->current_firmware_revision_aux[2] = bl_aux_ver[2];
		hpm1_component_properties[2]->current_firmware_revision_aux[3] = bl_aux_ver[3];
	}
	else // If not present, fill the version info as ZERO
	{
		hpm1_component_properties[2]->current_firmware_revision_major  = 0;
		hpm1_component_properties[2]->current_firmware_revision_minor  = 0x00;
		hpm1_component_properties[2]->current_firmware_revision_aux[0] = 0;
		hpm1_component_properties[2]->current_firmware_revision_aux[1] = 0;
		hpm1_component_properties[2]->current_firmware_revision_aux[2] = 0;
		hpm1_component_properties[2]->current_firmware_revision_aux[3] = 0;
	}
}

/*
 * Upgrade operation.
 *
 * This function copies the content from "addr" in the external flash
 * into "dest" of the internal flash memory.
 *
 * Before copy, it checks the integrity of the content in "addr".
 *
 */
static bool copy_component_ext_to_internal_flash( uint32_t external_block_number, uint32_t internal_block_number )
{
  metadata_fields_v0_t metadata_fields;

  uint32_t read_index = 0;
  uint32_t bytes_to_read;

  // Check integrity, otherwise don't copy
  if( !image_ext_flash_CRC_is_valid( external_block_number ) )
    return false;

  // Read metadata
  image_ext_flash_read( external_block_number, FW_METADATA_ADDR, sizeof(metadata_fields), (uint8_t*)&metadata_fields);

  // Write internal flash
  image_int_flash_open( internal_block_number );

  while( read_index < metadata_fields.image_size )
  {
    bytes_to_read = metadata_fields.image_size - read_index;
    if( bytes_to_read > UPGRADE_BUFFER_SIZE )
      bytes_to_read = UPGRADE_BUFFER_SIZE;

    image_ext_flash_read( external_block_number,read_index, bytes_to_read, upgrade_buffer );
    image_int_flash_write( upgrade_buffer, bytes_to_read );
    read_index += bytes_to_read;
  }

  image_int_flash_close();

  return true;
}

/*
 *
 * Implementation of the HPM1 functions defined by OpenIPMC submodule
 *
 */
void hpm1_cmd_initiate_backup_cb( uint8_t component_mask )
{
	// TODO: implement backup error status back to HPM1. For testing purposes we just ignore a backup failure

  image_ext_flash_openipmc_component_backup(component_mask);
  update_component_rollback_version(component_mask);
}


void hpm1_cmd_initiate_prepare_cb( uint8_t component_mask )
{
	// Not used
}

void hpm1_cmd_initiate_upload_for_upgrade_cb( uint8_t component_number )
{
  int temp_area_block;

  if(hpm1_component_properties[component_number] == NULL)
    return;

  temp_area_block = hpm1_component_properties[component_number]->storage_info.temp_start_block;

  image_ext_flash_open(temp_area_block);
}

void hpm1_cmd_initiate_upload_for_compare_cb( uint8_t component_number )
{

}

void hpm1_cmd_upload_cb( uint8_t component_number, uint8_t* block_data, uint8_t block_size  )
{
  if(hpm1_component_properties[component_number] == NULL)
    return;

  image_ext_flash_write(block_data, block_size);
}

int hpm1_cmd_upload_finish_cb( uint8_t component_number  )
{

  int run_start_block;
  int temp_start_block;

  if(hpm1_component_properties[component_number] == NULL)
    return HPM1_CB_RETURN_CHECKSUM_ERROR;

  temp_start_block = hpm1_component_properties[component_number]->storage_info.temp_start_block;

  image_ext_flash_close();
  if(!image_ext_flash_CRC_is_valid(temp_start_block) )
    return HPM1_CB_RETURN_CHECKSUM_ERROR;

  switch(component_number)
  {
  case 0:

    if (bootloader_disable())
    {
      // If the component is the Bootloader, it will copy from external to internal flash without activate command.
      run_start_block  = hpm1_component_properties[component_number]->storage_info.run_start_block;
      temp_start_block = hpm1_component_properties[component_number]->storage_info.temp_start_block;

      if ( copy_component_ext_to_internal_flash(temp_start_block,run_start_block) == false )
        return HPM1_CB_RETURN_CHECKSUM_ERROR;
    }
    else
    {
      return HPM1_CB_RETURN_COMPONENT_ERROR;
    }

    break;
  case 1:
    hpm1_component_properties[component_number]->boot_ctrl = BOOT_CTRL_LOAD_ADDR_FROM_EXT_FLASH;
    break;
  case 2:
    hpm1_component_properties[component_number]->boot_ctrl = BOOT_CTRL_LOAD_ADDR_FROM_EXT_FLASH;
    break;
  default:
    return HPM1_CB_RETURN_COMPONENT_ERROR;
  }



	return HPM1_CB_RETURN_OK;
}

void hpm1_cmd_activate_cb( uint8_t component_number )
{
  uint8_t cm7_boot_ctrl = hpm1_component_properties[1]->boot_ctrl;
  uint8_t cm4_boot_ctrl = hpm1_component_properties[2]->boot_ctrl;

  if( bootloader_enable() )
  {
    // Disable the IPMB channels
    GPIO_SET_STATE(RESET, IPMB_A_EN);
    GPIO_SET_STATE(RESET, IPMB_B_EN);
    vTaskDelay( 100/portTICK_PERIOD_MS );
    bootloader_schedule_load(cm4_boot_ctrl, cm7_boot_ctrl);

    //CM4 will NOT boot after reset.
    SYSCFG->UR1 &= ~SYSCFG_UR1_BCM4;
    RCC->GCR    &= ~RCC_GCR_BOOT_C2;

    //CM7 will boot after reset.
    SYSCFG->UR1 |= SYSCFG_UR1_BCM7;
    RCC->GCR    |=  RCC_GCR_BOOT_C1;

    NVIC_SystemReset();
  }

}

int  hpm1_cmd_manual_rollback_cb ( void )
{

  int run_start_block;
  int backup_start_block;

  if (bootloader_disable())
  {
    // If the component is the Bootloader, it will copy from external to internal flash without activate command.
    run_start_block    = hpm1_component_properties[0]->storage_info.run_start_block;
    backup_start_block = hpm1_component_properties[0]->storage_info.backup_start_block;

    if ( copy_component_ext_to_internal_flash(backup_start_block,run_start_block) == false )
         return HPM1_CB_RETURN_ROLLBACK_ERROR;
  }
  else
  {
    return HPM1_CB_RETURN_ROLLBACK_ERROR;
  }

  if( bootloader_enable() )
	{
		if( image_ext_flash_CRC_is_valid( 0 ) ) // Check if there is something valid in the backup area
		{
			bootloader_schedule_load( BOOT_CTRL_LOAD_BACKUP_FROM_EXT_FLASH, BOOT_CTRL_LOAD_BACKUP_FROM_EXT_FLASH );
			NVIC_SystemReset();
			return HPM1_CB_RETURN_OK;
		}
		else
		{
			return HPM1_CB_RETURN_ROLLBACK_ERROR;
		}
	}
	else
	{
		return HPM1_CB_RETURN_ROLLBACK_ERROR;
	}
}



