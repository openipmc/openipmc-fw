/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: yaffsfs_rtos.c
 * @brief	: RTOS integration interface for YAFFS2 file system
 *            with CMSIS_V2.
 * @date	: YYYY / MM / DD - 2021 / 10 / 04
 * @author 	: Antonio V. G. Bassi
 *
 */

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "yaffs_osglue.h"
#include "mt_printf.h"
#include "rtc_tools.h"
#include "yaffsfs_errno.h"
#include "syslog.h"
#include "emh_malloc.h"

/* Heap configuration for yaffs */
#define MAX_TASK_ERR_SIZE 10
#define KB 1024
#define HEAP_SIZE (128*KB)

/* YAFFS Lock timeout */
#define YAFFS_LOCK_TIMEOUT 1000 // 1 sec.



typedef struct
{
	TaskHandle_t calling_task;
	int err_var;
}yaffs_errno_t;

static yaffs_errno_t yaffs_err[MAX_TASK_ERR_SIZE] = {NULL};

uint8_t  __attribute__((section(".yaffs_heap"))) yaffs_heap[HEAP_SIZE];
void*  yaffs_heap_ptr = (void*)yaffs_heap;
size_t heap_size = HEAP_SIZE;
emh_heapId_t yaffs_heap_id = 0;


/* Multi-threaded context */
static SemaphoreHandle_t yaffs_mutex;

int yaffsfs_get_errno(void)
{
	xSemaphoreTake(yaffs_mutex, portMAX_DELAY);

	TaskHandle_t current_thread = xTaskGetCurrentTaskHandle();
	int retval = 0;
	for(int i = 0; i < MAX_TASK_ERR_SIZE; i++)
	{
		if( current_thread == yaffs_err[i].calling_task )
		{
			// Gets error code from calling task and cleans the entry.
			retval = yaffs_err[i].err_var;
			yaffs_err[i].calling_task = NULL;
			yaffs_err[i].err_var = 0;
		}
	}
	xSemaphoreGive(yaffs_mutex);

	return retval;
}

/*
 * @name   : yaffsfs_OSInitialisation
 * @brief  : Called to initialize RTOS context.
 *
 */
void yaffsfs_OSInitialisation( void )
{
	yaffs_mutex = xSemaphoreCreateMutex();
	yaffs_heap_id = emh_create(yaffs_heap_ptr, heap_size);
	return;
}

/*
 * @name	: yaffsfs_CurrentTime
 * @brief	: Gets current time from RTOS, time will
 * 			  be returned in seconds.
 *
 * @note	: TICK_RATE_HERTZ is set to 1000 Hz
 * 			  in FREERTOS_M7
 *
 * @note 	: The actual time one tick period represents
 * 			  depends on the value assigned to parameter
 * 			  TICK_RATE_HZ  within  FreeRTOSConfig.h.
 *
 * @note 	: If configUSE_16_BIT_TICKS  is  set  to  0,
 * 			  then the tick count will be held in a 32-bit
 * 			  variable.
 * @return    Current time in milliseconds.
 *
 */
u32 yaffsfs_CurrentTime( void )
{
	uint32_t currentTime = ( uint32_t )( rtc_tool_fetch_epoch() );
	return currentTime; 
}

/*
 * @name	yaffsfs_SetError
 * @brief	Called by Yaffs to set the system error.
 *
 * @note	The error codes passed here belong to the
 * 			POSIX interface, the codes are available at
 * 			header file "errno.h".
 *
 * @param	err :  Error code.
 *
 */
void yaffsfs_SetError( int err )
{
	TaskHandle_t current_thread = xTaskGetCurrentTaskHandle();
	for(int i = 0; i < MAX_TASK_ERR_SIZE; i++)
	{
		if( current_thread == yaffs_err[i].calling_task )
		{
			// Gets error code from calling task and cleans the entry.
			yaffs_err[i].err_var = err;
			return;
		}

	}

	// If task is not present at the table, add a new entry.
	for(int i = 0; i < MAX_TASK_ERR_SIZE; i++)
	{
		if( NULL == yaffs_err[i].calling_task )
		{
			yaffs_err[i].calling_task = current_thread;
			yaffs_err[i].err_var = err;
			return;
		}
	}

	// If we got here, we have a serious condition. Send it over for syslog.
	syslog_push("yaffs error", "errno stack has no space for storage!", SYSFAC_YAFFS, SYSSEV_ERROR);

	return;
}

/*
 *  @name	: yaffsfs_Lock
 *  @brief 	: Called by Yaffs to lock Yaffs from
 *  		  multi-threaded access.
 */
int yaffsfs_Lock( void )
{
  if( yaffs_mutex == NULL ) // YAFFS had not been initialized yet.
    return -1;

	if( xSemaphoreTake(yaffs_mutex, pdMS_TO_TICKS(YAFFS_LOCK_TIMEOUT)) != pdTRUE )
	  return -1;

	return 0;
}

/*
 *  @name	: yaffsfs_unlock
 *  @brief 	: Called by Yaffs to unlock Yaffs from
 *  		  multi-threaded access.
 */
int yaffsfs_Unlock( void )
{
  if(yaffs_mutex == NULL) // YAFFS had not been initialized yet.
    return -1;

	xSemaphoreGive(yaffs_mutex);
	return 0;
}

/*
 * @name		: yaffsfs_malloc
 * @brief		: Called by Yaffs to allocate memory.
 * @param size  : Desired amount of memory to be
 *  			  allocated.
 * @return ptr	: pointer to allocated memory segment.
 *
 */
void *yaffsfs_malloc( size_t size )
{
	void *ptr;
	ptr = emh_malloc(yaffs_heap_id, size);

	if(ptr == NULL)
		mt_printf("\r\nyaffs: Malloc error");

	return ptr;
}

/*
 * @name      : yaffsfs_free
 * @brief     : Called by Yaffs to free allocated memory segment.
 * @param ptr : Pointer to allocated segment.
 *
 */
void yaffsfs_free( void *ptr )
{
	emh_free(ptr);
	return;
}

/*
 *  @brief : Function to check if accesses to a memory region
 *           is valid.
 *  @param  addr            : Pointer to memory address.
 *  @param  size            : Size of memory position
 *  @param  write_request   : Write request flag.
 *  @return int : If 0 the memory region can be accessed,
 *  				otherwise the access is denied.
 *
 */
int yaffsfs_CheckMemRegion( const void* addr, size_t size, int write_request )
{
	( void )( write_request );
	( void )( size );

	if(!addr)
		return -1; /* Can't de-reference NULL pointers */
	else
		return 0; /* Access granted */
}

/*
 * @brief : Function to report a bug on yaffs2 source files.
 * @param file_name : Pointer to file name string.
 * @param line_no	: Line offset where error occurred.
 *
 */
void yaffs_bug_fn( const char *file_name, int line_no )
{
	mt_printf( "\r\nyaffs: There's a bug in %s at line %d", file_name, line_no );
	return;
}
