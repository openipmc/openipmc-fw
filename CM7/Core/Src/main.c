/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "lwip.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dimm_gpios.h"
#include "mt_printf.h"
#include "stream_buffer.h"
#include "printf.h"
#include "ipmc_tasks.h"
#include "ipmc_ios.h"
#include "fru_state_machine.h"
#include "ipmb_0.h"
#include "mgm_i2c.h"
#include "sense_i2c.h"
#include "network_ctrls.h"
#include "telnet_server.h"
#include "usbd_cdc_if.h"
#include "fw_metadata.h"
#include "custom_settings.h"
#include "ipmi_msg_manager.h"
#include "time.h"
#include "xvc_server.h"
#include "cm4_fw_tools.h"
#include "image_ext_flash.h"
#include "ext_flash_organization.h"
#include "rtc_tools.h"
#include "w25n01gv.h"
#include "rmcp.h"
#include "h7i2c_bare.h"
#include "h7uart_bare.h"
#include "syscfg.h"
#include "syslog.h"
#include "yimpl_conf.h"
#include "yimpl_tftp.h"
#include "sensors_manager.h"
#include "emh_portenv.h"
#include "sol.h"
#include "h7rng.h"
#include "mcp23s17ml_hal.h"
#include "uart_payload_interface.h"
#include "sol_impl.h"
#include "rmcp_impl.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef OPENIPMC_INCOMING_BRIDGE_REQUEST_TASK_STACK_SIZE
#define OPENIPMC_INCOMING_BRIDGE_REQUEST_TASK_STACK_SIZE OPENIPMC_INCOMING_REQUESTS_TASK_STACK_SIZE
#endif

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

QSPI_HandleTypeDef hqspi;

RTC_HandleTypeDef hrtc;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};
/* USER CODE BEGIN PV */

static osThreadId_t syslog_client_task_handle;
static const osThreadAttr_t syslog_client_task_attributes = {
  .name = "SyslogClientTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 384 * 4
};

osThreadId_t terminal_input_task_handle;
const osThreadAttr_t terminal_input_task_attributes = {
  .name = "TerminalInputTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 200* 4
};

osThreadId_t terminal_process_task_handle;
const osThreadAttr_t terminal_process_task_attributes = {
  .name = "TerminalProcessTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = TERMINAL_PROCESS_TASK_STACK_SIZE * 4
};

const osThreadAttr_t ipmb_0_msg_receiver_task_attributes = {
  .name = "MNG",
  .priority = (osPriority_t) osPriorityNormal3,
  .stack_size = 512 * 4
};

const osThreadAttr_t ipmb_0_msg_sender_task_attributes = {
  .name = "MNGO",
  .priority = (osPriority_t) osPriorityNormal1,
  .stack_size = 512 * 4
};

const osThreadAttr_t fru_state_machine_task_attributes = {
  .name = "SMM",
  .priority = (osPriority_t) osPriorityNormal1,
  .stack_size = OPENIPMC_FRU_STATE_MACHINE_TASK_STACK_SIZE * 4
};

const osThreadAttr_t ipmi_incoming_requests_manager_task_attributes = {
  .name = "IPMI_MSG_MGMT",
  .priority = (osPriority_t) osPriorityNormal2,
  .stack_size = OPENIPMC_INCOMING_REQUESTS_TASK_STACK_SIZE * 4
};

const osThreadAttr_t ipmi_bridge_ipmi_msg_task_attributtes = {
  .name = "IPMI_MSG_BRIDGE",
  .priority = (osPriority_t) osPriorityNormal1,
  .stack_size = OPENIPMC_INCOMING_BRIDGE_REQUEST_TASK_STACK_SIZE * 4
};

const osThreadAttr_t ipmc_handle_switch_task_attributes = {
  .name = "IPMC_HANDLE_TRANS",
  .priority = (osPriority_t) osPriorityNormal2,
  .stack_size = 128 * 4
};

const osThreadAttr_t ipmc_blue_led_blink_task_attributes = {
  .name = "BLUE_LED",
  .priority = (osPriority_t) osPriorityNormal2,
  .stack_size = 128 * 4
};

osThreadId_t vcp_output_task_handle;
const osThreadAttr_t vcp_output_task_attributes = {
  .name = "VcpOutputTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 64 * 4
};

const osThreadAttr_t hpm1_upgrade_task_attributes = {
  .name = "HPM1UpgradeTask",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 512 * 4
};

osThreadId_t custom_startup_task_handle;
const osThreadAttr_t custom_startup_task_attributes = {
  .name = "CustomStartupTask",
  .priority = (osPriority_t) CUSTOM_STARTUP_TASK_PRIORITY,
  .stack_size = CUSTOM_STARTUP_TASK_STACK_SIZE * 4
};

const osThreadAttr_t sensor_manager_task_attributes = {
  .name = "SensorManagerTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};

osThreadId_t mcp23s17_task_handle;
const osThreadAttr_t mcp23s17_task_attributes = {
  .name = "Mcp23s17Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};

const osThreadAttr_t rmcp_op_task_attributes = {
  .name       = "RMCPOpetationTask",
  .priority   = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};

const osThreadAttr_t pyldi_receiver_task_attributes = {
  .name = "PayloadInterfaceRxTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};

const osThreadAttr_t pyldi_sender_task_attributes = {
  .name = "PayloadInterfaceTxTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
 };

// UART variables
h7uart_periph_t  huart_cli;
h7uart_periph_init_config_t huart_cli_config;

h7uart_periph_t huart_sol;
h7uart_periph_init_config_t huart_sol_config;

h7uart_periph_t huart_pyldi;
h7uart_periph_init_config_t huart_pyldi_config;


// IO expander
mcp23s17_dev_t dev_1;
mcp23s17_pin_t control_12v;
mcp23s17_pin_t control_generic;

// Text IO
//static char uart4_input_char;
extern StreamBufferHandle_t terminal_input_stream;
static StreamBufferHandle_t vcp_output_stream = NULL;

/*
 * emh_malloc mutex.
 * Used for protecting emh_malloc submodule against
 * concurrent accesses.
 */
SemaphoreHandle_t emh_malloc_mtx;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_CRC_Init(void);
static void MX_QUADSPI_Init(void);
static void MX_RTC_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */
       void    uart_mapping( void );
static void    uart_cli_init( void );
extern void    terminal_input_task(void *argument);
extern void    terminal_process_task(void *argument);
extern void    openipmc_hal_init( void );
extern uint8_t get_haddress_pins( void );
extern void    set_benchtop_payload_power_level( uint8_t new_power_level );
static void    vcp_output_task(void *argument);
extern void    custom_startup_task(void*);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
  int32_t timeout;

/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
  /* Wait until CPU2 boots and enters in stop mode or timeout*/
  timeout = 0xFFFF;
  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
  if ( timeout < 0 )
  {
  //Error_Handler();
  }
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
HSEM notification */
/*HW semaphore Clock enable*/
__HAL_RCC_HSEM_CLK_ENABLE();
/*Take HSEM */
HAL_HSEM_FastTake(HSEM_ID_0);
/*Release HSEM in order to notify the CPU2(CM4)*/
HAL_HSEM_Release(HSEM_ID_0,0);
/* wait until CPU2 wakes up from stop mode */
timeout = 0xFFFF;
while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
if ( timeout < 0 )
{
//Error_Handler();
}
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_CRC_Init();
  MX_QUADSPI_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */
  uart_mapping();
  uart_cli_init();
  pyldi_init();
  mt_printf_init();
  rtc_tool_init();
  w25n01gv_mutex_init();
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */

  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */

  /*
   * TASK LAUNCHING SEQUENCE STRATEGY
   *
   * During task creation, FreeRTOS allocates their stacks in the heap with a crescent sequence.
   * In other side, since stack pointers grow negatively, a stack overflow always corrupts stacks
   * allocated before.
   *
   * Due to this fact we divide tasks in two groups:
   *    - Higher risk: which are more likely to overflow during the board-specific code
   *                   development. These tasks executes many user defined callbacks.
   *                   Error messages are generated over syslog and console for debugging.
   *
   *    - Lower risk: which are unlikely to overflow during the board-specific code
   *                  development. These tasks are well tested, do not execute
   *                  user defined code, and have a more deterministic behavior.
   *                  Tasks involved in error logging (terminal, network, syslog, telnet)
   *                  must fit into this group.
   *
   * Therefore, Higher risk tasks must be allocated before than Lower risk task
   */

  // Higher risk
  syslog_client_task_handle = osThreadNew(syslog_client_task, NULL, &syslog_client_task_attributes);
  terminal_process_task_handle = osThreadNew(terminal_process_task, NULL, &terminal_process_task_attributes);
  fru_state_machine_task_handle = osThreadNew(fru_state_machine_task, NULL, &fru_state_machine_task_attributes);
  ipmi_incoming_requests_manager_task_handle = osThreadNew(ipmi_incoming_requests_manager_task, NULL, &ipmi_incoming_requests_manager_task_attributes);
  custom_startup_task_handle = osThreadNew(custom_startup_task, NULL, &custom_startup_task_attributes);
  sensor_manager_task_handle = osThreadNew(sensors_manager_task, NULL, &sensor_manager_task_attributes);
  mcp23s17_task_handle = osThreadNew(mcp23s17_task,NULL,&mcp23s17_task_attributes);
  vTaskSuspend(custom_startup_task_handle);

  //Lower risk
  terminal_input_task_handle = osThreadNew(terminal_input_task, NULL, &terminal_input_task_attributes);
  ipmb_0_msg_receiver_task_handle = osThreadNew(ipmb_0_msg_receiver_task, NULL, &ipmb_0_msg_receiver_task_attributes);
  ipmb_0_msg_sender_task_handle = osThreadNew(ipmb_0_msg_sender_task, NULL, &ipmb_0_msg_sender_task_attributes);
  ipmc_handle_switch_task_handle = osThreadNew(ipmc_handle_switch_task, NULL, &ipmc_handle_switch_task_attributes);
  ipmc_blue_led_blink_task_handle = osThreadNew(ipmc_blue_led_blink_task, NULL, &ipmc_blue_led_blink_task_attributes);
  vcp_output_task_handle = osThreadNew(vcp_output_task, NULL, &vcp_output_task_attributes);
  hpm1_upgrade_task_handle = osThreadNew(hpm1_upgrade_task, NULL, &hpm1_upgrade_task_attributes);
  pyldi_receiver_task_handle = osThreadNew(pyldi_receiver_task,NULL,&pyldi_receiver_task_attributes);
  pyldi_sender_task_handle = osThreadNew(pyldi_sender_task,NULL,&pyldi_sender_task_attributes);
  ipmi_bridge_ipmi_msg_task_handle = osThreadNew(ipmi_bridge_ipmi_msg_task,NULL,&ipmi_bridge_ipmi_msg_task_attributtes);
  rmcp_op_task_handle = osThreadNew(rmcp_op_task,NULL,&rmcp_op_task_attributes);


  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_SMPS_1V8_SUPPLIES_LDO);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 2;
  RCC_OscInitStruct.PLL.PLLN = 64;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_I2C2
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_QSPI;
  PeriphClkInitStruct.QspiClockSelection = RCC_QSPICLKSOURCE_D1HCLK;
  PeriphClkInitStruct.I2c123ClockSelection = RCC_I2C123CLKSOURCE_D2PCLK1;
  PeriphClkInitStruct.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable USB Voltage detector
  */
  HAL_PWREx_EnableUSBVoltageDetector();
}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_BYTE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_ENABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x30C0EDFF;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_ENABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x30C0EDFF;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_ENABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief QUADSPI Initialization Function
  * @param None
  * @retval None
  */
static void MX_QUADSPI_Init(void)
{

  /* USER CODE BEGIN QUADSPI_Init 0 */

  /* USER CODE END QUADSPI_Init 0 */

  /* USER CODE BEGIN QUADSPI_Init 1 */

  /* USER CODE END QUADSPI_Init 1 */
  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 255;
  hqspi.Init.FifoThreshold = 1;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 31;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  hqspi.Init.FlashID = QSPI_FLASH_ID_1;
  hqspi.Init.DualFlash = QSPI_DUALFLASH_DISABLE;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN QUADSPI_Init 2 */

  /* USER CODE END QUADSPI_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ETH_RST_N_GPIO_Port, ETH_RST_N_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, IPMB_B_EN_Pin|IPMB_A_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ETH_TX_ER_GPIO_Port, ETH_TX_ER_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, EN_12V_Pin|FP_LED_2_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PYLD_RESET_GPIO_Port, PYLD_RESET_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : I2C_BB_SCL_Pin I2C_BB_SDA_Pin */
  GPIO_InitStruct.Pin = I2C_BB_SCL_Pin|I2C_BB_SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_0_Pin IPM_1_Pin IPM_3_Pin IPM_4_Pin
                           IPM_5_Pin USR_IO_10_Pin */
  GPIO_InitStruct.Pin = IPM_0_Pin|IPM_1_Pin|IPM_3_Pin|IPM_4_Pin
                          |IPM_5_Pin|USR_IO_10_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_6_Pin USR_IO_27_Pin USR_IO_14_Pin HANDLE_Pin
                           USR_IO_12_Pin PWR_GOOD_B_Pin USR_IO_33_Pin */
  GPIO_InitStruct.Pin = USR_IO_6_Pin|USR_IO_27_Pin|USR_IO_14_Pin|HANDLE_Pin
                          |USR_IO_12_Pin|PWR_GOOD_B_Pin|USR_IO_33_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_0_Pin USR_IO_1_Pin IPM_11_Pin USR_IO_23_Pin
                           USR_IO_25_Pin USR_IO_28_Pin USR_IO_29_Pin USR_IO_20_Pin
                           USR_IO_24_Pin USR_IO_22_Pin USR_IO_26_Pin */
  GPIO_InitStruct.Pin = USR_IO_0_Pin|USR_IO_1_Pin|IPM_11_Pin|USR_IO_23_Pin
                          |USR_IO_25_Pin|USR_IO_28_Pin|USR_IO_29_Pin|USR_IO_20_Pin
                          |USR_IO_24_Pin|USR_IO_22_Pin|USR_IO_26_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_14_Pin USR_IO_7_Pin IPM_7_Pin USR_IO_21_Pin
                           USR_IO_17_Pin */
  GPIO_InitStruct.Pin = IPM_14_Pin|USR_IO_7_Pin|IPM_7_Pin|USR_IO_21_Pin
                          |USR_IO_17_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_34_Pin IPM_10_Pin USR_IO_4_Pin USR_IO_5_Pin
                           USR_IO_8_Pin USR_IO_9_Pin */
  GPIO_InitStruct.Pin = USR_IO_34_Pin|IPM_10_Pin|USR_IO_4_Pin|USR_IO_5_Pin
                          |USR_IO_8_Pin|USR_IO_9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : ETH_INT_N_Pin */
  GPIO_InitStruct.Pin = ETH_INT_N_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(ETH_INT_N_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ETH_RST_N_Pin */
  GPIO_InitStruct.Pin = ETH_RST_N_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ETH_RST_N_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PC9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_13_Pin IPM_8_Pin USR_IO_2_Pin */
  GPIO_InitStruct.Pin = IPM_13_Pin|IPM_8_Pin|USR_IO_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_2_Pin IPM_6_Pin IPM_12_Pin IPM_9_Pin
                           ALARM_A_Pin USR_IO_18_Pin USR_IO_19_Pin */
  GPIO_InitStruct.Pin = IPM_2_Pin|IPM_6_Pin|IPM_12_Pin|IPM_9_Pin
                          |ALARM_A_Pin|USR_IO_18_Pin|USR_IO_19_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_B_EN_Pin IPMB_A_EN_Pin */
  GPIO_InitStruct.Pin = IPMB_B_EN_Pin|IPMB_A_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : IPMB_B_RDY_Pin */
  GPIO_InitStruct.Pin = IPMB_B_RDY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(IPMB_B_RDY_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_A_RDY_Pin HW_5_Pin HW_7_Pin */
  GPIO_InitStruct.Pin = IPMB_A_RDY_Pin|HW_5_Pin|HW_7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOK, &GPIO_InitStruct);

  /*Configure GPIO pins : HW_3_Pin HW_1_Pin HW_6_Pin HW_4_Pin */
  GPIO_InitStruct.Pin = HW_3_Pin|HW_1_Pin|HW_6_Pin|HW_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_15_Pin USR_IO_11_Pin USR_IO_31_Pin USR_IO_15_Pin
                           USR_IO_13_Pin USR_IO_16_Pin */
  GPIO_InitStruct.Pin = IPM_15_Pin|USR_IO_11_Pin|USR_IO_31_Pin|USR_IO_15_Pin
                          |USR_IO_13_Pin|USR_IO_16_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_3_Pin PWR_GOOD_A_Pin USR_IO_32_Pin USR_IO_30_Pin */
  GPIO_InitStruct.Pin = USR_IO_3_Pin|PWR_GOOD_A_Pin|USR_IO_32_Pin|USR_IO_30_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pin : ETH_TX_ER_Pin */
  GPIO_InitStruct.Pin = ETH_TX_ER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ETH_TX_ER_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : HW_2_Pin HW_0_Pin */
  GPIO_InitStruct.Pin = HW_2_Pin|HW_0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : ALARM_B_Pin MASTER_TRST_Pin */
  GPIO_InitStruct.Pin = ALARM_B_Pin|MASTER_TRST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : EN_12V_Pin FP_LED_2_Pin FP_LED_0_Pin FP_LED_1_Pin
                           FP_LED_BLUE_Pin */
  GPIO_InitStruct.Pin = EN_12V_Pin|FP_LED_2_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PYLD_RESET_Pin */
  GPIO_InitStruct.Pin = PYLD_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PYLD_RESET_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

// Function to mapping the UART to different proposes. It can be used as template.
__attribute__((weak)) void uart_mapping( void )
{
  // CLI UART CONFIGURATION
  huart_cli = H7UART_UART4;
  huart_cli_config.pin_rx        = H7UART_PIN_UART4_RX_PH14;
  huart_cli_config.pin_tx        = H7UART_PIN_UART4_TX_PB9;
  huart_cli_config.rcc_clksource = RCC_USART234578CLKSOURCE_PCLK1;
  huart_cli_config.function      = PERIPH_TX_RX;
  huart_cli_config.data_config   = DATA_WORD_LENGTH_8_NO_PARITY;
  huart_cli_config.fifo_enable   = FIFO_MODE_DISABLE;
  huart_cli_config.fifo_rx_thres = FIFO_TH_1_8;
  huart_cli_config.presc         = H7UART_PRESC_DIV_1;
  huart_cli_config.baud_rate     = 115200UL;

  // SOL UART CONFIGURATION
  huart_sol = H7UART_USART1;
  huart_sol_config.pin_rx        = H7UART_PIN_USART1_RX_PA10;
  huart_sol_config.pin_tx        = H7UART_PIN_USART1_TX_PB14;
  huart_sol_config.rcc_clksource = RCC_USART1CLKSOURCE_D2PCLK2;
  huart_sol_config.function      = PERIPH_TX_RX;
  huart_sol_config.data_config   = DATA_WORD_LENGTH_8_NO_PARITY;
  huart_sol_config.fifo_enable   = FIFO_MODE_DISABLE;
  huart_sol_config.fifo_rx_thres = FIFO_TH_1_8;
  huart_sol_config.presc         = H7UART_PRESC_DIV_1;
  huart_sol_config.baud_rate     = 115200UL;

  // PAYLOAD INTERFACE CONFIGURATION
  huart_pyldi = H7UART_UART8; // modified from default
  huart_pyldi_config.pin_rx       = H7UART_PIN_UART8_RX_PE0; // USR_IO_1 OpenIPMC-HW
  huart_pyldi_config.pin_tx       = H7UART_PIN_UART8_TX_PE1; // USR_IO_0 OpenIPMC-HW
  huart_pyldi_config.rcc_clksource = RCC_USART234578CLKSOURCE_PCLK1;
  huart_pyldi_config.function      = PERIPH_TX_RX;
  huart_pyldi_config.data_config   = DATA_WORD_LENGTH_8_NO_PARITY;
  huart_pyldi_config.fifo_enable   = FIFO_MODE_DISABLE;
  huart_pyldi_config.fifo_rx_thres = FIFO_TH_1_8;
  huart_pyldi_config.presc         = H7UART_PRESC_DIV_1;
  huart_pyldi_config.baud_rate     = 115200UL;
}

// Function prototype for rx uart callback
void cli_rx_callback(uint8_t* data ,uint32_t len);

// CLI UART function initialization
static void uart_cli_init( void )
{
  h7uart_uart_ret_code_t ret;

  huart_cli_config.rx_callback = cli_rx_callback;

  ret =  h7uart_uart_init_by_config(huart_cli,&huart_cli_config);

  if (ret != H7UART_RET_CODE_OK )
    Error_Handler();
}


void callback_io_expander(uint8_t cap)
{
  mt_printf("It works!");
}

void Pin_12v_enable(void)
{
  mcp23s17_ret_code_t ret;
 // mcp23s17_dev_t dev_1;
 // mcp23s17_pin_t control_12v;


  /* IO-Expander device configuration */

  dev_1.spi_periph = H7SPI_SPI4;    // SPI interface.
  dev_1.mcp23s17_addr = 0x00;       // MCP23S17 address device number.
  dev_1.port_reset = GPIOE;
  dev_1.pin_reset  = GPIO_PIN_3;
  dev_1.port_interrupt = GPIOI;
  dev_1.pin_interrupt  = GPIO_PIN_3;


  ret = mcp23s17_full_reset(&dev_1);


  ret = mcp23s17_init(&dev_1);


  if (ret != MCP23S17_RET_OK)
    return;

  /* IO-Expander pin configuration */
  control_12v.dev = &dev_1;
  control_12v.port = MCP23S17_PORTA;
  control_12v.pin_number = GPX_0_MCP23S17;
  control_12v.pin_callback = NULL;

  ret = mcp23s17_config_pin_output(&control_12v);

  if (ret != MCP23S17_RET_OK)
    return;

  ret = mcp23s17_write_pin_value(&control_12v,HIGH_MCP23S17);

  if (ret != MCP23S17_RET_OK)
    return;

  ret = mcp23s17_write_pin_value(&control_12v,LOW_MCP23S17);

  if (ret != MCP23S17_RET_OK)
    return;
}

// CLI Rx UART Callback
void cli_rx_callback(uint8_t* data ,uint32_t len)
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  // Checks if there is Frame Error state in the transmission.
  // In UART normal mode, this error is an excessive noise in Rx/Tx lines.
  if ( h7uart_get_state(huart_cli) == H7UART_FSM_STATE_ERROR_FE )
    return;

#if CLI_AVAILABLE_ON_UART
  if ( terminal_input_stream != NULL )
  {
    xStreamBufferSendFromISR(terminal_input_stream,data,len, &xHigherPriorityTaskWoken);
  }
#endif
}

// Receiver callback from telnet. Port 23: IPMC CLI
void telnet_receiver_callback_cli_23( uint8_t* buff, uint16_t len )
{
    if( terminal_input_stream != NULL )
    {
      for( int i=0; i<len; ++i) if(buff[i] == 127) buff[i] = 8; //Convert DEL into BACK SPACE
      xStreamBufferSend( terminal_input_stream, buff, len, 0);
    }
}

// Command callback from telnet. Port 23: IPMC CLI
void telnet_command_callback_cli_23( uint8_t* buff, uint16_t len )
{
  // Incoming telnet commands are ignored
  // When trying to negotiate, send the server conditions
  // WILL ECHO is used here to prevent local echo in the client
  uint8_t telnet_negotiation_commands[]={255, 251, 1, 255, 254, 34}; // WILL ECHO; DON'T LINEMODE
  telnet_transmit(telnet_negotiation_commands, 6);
}

/*
 * Task to manage character output via VCP
 *
 * This task takes care of accumulating bytes in a stream while VCP is busy.
 * In case it is busy, it takes care of retry while stream keeps accumulating
 */
static void vcp_output_task(void *argument)
{
	vcp_output_stream = xStreamBufferCreate(10, 1);
	uint8_t buff[10];

  while(1)
  {
    int rcvd = xStreamBufferReceive( vcp_output_stream, buff, 10, portMAX_DELAY );
    while( CDC_Transmit_FS(buff, rcvd) == USBD_BUSY )
      vTaskDelay( 50/portTICK_PERIOD_MS );
  }
}

/*
 * Implements the putchar for printf and vprintf
 */
void _putchar(char character)
{
// CLI via UART
#if CLI_AVAILABLE_ON_UART

  h7uart_uart_tx(huart_cli,(uint8_t*)(&character),1,1000);
#endif

  // CLI via telnet
  telnet_transmit((uint8_t*)(&character), 1);

  // CLI on VCP
  if( vcp_output_stream != NULL )
    xStreamBufferSend( vcp_output_stream, (uint8_t*)(&character), 1, 0);
}

static void set_rtc_time( void )
{
  uint8_t  completion_code;
  uint8_t  response_bytes[24];
  int      res_len;
  time_t epoch;
  struct tm* time_tm;
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  // Get time from Shelf Manager (NetFn:0x0A, cmd:0x48)
  int ipmi_status = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0,
            0x0A, 0x48, NULL, 0, &completion_code, response_bytes, &res_len );

  // Set the RTC time
  if (ipmi_status == IPMI_MSG_SEND_OK)
  {
    epoch = (time_t)(((uint32_t)response_bytes[3]<<24) | ((uint32_t)response_bytes[2]<<16) | ((uint32_t)response_bytes[1]<<8) | ((uint32_t)response_bytes[0]<<0));
    time_tm = gmtime (&epoch);

    sDate.Year    = time_tm->tm_year-100;
    sDate.Month   = time_tm->tm_mon+1;
    sDate.Date    = time_tm->tm_mday;
    sDate.WeekDay = 1;
    sTime.Hours   = time_tm->tm_hour;
    sTime.Minutes = time_tm->tm_min;
    sTime.Seconds = time_tm->tm_sec;
    HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
    HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

    mt_printf_tstamp("Set RTC.");
    syslog_push("RTC", "Set RTC", SYSFAC_USER_LVL, SYSSEV_DEBUG);
  }
  else
  {
    mt_printf_tstamp("Set RTC FAILED.");
    syslog_push("RTC", "Set RTC FAILED", SYSFAC_USER_LVL, SYSSEV_DEBUG);
  }
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{

  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 5 */

  // Verify and update CM4 boot address if needed
  switch( cm4_fw_update_boot_address() )
  {
  case cm4_fw_boot_addr_updated:
    mt_printf("CM4 boot address updated. Rebooting...\r\n");
    NVIC_SystemReset();
    break;
  case cm4_fw_boot_addr_error:
    mt_printf("CM4 boot address update FAILED.\r\n");
    break;
  default:
    break;
  }

  /*
   * Check if there is an image in external flash TEMP area and, if it is equal
   * to the current running FW, delete TEMP.
   *
   * It usually means that an upgrade was just done.
   */

  if( image_ext_flash_is_equal_to_running(0) ) // Bootloader component
  {
    mt_printf("Deleting Bootloader image present in TEMP area...\r\n");
    image_ext_flash_delete( EXT_FLASHORG_BL7_TEMP_AREA_START_BLOCK );
  }

  if( image_ext_flash_is_equal_to_running(1) ) // CM7 component
  {
    mt_printf("Deleting CM7 image present in TEMP area...\r\n");
    image_ext_flash_delete( EXT_FLASHORG_CM7_TEMP_AREA_START_BLOCK );
  }

  if( image_ext_flash_is_equal_to_running(2) ) // CM4 component
  {
    mt_printf("Deleting CM4 image present in TEMP area...\r\n");
    image_ext_flash_delete( EXT_FLASHORG_CM4_TEMP_AREA_START_BLOCK );
  }

  // Initializations for DIMM peripherals and OpenIPMC
  openipmc_hal_init();
  mgm_i2c_init();
  sense_i2c_init();

  // Check for Benchtop mode
  uint8_t const haddress = get_haddress_pins();
  if( haddress == 0x7F )
    set_benchtop_payload_power_level(2);

  set_rtc_time();

  // Releases the CustomStartupTask where all the board-specific processes can start and run.
  vTaskResume(custom_startup_task_handle);

   /* Initialise file system drivers and mount partition */
  yimpl_setup();

  /* Initialise random number generator peripheral */
  h7rng_init();

  /* init code for LWIP */
  MX_LWIP_Init();

  // Opens telnet port 23 for the remote IPMC CLI
  telnet_create (23, &telnet_receiver_callback_cli_23, &telnet_command_callback_cli_23);

  /* Starts XVC server */
  xvc_init_server();

  // Launch TFTP server
  yimpl_tftp_launch_server();

  vTaskDelay( 1500/portTICK_PERIOD_MS );

  /* syslog client */
  syslog_init();
  syslog_push("INFO", "IPMC Started!", SYSFAC_USER_LVL, SYSSEV_DEBUG);

  /* rmcp server */
  rmcp_impl_init();

  /* configures SOL UART peripheral */
  sol_impl_init();

  // Releases the CustomStartupTask where all the board-specific processes can start and run.
  vTaskResume(custom_startup_task_handle);

  int set_rtc_time_ctr = 0;
  /* Infinite loop */

  for(;;)
  {
    vTaskDelay( pdMS_TO_TICKS(1000) );
    // Mechanism to keep RTC correct (set each 1 hour)
    if( set_rtc_time_ctr > 3600 )
    {
      set_rtc_time();
      set_rtc_time_ctr = 0;
    }
    set_rtc_time_ctr ++;
  }
  /* USER CODE END 5 */
}

/* MPU Configuration */

void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct = {0};

  /* Disables the MPU */
  HAL_MPU_Disable();
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.BaseAddress = 0x30040000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_32KB;
  MPU_InitStruct.SubRegionDisable = 0x0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL1;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER1;
  MPU_InitStruct.BaseAddress = 0x30040000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
  MPU_InitStruct.SubRegionDisable = 0x0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}
/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
