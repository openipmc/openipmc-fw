/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file	: 	yimpl_cli.h
 * @brief	:	Terminal callback for yaffs resource header.
 * @date	:	YYYY / MM / DD - 2022 / 03 / 30
 * @author	:	Antonio V. G. Bassi
 */

#ifndef SRC_YIMPL_CLI_H_
#define SRC_YIMPL_CLI_H_

/* useful macros */
#define KB 1024
#define MB (KB*KB)
#define DIRSTR_LEN 64

#define YIMPL_CLI_FORMAT_NAME	"format"
#define YIMPL_CLI_MOUNT_NAME	"mount"
#define YIMPL_CLI_UMOUNT_NAME	"umount"
#define YIMPL_CLI_MKDIR_NAME	"mkdir"
#define YIMPL_CLI_REMOVE_NAME	"rm"
#define YIMPL_CLI_LIST_NAME		"ls"
#define YIMPL_CLI_WRITE_NAME	"write"
#define YIMPL_CLI_PRINT_NAME	"print"
#define YIMPL_CLI_CHMOD_NAME	"chmod"
#define YIMPL_CLI_MEMSTAT_NAME "memstat"
#define YIMPL_CLI_READBBM_NAME "bbm"

#define YIMPL_CLI_FORMAT_CALLBACK	format_cb
#define YIMPL_CLI_MOUNT_CALLBACK    mount_cb
#define YIMPL_CLI_UMOUNT_CALLBACK   umount_cb
#define YIMPL_CLI_MKDIR_CALLBACK    mkdir_cb
#define YIMPL_CLI_REMOVE_CALLBACK   remove_cb
#define YIMPL_CLI_LIST_CALLBACK     list_cb
#define YIMPL_CLI_WRITE_CALLBACK    write_cb
#define YIMPL_CLI_PRINT_CALLBACK    print_cb
#define YIMPL_CLI_CHMOD_CALLBACK    chmod_cb
#define YIMPL_CLI_MEMSTAT_CALLBACK  memstat_cb
#define YIMPL_CLI_READBBM_CALLBACK  read_bbm_lut_cb

/* Command-Line description to be displayed on terminal process
 * at "help" command
 */
#define YIMPL_CLI_FORMAT_DESCRIPTION "\
Formats currently installed flash device: \r\n\
Current device : W25N01GV: 1G-bit / 128M-byte, Winbond Semiconductors. \r\n\
Takes following arguments: \r\n\
\t 1.<-u> : Unmounts device before formatting. \r\n\
\t 2.<-f> : Forces an unmount even if device is busy, this is not recommended.\r\n\
\t 3.<-r> : Remounts device after formatting. \r\n"

#define YIMPL_CLI_MOUNT_DESCRIPTION "\
Mounts partition on currently installed device. \r\n\
Current device : W25N01GV: 1G-bit / 128M-byte, Winbond Semiconductors. \r\n"

#define YIMPL_CLI_UMOUNT_DESCRIPTION "\
Unmount partition on currently installed device. \r\n\
Current device: W25N01GV: 1G-bit / 128M-byte, Winbond Semiconductors. \r\n\
Takes following arguments: \r\n\
\t 1.<-f> : Forces an unmount. \r\n"

#define YIMPL_CLI_WRITE_DESCRIPTION	"\
Takes user input and writes into a file. \r\n\
Takes following arguments : \r\n\
\t1.[-mk | -ow | -ap] : \r\n\
\t\t mk -> Creates new file, if it already exists\r\n\
\t\t       abort operation.\r\n\
\t\t ow -> Creates new file, if it already exists\r\n\
\t\t       overwrite it.\r\n\
\t\t ap -> Creates new file, if it already exists\r\n\
\t\t       appends user input at the end of the file. \r\n\
\t2. [path] : Path to file. \r\n\
\t3. [input] : User input. \r\n"

#define YIMPL_CLI_PRINT_DESCRIPTION "\
Prints FILE to standard output.\r\n\
Takes following arguments: \r\n\
\t1. [path] : Path to file. \r\n"

#define YIMPL_CLI_MKDIR_DESCRIPTION "\
Creates a directory, if it does not exist.\r\n\
Takes following arguments: \r\n\
\t1.[path] : Path to directory."

#define YIMPL_CLI_REMOVE_DESCRIPTION "\
Deletes file or directory permanently. If it is a\r\n\
\t     directory the user must first remove any nested\r\n\
\t     directories or files on given path.\r\n\
Takes following arguments:\r\n\
\t1.[path] : Path to file or directory."

#define YIMPL_CLI_LIST_DESCRIPTION "\
Lists information about the contents of a directory. \r\n\
Takes following arguments: \r\n\
\t2.[path] : Target directory. \r\n\
e.g : ls Dir, ls Dir/folder...\r\n\
If a directory is not specified, root directory will be listed instead."

#define YIMPL_CLI_CHMOD_DESCRIPTION "\
Changes file system object permissions.\r\n\
\t     Currently only owner READ and WRITE permissions are supported.\r\n\
Takes following arguments: \r\n\
\t1.[-rd | -wr] Adds read or write permissions to a file or directory.\r\n\
\t\t     Can be combined to grant both permissions.\r\n\
\t2.[Path] Path to file or directory."

#define YIMPL_CLI_MEMSTAT_DESCRIPTION "\
Prints flash device's information and memory statistics. \r\n"

#define YIMPL_CLI_READBBM_DESCRIPTION  "\
Prints logical block address (LBA) to physical block address (PBA) links\r\n\
look-up table on flash device for bad-block management."

extern uint8_t format_cb(void);
extern uint8_t mount_cb(void);
extern uint8_t umount_cb(void);
extern uint8_t mkdir_cb(void);
extern uint8_t remove_cb(void);
extern uint8_t list_cb(void);
extern uint8_t write_cb(void);
extern uint8_t print_cb(void);
extern uint8_t chmod_cb(void);
extern uint8_t memstat_cb(void);
extern uint8_t read_bbm_lut_cb(void);

#endif /* SRC_YIMPL_CLI_H_ */
