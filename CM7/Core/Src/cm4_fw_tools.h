#ifndef CM4_FW_TOOLS
#define CM4_FW_TOOLS

#include <stdbool.h>

typedef enum
{
	cm4_fw_boot_addr_error,
	cm4_fw_boot_addr_ok,
	cm4_fw_boot_addr_updated

}cm4_fw_boot_addr_status_t;


bool cm4_fw_is_present( uint8_t* major_version, uint8_t* minor_version, uint8_t aux_version[4] );

cm4_fw_boot_addr_status_t cm4_fw_update_boot_address( void );

#endif /* CM4_FW_TOOLS */
