/*
 *    OpenIPMC-FW
 *    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  @file   dimm_gpios.c
 *
 *  @brief  Implements function for DIMM connector control GPIOS like 12V_EN signal. *
 *
 *  @date   YYYY / MM / DD - 2024 / 06 / 10
 *  @author Carlos Ruben Dell'Aquila
 *
 */

#include "dimm_gpios.h"
#include "mcp23s17ml_hal.h"


static mcp23s17_dev_t pyld_12v_en_io_dev = { .spi_periph =  H7SPI_SPI4,  // SPI interface.
                                             .mcp23s17_addr = 0x05,      // MCP23S17 address device number.
                                             .port_reset = GPIOE,
                                             .port_interrupt = GPIOI,
                                             .pin_interrupt  = GPIO_PIN_3 };

static mcp23s17_pin_t pyld_12v_en_io_pin = { .dev  = &pyld_12v_en_io_dev,
                                             .port = MCP23S17_PORTA,
                                             .pin_number = GPX_5_MCP23S17,
                                             .pin_callback = NULL };

static int pyld_12v_en_init_flag = 0;

static int pyld_12v_en_init( void )
{
  if(pyld_12v_en_init_flag == 0)
  {
    mcp23s17_ret_code_t ret;
    uint8_t io_dir_config;

    // specific for OpenIPMC-HW v1.0, v1.1,
    // - the initialization is done by ioc generate code.

    // specific for OpenIPMC-HW v1.2
    ret = mcp23s17_init(&pyld_12v_en_io_dev);
    if( ret != MCP23S17_RET_OK )
      return 0;

    ret = mcp23s17_get_pin_config(&pyld_12v_en_io_pin, &io_dir_config);
    if(ret != MCP23S17_RET_OK)
      return 0;

    if( io_dir_config == 1 )
    {
      // the pin is an INPUT and it should be configured
      ret = mcp23s17_config_pin_output(&pyld_12v_en_io_pin);
      if (ret != MCP23S17_RET_OK)
        return 0;

      ret = mcp23s17_write_pin_value(&pyld_12v_en_io_pin,LOW_MCP23S17);
      if (ret != MCP23S17_RET_OK)
        return 0;
    }
    pyld_12v_en_init_flag = 1;
  }

  return 1;
}


void pyld_12v_en_set_state(pyld_12V_en_state_t state)
{

  // It is a lazy initialization function
  pyld_12v_en_init();

  // specific for OpenIPMC-HW v1.0, v1.1
  if (state == PYLD_12V_SET)
  {
    GPIO_SET_STATE(SET,EN_12V);
  }
  else
  {
    GPIO_SET_STATE(RESET,EN_12V);
  }

  // specific for OpenIPMC-HW v1.2
  mcp23s17_write_pin_value(&pyld_12v_en_io_pin, (uint8_t) state);
}






