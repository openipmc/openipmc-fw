/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This file implements a driver for W25N01GV Winbond Flash Memory over STM32 QSPI
 *
 * This driver is also compatible with the W74M01GV Winbond Flash Memory.
 *
 * @file	: 	w25n01gv.c
 * @brief	:	Winbond 25n01gvzeig flash device driver functions.
 * @date	:	YYYY / MM / DD - 2022 / 12 / 12
 * @authors	:	Antonio V. G. Bassi, André M. Cascadan, Luigi Calligaris
 */

#include "main.h"
#include "w25n01gv.h"

// FreeRTOS include for the mutex
#include "FreeRTOS.h"
#include "semphr.h"

/*** Instruction table for buffer read mode ***/
#define W25N01GV_DEV_RESET               0xFF
#define W25N01GV_JEDEC_ID                0x9F
#define W25N01GV_READ_STATUS_REG         0x05
#define W25N01GV_WRITE_STATUS_REG        0x1F
#define W25N01GV_WRITE_ENABLE            0x06
#define W25N01GV_WRITE_DISABLE           0x04
#define W25N01GV_BAD_BLOCK_MGMT          0xA1
#define W25N01GV_READ_BAD_BLOCK_LUT      0xA5
#define W25N01GV_LAST_ECC_FAIL_PAGE      0xA9
#define W25N01GV_BLOCK_ERASE             0xD8
#define W25N01GV_PROG_DATA_LOAD          0x02
#define W25N01GV_RANDOM_PROG_DATA_LOAD   0x84
#define W25N01GV_QUAD_PROG_DATA_LOAD     0x32
#define W25N01GV_RANDOM_Q_PROG_DATA_LOAD 0x34
#define W25N01GV_EXEC_PROG                0x10
#define W25N01GV_PAGE_DATA_READ          0x13
#define W25N01GV_DATA_READ               0x03
#define W25N01GV_FAST_DATA_READ          0x0B
#define W25N01GV_4BYTE_FAST_READ         0x0C
#define W25N01GV_DUAL_FAST_READ          0x3B
#define W25N01GV_4BYTE_DUAL_FAST_READ    0x3C
#define W25N01GV_QUAD_FAST_READ          0x6B
#define W25N01GV_4BYTE_QUAD_FAST_READ    0x6C
#define W25N01GV_DUAL_FAST_READ_IO       0xBB
#define W25N01GV_4BYTE_DUAL_FAST_READ_IO 0xBC
#define W25N01GV_QUAD_FAST_READ_IO       0xEB
#define W25N01GV_4BYTE_QUAD_FAST_READ_IO 0xEC

#define INIT_CMD_WITH_DEFAULTS(COMMAND)                              \
	{                                                                \
        COMMAND.Instruction = 0;                                     \
        COMMAND.InstructionMode = QSPI_INSTRUCTION_1_LINE;           \
		                                                             \
		COMMAND.Address = 0;                                         \
		COMMAND.AddressMode = QSPI_ADDRESS_NONE;                     \
		COMMAND.AddressSize = QSPI_ADDRESS_8_BITS;                   \
                                                                     \
        COMMAND.AlternateBytes = 0;                                  \
		COMMAND.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;       \
		COMMAND.AlternateBytesSize = QSPI_ALTERNATE_BYTES_8_BITS;    \
                                                                     \
        COMMAND.DummyCycles = 0;                                     \
		                                                             \
		COMMAND.DataMode = QSPI_DATA_NONE;                           \
        COMMAND.NbData = 0;                                          \
		                                                             \
		COMMAND.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;        \
		COMMAND.DdrMode = QSPI_DDR_MODE_DISABLE;                     \
		COMMAND.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;                 \
	}


// Handler to the QSPI, which must be already configured
extern QSPI_HandleTypeDef hqspi;

static SemaphoreHandle_t ext_flash_mutex = NULL;

void w25n01gv_device_reset(void)
{

	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_DEV_RESET; // Device RESET

	HAL_QSPI_Command  ( &hqspi, &cmd,  100 );
}

uint8_t w25n01gv_get_status_reg(int reg)
{

	QSPI_CommandTypeDef cmd;
	uint8_t data;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_READ_STATUS_REG; // Read Status Register
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.Address     = reg;
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData      = 1;

	HAL_QSPI_Command ( &hqspi, &cmd,  100 );
	HAL_QSPI_Receive ( &hqspi, &data, 100 );

	return data;
}

void w25n01gv_set_status_reg(int reg, uint8_t value)
{

	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_WRITE_STATUS_REG; // Write Status Register
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.Address     = reg;
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData      = 1;

	HAL_QSPI_Command ( &hqspi, &cmd,   100 );
	HAL_QSPI_Transmit( &hqspi, &value, 100 );
}

_Bool w25n01gv_is_busy(void)
{

	QSPI_CommandTypeDef cmd;
	uint8_t data;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_READ_STATUS_REG; // Read Status Register
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.Address     = W25N01GV_STATUS_REG_3; // Status Register-3
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData      = 1;

	HAL_QSPI_Command ( &hqspi, &cmd,  100 );
	HAL_QSPI_Receive ( &hqspi, &data, 100 );

	return (data & 0x01);
}

void w25n01gv_write_enable(void)
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = 0x06; // Write Enable

	HAL_QSPI_Command  ( &hqspi, &cmd, 100 );
}

void w25n01gv_write_disable(void)
{
        QSPI_CommandTypeDef cmd;

        INIT_CMD_WITH_DEFAULTS(cmd);

        cmd.Instruction = W25N01GV_WRITE_DISABLE;

        HAL_QSPI_Command ( &hqspi, &cmd, 100 );
        return;
}

uint8_t w25n01gv_ecc_check( void )
{
	QSPI_CommandTypeDef cmd;
	uint8_t ecc_stat = 0x00;
	const uint8_t bit_mask = 0x30;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_READ_STATUS_REG;
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.Address		= W25N01GV_STATUS_REG_3;
	cmd.DataMode	= QSPI_DATA_1_LINE;
	cmd.NbData		= 1;

	if ( HAL_ERROR == HAL_QSPI_Command(&hqspi, &cmd     , 100) ) {ecc_stat |= bit_mask;}
	if ( HAL_ERROR == HAL_QSPI_Receive(&hqspi, &ecc_stat, 100) ) {ecc_stat |= bit_mask;}

	return ( ecc_stat & bit_mask );
}

/*
 * Program Data Load
 *
 * Load data into the Data Buffer
 *
 *   column : The number of the page column to be loaded
 *      len : Number of bytes to be loaded
 *     data : Buffer containing the data to be loaded
 */
void w25n01gv_data_load( uint16_t column, uint32_t len, uint8_t* data )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_PROG_DATA_LOAD; // Program Data Load
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_16_BITS;
	cmd.Address     = column;
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData      = len;

	HAL_QSPI_Command  ( &hqspi, &cmd, 100 );
	HAL_QSPI_Transmit ( &hqspi, data, 100 );
}

/*
 * Quad Program Data Load
 *
 * Load data into the Data Buffer
 *
 *   column : The number of the page column to be loaded
 *      len : Number of bytes to be loaded
 *     data : Buffer containing the data to be loaded
 */
void w25n01gv_quad_data_load( uint16_t column, uint32_t len, uint8_t* data )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_QUAD_PROG_DATA_LOAD; // Quad Program Data Load
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_16_BITS;
	cmd.Address     = column;
	cmd.DataMode    = QSPI_DATA_4_LINES;
	cmd.NbData      = len;

	HAL_QSPI_Command  ( &hqspi, &cmd, 100 );
	HAL_QSPI_Transmit ( &hqspi, data, 100 );
}

/*
 * Program Execute
 *
 * Program the content of the Data Buffer into a page.
 *
 *   page : The number of the memory page to be programmed, form 0 to 65535 (16 bits)
 */
void w25n01gv_program_execute( uint16_t page )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_EXEC_PROG;
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_24_BITS; // 1 dummy byte + 2 address bytes
	cmd.Address     = page;

	HAL_QSPI_Command  ( &hqspi, &cmd,  100 );
}

/*
 * Page Data Read
 *
 * Load the content of a page into the Data Buffer
 *
 *   page : The number of the memory page to be read, form 0 to 65535 (16 bits)
 */
void w25n01gv_page_data_read( uint16_t page )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_PAGE_DATA_READ;
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_24_BITS; // 1 dummy byte + 2 address bytes
	cmd.Address     = page;

	HAL_QSPI_Command  ( &hqspi, &cmd,  100 );
}

/*
 * Read
 *
 * Read data from the Data Buffer
 *
 *   column : The number of the page column to be read
 *      len : Number of bytes to be read
 *     data : Buffer to receive the bytes
 */
void w25n01gv_read( uint16_t column, uint32_t len, uint8_t* data )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_DATA_READ; // Read
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_16_BITS;
	cmd.Address     = column;
	cmd.DummyCycles = 8;
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData      = len;

	HAL_QSPI_Command (&hqspi, &cmd, 100);
	HAL_QSPI_Receive (&hqspi, data, 100);
}

/*
 * Fast Read Quad Output
 *
 * Read data from the Data Buffer
 *
 *   column : The number of the page column to be read
 *      len : Number of bytes to be read
 */
void w25n01gv_fast_read_quad( uint16_t column, uint32_t len, uint8_t* data )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_QUAD_FAST_READ; // Fast Read Quad Output
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_16_BITS;
	cmd.Address     = column;
	cmd.DummyCycles = 8;
	cmd.DataMode    = QSPI_DATA_4_LINES;
	cmd.NbData      = len;

	HAL_QSPI_Command (&hqspi, &cmd, 100);
	HAL_QSPI_Receive (&hqspi, data, 100);
}

/*
 * Erase one block
 *
 *   block : The number of the memory block to be erased, form 0 to 1023 (10 bits)
 */
void w25n01gv_block_erase( uint16_t block )
{
	QSPI_CommandTypeDef cmd;

	INIT_CMD_WITH_DEFAULTS(cmd);

	uint16_t page = (block & 0x3FF) <<6;

	cmd.Instruction = W25N01GV_BLOCK_ERASE; // Block Erase
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.AddressSize = QSPI_ADDRESS_24_BITS; // 1 dummy byte + 2 address bytes
	cmd.Address     = page;

	HAL_QSPI_Command  (&hqspi, &cmd, 100);
}

int w25n01gv_read_bbm_lut(uint16_t* lut_out)
{
	QSPI_CommandTypeDef cmd;
	int ret = 0;

	INIT_CMD_WITH_DEFAULTS(cmd);

	cmd.Instruction = W25N01GV_READ_BAD_BLOCK_LUT;
	cmd.AddressMode = QSPI_ADDRESS_1_LINE;
	cmd.DataMode    = QSPI_DATA_1_LINE;
	cmd.NbData		  = W25N01GV_LUT_NBYTES;
	cmd.DummyCycles = 8;

	if ( HAL_ERROR == HAL_QSPI_Command(&hqspi,          &cmd, 100) ) { ret = -1; }

  uint8_t* lut_dev_bytes = (void*) lut_out;
  if ( HAL_ERROR == HAL_QSPI_Receive(&hqspi, lut_dev_bytes, 100) ) { ret = -1; }

  for (size_t i = 0; i < W25N01GV_LUT_NBYTES; i += 4)
  {
      uint32_t* lut_dev_words = (uint32_t*) (lut_dev_bytes + i);

      // REV16 swaps the endianness of the two consecutive 16-bits shorts in a 32-bit word.
      // This is a function documented in ARM Compiler toolchain Assembler Reference Version 5.03
      *lut_dev_words = __REV16(*lut_dev_words);
  }

	return ret;
}


/*
 * Mutex for multithread safety
 */
void w25n01gv_mutex_init( void )
{
	ext_flash_mutex = xSemaphoreCreateMutex();
}

void w25n01gv_mutex_take( void )
{
	if(xSemaphoreTake( ext_flash_mutex, pdMS_TO_TICKS(10000) ) != pdTRUE) // 10 seconds should be enough!
		mt_printf("timout\r\n");
}

void w25n01gv_mutex_give( void )
{
	xSemaphoreGive( ext_flash_mutex );
}

/*
 * QSPI test
 *
 * Load and read back the Data Buffer to check the device responsivity via QSPI
 *
 * return: 0 if ok; 0 if fail.
 */
//uint8_t flash_qspi_test( void )
//{
//	flash_device_reset();
//
//	flash_set_status_reg(0xA0, 0x00);
//
//	for( int j = 0; j<2048; j++ )
//	 flash_buffer_test[j] = (j & 0xFF);
//
//	flash_write_enable();
//	flash_quad_data_load( 0, 2048, flash_buffer_test );
//
//	for( int j = 0; j<2048; j++ )
//		  flash_buffer_test[j] = 0;
//
//	flash_fast_read_quad( 0, 2048, flash_buffer_test );
//
//	for( int j = 0; j<2048; j++ )
//		if(flash_buffer_test[j] != (j & 0xFF))
//			return 1; // FAIL
//
//	return 0;
//}




