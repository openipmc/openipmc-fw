/*
 * OpenIPMC-FW
 * Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This file implements a driver for W25N01GV Winbond Flash Memory over STM32 QSPI
 *
 * This driver is also compatible with the W74M01GV Winbond Flash Memory.
 *
 * @file	: 	w25n01gv.c
 * @brief	:	Winbond 25n01gvzeig flash device driver functions.
 * @date	:	YYYY / MM / DD - 2022 / 12 / 12
 * @authors	:	Antonio V. G. Bassi, André M. Cascadan, Luigi Calligaris
 */

#ifndef W25N01GV_H
#define W25N01GV_H

#include <stdint.h>
#include <stdbool.h>

/*** Flash device status registers ***/
#define W25N01GV_STATUS_REG_1 0xA0
#define W25N01GV_STATUS_REG_2 0xB0
#define W25N01GV_STATUS_REG_3 0xC0

#define W25N01GV_STATUS_REGx_0 0x01
#define W25N01GV_STATUS_REGx_1 0x02
#define W25N01GV_STATUS_REGx_2 0x04
#define W25N01GV_STATUS_REGx_3 0x08
#define W25N01GV_STATUS_REGx_4 0x10
#define W25N01GV_STATUS_REGx_5 0x20
#define W25N01GV_STATUS_REGx_6 0x40
#define W25N01GV_STATUS_REGx_7 0x80

#define W25N01GV_LUT_NLINKS  (20U)
#define W25N01GV_LUT_NSHORTS (W25N01GV_LUT_NLINKS  * 2U)
#define W25N01GV_LUT_NBYTES  (W25N01GV_LUT_NSHORTS * 2U)

/*** driver functions ***/
extern void    w25n01gv_device_reset(void);
extern uint8_t w25n01gv_get_status_reg(int reg);
extern void    w25n01gv_set_status_reg(int reg, uint8_t value);
extern bool    w25n01gv_is_busy(void);
extern void    w25n01gv_write_enable(void);
extern void    w25n01gv_write_disable(void);
extern uint8_t w25n01gv_ecc_check(void);
extern void    w25n01gv_data_load(uint16_t column, uint32_t len, uint8_t* data);
extern void    w25n01gv_quad_data_load(uint16_t column, uint32_t len, uint8_t* data);
extern void    w25n01gv_program_execute(uint16_t page );
extern void    w25n01gv_page_data_read(uint16_t page );
extern void    w25n01gv_read(uint16_t column, uint32_t len, uint8_t* data);
extern void    w25n01gv_fast_read_quad(uint16_t column, uint32_t len, uint8_t* data);
extern void    w25n01gv_block_erase(uint16_t block);
extern int     w25n01gv_read_bbm_lut(uint16_t* lut);
extern void    w25n01gv_mutex_init( void );
extern void    w25n01gv_mutex_take( void );
extern void    w25n01gv_mutex_give( void );

#endif
