/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      xvc_jtag.c                                                        */
/* @brief     JTAG instruction shifter implementation source code.              */
/* @authors   Luigi Calligaris, Antonio Vitor Grossi Bassi                      */
/********************************************************************************/

//////////////
// INCLUDES //
//////////////

#include <string.h>

#include "xvc_jtag.h"
#include "xvc_jtag_patternluts.h"

#include <stdint.h>
#include "stm32h745xx.h"
#include "stm32h7xx_hal.h"
#include "stm32h7xx_hal_gpio.h"

#include "xvc_common.h"

//////////////////////
// MACROS AND TYPES //
//////////////////////

#define XVC_SHIFT_LENGTH_MAX_BITS 2048

#define XVC_PATTERN_LENGTH_MAX_WORDS (XVC_SHIFT_LENGTH_MAX_BITS + XVC_SHIFT_LENGTH_MAX_BITS + 1)
#define XVC_DMA_LENGTH_MAX_WORDS     (XVC_PATTERN_LENGTH_MAX_WORDS)

#define XVC_PATTERN_LENGTH_MAX_BYTES_TDITMS (XVC_PATTERN_LENGTH_MAX_WORDS * sizeof(uint32_t)) // TDI,TMS words are 32-bits (BSRR)
#define XVC_PATTERN_LENGTH_MAX_BYTES_TDO    (XVC_PATTERN_LENGTH_MAX_WORDS * sizeof(uint16_t)) // TDO words are 16-bits (IDR)
#define XVC_DMA_LENGTH_MAX_BYTES_TDITMS     (XVC_PATTERN_LENGTH_MAX_BYTES_TDITMS)
#define XVC_DMA_LENGTH_MAX_BYTES_TDO        (XVC_PATTERN_LENGTH_MAX_BYTES_TDO)


enum clock_first_edge_t {
	FIRST_LOW,
	FIRST_HIGH
};
typedef enum clock_first_edge_t clock_first_edge_t;

enum clock_last_edge_t {
	LAST_LOW,
	LAST_HIGH,
	LAST_INDIFFERENT
};
typedef enum clock_last_edge_t clock_last_edge_t;

static enum
{
	JTAG_STATE_IDLE = 0,
	JTAG_STATE_SHIFTING,
	JTAG_STATE_SHIFTED
}jtag_fsm_state = 0;


// IPC data goes to absolute memory position (see linker script)
volatile xvc_ipc_data_t xvc_ipc_data __attribute__((section(".xvcjtag"))) = {0};


///////////////////////////
// FUNCTION DECLARATIONS //
///////////////////////////
static void        fill_clock_gpiopattern(clock_first_edge_t clock_first_edge, clock_last_edge_t clock_last_edge, uint16_t pin_bitmask, uint32_t* pattern_begin, uint32_t* pattern_end);
static void        xvcvec_add_gpiopattern(uint8_t const* const bytevec, size_t const nshift, uint32_t* pattern_array_first_elem, uint32_t const lut_p[256][8]);
static void        xvcbyte_add_gpiopattern(uint8_t const byte, size_t const nshift_inside_byte, uint32_t* pattern_array_first_elem, uint32_t const lut_p[256][8]);
static inline void xvc_encode_tms(unsigned nshift, uint8_t const* tms_bytevec, uint32_t* gpio_patternvec);
static inline void xvc_encode_tdi(unsigned nshift, uint8_t const* tdi_bytevec, uint32_t* gpio_patternvec);
static void        xvc_start_shift(uint32_t const first_byte, uint32_t const nshift, uint8_t const* tms_bytevec, uint8_t const* tdi_bytevec);
static int         xvc_is_busy_shifting();

//////////////////////
// GLOBAL VARIABLES //
//////////////////////

// External variables to access the hardware resources
extern TIM_HandleTypeDef htim8;
extern DMA_HandleTypeDef hdma_tim8_ch1;
extern DMA_HandleTypeDef hdma_tim8_ch2;
extern DMA_HandleTypeDef hdma_tim8_ch3;

static uint32_t xvc_jtag_pattern_out_gpioh[XVC_DMA_LENGTH_MAX_WORDS];
static uint32_t xvc_jtag_pattern_out_gpioj[XVC_DMA_LENGTH_MAX_WORDS];
static uint16_t xvc_jtag_pattern_in_gpiod [XVC_DMA_LENGTH_MAX_WORDS];

// Control flags
//static uint32_t g_nshift_last = 0;
static uint32_t total_already_shifted, current_being_shifted;
static int      DMA_Stream2_Busy = 0;
static int      DMA_Stream3_Busy = 0;
static int      DMA_Stream4_Busy = 0;

static void xvcvec_add_gpiopattern(uint8_t const* const bytevec, unsigned nshift, uint32_t* pattern_array_first_elem, uint32_t const lut_p[256][8])
{
	unsigned const ibytestop = (nshift + 7) / 8;
	for (size_t ibyte = 0; ibyte < ibytestop; ++ibyte)
	{
		if (nshift > 7)
		{
			xvcbyte_add_gpiopattern(*(bytevec + ibyte), 8, pattern_array_first_elem + (ibyte * 16), lut_p);
			nshift -= 8;
		}
		else
		{
			xvcbyte_add_gpiopattern(*(bytevec + ibyte), nshift, pattern_array_first_elem + (ibyte * 16), lut_p);
		}
	}
}

static void xvcbyte_add_gpiopattern(uint8_t const byte, unsigned nshift_inside_byte, uint32_t* pattern_array_first_elem, uint32_t const lut_p[256][8])
{
	// From XVC protocol definition: "Bit 0 in Byte 0 of this vector is shifted out first."
	// NOTE: "vector" is the vector of bytes holding the packed bits to be shifted .
	// Notice how this definition entails a zig-zag (7<--0,7<--0,...) traversal of the bits in the vector.

	// Optimization and loop unrolling for the specific case of full byte
	if (8 == nshift_inside_byte)
	{
		*(pattern_array_first_elem     ) |= lut_p[byte][7];
		*(pattern_array_first_elem +  1) |= lut_p[byte][7];
		*(pattern_array_first_elem +  2) |= lut_p[byte][6];
		*(pattern_array_first_elem +  3) |= lut_p[byte][6];
		*(pattern_array_first_elem +  4) |= lut_p[byte][5];
		*(pattern_array_first_elem +  5) |= lut_p[byte][5];
		*(pattern_array_first_elem +  6) |= lut_p[byte][4];
		*(pattern_array_first_elem +  7) |= lut_p[byte][4];
		*(pattern_array_first_elem +  8) |= lut_p[byte][3];
		*(pattern_array_first_elem +  9) |= lut_p[byte][3];
		*(pattern_array_first_elem + 10) |= lut_p[byte][2];
		*(pattern_array_first_elem + 11) |= lut_p[byte][2];
		*(pattern_array_first_elem + 12) |= lut_p[byte][1];
		*(pattern_array_first_elem + 13) |= lut_p[byte][1];
		*(pattern_array_first_elem + 14) |= lut_p[byte][0];
		*(pattern_array_first_elem + 15) |= lut_p[byte][0];
	}
	else
	{
		// Since passing nshift_inside_byte > 8 can lead to a catastrophic out-of-bounds access, sanitize it
		unsigned const nshift_inside_byte_duplicated = 2 * nshift_inside_byte;

		// remember: for each data bit we need to hold it for 2 patterns (i.e. one TCK cycle)
		for (uint32_t i = 0; i < nshift_inside_byte_duplicated; ++i)
		{
			*(pattern_array_first_elem + i) |= lut_p[byte][7 - i/2];
		}
	}
}

static inline void xvc_encode_tms(unsigned nshift, uint8_t const* tms_bytevec, uint32_t* gpio_patternvec)
{
	xvcvec_add_gpiopattern(tms_bytevec, nshift, gpio_patternvec, xvc_jtag_patternlut_pin6);
}

static inline void xvc_encode_tdi(unsigned nshift, uint8_t const* tdi_bytevec, uint32_t* gpio_patternvec)
{
	xvcvec_add_gpiopattern(tdi_bytevec, nshift, gpio_patternvec, xvc_jtag_patternlut_pin7);
}

void xvc_decode_tdo(uint32_t const first_byte, unsigned nshift, uint16_t const* gpio_patternvec, uint8_t* tdo_bytevec)
{
	// First capture (0) is useless, as the device has not been clocked yet and TDO is undefined
	unsigned ipattern   = 1;
	unsigned ibitremain = nshift;
	unsigned ibyte      = first_byte;

	while (ibitremain > 0)
	{
		if (ibitremain >= 32)
		{
			tdo_bytevec[ibyte] =
				(gpio_patternvec[ipattern + 14] & GPIO_PIN_14 ? 0b10000000 : 0) +
				(gpio_patternvec[ipattern + 12] & GPIO_PIN_14 ? 0b01000000 : 0) +
				(gpio_patternvec[ipattern + 10] & GPIO_PIN_14 ? 0b00100000 : 0) +
				(gpio_patternvec[ipattern +  8] & GPIO_PIN_14 ? 0b00010000 : 0) +
				(gpio_patternvec[ipattern +  6] & GPIO_PIN_14 ? 0b00001000 : 0) +
				(gpio_patternvec[ipattern +  4] & GPIO_PIN_14 ? 0b00000100 : 0) +
				(gpio_patternvec[ipattern +  2] & GPIO_PIN_14 ? 0b00000010 : 0) +
				(gpio_patternvec[ipattern +  0] & GPIO_PIN_14 ? 0b00000001 : 0) ;

			tdo_bytevec[ibyte + 1] =
				(gpio_patternvec[ipattern + 30] & GPIO_PIN_14 ? 0b10000000 : 0) +
				(gpio_patternvec[ipattern + 28] & GPIO_PIN_14 ? 0b01000000 : 0) +
				(gpio_patternvec[ipattern + 26] & GPIO_PIN_14 ? 0b00100000 : 0) +
				(gpio_patternvec[ipattern + 24] & GPIO_PIN_14 ? 0b00010000 : 0) +
				(gpio_patternvec[ipattern + 22] & GPIO_PIN_14 ? 0b00001000 : 0) +
				(gpio_patternvec[ipattern + 20] & GPIO_PIN_14 ? 0b00000100 : 0) +
				(gpio_patternvec[ipattern + 18] & GPIO_PIN_14 ? 0b00000010 : 0) +
				(gpio_patternvec[ipattern + 16] & GPIO_PIN_14 ? 0b00000001 : 0) ;

			tdo_bytevec[ibyte + 2] =
				(gpio_patternvec[ipattern + 46] & GPIO_PIN_14 ? 0b10000000 : 0) +
				(gpio_patternvec[ipattern + 44] & GPIO_PIN_14 ? 0b01000000 : 0) +
				(gpio_patternvec[ipattern + 42] & GPIO_PIN_14 ? 0b00100000 : 0) +
				(gpio_patternvec[ipattern + 40] & GPIO_PIN_14 ? 0b00010000 : 0) +
				(gpio_patternvec[ipattern + 38] & GPIO_PIN_14 ? 0b00001000 : 0) +
				(gpio_patternvec[ipattern + 36] & GPIO_PIN_14 ? 0b00000100 : 0) +
				(gpio_patternvec[ipattern + 34] & GPIO_PIN_14 ? 0b00000010 : 0) +
				(gpio_patternvec[ipattern + 32] & GPIO_PIN_14 ? 0b00000001 : 0) ;

			tdo_bytevec[ibyte + 3] =
				(gpio_patternvec[ipattern + 62] & GPIO_PIN_14 ? 0b10000000 : 0) +
				(gpio_patternvec[ipattern + 60] & GPIO_PIN_14 ? 0b01000000 : 0) +
				(gpio_patternvec[ipattern + 58] & GPIO_PIN_14 ? 0b00100000 : 0) +
				(gpio_patternvec[ipattern + 56] & GPIO_PIN_14 ? 0b00010000 : 0) +
				(gpio_patternvec[ipattern + 54] & GPIO_PIN_14 ? 0b00001000 : 0) +
				(gpio_patternvec[ipattern + 52] & GPIO_PIN_14 ? 0b00000100 : 0) +
				(gpio_patternvec[ipattern + 50] & GPIO_PIN_14 ? 0b00000010 : 0) +
				(gpio_patternvec[ipattern + 48] & GPIO_PIN_14 ? 0b00000001 : 0) ;

			ibyte      +=  4;
			ipattern   += 64;
			ibitremain -= 32;
		}
		else if (ibitremain >= 8)
		{
			tdo_bytevec[ibyte] =
				(gpio_patternvec[ipattern + 14] & GPIO_PIN_14 ? 0b10000000 : 0) +
				(gpio_patternvec[ipattern + 12] & GPIO_PIN_14 ? 0b01000000 : 0) +
				(gpio_patternvec[ipattern + 10] & GPIO_PIN_14 ? 0b00100000 : 0) +
				(gpio_patternvec[ipattern +  8] & GPIO_PIN_14 ? 0b00010000 : 0) +
				(gpio_patternvec[ipattern +  6] & GPIO_PIN_14 ? 0b00001000 : 0) +
				(gpio_patternvec[ipattern +  4] & GPIO_PIN_14 ? 0b00000100 : 0) +
				(gpio_patternvec[ipattern +  2] & GPIO_PIN_14 ? 0b00000010 : 0) +
				(gpio_patternvec[ipattern +  0] & GPIO_PIN_14 ? 0b00000001 : 0) ;

			++ibyte;
			ipattern   += 16;
			ibitremain -=  8;
		}
		else
		{
			uint8_t res = 0;
			switch(ibitremain)
			{
				case 7:
					if ( gpio_patternvec[ipattern + 12] & GPIO_PIN_14 ) res += 0b01000000;
				case 6:
					if ( gpio_patternvec[ipattern + 10] & GPIO_PIN_14 ) res += 0b00100000;
				case 5:
					if ( gpio_patternvec[ipattern +  8] & GPIO_PIN_14 ) res += 0b00010000;
				case 4:
					if ( gpio_patternvec[ipattern +  6] & GPIO_PIN_14 ) res += 0b00001000;
				case 3:
					if ( gpio_patternvec[ipattern +  4] & GPIO_PIN_14 ) res += 0b00000100;
				case 2:
					if ( gpio_patternvec[ipattern +  2] & GPIO_PIN_14 ) res += 0b00000010;
				case 1:
					if ( gpio_patternvec[ipattern +  0] & GPIO_PIN_14 ) res += 0b00000001;
				default:
					tdo_bytevec[ibyte] = res;
			}
			ibitremain = 0;
		}
	}
}


static int xvc_is_busy_shifting()
{
	return ( DMA_Stream2_Busy | DMA_Stream3_Busy | DMA_Stream4_Busy );
}


static void xvc_start_shift(uint32_t const first_byte, uint32_t const nshift, uint8_t const* tms_bytevec, uint8_t const* tdi_bytevec)
{
	static int initialized = 0;

	// If this function hasn't been called, initialize clock signal.
	if (initialized == 0)
	{
		// Configure capture/compare registers for phase matching.
		TIM8->CCR1 = 19;
		TIM8->CCR2 = 18;
		TIM8->CCR3 = 17;

		// Thanks to the static init flag, this function runs only once
		fill_clock_gpiopattern(FIRST_LOW, LAST_HIGH, GPIO_PIN_11, xvc_jtag_pattern_out_gpioh, xvc_jtag_pattern_out_gpioh + XVC_PATTERN_LENGTH_MAX_WORDS);

		// Enable interrupt signals on Control Register
		__HAL_DMA_DISABLE(htim8.hdma[TIM_DMA_ID_CC1]);
		while(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC1]->Instance)->CR &  DMA_SxCR_EN ) asm("nop");
		MODIFY_REG(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC1]->Instance)->CR, (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME | DMA_IT_HT), (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME));
		__HAL_DMA_ENABLE(htim8.hdma[TIM_DMA_ID_CC1]);

		__HAL_DMA_DISABLE(htim8.hdma[TIM_DMA_ID_CC2]);
		while(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC2]->Instance)->CR &  DMA_SxCR_EN ) asm("nop");
		MODIFY_REG(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC2]->Instance)->CR, (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME | DMA_IT_HT), (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME));
		__HAL_DMA_ENABLE(htim8.hdma[TIM_DMA_ID_CC2]);

		__HAL_DMA_DISABLE(htim8.hdma[TIM_DMA_ID_CC3]);
		while(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC3]->Instance)->CR &  DMA_SxCR_EN ) asm("nop");
		MODIFY_REG(((DMA_Stream_TypeDef   *)htim8.hdma[TIM_DMA_ID_CC3]->Instance)->CR, (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME | DMA_IT_HT), (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME));
		__HAL_DMA_ENABLE(htim8.hdma[TIM_DMA_ID_CC3]);

		initialized = 1;
	}

	size_t const pattern_length = nshift + nshift;
	size_t const dma_length     = pattern_length;

	// In USE_FULL_ASSERT mode, check if we are going out of bounds
	assert_param(dma_length <= XVC_DMA_LENGTH_MAX_WORDS);

	// Clear bit set-reset register pattern buffer in order to load new ones.
	memset(xvc_jtag_pattern_out_gpioj, 0x00, ( size_t )( XVC_DMA_LENGTH_MAX_BYTES_TDITMS ) );
	xvcvec_add_gpiopattern(tms_bytevec + first_byte, nshift, xvc_jtag_pattern_out_gpioj, xvc_jtag_patternlut_pin6);
	xvcvec_add_gpiopattern(tdi_bytevec + first_byte, nshift, xvc_jtag_pattern_out_gpioj, xvc_jtag_patternlut_pin7);


	size_t const arr = 19;
	size_t const psc =  xvc_ipc_data.clk_prescaler;

	// Disable DMA triggering from TIM8
	TIM8->DIER = 0x00;

	// Configure Repetition Counter, Auto-Reload and Pre-scaler registers.
	TIM8->RCR  = dma_length;
	TIM8->ARR  = arr;
	TIM8->PSC  = psc;

	// "Dummy" repetition cycle of zero turns.
	TIM8->CR1 |= (TIM_CR1_CEN);
	while( TIM8->CR1 & TIM_CR1_CEN ) asm("nop");
	/*
	 * REP_CNT is loaded with the RCR value only after a repetition cycle. It means
	 * that a "dummy" repetition cycle (of zero turns, in this case) is neede to load
	 * the expected 'dma_length' value into the REP_CNT.
	 *
	 * See the "TIM8->RCR  = 0;" line below.
	 */

	// DMA Stream busy flags, each one cleared on the respective stream IRQ handler
	DMA_Stream2_Busy = 1;
	DMA_Stream3_Busy = 1;
	DMA_Stream4_Busy = 1;

	__HAL_UNLOCK(&hdma_tim8_ch1);
	__HAL_UNLOCK(&hdma_tim8_ch2);
	__HAL_UNLOCK(&hdma_tim8_ch3);

	hdma_tim8_ch1.State = HAL_DMA_STATE_READY;
	hdma_tim8_ch2.State = HAL_DMA_STATE_READY;
	hdma_tim8_ch3.State = HAL_DMA_STATE_READY;


	((DMA_TypeDef*)DMA1)->LISR = 0x3F<<22 | 0x3F<<16;
	((DMA_TypeDef*)DMA1)->HISR = 0x3F<<0;

	// Configure Addresses for DMA Transfers
	HAL_DMA_Start(htim8.hdma[TIM_DMA_ID_CC1], (uint32_t)(&xvc_jtag_pattern_out_gpioj), (uint32_t)(&GPIOJ->BSRR), dma_length);
	HAL_DMA_Start(htim8.hdma[TIM_DMA_ID_CC2], (uint32_t)(&GPIOD->IDR),   (uint32_t)(&xvc_jtag_pattern_in_gpiod), dma_length);
	HAL_DMA_Start(htim8.hdma[TIM_DMA_ID_CC3], (uint32_t)(&xvc_jtag_pattern_out_gpioh), (uint32_t)(&GPIOH->BSRR), dma_length);


	// Enable DMA triggering from TIM8
	TIM8->DIER = ( TIM_DMA_CC1 | TIM_DMA_CC2 | TIM_DMA_CC3 );
	/* Start the timer (remember we are in one-pulse mode: this triggers just one full turn of the counter)
	 * Bit-banging starts here!
	 */

	// Set RCR to 0 for the "Dummy" repetition cycle (see above)
	TIM8->RCR = 0;

	TIM8->CR1 |= (TIM_CR1_CEN);
}

void DMA1_Stream2_IRQHandler()
{
	// Clear the various possible interrupt flags in DMA1 Stream2
	((DMA_TypeDef*)DMA1)->LIFCR = DMA_LIFCR_CTCIF2 | DMA_LIFCR_CTEIF2 | DMA_LIFCR_CDMEIF2 | DMA_LIFCR_CFEIF2;

	DMA_Stream2_Busy = 0;
	// Clear the interrupt flag for DMA1 Stream2 in NVIC
	NVIC_ClearPendingIRQ(DMA1_Stream2_IRQn);
}

void DMA1_Stream3_IRQHandler()
{
	// Clear the various possible interrupt flags in DMA1 Stream3
	((DMA_TypeDef*)DMA1)->LIFCR = DMA_LIFCR_CTCIF3  | DMA_LIFCR_CTEIF3 | DMA_LIFCR_CDMEIF3 | DMA_LIFCR_CFEIF3;

	DMA_Stream3_Busy = 0;
	// Clear the interrupt flag for DMA1 Stream3 in NVIC
	NVIC_ClearPendingIRQ(DMA1_Stream3_IRQn);
}

void DMA1_Stream4_IRQHandler()
{
	// Clear the various possible interrupt flags in DMA1 Stream4
	((DMA_TypeDef*)DMA1)->HIFCR = DMA_HIFCR_CTCIF4 | DMA_HIFCR_CTEIF4  | DMA_HIFCR_CDMEIF4 | DMA_HIFCR_CFEIF4;

	DMA_Stream4_Busy = 0;
	// Clear the interrupt flag for DMA1 Stream4 in NVIC
	NVIC_ClearPendingIRQ(DMA1_Stream4_IRQn);
}


static void fill_clock_gpiopattern(clock_first_edge_t clock_first_edge, clock_last_edge_t clock_last_edge,
		uint16_t pin_bitmask, uint32_t* pattern_begin, uint32_t* pattern_end)
{
	uint32_t const set_bitmask    = (uint32_t)pin_bitmask;
	uint32_t const reset_bitmask  = set_bitmask << 16;
	uint32_t const ignore_bitmask = ~(set_bitmask | reset_bitmask);

	unsigned toggle = (clock_first_edge == FIRST_LOW) ? 0U : ~(0U);

	uint32_t* it = pattern_begin;
	while (it != pattern_end)
	{
		*it = toggle ? ( (*it) & ignore_bitmask ) | set_bitmask : ( (*it) & ignore_bitmask ) | reset_bitmask;

		toggle = ~toggle;

		++it;
	}

	// Check whether we need to correct the last pattern
	--it;
	switch(clock_last_edge)
	{
	case LAST_HIGH:
		*it = ( (*it) & ignore_bitmask ) | set_bitmask;
		break;
	case LAST_LOW:
		*it = ( (*it) & ignore_bitmask ) | reset_bitmask;
		break;
	case LAST_INDIFFERENT:
		break;
	default:
		break;
	}
}

void xvc_jtag_fsm_update(void)
{

	static uint32_t empty_loop_counter = 0;

	// This wastes the first 200 million cycles
	// TODO: This is a workaround to avoid CM4 to enter in hard fault during the startup. Investigation is needed for a better solution.
	if (empty_loop_counter < 200000000)
	{
		++empty_loop_counter;
		return;
	}


	switch( jtag_fsm_state )
	{

		case JTAG_STATE_IDLE :
			if( HAL_OK == HAL_HSEM_FastTake(HSEM_ID_1) )
			{
				// Ready to receive JTAG instruction shift
				if( 0 != xvc_ipc_data.start_shift )
				{
					total_already_shifted = 0;
					current_being_shifted = xvc_ipc_data.nshift;
					if( current_being_shifted > XVC_SHIFT_LENGTH_MAX_BITS) // truncates if it is too big
						current_being_shifted = XVC_SHIFT_LENGTH_MAX_BITS;
					xvc_start_shift(0, current_being_shifted, xvc_ipc_data.bytevec_tms_data, xvc_ipc_data.bytevec_tdi_tdo_data);

					xvc_ipc_data.start_shift = 0; // Reset the start flag since it is already sampled.
					jtag_fsm_state = JTAG_STATE_SHIFTING;
				}
				HAL_HSEM_Release(HSEM_ID_1, 0);
			}
			break;

		case JTAG_STATE_SHIFTING :
			// Shifting Process has started
			if( !( xvc_is_busy_shifting() ) )
			{
				if( total_already_shifted + current_being_shifted >= xvc_ipc_data.nshift)
				{
					// Stop shifting
					jtag_fsm_state = JTAG_STATE_SHIFTED;
				}
				else
				{
					// Decode and continue shifting
					xvc_decode_tdo( total_already_shifted>>3, current_being_shifted, xvc_jtag_pattern_in_gpiod, xvc_ipc_data.bytevec_tdi_tdo_data);
					total_already_shifted += current_being_shifted;
					current_being_shifted = xvc_ipc_data.nshift - total_already_shifted;
					if( current_being_shifted > XVC_SHIFT_LENGTH_MAX_BITS) // truncates if it is too big
						current_being_shifted = XVC_SHIFT_LENGTH_MAX_BITS;
					xvc_start_shift( total_already_shifted>>3 , current_being_shifted, xvc_ipc_data.bytevec_tms_data, xvc_ipc_data.bytevec_tdi_tdo_data);
				}

			}
			break;

		case JTAG_STATE_SHIFTED:
			// Shifting process has ended, time to send TDO
			if( HAL_OK == HAL_HSEM_FastTake(HSEM_ID_1) )
			{
				xvc_decode_tdo( total_already_shifted>>3,current_being_shifted, xvc_jtag_pattern_in_gpiod, xvc_ipc_data.bytevec_tdi_tdo_data);
				total_already_shifted += current_being_shifted;
				xvc_ipc_data.shift_done = 1;
				jtag_fsm_state = JTAG_STATE_IDLE;

				HAL_HSEM_Release(HSEM_ID_1, 0);
			}
			break;

		default :
			break;
	}
}




/**
 *'xvc_ipc_data' Initialization
 *
 * The xvc_ipc_data struct is used as Inter Processor Communication (IPC) mechanincs to exchage information between Cortex-M7 and
 * M4 and this function does all zero initialization of it.
 *
 * In the normal program execution flow, the hardware mutex (HSEM_ID_1) should be taken by the CM7 Core in order to fill xvc_ipc_data.rcvd_data buffer, load
 * xvc_ipc_data.bytevec_tdi_tdo_data pointer variable whith the buffer address and then set xvc_ipc_data.start_shift = 1. After that, CM7 releases the mutex.
 *
 * As soon as possible, CM4 gets xvc_ipc_data.start_shift != 0 and takes the HSEM_ID_1 mutex. Then it starts to shift the buffer to the GPIO pins connected
 * to Xilinx device's JTAG.
 *
 * if xvc_ipc_data has uninitialized values at system startup, the sequence described before can be inverted producing a hardware fault in CM4 because it can
 * try to access an ilegal memory address and a timeout erron in CM7 because it cannot take the mutex.
 */
void xvc_ipc_data_init(void)
{
  memset(&xvc_ipc_data,0,sizeof(xvc_ipc_data_t));
}
