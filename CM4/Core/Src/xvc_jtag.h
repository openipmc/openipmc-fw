/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/
/* @file      xvc_jtag.c                                                        */
/* @brief     JTAG instruction shifter implementation header                    */
/* @authors   Luigi Calligaris, Antonio Vitor Grossi Bassi                      */
/********************************************************************************/

#ifndef SRC_XVC_JTAG_H_
#define SRC_XVC_JTAG_H_

void xvc_ipc_data_init(void);
void xvc_jtag_fsm_update(void);

#endif /* SRC_XVC_JTAG_H_ */
